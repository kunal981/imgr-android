package com.ngohung.widget;

@SuppressWarnings("serial")
public class ContactItemException extends Exception {

	public ContactItemException() {
	}

	public ContactItemException(String msg) {
		super(msg);
	}

}
