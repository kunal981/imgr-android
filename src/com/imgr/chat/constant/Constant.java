package com.imgr.chat.constant;

public class Constant {

	// public static final String Register_URL =

	public static final String BUBBLE_COLOR = "http://imgrapp.com/release/index.php/ws/appuser/userbubble";

	public static final String CHAT_HISTORY = "http://imgrapp.com/release/index.php/ws/appuser/getuserchat";
	public static final String USER_INFO_URL = "http://imgrapp.com/release/index.php/ws/appuser/updateuser";

	public static final String CREATE_PERSONAL_PROMO = "https://imgrapp.com/release/index.php/ws/appuser/createSelfPromo";
	public static final String UPDATE_PERSONAL_PROMO = "https://imgrapp.com/release/index.php/ws/appuser/updateSelfPromo";
	public static final String DELETE_PERSONAL_PROMO = "https://imgrapp.com/release/index.php/ws/appuser/deletePersonalPromo?";
	public static final String RECENT_MESSAGE_LIST = "http://imgrapp.com/release/index.php/ws/appuser/getchatusers";

	public static final String PromoStatus_EDIT = "http://imgrapp.com/release/index.php/ws/appuser/androidPromoStatus";

	public static final String PROMO_IMAGES_GALLERY = "https://imgrapp.com/release/index.php/ws/appuser/getImgrImages?apiKey=imgr&username=";

	public static final String PERSONAL_PROMO_CHAT_FETCH = "https://imgrapp.com/release/index.php/ws/appuser/getPromoDetails?apiKey=imgr&promo_id=";

	public static final String DeviceToken = "https://imgrapp.com/release/index.php/ws/appuser/updateDeviceToken?";
	public static final String SponsoredAds = "https://imgrapp.com/release/index.php/ws/appuser/getPromoList?";
	public static final String GET_PromoStatus_EDIT = "https://imgrapp.com/release/index.php/ws/appuser/setPromoStatus?";

	public static final String CHAT_NOTIFICATION = "http://imgrapp.com/release/index.php/ws/appuser/sendchatnotification";
	public static final String AFTER_CHAT_NOTIFICATION = "http://imgrapp.com/release/index.php/ws/appuser/androidnotificationdata";
	public static final String Register_URL = "https://imgrapp.com/release/index.php/ws/webservice/registeration";
	public static final String verifyRegistration_URL = "https://imgrapp.com/release/index.php/ws/webservice/verifyRegistration";
	public static final String IMGERContact = "https://imgrapp.com/release/index.php/ws/appuser/isImgrUser";
	public static final String IMGRCHAT = "imgrchat";
	public static final String IMGRNOTIFICATION_RECEIVE = "imgrnotification_receive";
	public static final String classdefine = "class";
	public static final String PHONENO = "phoneno";
	public static final String USERNAME_XMPP = "usernamexmpp";
	public static final String PASSWORD_XMPP = "passwordxmpp";
	public static final String COUNTRYCODE = "countrycode";
	public static final String FRAGMENT = "openfragment";

	public static final String SPONSORED_ADS = "_sponsoredads";
	public static final String PERSONAL_ADS = "_personalads";

	public static final String USEDPHONE = "insidePhone";

	public static final String SERVERNAME = "64.235.48.26";
	public static final int PORTNAME = 5222;
	public static final String XMPP_DNS_Name = "@imgrapp.com";
	public static final String GCM_PROJECT_NO = "357582208681";
	public static final boolean DEBUG = true;
	public static final String DEBUG_TAG = "Imgr";
	public static final String GCM_ID = "reg_id";
	public static final String IMGRCHATGCM = "imgrchatgcm";
	public static final String FONT_SET = "_font";

	public static final String BUBBLE_SET = "_bubble";
	public static final String BUBBLE_SET_OVERRIDE = "_override";
	public static final String BUBBLE_SET_OVERRIDE_FIXED = "_overridefixed";
	public static final String BUBBLE_SET_FREIND = "_bubblefreind";

	public static final String on_off_ads = "_onoff";
	public static final String SPONSORED_on_off_ads = "_sponsored_onoff";
	public static String DELETEPROMO = "false";

	public static String SETTING_MAIN_PROMO_ON_OFF = "_promo_on_off_setting";
	public static String SETTING_PROMO_AUTO_ROTATE = "_promo_auto_rotate_setting";
	public static String SETTING_NOTIFICATION_ON_OFF = "_notification_on_off_setting";

	public static int HANDLE_BACK_CONATCT_ADD = 0;
	public static int HANDLE_BACK_CONATCT_SCREEN_ONLY = 0;
	public static int HANDLE_BACK_IMGRFRDS = 0;
	public static int UPDATE_SUCCESSFULLY = 0;

	public static int PROMO_OFF_NOW_SELECT_ONE = 0;

	public static String SPONSORED_PROMO_OFF_ID = "0";

	public static int SPONSORED_PROMO_OFF_ = 0;
	public static String _ID = "0";

	public static String FRIEND_ID_PROMO = "0";
	public static String YOUR_ID_PROMO = "0";

	public static String NAME_CHANGE = "";
	public static int NAME_CHANGE_COUNT = 0;

	public static int CONNECTION_LOST = 0;

}
