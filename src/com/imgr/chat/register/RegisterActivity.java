package com.imgr.chat.register;

/**
 * author: amit agnihotri
 */
import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.R;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.number.PlusClass;
import com.imgr.chat.number.UsPhoneNumberFormatter;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;

public class RegisterActivity extends AppBaseActivity {
	ImageView imgEnter;
	EditText edtCode, edtPhoneNo;
	String strPhoneNo, strCode;
	String versionName, deviceid;
	ConnectionDetector mConnectionDetector;

	SharedPreferences sharedPreferences;
	Editor editor;
	AlertDialog pinDialog;
	UsPhoneNumberFormatter addLineNumberFormatter;
	PlusClass plusClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		registerBaseActivityReceiver();
		mConnectionDetector = new ConnectionDetector(this);
		imgEnter = (ImageView) findViewById(R.id.img_enter);
		edtCode = (EditText) findViewById(R.id.edtCode);
		edtPhoneNo = (EditText) findViewById(R.id.edt_phoneno);
		addLineNumberFormatter = new UsPhoneNumberFormatter(
				new WeakReference<EditText>(edtPhoneNo));
		plusClass = new PlusClass(new WeakReference<EditText>(edtCode));
		edtPhoneNo.addTextChangedListener(addLineNumberFormatter);
		edtCode.addTextChangedListener(plusClass);

		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		try {
			versionName = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("versionName:", "" + versionName);
		deviceid = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceid:", "" + deviceid);

		// edtCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		// @Override
		// public void onFocusChange(View v, boolean hasFocus) {
		// if (hasFocus) {
		// if (edtCode.getText().toString().length() == 0) {
		// edtCode.setText("");
		// }
		// }
		//
		// }
		// });

		// edtCode.addTextChangedListener(new TextWatcher() {
		//
		// @Override
		// public void onTextChanged(CharSequence s, int start, int before,
		// int count) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void beforeTextChanged(CharSequence s, int start, int count,
		// int after) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void afterTextChanged(Editable s) {
		// // TODO Auto-generated method stub
		// if(s.toString().length()==0){
		// edtCode.setText("+");
		// }
		//
		// }
		// });
		edtCode.setSelection(edtCode.getText().length());
		imgEnter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strCode = edtCode.getText().toString();
				strPhoneNo = edtPhoneNo.getText().toString();
				strPhoneNo = strPhoneNo.replace(" ", "").replace("(", "")
						.replace(")", "").replace("-", "");
				Log.e("strPhoneNo: ", "" + strPhoneNo);
				boolean receive = validation(strCode, strPhoneNo);
				if (receive) {
					if (mConnectionDetector.isConnectingToInternet()) {
						showInputDialog(edtPhoneNo.getText().toString(),
								strCode);

					} else {
						LogMessage.showDialog(RegisterActivity.this, null,
								"No Internet Connection", null, "Ok");

					}

				}

			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	protected boolean validation(String strCode, String strPhoneNo) {
		// TODO Auto-generated method stub
		Log.e("strCode: ", "" + strCode);
		if (strPhoneNo.matches("")) {
			LogMessage.showDialog(RegisterActivity.this, null,
					"Please enter correct phone number", null, "OK");
			return false;
		}
		if (strPhoneNo.length() < 10) {
			LogMessage.showDialog(RegisterActivity.this, null,
					"Please enter valid phone number", null, "OK");
			return false;
		}
		if (strCode.matches("")) {
			edtCode.setText("+1");
			// LogMessage.showDialog(RegisterActivity.this, null,
			// "Please enter your Country code", null, "Ok");
			return true;
		}
		if (strCode.equals("+1")) {

			return true;
		}
		if (!strCode.matches("\\+\\d\\d?\\d?")) {
			LogMessage.showDialog(RegisterActivity.this, null,
					"Please enter valid country code", null, "OK");
			return false;
		}
		return true;
	}

	public class RegisterPhone extends AsyncTask<String, Void, String> {
		public RegisterPhone() {
		}

		@Override
		protected String doInBackground(String... params) {
			// String result = JsonParserConnector.PostRegisterPhone("imgr",
			// "AMIT", "0091", "2", edtPhoneNo.getText().toString(),
			// "android", deviceid);
			String result = JsonParserConnector.PostRegisterPhone("imgr",
					"1.0", edtCode.getText().toString(), "2", strPhoneNo,
					"android", deviceid);
			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				sharedPrefernces();
				// TODO Auto-generated method stub
				startActivity(new Intent(RegisterActivity.this,
						VerificationActivity.class));
				// Intent intent_verification = new
				// Intent(RegisterActivity.this,
				// VerificationActivity.class);
				// intent_verification.putExtra("code",
				// strCode);
				// intent_verification.putExtra(
				// "phonenumber", strPhoneNo);

				// startActivity(intent_verification);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

				finish();
			} else {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(RegisterActivity.this);
		}

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();
		editor.putString(Constant.classdefine, "RegisterActivity");
		editor.putString(Constant.PHONENO, edtPhoneNo.getText().toString());
		editor.putString(Constant.COUNTRYCODE, edtCode.getText().toString());

		editor.commit();

	}

	protected void showInputDialog(String mString, String mString2) {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.register_dialog, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final Button mButtonyes = (Button) v.findViewById(R.id.yes_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.phone_text);
		if (mString2.equals("")) {
			mString2 = "+1";
		}
		mTextView.setText(mString2 + " " + mString);
		// pinCode.setHint("Enter Pin");
		// pinCode.setGravity(C);

		// pinCode.setBackgroundColor(Color.parseColor("#ffffff"));
		pinDialog = new AlertDialog.Builder(RegisterActivity.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();
						new RegisterPhone().execute();

					}
				});
			}
		});

		pinDialog.show();

	}

}
