package com.imgr.chat.register;

/**
 * author: amit agnihotri
 */
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.search.UserSearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.R;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.number.PlusClass;
import com.imgr.chat.number.UsPhoneNumberFormatter;
import com.imgr.chat.register.RegisterActivity.RegisterPhone;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;
import com.imgr.chat.xmpp.ReadReceipt;

public class VerificationActivity extends AppBaseActivity {
	String code_str, phone_str, string_code, string_contactno, deviceid, regid;
	String name, password;
	EditText edtCode, edtPhoneNo, mEditTextverfilycode;
	Button btnEdit;
	ImageView imgEnter;
	Button mButton_SendAgain;
	ConnectionDetector mConnectionDetector;

	SharedPreferences sharedPreferences;
	AlertDialog pinDialog;
	Editor editor;
	SharedPreferences mSharedPreferences_gcm;

	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;

	ConnectionConfiguration config;
	XMPPConnection connection;
	UsPhoneNumberFormatter addLineNumberFormatter;
	PlusClass plusClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verification);
		registerBaseActivityReceiver();
		connectivity_Manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		activity = this;
		config = new ConnectionConfiguration("64.235.48.26", 5222);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setSecurityMode(SecurityMode.enabled);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		connection.DEBUG_ENABLED = true;
		config.setSASLAuthenticationEnabled(true);
		connection = new XMPPConnection(config);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mButton_SendAgain = (Button) findViewById(R.id.btn_SendAgain);
		phone_str = sharedPreferences.getString(Constant.PHONENO, "");
		code_str = sharedPreferences.getString(Constant.COUNTRYCODE, "");
		mConnectionDetector = new ConnectionDetector(this);
		deviceid = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceid:", "" + deviceid);
		// code_str = getIntent().getStringExtra("code");
		// phone_str = getIntent().getStringExtra("phonenumber");
		edtCode = (EditText) findViewById(R.id.edtCode);
		edtPhoneNo = (EditText) findViewById(R.id.edt_phoneno);
		mEditTextverfilycode = (EditText) findViewById(R.id.edt_enter_code);
		btnEdit = (Button) findViewById(R.id.btn_Edit);
		imgEnter = (ImageView) findViewById(R.id.img_enter);
		addLineNumberFormatter = new UsPhoneNumberFormatter(
				new WeakReference<EditText>(edtPhoneNo));
		edtPhoneNo.addTextChangedListener(addLineNumberFormatter);
		edtPhoneNo.setSelection(edtPhoneNo.getText().length());
		edtCode.setSelection(edtCode.getText().length());
		edtCode.setText(code_str);
		plusClass = new PlusClass(new WeakReference<EditText>(edtCode));

		edtCode.addTextChangedListener(plusClass);
		edtPhoneNo.setText(phone_str);
		string_code = edtCode.getText().toString();
		string_contactno = edtPhoneNo.getText().toString();
		edtPhoneNo.setFocusable(false);
		edtCode.setFocusable(false);

		btnEdit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (btnEdit.getText().toString().equals("Done")) {
					phone_str = edtPhoneNo.getText().toString();
					phone_str = phone_str.replace(" ", "").replace("(", "")
							.replace(")", "").replace("-", "");
					boolean receive = validation_phone(edtCode.getText()
							.toString(), phone_str);
					if (receive) {

						showInputDialog_Done(phone_str, edtCode.getText()
								.toString());

					} else {
						btnEdit.setText("Done");
					}
				} else {
					btnEdit.setText("Done");
					edtPhoneNo.setSelection(edtPhoneNo.getText().length());
					edtPhoneNo.setFocusable(true);
					edtCode.setFocusable(true);
					edtCode.setFocusableInTouchMode(true);
					edtPhoneNo.setFocusableInTouchMode(true);
					edtPhoneNo.setBackgroundColor(Color.WHITE);
					edtCode.setBackgroundColor(Color.WHITE);
				}

				// edtPhoneNo.setText("");
			}
		});
		mButton_SendAgain.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				phone_str = edtPhoneNo.getText().toString();
				phone_str = phone_str.replace(" ", "").replace("(", "")
						.replace(")", "").replace("-", "");
				boolean receive = validation_phone(
						edtCode.getText().toString(), phone_str);

				if (receive) {
					edtPhoneNo.setFocusable(false);
					edtCode.setFocusable(false);
					edtPhoneNo.setBackgroundColor(Color.TRANSPARENT);
					edtCode.setBackgroundColor(Color.TRANSPARENT);
					btnEdit.setText("Edit");
					if (mConnectionDetector.isConnectingToInternet()) {
						new RegisterPhone().execute();
					} else {
						LogMessage.showDialog(VerificationActivity.this, null,
								"No Internet Connection", null, "Ok");
					}
				}

			}
		});

		imgEnter.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				phone_str = edtPhoneNo.getText().toString();
				phone_str = phone_str.replace(" ", "").replace("(", "")
						.replace(")", "").replace("-", "");

				boolean receive = validation(mEditTextverfilycode.getText()
						.toString(), phone_str, edtCode.getText().toString());
				edtPhoneNo.setFocusable(false);
				edtCode.setFocusable(false);
				edtPhoneNo.setBackgroundColor(Color.TRANSPARENT);
				edtCode.setBackgroundColor(Color.TRANSPARENT);
				btnEdit.setText("Edit");
				if (receive) {

					if (mConnectionDetector.isConnectingToInternet()) {
						new Verfication().execute();

					} else {
						LogMessage.showDialog(VerificationActivity.this, null,
								"No Internet Connection", null, "Ok");
					}
				}

			}
		});

		mSharedPreferences_gcm = this.getSharedPreferences(
				Constant.IMGRCHATGCM, Context.MODE_PRIVATE);
		editor = mSharedPreferences_gcm.edit();
		regid = mSharedPreferences_gcm.getString(Constant.GCM_ID, null);

		Log.e("regid: ", "" + regid);

		/**
		 * GCM
		 */

		if (regid == null) {
			initGcm();
		} else {

		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	protected boolean validation_phone(String strCode, String strPhoneNo) {
		// TODO Auto-generated method stub
		if (strPhoneNo.matches("")) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter your phone number", null, "OK");
			return false;
		}
		if (strPhoneNo.length() < 10) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter valid phone number", null, "OK");
			return false;
		}
		if (strCode.matches("")) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter your Country code", null, "OK");
			return false;
		}
		if (!strCode.matches("\\+\\d\\d?\\d?")) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter valid country code", null, "OK");
			return false;
		}
		return true;
	}

	protected boolean validation(String verfilycode, String phone,
			String strcode) {
		// TODO Auto-generated method stub
		if (verfilycode.matches("")) {
			LogMessage.showDialog(VerificationActivity.this,
					"Registration Failed",
					"Please enter valid activation code", null, "OK");
			return false;
		}
		if (phone.matches("")) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter your phone number.", null, "OK");
			return false;
		}
		if (phone.length() < 10) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter valid phone number", null, "OK");
			return false;
		}
		if (strcode.matches("")) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter your Country code", null, "OK");
			return false;
		}
		if (strcode.equals("+1")) {

			return true;
		}
		if (!strcode.matches("\\+\\d\\d?\\d?")) {
			LogMessage.showDialog(VerificationActivity.this, null,
					"Please enter valid country code", null, "OK");
			return false;
		}

		return true;
	}

	public class Verfication extends AsyncTask<String, Void, String> {
		public Verfication() {
		}

		@Override
		protected String doInBackground(String... params) {
			String result = JsonParserConnector.PostVerfication(
					mEditTextverfilycode.getText().toString(), "imgr",
					deviceid, phone_str);
			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			try {
				if (result.equals("true")) {

					getData();

				} else {
					mEditTextverfilycode.setText("");
					LogMessage.showDialog(VerificationActivity.this, null,
							"Please enter valid activation code", null, "Ok");
				}
			} catch (Exception e) {
				mEditTextverfilycode.setText("");
				LogMessage.showDialog(VerificationActivity.this, null,
						"Please enter valid activation code", null, "Ok");
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(VerificationActivity.this);
		}

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();
		editor.putString(Constant.classdefine, "VerificationActivity");
		editor.putString(Constant.PHONENO, edtPhoneNo.getText().toString());
		editor.putString(Constant.USERNAME_XMPP, name);
		editor.putString(Constant.PASSWORD_XMPP, password);
		editor.putString(Constant.COUNTRYCODE, edtCode.getText().toString());
		editor.putString(Constant.FONT_SET, "normal");
		editor.commit();

	}

	public class RegisterPhone extends AsyncTask<String, Void, String> {
		public RegisterPhone() {
		}

		@Override
		protected String doInBackground(String... params) {
			String result = JsonParserConnector.PostRegisterPhone("imgr",
					"1.0", edtCode.getText().toString(), "2", phone_str,
					"android", deviceid);

			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				// sharedPrefernces();
				Send_again_popup();

			} else {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(VerificationActivity.this);
		}

	}

	public class RegisterPhoneDONE extends AsyncTask<String, Void, String> {
		public RegisterPhoneDONE() {
		}

		@Override
		protected String doInBackground(String... params) {
			String result = JsonParserConnector.PostRegisterPhone("imgr",
					"1.0", edtCode.getText().toString(), "2", phone_str,
					"android", deviceid);

			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				// sharedPrefernces();
				Send_again_popup();

			} else {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(VerificationActivity.this);
		}

	}

	protected void showInputDialog(String mString, String mString2) {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.verifly_dialog, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.phone_text);
		mTextView.setText(mString + " " + mString2);

		pinDialog = new AlertDialog.Builder(VerificationActivity.this).setView(
				v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();
						sharedPrefernces();
						Intent intent_profile = new Intent(
								VerificationActivity.this,
								CompleteProfileActivity.class);

						startActivity(intent_profile);
						overridePendingTransition(R.anim.fade_in,
								R.anim.fade_out);

						finish();
					}
				});

			}
		});

		pinDialog.show();

	}

	protected void Send_again_popup() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.send_again_popup, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);

		pinDialog = new AlertDialog.Builder(VerificationActivity.this).setView(
				v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						// sharedPrefernces();
						pinDialog.cancel();

					}
				});

			}
		});

		pinDialog.show();

	}

	//
	public void getData() {
		name = phone_str + "_" + deviceid;
		password = phone_str;
		Log.e("name: ", "" + name);

		new Signup_User().execute();
	}

	public class Signup_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {

			UI.showProgressDialog(VerificationActivity.this);
		}

		@Override
		protected String doInBackground(String... params) {
			Connection_Pool connection_Pool = Connection_Pool.getInstance();

			Log.e("Before Connection ", "Before Connection ");

			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				// AccountManager.getInstance(connection);
				AccountManager am = new AccountManager(connection);
				Log.e("Registration Details:", "UserName = " + name
						+ "  Password is ==" + password);
				Map<String, String> mp = new HashMap<String, String>();

				// adding or set elements in Map by put method key and value
				// pair
				mp.put("username", name);
				mp.put("plainPassword", password);
				// am.createAccount(config.userName, config.password);
				am.createAccount(name, password, mp);
				connection.login(name, password);
				connection_Pool.setConnection(connection);
			}
			/*** end of if http_Response ***/

			catch (XMPPException e) {
				Log.w("Cannot connect to XMPP server with default admin username and password.",
						"0");
				Log.e("Error is ", e.toString());

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result != null) {

				Log.i("String Returned : ", result);
				Toast.makeText(activity, "Registered successfully !",
						Toast.LENGTH_LONG).show();
				Log.e("ConnectionId", "" + connection.getConnectionID());
				Log.e("Servicename", "" + connection.getServiceName());
				Log.e("User", "" + connection.getUser());
				if (connection.getUser() == null) {
					new login_Existing_User().execute();
				} else {
					showInputDialog(edtCode.getText().toString(), edtPhoneNo
							.getText().toString());
				}
			} else {

				Log.i("String Returned : ", result);
				Toast.makeText(
						activity,
						"There is already an account registered with this email",
						Toast.LENGTH_LONG).show();

			}

		}
		/*** end of onPostExecute ***/
	}

	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {
			UI.showProgressDialog(VerificationActivity.this);
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("doInBackground", "doInBackground");
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			connection = new XMPPConnection(config);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			configure(ProviderManager.getInstance());
			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);
			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				Log.e("connected", "server connect");

				// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
				Log.e("" + name, "" + password);
				connection.login(name, password);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {
					Log.e("XMPPChatDemoActivity",
							"--------------------------------------");
					Log.e("XMPPChatDemoActivity", "RosterEntry " + entry);
					Log.e("XMPPChatDemoActivity", "User: " + entry.getUser());
					Log.e("XMPPChatDemoActivity", "Name: " + entry.getName());
					Log.e("XMPPChatDemoActivity",
							"Status: " + entry.getStatus());
					Log.e("XMPPChatDemoActivity", "Type: " + entry.getType());
					Presence entryPresence = roster
							.getPresence(entry.getUser());

					Log.e("XMPPChatDemoActivity", "Presence Status: "
							+ entryPresence.getStatus());
					Log.e("XMPPChatDemoActivity", "Presence Type: "
							+ entryPresence.getMode());

					Presence.Type type = entryPresence.getType();
					if (type == Presence.Type.available)
						Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
					Log.e("XMPPChatDemoActivity", "Presence : " + entryPresence);
				}
			} catch (XMPPException e) {
				Log.e("Cannot connect to XMPP server with default admin username and password.",
						"0");
				Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			Log.e("result", "" + result);
			if (result != null) {
				Log.e("ConnectionId", "" + connection.getConnectionID());
				Log.e("Servicename", "" + connection.getServiceName());
				Log.e("User", "" + connection.getUser());
				if (connection.getUser().equals(null)) {
					Log.e("", "calling");

				} else {
					Log.e("", "calling");
					showInputDialog(edtCode.getText().toString(), edtPhoneNo
							.getText().toString());
				}

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getApplicationContext(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

		/*** end of onPostExecute ***/

		public void configure(ProviderManager pm) {
			// Private Data Storage
			pm.addIQProvider("query", "jabber:iq:private",
					new PrivateDataManager.PrivateDataIQProvider());
			// Time
			try {
				pm.addIQProvider("query", "jabber:iq:time",
						Class.forName("org.jivesoftware.smackx.packet.Time"));
			} catch (ClassNotFoundException e) {
				Log.w("TestClient",
						"Can't load class for org.jivesoftware.smackx.packet.Time");
			}

			// Roster Exchange
			pm.addExtensionProvider("x", "jabber:x:roster",
					new RosterExchangeProvider());

			// Message Events
			pm.addExtensionProvider("x", "jabber:x:event",
					new MessageEventProvider());

			// Chat State
			pm.addExtensionProvider("active",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("composing",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("paused",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("inactive",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("gone",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			// XHTML
			pm.addExtensionProvider("html",
					"http://jabber.org/protocol/xhtml-im",
					new XHTMLExtensionProvider());

			// Group Chat Invitations
			pm.addExtensionProvider("x", "jabber:x:conference",
					new GroupChatInvitation.Provider());

			// Service Discovery # Items
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());

			// Service Discovery # Info
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Data Forms
			pm.addExtensionProvider("x", "jabber:x:data",
					new DataFormProvider());

			// MUC User
			pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
					new MUCUserProvider());

			// MUC Admin
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
					new MUCAdminProvider());

			// MUC Owner
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
					new MUCOwnerProvider());

			// SharedGroupsInfo
			pm.addIQProvider("sharedgroup",
					"http://www.jivesoftware.org/protocol/sharedgroup",
					new SharedGroupsInfo.Provider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Delayed Delivery
			pm.addExtensionProvider("x", "jabber:x:delay",
					new DelayInformationProvider());

			// Version
			try {
				pm.addIQProvider("query", "jabber:iq:version",
						Class.forName("org.jivesoftware.smackx.packet.Version"));
			} catch (ClassNotFoundException e) {
				// Not sure what's happening here.
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Offline Message Requests
			pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
					new OfflineMessageRequest.Provider());

			// Offline Message Indicator
			pm.addExtensionProvider("offline",
					"http://jabber.org/protocol/offline",
					new OfflineMessageInfo.Provider());

			// Last Activity
			pm.addIQProvider("query", "jabber:iq:last",
					new LastActivity.Provider());

			// User Search
			pm.addIQProvider("query", "jabber:iq:search",
					new UserSearch.Provider());

			// JEP-33: Extended Stanza Addressing
			pm.addExtensionProvider("addresses",
					"http://jabber.org/protocol/address",
					new MultipleAddressesProvider());

			// FileTransfer
			pm.addIQProvider("si", "http://jabber.org/protocol/si",
					new StreamInitiationProvider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());

			// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
			// IBBProviders.Open());

			// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
			// IBBProviders.Close());

			// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
			// new
			// IBBProviders.Data());

			// Privacy
			pm.addIQProvider("query", "jabber:iq:privacy",
					new PrivacyProvider());

			pm.addIQProvider("command", "http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider());
			pm.addExtensionProvider("malformed-action",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.MalformedActionError());
			pm.addExtensionProvider("bad-locale",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadLocaleError());
			pm.addExtensionProvider("bad-payload",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadPayloadError());
			pm.addExtensionProvider("bad-sessionid",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadSessionIDError());
			pm.addExtensionProvider("session-expired",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.SessionExpiredError());

			pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
					DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
			pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
					new DeliveryReceiptRequest().getNamespace(),
					new DeliveryReceiptRequest.Provider());
			/*
			 * pm.addExtensionProvider(ReadReceipt.ELEMENT,
			 * ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
			 */
		}

	}

	/**
	 * 
	 * GCM WORK
	 * 
	 */

	private void initGcm() {

		GCMRegistrar.checkDevice(this);

		GCMRegistrar.checkManifest(this);

		regid = GCMRegistrar.getRegistrationId(this);

		if (regid.equals("")) {
			GCMRegistrar.register(this, Constant.GCM_PROJECT_NO);

			if (Constant.DEBUG)
				Log.i(Constant.DEBUG_TAG,
						"UiMain - No Regid Found. Starting registration");
		} else {

			if (GCMRegistrar.isRegisteredOnServer(VerificationActivity.this)) {
				if (Constant.DEBUG)
					Log.v(Constant.DEBUG_TAG,
							"UiMain - Regid registered on server");
			} else {
				try {
					// sendHttpPushRegister();
				} catch (Exception e) {
					e.printStackTrace();

				}
			}

		}

	}

	protected void showInputDialog_Done(String mString, String mString2) {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.register_dialog, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final Button mButtonyes = (Button) v.findViewById(R.id.yes_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.phone_text);
		if (mString2.equals("")) {
			mString2 = "+1";
		}
		mTextView.setText(mString2 + " " + mString);
		// pinCode.setHint("Enter Pin");
		// pinCode.setGravity(C);

		// pinCode.setBackgroundColor(Color.parseColor("#ffffff"));
		pinDialog = new AlertDialog.Builder(VerificationActivity.this).setView(
				v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();
						edtPhoneNo.setFocusable(false);
						edtCode.setFocusable(false);
						edtPhoneNo.setBackgroundColor(Color.TRANSPARENT);
						edtCode.setBackgroundColor(Color.TRANSPARENT);
						btnEdit.setText("Edit");
						new RegisterPhoneDONE().execute();

					}
				});
			}
		});

		pinDialog.show();

	}

}
