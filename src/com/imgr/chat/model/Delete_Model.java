package com.imgr.chat.model;

public class Delete_Model {
	public String phoneno;
	public String contactname;
	public String fullname;
	public String last;
	public String unique_id;
	public boolean box;
	public String jid;
	public String message;
	public String time;
	public String date;

	public Delete_Model(String _phone, String fullname, String uniueid,
			String jid, String message, String time, String date, boolean _box) {
		phoneno = _phone;
		// contactname = name;
		this.unique_id = uniueid;
		this.message = message;
		this.time = time;
		this.date = date;
		this.fullname = fullname;
		this.jid = jid;
		box = _box;
	}

	public String toLowerCase() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return this.contactname;
	}

	public String getdate() {
		return this.date;
	}

	public String getTIME() {
		return this.time;
	}

	public String getMessage() {
		return this.message;
	}

	public String getJID() {
		return this.jid;
	}

	public String getNumber() {
		return this.phoneno;
	}

	public String getFullname() {
		return this.fullname;
	}

	public String getlast() {
		return this.last;
	}

	public String getuniqueid() {
		return this.unique_id;
	}
}
