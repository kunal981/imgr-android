package com.imgr.chat.contact;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.ExampleContactItem;
import com.imgr.chat.model.SingleModelAdd;
import com.imgr.chat.number.UsPhoneNumberFormatter;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;

public class AddContact extends Activity {
	TextView txtBack;
	String mStringEmail, mStringPhoneNo;
	EditText mEditTextPhoneNo, mEditTextFirstName, mEditTextLastName,
			mEditTextEmail;
	Button mButtonDone;
	AlertDialog pinDialog;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	List<SingleModelAdd> mSingleModelAdds = null;
	ConnectionDetector mConnectionDetector;
	UsPhoneNumberFormatter addLineNumberFormatter;
	String unquie_id, phoneNumber;

	ArrayList<String> Imgrcontactunquieid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addcontact);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mConnectionDetector = new ConnectionDetector(this);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mEditTextPhoneNo = (EditText) findViewById(R.id.edit_phoneno);
		mEditTextFirstName = (EditText) findViewById(R.id.edit_firstname);
		mEditTextLastName = (EditText) findViewById(R.id.edit_lastname);
		mEditTextEmail = (EditText) findViewById(R.id.edit_email);
		mButtonDone = (Button) findViewById(R.id.btn_done);
		addLineNumberFormatter = new UsPhoneNumberFormatter(
				new WeakReference<EditText>(mEditTextPhoneNo));
		Imgrcontactunquieid = new ArrayList<String>();
		mEditTextPhoneNo.addTextChangedListener(addLineNumberFormatter);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constant.HANDLE_BACK_CONATCT_ADD = 0;
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		mButtonDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mStringPhoneNo = mEditTextPhoneNo.getText().toString();
				mStringPhoneNo = mStringPhoneNo.replace("[(", "")
						.replace(")", "").replace(" ", "").replace(")]", "")
						.replace("(", "").replace("-", "").replace("]", "")
						.replace("[", "").replace(" ", "").replace("(", "")
						.replace(")", "").replace("-", "");
				boolean receive = validation(mEditTextLastName.getText()
						.toString(), mStringPhoneNo, mEditTextFirstName
						.getText().toString(), mEditTextEmail.getText()
						.toString());
				if (receive) {
					if (mConnectionDetector.isConnectingToInternet()) {
						if (mEditTextEmail.getText().toString().equals("")) {
							mStringEmail = "No Email";
							// mEditTextEmail.setText("No Email");
						} else {
							mStringEmail = mEditTextEmail.getText().toString();
						}
						boolean flag = true;
						if (flag) {
							boolean flag1 = mDatasourceHandler
									.Number_Already_Exit(mStringPhoneNo);
							Log.e("flag1: ", "" + flag1);
							if (flag1) {
								LogMessage.showDialog(AddContact.this, null,
										"Contact Already Exist", null, "OK");
								mEditTextPhoneNo.setText("");
								mEditTextLastName.setText("");
								mEditTextFirstName.setText("");
								mEditTextEmail.setText("");
							} else {
								ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

								int rawContactID = ops.size();

								Log.e("rawContactID: ", "" + rawContactID);

								// Adding insert operation to operations list
								// to insert a new raw contact in the table
								// ContactsContract.RawContacts
								ops.add(ContentProviderOperation
										.newInsert(
												ContactsContract.RawContacts.CONTENT_URI)
										.withValue(
												ContactsContract.RawContacts.ACCOUNT_TYPE,
												null)
										.withValue(RawContacts.ACCOUNT_NAME,
												null).build());

								// Adding insert operation to operations list
								// to insert display name in the table
								// ContactsContract.Data
								ops.add(ContentProviderOperation
										.newInsert(
												ContactsContract.Data.CONTENT_URI)
										.withValueBackReference(
												ContactsContract.Data.RAW_CONTACT_ID,
												rawContactID)
										.withValue(
												ContactsContract.Data.MIMETYPE,
												StructuredName.CONTENT_ITEM_TYPE)
										.withValue(
												StructuredName.DISPLAY_NAME,
												mEditTextFirstName.getText()
														.toString()
														+ " "
														+ mEditTextLastName
																.getText()
																.toString())
										.build());

								// Adding insert operation to operations list
								// to insert Mobile Number in the table
								// ContactsContract.Data
								ops.add(ContentProviderOperation
										.newInsert(
												ContactsContract.Data.CONTENT_URI)
										.withValueBackReference(
												ContactsContract.Data.RAW_CONTACT_ID,
												rawContactID)
										.withValue(
												ContactsContract.Data.MIMETYPE,
												Phone.CONTENT_ITEM_TYPE)
										.withValue(
												Phone.NUMBER,
												mEditTextPhoneNo.getText()
														.toString())
										.withValue(
												Phone.TYPE,
												CommonDataKinds.Phone.TYPE_MOBILE)
										.build());

								// Adding insert operation to operations list
								// to insert Work Email in the table
								// ContactsContract.Data
								ops.add(ContentProviderOperation
										.newInsert(
												ContactsContract.Data.CONTENT_URI)
										.withValueBackReference(
												ContactsContract.Data.RAW_CONTACT_ID,
												rawContactID)
										.withValue(
												ContactsContract.Data.MIMETYPE,
												Email.CONTENT_ITEM_TYPE)
										.withValue(Email.ADDRESS, mStringEmail)
										.withValue(Email.TYPE, Email.TYPE_WORK)
										.build());

								try {
									// Executing all the insert operations as a
									// single database transaction
									getContentResolver().applyBatch(
											ContactsContract.AUTHORITY, ops);
									Toast.makeText(getBaseContext(),
											"Contact is successfully added",
											Toast.LENGTH_SHORT).show();

								} catch (RemoteException e) {
									e.printStackTrace();
								} catch (OperationApplicationException e) {
									e.printStackTrace();
								}

								Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
								String _ID = ContactsContract.Contacts._ID;
								String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
								String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

								Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
								String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
								String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

								StringBuffer output = new StringBuffer();

								ContentResolver contentResolver = getApplicationContext()
										.getContentResolver();

								Cursor cursor = contentResolver.query(
										CONTENT_URI, null, null, null, null);

								// Loop for every contact in the phone
								if (cursor.getCount() > 0) {

									while (cursor.moveToNext()) {

										String contact_id = cursor
												.getString(cursor
														.getColumnIndex(_ID));
										String name = cursor.getString(cursor
												.getColumnIndex(DISPLAY_NAME));

										int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
												.getColumnIndex(HAS_PHONE_NUMBER)));

										if (hasPhoneNumber > 0) {

											output.append("\n First Name:"
													+ name);

											// Query and loop for every phone
											// number
											// of the contact
											Cursor phoneCursor = contentResolver
													.query(PhoneCONTENT_URI,
															null,
															Phone_CONTACT_ID
																	+ " = ?",
															new String[] { contact_id },
															null);

											while (phoneCursor.moveToNext()) {
												phoneNumber = phoneCursor.getString(phoneCursor
														.getColumnIndex(NUMBER));

												// Log.e("name: ", ""+name);
												if (name.equals("")) {

												} else {

													if (phoneNumber
															.equals(mEditTextPhoneNo
																	.getText()
																	.toString())) {
														unquie_id = contact_id;
													}

												}

											}

											phoneCursor.close();

										}

									}
								}

								// unquie_id=""+Imgrcontactunquieid.size();

								Log.e("unquie_id: ", "" + unquie_id);

								mDatasourceHandler
										.SingleUnquie(mStringPhoneNo,
												mEditTextFirstName.getText()
														.toString(),
												mEditTextLastName.getText()
														.toString(), null,
												null, "0", mStringEmail,
												unquie_id, "false");

								Constant.HANDLE_BACK_CONATCT_ADD = 2;
								new ADDIMGRCONTACT().execute();
							}

						} else {
							Constant.HANDLE_BACK_CONATCT_ADD = 0;
							LogMessage.showDialog(AddContact.this, null,
									"Contact Already Exist", null, "OK");
						}

					} else {
						LogMessage.showDialog(AddContact.this, null,
								"No Internet Connection", null, "OK");
					}
				} else {

				}

			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Constant.HANDLE_BACK_CONATCT_ADD = 0;
		finish();
		overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
	}

	protected boolean validation(String lastname, String strPhoneNo,
			String firstname, String email) {
		// TODO Auto-generated method stub
		String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		if (strPhoneNo.matches("")) {
			LogMessage.showDialog(AddContact.this, null,
					"Please enter valid phone number.", null, "OK");
			return false;
		}
		if (email.equals("")) {
			// mEditTextEmail.setText("No Email");
			return true;
		}
		if (email.equals("No Email")) {
			return true;
		}
		if (email.length() > 0) {
			if (!email.matches(emailPattern)) {
				LogMessage.showDialog(AddContact.this, null,
						"Please enter valid email.", null, "OK");
				return false;
			}
			// mEditTextEmail.setText("No Email");
			return true;
		}

		// if (strPhoneNo.length() < 10) {
		// LogMessage.showDialog(AddContact.this, null,
		// "Please enter valid phone number.", null, "Ok");
		// return false;
		// }

		return true;
	}

	public class ADDIMGRCONTACT extends AsyncTask<String, Void, String> {
		public ADDIMGRCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {
			String mString = JsonParserConnector.AddContactPhone("apiKey",
					mStringPhoneNo);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			mSingleModelAdds = JsonParserConnector.SingleContactupdate(result);
			Updateallcontactlist(mSingleModelAdds);
			// Log.e("Activity mSingleModelAdds: ", "" +
			// mSingleModelAdds.size());
			UI.hideProgressDialog();

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(AddContact.this);
		}

	}

	private void Updateallcontactlist(List<SingleModelAdd> mSingleModelAdds) {
		Constant.HANDLE_BACK_CONATCT_ADD = 1;

		for (int i = 0; i < mSingleModelAdds.size(); i++) {
			final SingleModelAdd item = mSingleModelAdds.get(i);

			mDatasourceHandler.UpdateImgrSingleContact(item.getPhonenumber()
					.replace("#", "").replace("*", ""), item.getJid(),
					item.getMemberId(), item.getIsImgrUser(), item.getBase64());

		}
		showInputDialog();

		// finish();
		// overridePendingTransition(R.anim.slide_out_left,
		// R.anim.slide_in_right);

	}

	protected void showInputDialog() {

		final View v = LayoutInflater.from(this).inflate(
				R.layout.inviteimgrpeople_dialog, null);
		pinDialog = new AlertDialog.Builder(AddContact.this).setView(v)
				.create();
		final Button mButtonPop = (Button) v.findViewById(R.id.ok_dialog);

		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

						// finish();
						// overridePendingTransition(R.anim.slide_out_left,
						// R.anim.slide_in_right);
						Intent mIntent_add = new Intent(AddContact.this,
								InviteImgrPeople.class);
						mIntent_add.putExtra("name_select_invite",
								mEditTextFirstName.getText().toString());
						mIntent_add.putExtra("phone_id", unquie_id);

						mIntent_add.putExtra(
								"phoneno_select_invite",
								mEditTextPhoneNo.getText().toString()
										.replace("[(", "").replace(")", "")
										.replace(" ", "").replace(")]", "")
										.replace("(", "").replace("-", "")
										.replace("]", "").replace("[", ""));
						startActivity(mIntent_add);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);

						finish();

					}
				});

			}
		});

		pinDialog.show();

	}

}
