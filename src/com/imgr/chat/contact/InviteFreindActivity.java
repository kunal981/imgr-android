package com.imgr.chat.contact;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.adapter.InviteAdapter;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.FastListView;
import com.imgr.chat.model.Product;

public class InviteFreindActivity extends Activity {
	// ListView mListView;
	FastListView mListView;
	InviteAdapter mAdapter;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	Button mButton_selectall;
	TextView txtBack;
	private EditText searchBox;

	ArrayList<String> contactname = new ArrayList<String>();
	ArrayList<String> contactlast = new ArrayList<String>();
	ArrayList<String> contactnumber = new ArrayList<String>();
	ArrayList<Product> products = new ArrayList<Product>();
	String result;
	ArrayList<String> mArrayListPHONE = new ArrayList<String>();
	Button mButtonInvite;

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.activity_invite_listview);
		mListView = (FastListView) findViewById(R.id.list_View_IMGR);
		mButton_selectall = (Button) findViewById(R.id.selectall);
		mButtonInvite = (Button) findViewById(R.id.btn_invite);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		fetchinfo();
		mButtonInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mArrayListPHONE.clear();
				for (Product p : mAdapter.getBox()) {
					if (p.box) {
						result = "\n" + p.getNumber();
						mArrayListPHONE.add(p.getNumber());

					}
				}

				Log.e("mArrayListPHONE: ", "" + mArrayListPHONE);
				String mString = mArrayListPHONE.toString().replace("[", "")
						.replace("]", "").replace(",", "");
				Log.e("", "" + mString);
				StringBuilder uri = new StringBuilder("sms:");
				for (int i = 0; i < mArrayListPHONE.size(); i++) {
					uri.append(mArrayListPHONE.get(i));
					uri.append(", ");
				}

				Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
				smsVIntent.setType("vnd.android-dir/mms-sms");
				// smsVIntent.putExtra("address", mString);
				smsVIntent.setData(Uri.parse(uri.toString()));
				smsVIntent
						.putExtra(
								"sms_body",
								"Check out imgr instant messaging app.Download it today from http://imgr.im/app");
				try {
					startActivity(smsVIntent);
				} catch (Exception ex) {
					Toast.makeText(getApplicationContext(),
							"Your sms has failed...", Toast.LENGTH_LONG).show();

					ex.printStackTrace();

				}

			}
		});

		mButton_selectall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mButton_selectall.getText().toString().equals("Select All")) {
					products.clear();
					for (int i = 0; i < contactnumber.size(); i++) {
						products.add(new Product(contactnumber.get(i),
								contactname.get(i), contactlast.get(i),
								contactname.get(i) + " " + contactlast.get(i),
								true));
						// products.add(new Product(contactnumber.get(i),
						// contactname
						// .get(i), true));
					}
					mAdapter.notifyDataSetChanged();
					mButton_selectall.setText("Deselect All");
				} else {
					products.clear();
					for (int i = 0; i < contactnumber.size(); i++) {
						products.add(new Product(contactnumber.get(i),
								contactname.get(i), contactlast.get(i),
								contactname.get(i) + " " + contactlast.get(i),
								false));
						// products.add(new Product(contactnumber.get(i),
						// contactname
						// .get(i), true));
					}
					mAdapter.notifyDataSetChanged();
					mButton_selectall.setText("Select All");
				}

			}
		});
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		searchBox = (EditText) findViewById(R.id.input_search_query);
		searchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					mAdapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@SuppressLint("DefaultLocale")
			@Override
			public void afterTextChanged(Editable s) {
				// searchString = searchBox.getText().toString().trim()
				// .toUpperCase();
				// searchBox.setText("");

			}
		});

	}

	public void fetchinfo() {

		try {
			mCursor = mDatasourceHandler.AllContacts();
			// Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			contactname.clear();
			contactnumber.clear();
			contactlast.clear();
			do {

				contactname.add(mCursor.getString(1).trim());
				contactnumber.add(mCursor.getString(2).trim());
				contactlast.add(mCursor.getString(3).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		Log.e("contactlast: ", "" + contactlast);
		for (int i = 0; i < contactnumber.size(); i++) {
			products.add(new Product(contactnumber.get(i), contactname.get(i),
					contactlast.get(i), contactname.get(i) + " "
							+ contactlast.get(i), false));
		}
		mAdapter = new InviteAdapter(InviteFreindActivity.this, contactname,
				contactnumber, products);

		mListView.setAdapter(mAdapter);
	}
}
