package com.imgr.chat.contact;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.SingleModelAdd;
import com.imgr.chat.number.PlusClass;
import com.imgr.chat.number.UsPhoneNumberFormatter;
import com.imgr.chat.util.LogMessage;

public class EditAddContact extends Activity {
	TextView txtBack;

	String mStringLastestPhoneNo, mStringLatestCountryCode;

	EditText mEditTextFirstName, mEditTextLastName, mEditTextEmail;
	EditText mEditTextPhoneNo, mEditTextCountryCode;
	Button mButtonDone;
	AlertDialog pinDialog;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	List<SingleModelAdd> mSingleModelAdds = null;
	ConnectionDetector mConnectionDetector;
	String mString_name, mString_phone_no, mString_email, mString_lastname,
			mString_older, mStringPhoneId;
	boolean flag = false;
	UsPhoneNumberFormatter addLineNumberFormatter;
	PlusClass plusClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_add_contact);

		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mConnectionDetector = new ConnectionDetector(this);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mEditTextPhoneNo = (EditText) findViewById(R.id.edt_phoneno);
		mEditTextCountryCode = (EditText) findViewById(R.id.edtCode);
		// mEditTextPhoneNo = (Button) findViewById(R.id.edit_phoneno);
		mEditTextFirstName = (EditText) findViewById(R.id.edit_firstname);
		mEditTextLastName = (EditText) findViewById(R.id.edit_lastname);
		mEditTextEmail = (EditText) findViewById(R.id.edit_email);
		mButtonDone = (Button) findViewById(R.id.btn_done);
		mStringPhoneId = getIntent().getStringExtra("EDIT_PHONE_ID");
		mString_name = getIntent().getStringExtra("EDIT_NAME");
		mString_phone_no = getIntent().getStringExtra("EDIT_PHONE");

		mString_email = getIntent().getStringExtra("EDIT_MAIL");
		mString_lastname = getIntent().getStringExtra("EDIT_LASTNAME");
		addLineNumberFormatter = new UsPhoneNumberFormatter(
				new WeakReference<EditText>(mEditTextPhoneNo));
		plusClass = new PlusClass(new WeakReference<EditText>(
				mEditTextCountryCode));
		mEditTextPhoneNo.addTextChangedListener(addLineNumberFormatter);
		mEditTextCountryCode.addTextChangedListener(plusClass);
		mString_older = mString_phone_no;
		// String[] separated = mString_phone_no.split("(");
		// separated[0]; // this will contain "Fruit"
		// separated[1]; // this will contain " they taste good"
		// You may want to remove the space to the second String:

		// separated[1] = separated[1].trim();
		// There are other ways to do it. For instance, you can use the
		// StringTokenizer class (from java.util):

		StringTokenizer tokens = new StringTokenizer(mString_phone_no, "(");
		if (tokens.countTokens() == 2) {
			String first = tokens.nextToken();// this will contain "Fruit"
			String second = tokens.nextToken();//
			Log.e("first:", "" + first);
			Log.e("second:", "" + second);
			mEditTextCountryCode.setText(first);
			mEditTextPhoneNo.setText("(" + second);
		} else {
			mEditTextPhoneNo.setText(mString_phone_no);
		}

		// long phoneNum = Long.parseLong(mString_phone_no);
		// mString_older = mString_phone_no;
		// //
		// System.out.println(String.valueOf(phoneNum).replaceFirst("(\\d{3})(\\d{3})(\\d+)",
		// // "($1) $2-$3"));
		// Log.e("-----------",
		// ""
		// + String.valueOf(phoneNum).replaceFirst(
		// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		// mString_phone_no = String.valueOf(phoneNum).replaceFirst(
		// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
		Log.e("Before mString_phone_no: ", "" + mString_phone_no);

		mEditTextFirstName.setText(mString_name);
		mEditTextLastName.setText(mString_lastname);
		mEditTextFirstName.setSelection(mEditTextFirstName.getText().length());
		mEditTextLastName.setSelection(mEditTextLastName.getText().length());
		if (mString_email.equals("No Email")) {
			mEditTextEmail.setText("");
		} else if (mString_email.equals("")) {
			mEditTextEmail.setText("");
		} else {
			mEditTextEmail.setText(mString_email);
		}

		mEditTextEmail.setSelection(mEditTextEmail.getText().length());
		// mEditTextPhoneNo.setSelection(mEditTextPhoneNo.getText().length());
		mEditTextPhoneNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// showInputDialog_Done(mEditTextPhoneNo.getText().toString());
			}
		});
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		mButtonDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean receive = validation(mEditTextLastName.getText()
						.toString(), mEditTextPhoneNo.getText().toString(),
						mEditTextFirstName.getText().toString(), mEditTextEmail
								.getText().toString());
				if (receive) {
					if (mConnectionDetector.isConnectingToInternet()) {
						if (mEditTextEmail.getText().toString().equals("")) {
							// mEditTextEmail.setText("No Email");
							mString_email = "No Email";
						} else {
							mString_email = mEditTextEmail.getText().toString();
						}
						mString_older = mString_older.replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", "").replace(" ", "")
								.replace("(", "").replace(")", "")
								.replace("-", "");
						String mStringPhone = mEditTextPhoneNo.getText()
								.toString();
						mStringPhone = mStringPhone.replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", "").replace(" ", "")
								.replace("(", "").replace(")", "")
								.replace("-", "");
						Log.e("NEW:", "" + mStringPhone);
						Log.e("OLDER:", "" + mString_older);
						mStringLatestCountryCode = mEditTextCountryCode
								.getText().toString().trim();
						update(mStringPhoneId, mEditTextFirstName.getText()
								.toString(), mEditTextLastName.getText()
								.toString(), mStringLatestCountryCode + ""
								+ mStringPhone);

						mDatasourceHandler.UpdateContact(
								mStringLatestCountryCode + "" + mString_older,
								mStringLatestCountryCode + "" + mStringPhone,
								mEditTextFirstName.getText().toString(),
								mEditTextLastName.getText().toString(),
								mString_email, mStringPhoneId);

						InviteImgrPeople.mString_phone_no = mStringPhone;

						InviteImgrPeople.mStringPhoneId = mStringPhoneId;
						finish();
						overridePendingTransition(R.anim.slide_out_left,
								R.anim.slide_in_right);
					} else {
						LogMessage.showDialog(EditAddContact.this, null,
								"No Internet Connection", null, "OK");
					}
				} else {

				}

			}
		});
	}

	public void formatnumbercorrectway(String mString) {
		if (mString.contains("+")) {
			flag = true;
			mString = mString.replace("+", "");
		} else {
			flag = false;
		}
		if (mString.length() == 10) {
			Log.e("", "Indide 10");
			mString_phone_no = String.valueOf(mString).replaceFirst(
					"(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
			// mString_new_phone_no = String.valueOf(phoneNum).replaceFirst(
			// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		} else if (mString.length() == 11) {
			Log.e("", "Indide 11");
			mString_phone_no = String.valueOf(mString).replaceFirst(
					"(\\d{1})(\\d{3})(\\d{3})(\\d+)", "$1 ($2) $3-$4");

		} else if (mString.length() == 12) {
			Log.e("", "Indide 12 " + mString);
			mString_phone_no = String.valueOf(mString).replaceFirst(
					"(\\d{2})(\\d{3})(\\d{3})(\\d+)", "$1 ($2) $3-$4");
			Log.e("", "Indide 12: " + mString_phone_no);

		} else if (mString.length() == 13) {
			Log.e("", "Indide 13");
			mString_phone_no = String.valueOf(mString).replaceFirst(
					"(\\d{3})(\\d{3})(\\d{3})(\\d+)", "$1 ($2) $3-$4");

		} else if (mString.length() == 14) {
			Log.e("", "Indide 14");
			mString_phone_no = String.valueOf(mString).replaceFirst(
					"(\\d{4})(\\d{3})(\\d{3})(\\d+)", "$1 ($2) $3-$4");

		} else if (mString.length() == 15) {
			Log.e("", "Indide 15");
			mString_phone_no = String.valueOf(mString).replaceFirst(
					"(\\d{5})(\\d{3})(\\d{3})(\\d+)", "$1 ($2) $3-$4");

		}
		if (flag) {
			flag = false;
			mEditTextPhoneNo.setText("+" + mString_phone_no);
		} else {
			flag = false;
			mEditTextPhoneNo.setText(mString_phone_no);
		}

	}

	protected void showInputDialog_Done(String mString) {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.editphonenumberpopup, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);

		final Button mButtonyes = (Button) v.findViewById(R.id.OK);
		final EditText mEditText = (EditText) v
				.findViewById(R.id.editphone_change);

		mEditText.setText(mString);
		mEditText.setSelection(mEditText.getText().length());
		// pinCode.setBackgroundColor(Color.parseColor("#ffffff"));
		pinDialog = new AlertDialog.Builder(EditAddContact.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						String mString = mEditText.getText().toString();
						formatnumbercorrectway(mString.replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", ""));
						pinDialog.cancel();

					}
				});
			}
		});

		pinDialog.show();

	}

	public void update(String id, String firstname, String last,
			String mStringPhone) {
		// int id = 1;
		// String firstname = "Contact's first name";
		// String lastname = "Last name";
		// String number = "000 000 000";
		// String photo_uri =
		// "android.resource://com.my.package/drawable/default_photo";

		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

		// Name
		Builder builder = ContentProviderOperation
				.newUpdate(ContactsContract.Data.CONTENT_URI);
		builder.withSelection(
				ContactsContract.Data.CONTACT_ID + "=?" + " AND "
						+ ContactsContract.Data.MIMETYPE + "=?",
				new String[] {
						id,
						ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE });

		// ops.add(ContentProviderOperation
		// .newUpdate(ContactsContract.Data.CONTENT_URI)
		// .withSelection(
		// ContactsContract.Data.CONTACT_ID
		// + "=? AND "
		// + ContactsContract.Data.MIMETYPE
		// + "=?",
		// new String[] {
		// contactId,
		// ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE })
		// .withValue(
		// ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
		// newName).build());

		// builder.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
		// lastname);
		builder.withValue(
				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
				firstname + " " + last);
		ops.add(builder.build());

		Builder builder1 = ContentProviderOperation
				.newUpdate(ContactsContract.Data.CONTENT_URI);
		builder1.withSelection(
				ContactsContract.Data.CONTACT_ID + "=?" + " AND "
						+ ContactsContract.Data.MIMETYPE + "=?" + " AND "
						+ ContactsContract.CommonDataKinds.Organization.TYPE
						+ "=?",
				new String[] {
						id,
						ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
						String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE) });

		builder1.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
				mStringPhone);

		ops.add(builder1.build());

		// Update
		try {
			getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected boolean validation(String lastname, String strPhoneNo,
			String firstname, String email) {
		// TODO Auto-generated method stub
		String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		if (strPhoneNo.matches("")) {
			LogMessage.showDialog(EditAddContact.this, null,
					"Please enter valid phone number.", null, "OK");
			return false;
		}
		// if (email.equals("")) {
		// LogMessage.showDialog(EditAddContact.this, null,
		// "Please enter Email.", null, "Ok");
		// // mEditTextEmail.setText("No Email");
		// return false;
		// }
		// if (email.equals("No Email")) {
		// return true;
		// }
		if (email.length() > 0) {
			if (!email.matches(emailPattern)) {
				LogMessage.showDialog(EditAddContact.this, null,
						"Please enter valid email.", null, "OK");
				return false;
			}
			// mEditTextEmail.setText("No Email");
			return true;
		}

		// if (strPhoneNo.length() < 10) {
		// LogMessage.showDialog(EditAddContact.this, null,
		// "Please enter valid phone number.", null, "Ok");
		// return false;
		// }

		return true;
	}

}
