package com.imgr.chat.contact;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.views.BetterPopupWindow;

public class InviteImgrPeople extends Activity {
	String mString_name, mStringLastname, mString_new_phone_no;
	public static String mString_phone_no, mStringPhoneId;
	TextView mTextView_textview_name, mTextView_textview_no, mTextViewEmail;
	TextView txtBack, mTextViewInvite;
	Button mButton_edit;
	String mStringEmail;
	public static int update = 0;

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	int click_invite = 0;
	FrameLayout layout;
	// FrameLayout mFrameLayout;
	DemoPopupWindow dw;
	boolean plus = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inviteimgr);
		layout = (FrameLayout) findViewById(R.id.linear);
		// mFrameLayout=(FrameLayout)findViewById(R.id.pop_up_view);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mTextViewEmail = (TextView) findViewById(R.id.textview_email);
		mTextView_textview_name = (TextView) findViewById(R.id.textview_name);
		mTextView_textview_no = (TextView) findViewById(R.id.textview_no);
		mButton_edit = (Button) findViewById(R.id.btn_edit);
		mString_name = getIntent().getStringExtra("name_select_invite");
		mString_phone_no = getIntent().getStringExtra("phoneno_select_invite");
		mStringPhoneId = getIntent().getStringExtra("phone_id");
		// Log.e("mString_phone_no: ", ""+mString_phone_no);
		click_invite = getIntent().getIntExtra("invite", 0);
		mTextView_textview_name.setText(mString_name);
		mTextView_textview_no.setText(mString_phone_no);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mTextViewInvite = (TextView) findViewById(R.id.invite_people);
		// mFrameLayout.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// dw.dismiss();
		// mFrameLayout.setVisibility(View.GONE);
		//
		// }
		// });
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				update = 1;
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		mButton_edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				click_invite = 0;
				Intent mIntent = new Intent(InviteImgrPeople.this,
						EditAddContact.class);
				mIntent.putExtra("EDIT_NAME", mString_name);

				mIntent.putExtra("EDIT_PHONE_ID", mStringPhoneId);
				Log.e("stering: ", ""
						+ mTextView_textview_no.getText().toString());
				mIntent.putExtra("EDIT_PHONE", mTextView_textview_no.getText()
						.toString());
				mIntent.putExtra("EDIT_MAIL", mStringEmail);
				mIntent.putExtra("EDIT_LASTNAME", mStringLastname);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);

			}
		});
		mTextViewInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mStringEmail.equals("")) {
					click_invite = 1;
					try {
						Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
						smsVIntent.setType("vnd.android-dir/mms-sms");
						smsVIntent.putExtra("address", mString_phone_no);
						smsVIntent
								.putExtra(
										"sms_body",
										"Check out imgr instant messaging app. Download it today from http://imgr.im/app");

						startActivity(smsVIntent);
					} catch (Exception ex) {
						Toast.makeText(getApplicationContext(),
								"Your sms has failed...", Toast.LENGTH_LONG)
								.show();

						ex.printStackTrace();

					}
				} else if (mStringEmail.equals("No Email")) {
					click_invite = 1;
					try {
						Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
						smsVIntent.setType("vnd.android-dir/mms-sms");
						smsVIntent.putExtra("address", mString_phone_no);
						smsVIntent
								.putExtra(
										"sms_body",
										"Check out imgr instant messaging app. Download it today from http://imgr.im/app");

						startActivity(smsVIntent);
					} catch (Exception ex) {
						Toast.makeText(getApplicationContext(),
								"Your sms has failed...", Toast.LENGTH_LONG)
								.show();

						ex.printStackTrace();

					}
				} else {
					click_invite = 1;
					// layout.setVisibility(View.GONE);
					// mFrameLayout.setVisibility(View.VISIBLE);
					// mFrameLayout.setBackgroundColor(Color.parseColor("#99000000"));
					dw = new DemoPopupWindow(v, InviteImgrPeople.this);
					dw.showLikeQuickAction(0, 0);
				}

			}
		});
	}

	// ************** Class for pop-up window **********************
	/**
	 * The Class DemoPopupWindow.
	 */
	private class DemoPopupWindow extends BetterPopupWindow {

		/**
		 * Instantiates a new demo popup window.
		 * 
		 * @param anchor
		 *            the anchor
		 * @param cnt
		 *            the cnt
		 */
		public DemoPopupWindow(View anchor, Context cnt) {
			super(anchor);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cellalert24.Views.BetterPopupWindow#onCreate()
		 */
		@Override
		protected void onCreate() {
			// inflate layout
			LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			ViewGroup root = (ViewGroup) inflater.inflate(
					R.layout.share_choose_popup, null);

			TextView txtmail = (TextView) root.findViewById(R.id.txtMail);
			TextView txtmessage = (TextView) root.findViewById(R.id.txtMessage);
			Button mButton = (Button) root.findViewById(R.id.cancelBtn);

			txtmail.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();
					sendEmail();

				}
			});
			txtmessage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();
					Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
					smsVIntent.setType("vnd.android-dir/mms-sms");
					smsVIntent.putExtra("address", mString_phone_no);
					smsVIntent
							.putExtra(
									"sms_body",
									"Check out imgr instant messaging app.Download it today from http://imgr.im/app");

					startActivity(smsVIntent);

				}
			});
			mButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();

				}
			});

			this.setContentView(root);
		}

	}

	protected void sendEmail() {
		Log.e("Send email", "" + mStringEmail);
		String[] TO = { mStringEmail };
		String[] CC = { "" };
		Intent emailIntent = new Intent(Intent.ACTION_SEND);

		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.setType("text/plain");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		emailIntent.putExtra(Intent.EXTRA_CC, CC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "IMGR Android Application");
		emailIntent
				.putExtra(
						Intent.EXTRA_TEXT,
						"Check out imgr instant messaging app.Download it today from http://imgr.im/app");

		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			// finish();
			Log.i("Finished sending email...", "");
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(InviteImgrPeople.this,
					"There is no email client installed.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		update = 1;
		finish();
		overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			if (click_invite == 1) {
				Log.e("", "CALLING");
				click_invite = 0;
				mTextViewInvite.setText("RESEND INVITE");
				mString_phone_no = mString_phone_no.replace(" ", "")
						.replace("(", "").replace(")", "").replace("-", "")
						.replace("*", "").replace("[(", "").replace(")", "")
						.replace(" ", "").replace(")]", "").replace("(", "")
						.replace("-", "").replace("]", "").replace("[", "");
				mDatasourceHandler.Update_Invite_Contact(mStringPhoneId);
			}
			Log.e("First mString_phone_no", mString_phone_no + "");
			mString_phone_no = mString_phone_no.replace(" ", "")
					.replace("(", "").replace(")", "").replace("-", "");
			Log.e("INVITE mString_phone_no:", "" + mString_phone_no);
			mCursor = mDatasourceHandler.FetchContact(mStringPhoneId);
			Log.e("mString_phone_no", mString_phone_no + "");
			Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {

			do {

				mString_name = mCursor.getString(1).trim();
				mString_phone_no = mCursor.getString(2).trim();
				mStringLastname = mCursor.getString(3).trim();
				mStringEmail = mCursor.getString(6).trim();

				Log.e("mString_name Fragment: ", mString_name + "");
				Log.e("mString_phone_no Fragment: ", mString_phone_no + "");

				Log.e("mStringEmail Fragment: ", mStringEmail + "");

			} while (mCursor.moveToNext());

			mCursor.close();
		}

		if (mCursor.getCount() != 0) {

			mCursor.close();

			mTextView_textview_name.setText(mString_name + " "
					+ mStringLastname);

			// if (mString_phone_no.length() >= 10) {
			// mString_phone_no=mString_phone_no.substring(mString_phone_no.length()
			// - 10);
			// Log.e("if mString_phone_no: ", ""+mString_phone_no);
			// }
			// else{
			// mString_phone_no=mString_phone_no;
			// Log.e("else mString_new_phone_no: ", ""+mString_new_phone_no);
			// }
			mString_phone_no = mString_phone_no.trim();
			if (mString_phone_no.contains("+")) {
				mString_phone_no = mString_phone_no.replace("+", "");
				plus = true;
			} else {
				plus = false;
			}
			Log.e("Before mString_phone_no: ", mString_phone_no + "");
			if (mString_phone_no.length() == 10) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
				// mString_new_phone_no = String.valueOf(phoneNum).replaceFirst(
				// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
			} else if (mString_phone_no.length() == 11) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{1})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 12) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{2})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 13) {
				Log.e("", "Indide Indsidfhisjkf");
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{3})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 14) {

				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{4})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 15) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{5})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			}
			Log.e("After mString_phone_no: ", mString_phone_no + "");

			if (plus) {
				plus = false;
				mTextView_textview_no.setText("+" + mString_phone_no);
			} else {
				plus = false;
				mTextView_textview_no.setText(mString_phone_no);
			}

			if (mStringEmail.equals("")) {
				mTextViewEmail.setText("No Email");
			} else {
				mTextViewEmail.setText(mStringEmail);
			}

		}
	}

}
