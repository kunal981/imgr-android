package com.imgr.chat.contact;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.message.ChatScreen;
import com.imgr.chat.setting.BlockedContacts;
import com.imgr.chat.util.LogMessage;
import com.joooonho.SelectableRoundedImageView;

public class BlockAndMeaageActivity extends Activity {
	String mString_name, mString_phone_no, string, mStringlast, mStringLast,
			mString_new_phone_no;
	TextView mTextView_textview_name, mTextView_textview_no, mTextViewEmail;
	TextView txtBack, text_changed;
	Button mButtonEdit;
	String mStringEmail, mStringProfile, mStringPhoneId;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	SelectableRoundedImageView mImageViewProfile;
	FrameLayout mFrameLayout, mFrameLayout_block_contact;
	Bitmap targetBitmap;
	AlertDialog pinDialog;
	boolean plus = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sendmessage_blockcontact);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mImageViewProfile = (SelectableRoundedImageView) findViewById(R.id.image_profile);
		mFrameLayout = (FrameLayout) findViewById(R.id.send_message);
		mFrameLayout_block_contact = (FrameLayout) findViewById(R.id.block_contact);
		mTextView_textview_name = (TextView) findViewById(R.id.textview_name);
		mTextView_textview_no = (TextView) findViewById(R.id.textview_no);
		mString_name = getIntent().getStringExtra("name_select");
		mStringPhoneId = getIntent().getStringExtra("phone_id");
		mString_phone_no = getIntent().getStringExtra("phoneno_select");
		mTextViewEmail = (TextView) findViewById(R.id.textview_email);
		text_changed = (TextView) findViewById(R.id.text_changed);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mButtonEdit = (Button) findViewById(R.id.btn_edit);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constant.HANDLE_BACK_CONATCT_ADD = 0;
				if (Constant.HANDLE_BACK_CONATCT_SCREEN_ONLY == 1) {

				} else {
					Constant.HANDLE_BACK_CONATCT_SCREEN_ONLY = 0;
				}
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		mFrameLayout_block_contact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (text_changed.getText().toString().equals("Block Contact")) {
					mDatasourceHandler.UpdateBlockContact(mStringPhoneId,
							"true");

					Send_again_popup();
				} else {

					mDatasourceHandler.UpdateBlockContact(mStringPhoneId,
							"false");

					Send_again_popup_();
				}

				// LogMessage.showDialog(BlockAndMeaageActivity.this, null,
				// "This feature availble in next module.", null, "Ok");
			}
		});
		mFrameLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_info = new Intent(BlockAndMeaageActivity.this,
						ChatScreen.class);
				mString_phone_no = mTextView_textview_no.getText().toString();
				mString_phone_no = mString_phone_no.replace(" ", "")
						.replace("(", "").replace(")", "").replace("-", "");
				mIntent_info.putExtra("Chat_name_select", mString_name);
				mIntent_info.putExtra("Chat_no_select", mString_phone_no);
				mIntent_info.putExtra("phone_id", mStringPhoneId);
				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		mButtonEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(BlockAndMeaageActivity.this,
						EditScreenImgr.class);
				mIntent.putExtra("NAME", mString_name);
				mIntent.putExtra("LAST", mStringlast);
				mIntent.putExtra("Edit_phone_id", mStringPhoneId);
				mIntent.putExtra("PHONE_NO", mTextView_textview_no.getText()
						.toString());
				mIntent.putExtra("EMAIL", mStringEmail);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);

			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e("mStringPhoneId: ", "" + mStringPhoneId);

		try {
			mString_phone_no = mString_phone_no.replace(" ", "")
					.replace("(", "").replace(")", "").replace("-", "");
			mCursor = mDatasourceHandler.FetchContact(mStringPhoneId);
			Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {

			do {

				mString_name = mCursor.getString(1).trim();
				mString_phone_no = mCursor.getString(2).trim();
				mStringlast = mCursor.getString(3).trim();
				mStringEmail = mCursor.getString(6).trim();
				mStringProfile = mCursor.getString(7).trim();
				Log.e("mString_name Fragment: ", mString_name + "");
				Log.e("mString_phone_no Fragment: ", mString_phone_no + "");

			} while (mCursor.moveToNext());

			mCursor.close();
		}

		if (mCursor.getCount() != 0) {

			mCursor.close();

			mTextView_textview_name.setText(mString_name + " " + mStringlast);

			mString_phone_no = mString_phone_no.trim();
			if (mString_phone_no.contains("+")) {
				mString_phone_no = mString_phone_no.replace("+", "");
				plus = true;
			} else {
				plus = false;
			}
			Log.e("Before mString_phone_no: ", mString_phone_no + "");
			if (mString_phone_no.length() == 10) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
				// mString_new_phone_no = String.valueOf(phoneNum).replaceFirst(
				// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
			} else if (mString_phone_no.length() == 11) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{1})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 12) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{2})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 13) {
				Log.e("", "Indide Indsidfhisjkf");
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{3})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 14) {

				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{4})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			} else if (mString_phone_no.length() == 15) {
				mString_phone_no = String.valueOf(mString_phone_no)
						.replaceFirst("(\\d{5})(\\d{3})(\\d{3})(\\d+)",
								"$1 ($2) $3-$4");

			}
			Log.e("After mString_phone_no: ", mString_phone_no + "");

			if (plus) {
				plus = false;
				mTextView_textview_no.setText("+" + mString_phone_no);
			} else {
				plus = false;
				mTextView_textview_no.setText(mString_phone_no);
			}

			if (mStringEmail.equals("")) {
				mTextViewEmail.setText("No Email");
			} else {
				mTextViewEmail.setText(mStringEmail);
			}

			if (mStringProfile.equals("Null")) {

			} else {
				Bitmap mBitmap = ConvertToImage(mStringProfile);
				mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
				mImageViewProfile.setOval(true);
				mImageViewProfile.setImageBitmap(mBitmap);

			}
		}

	}

	public Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		int targetWidth = 100;
		int targetHeight = 100;
		targetBitmap = Bitmap.createBitmap(targetWidth,

		targetHeight, Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);

		return targetBitmap;
	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	protected void Send_again_popup() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.send_again_popup, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.text_change);

		mTextView
				.setText("The Contact has been blocked.If you want to unblock go to Settings > Blocked Contacts");
		mTextView.setTypeface(null, Typeface.BOLD);
		pinDialog = new AlertDialog.Builder(BlockAndMeaageActivity.this)
				.setView(v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						text_changed.setText("Unblock Contact");
						Constant.HANDLE_BACK_CONATCT_SCREEN_ONLY = 1;
						pinDialog.cancel();

					}
				});

			}
		});

		pinDialog.show();

	}

	protected void Send_again_popup_() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.send_again_popup, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.text_change);
		mTextView.setText("The Contact has been unblocked for you.");
		mTextView.setTypeface(null, Typeface.BOLD);
		pinDialog = new AlertDialog.Builder(BlockAndMeaageActivity.this)
				.setView(v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						text_changed.setText("Block Contact");
						Constant.HANDLE_BACK_CONATCT_SCREEN_ONLY = 0;
						pinDialog.cancel();

					}
				});

			}
		});

		pinDialog.show();

	}
}
