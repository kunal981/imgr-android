package com.imgr.chat.contact;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView.ScaleType;
import android.widget.Switch;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.util.LogMessage;
import com.joooonho.SelectableRoundedImageView;

public class EditScreenImgr extends Activity {

	TextView txtBack, mEditTextPhoneNo;
	EditText mEditTextFirstName, mEditTextLastname, mEditTextEmail;
	String mString_name, mString_phone_no, mString_email, mString_lastname,
			mStringProfile, mStringPhoneId;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	Button mButtonDone;
	SelectableRoundedImageView mImageViewProfile;
	Switch switch_promo, switch_auto_promo;
	CheckBox checkbox_sponsored, checkbox_personal;

	String setPromo, setAutorotate, setSponsoredads, setpersonalads,
			setuniqueid;

	FrameLayout mFrameLayoutAuto, mFrameLayoutSponsored, mFrameLayoutPersonal;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actvity_editscreenimgr);
		mImageViewProfile = (SelectableRoundedImageView) findViewById(R.id.image_profile);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mButtonDone = (Button) findViewById(R.id.btn_done);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mString_name = getIntent().getStringExtra("NAME");
		mString_phone_no = getIntent().getStringExtra("PHONE_NO");
		mString_email = getIntent().getStringExtra("EMAIL");
		mString_lastname = getIntent().getStringExtra("LAST");
		mStringPhoneId = getIntent().getStringExtra("Edit_phone_id");
		mEditTextFirstName = (EditText) findViewById(R.id.text_first_name);
		mEditTextLastname = (EditText) findViewById(R.id.text_last_name);
		mEditTextEmail = (EditText) findViewById(R.id.text_Email);
		mEditTextPhoneNo = (TextView) findViewById(R.id.text_phone_no);
		switch_promo = (Switch) findViewById(R.id.switch_promo);
		switch_auto_promo = (Switch) findViewById(R.id.switch_auto_promo);
		checkbox_sponsored = (CheckBox) findViewById(R.id.checkbox_sponsored);
		checkbox_personal = (CheckBox) findViewById(R.id.checkbox_personal);
		mFrameLayoutAuto = (FrameLayout) findViewById(R.id.auto_frame);
		mFrameLayoutSponsored = (FrameLayout) findViewById(R.id.sponsored_frame);
		mFrameLayoutPersonal = (FrameLayout) findViewById(R.id.personal_frame);

		// long phoneNum = Long.parseLong(mString_phone_no);
		// //
		// System.out.println(String.valueOf(phoneNum).replaceFirst("(\\d{3})(\\d{3})(\\d+)",
		// // "($1) $2-$3"));
		// Log.e("-----------",
		// ""
		// + String.valueOf(phoneNum).replaceFirst(
		// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3"));
		// mString_phone_no = String.valueOf(phoneNum).replaceFirst(
		// "(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
		if (mString_name != null) {
			if (mString_name.equals("No Name")) {
				mString_name = "";
			}
			if (mString_name.equals("No")) {
				mString_name = "";
			}
			if (mString_lastname.equals("Name")) {
				mString_lastname = "";
			}
		}
		mEditTextPhoneNo.setText(mString_phone_no);
		mEditTextFirstName.setText(mString_name);

		mEditTextLastname.setText(mString_lastname);
		if (mString_email.equals("No Email")) {
			mEditTextEmail.setText("");
		} else {
			mEditTextEmail.setText(mString_email);
		}
		// mEditTextEmail.setText(mString_email);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constant.HANDLE_BACK_IMGRFRDS = 0;
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		mButtonDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean receive = validation(mEditTextEmail.getText()
						.toString());
				if (receive) {
					if (mEditTextEmail.getText().toString().equals("")) {
						// mEditTextEmail.setText("No Email");
						mString_email = "No Email";
					} else {
						mString_email = mEditTextEmail.getText().toString();
					}
					mString_phone_no = mEditTextPhoneNo.getText().toString();
					mString_phone_no = mString_phone_no.replace(" ", "")
							.replace("(", "").replace(")", "").replace("-", "");
					String firstname = mEditTextFirstName.getText().toString()
							.trim();
					String lastname = mEditTextLastname.getText().toString()
							.trim();
					if (firstname.equals("")) {
						firstname = "No Name";
					}
					if (lastname.equals("") && firstname.equals("")) {
						firstname = "No Name";
					}
					if (lastname.equals("")) {
						lastname = "";
					}
					update(mStringPhoneId, firstname, lastname);
					// updateContact(mEditTextFirstName.getText()
					// .toString()+""+mEditTextLastname.getText()
					// .toString(),mString_phone_no);
					mDatasourceHandler.UpdateContact(mString_phone_no,
							mString_phone_no, firstname, lastname,
							mString_email, mStringPhoneId);
					Constant.HANDLE_BACK_IMGRFRDS = 1;
					finish();
					overridePendingTransition(R.anim.slide_out_left,
							R.anim.slide_in_right);
				}

				// Intent mIntent_add = new Intent(EditScreenImgr.this,
				// InviteImgrPeople.class);
				// mIntent_add.putExtra("name_select_invite",
				// mEditTextFirstName.getText().toString());
				// mIntent_add.putExtra(
				// "phoneno_select_invite",
				// mEditTextPhoneNo.getText().toString()
				// .replace("[(", "").replace(")", "")
				// .replace(" ", "").replace(")]", "")
				// .replace("(", "").replace("-", "")
				// .replace("]", "").replace("[", ""));
				// startActivity(mIntent_add);
				// overridePendingTransition(R.anim.slide_in_left,
				// R.anim.slide_out_right);

			}
		});

		mEditTextLastname.setSelection(mEditTextLastname.getText().length());
		mEditTextEmail.setSelection(mEditTextEmail.getText().length());

		mEditTextFirstName.setSelection(mEditTextFirstName.getText().length());

		switch_promo.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					mDatasourceHandler.UpdatePROMO(mStringPhoneId, "true");
					mFrameLayoutAuto.setVisibility(View.VISIBLE);

					mFrameLayoutPersonal.setVisibility(View.VISIBLE);
					mFrameLayoutSponsored.setVisibility(View.VISIBLE);
				} else {
					mDatasourceHandler.UpdatePROMO(mStringPhoneId, "false");
					mDatasourceHandler.UpdateAUTO_PERSONALADS(mStringPhoneId,
							"false");
					mDatasourceHandler
							.UpdateAUTO_PROMO(mStringPhoneId, "false");
					mDatasourceHandler.UpdateAUTO_SPONSOREDADS(mStringPhoneId,
							"true");
					mFrameLayoutAuto.setVisibility(View.GONE);

					mFrameLayoutPersonal.setVisibility(View.GONE);
					mFrameLayoutSponsored.setVisibility(View.GONE);
				}

			}
		});
		switch_auto_promo
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mDatasourceHandler.UpdateAUTO_PROMO(mStringPhoneId,
									"true");
						} else {
							mDatasourceHandler.UpdateAUTO_PROMO(mStringPhoneId,
									"false");
						}
					}
				});
		checkbox_sponsored
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mDatasourceHandler.UpdateAUTO_SPONSOREDADS(
									mStringPhoneId, "true");
						} else {
							mDatasourceHandler.UpdateAUTO_SPONSOREDADS(
									mStringPhoneId, "false");
						}
					}
				});
		checkbox_personal
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mDatasourceHandler.UpdateAUTO_PERSONALADS(
									mStringPhoneId, "true");
						} else {
							mDatasourceHandler.UpdateAUTO_PERSONALADS(
									mStringPhoneId, "false");
						}
					}
				});

	}

	public void update(String id, String firstname, String last) {
		// int id = 1;
		// String firstname = "Contact's first name";
		// String lastname = "Last name";
		// String number = "000 000 000";
		// String photo_uri =
		// "android.resource://com.my.package/drawable/default_photo";

		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

		// Name
		Builder builder = ContentProviderOperation
				.newUpdate(ContactsContract.Data.CONTENT_URI);
		builder.withSelection(
				ContactsContract.Data.CONTACT_ID + "=?" + " AND "
						+ ContactsContract.Data.MIMETYPE + "=?",
				new String[] {
						id,
						ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE });
		// builder.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
		// lastname);
		builder.withValue(
				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
				firstname + " " + last);
		ops.add(builder.build());

		// Number
		// builder =
		// ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
		// builder.withSelection(ContactsContract.Data.CONTACT_ID + "=?" +
		// " AND " + ContactsContract.Data.MIMETYPE + "=?"+ " AND " +
		// ContactsContract.CommonDataKinds.Organization.TYPE + "=?", new
		// String[]{id,
		// ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
		// String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)});
		// builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
		// number);
		// ops.add(builder.build());

		// Update
		try {
			getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			mString_phone_no = mString_phone_no.replace(" ", "")
					.replace("(", "").replace(")", "").replace("-", "");
			mCursor = mDatasourceHandler.FetchContact(mStringPhoneId);
			Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {

			do {

				mStringProfile = mCursor.getString(7).trim();

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		Log.e("mStringProfile: ", "" + mStringProfile);
		if (mCursor.getCount() == 0) {

		} else if (mStringProfile.equals("Null")) {

		} else {
			Bitmap mBitmap = ConvertToImage(mStringProfile);
			mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
			mImageViewProfile.setOval(true);
			mImageViewProfile.setImageBitmap(mBitmap);
		}
		mCursor = mDatasourceHandler.User_Set_Fetch(mStringPhoneId);
		if (mCursor.getCount() != 0) {

			do {

				setPromo = mCursor.getString(0).trim();
				setAutorotate = mCursor.getString(1).trim();
				setSponsoredads = mCursor.getString(2).trim();
				setpersonalads = mCursor.getString(3).trim();
				setuniqueid = mCursor.getString(4).trim();

			} while (mCursor.moveToNext());

			mCursor.close();
			if (setPromo.equals("true")) {
				switch_promo.setChecked(true);
			} else {
				mFrameLayoutAuto.setVisibility(View.GONE);

				mFrameLayoutPersonal.setVisibility(View.GONE);
				mFrameLayoutSponsored.setVisibility(View.GONE);
			}
			if (setAutorotate.equals("true")) {
				switch_auto_promo.setChecked(true);
			}
			if (setSponsoredads.equals("true")) {
				checkbox_sponsored.setChecked(true);
			}
			if (setpersonalads.equals("true")) {
				checkbox_personal.setChecked(true);
			}
		}

	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);
			// return BitmapFactory.decodeByteArray(imageAsBytes, 0,
			// imageAsBytes.length);
			// BaseConvert.decode(image.getBytes());
			// InputStream stream = new ByteArrayInputStream(image.getBytes());
			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);
			// Log.e("Ben", "Image Converted");
			// BitmapFactory.Options bitmapOptions = new
			// BitmapFactory.Options();
			// bitmapOptions.inSampleSize = 4;
			// // //
			// Bitmap thumbnail = BitmapFactory
			// .decodeFile(bitmap.toString(), bitmapOptions);
			//
			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	protected boolean validation(String email) {
		// TODO Auto-generated method stub
		String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		// if (email.equals("")) {
		// mEditTextEmail.setText("No Email");
		// return true;
		// }
		// if (email.equals("No Email")) {
		// return true;
		// }
		if (email.length() > 0) {
			if (!email.matches(emailPattern)) {
				LogMessage.showDialog(EditScreenImgr.this, null,
						"Please enter valid email.", null, "OK");
				return false;
			}
			// mEditTextEmail.setText("No Email");
			return true;
		}

		return true;
	}

}
