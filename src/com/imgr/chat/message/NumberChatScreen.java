package com.imgr.chat.message;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.ChatStateListener;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.BaseConvert;
import com.imgr.chat.R;
import com.imgr.chat.adapter.ChatAdapter;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.cropping.CropOption;
import com.imgr.chat.cropping.CropOptionAdapter;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.setting.SeeSponsoredandPersonal;
import com.imgr.chat.util.ConnectionLost;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;
import com.imgr.chat.xmpp.chatstate;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class NumberChatScreen extends Activity {
	AlertDialog pinDialog;
	boolean adv_on_off = false;
	SharedPreferences sharedPreferences;
	Editor editor;
	String mStringPhone, mStringusername, mStringPassword, mStringHeader,
			mStringFooter;
	String s;
	// xmpp objects
	ConnectivityManager connectivity_Manager;
	ConnectionConfiguration config;
	Activity activity;
	ConnectionDetector mConnectionDetector;
	XMPPConnection connection;
	Message message;
	Roster roster;
	ArrayList<String> mArrayDATE;
	// database
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;

	String fromJID = null, toJID = null;
	String RECENTFROMJID = null, RECENTTOJID = null;

	// camera and gallery
	private static final int CROP_FROM_CAMERA = 3;
	private Uri mImageCaptureUri;
	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	// private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int PICK_FROM_CAMERA = 1;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "ImgrChat";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;

	JSONObject personaljsonObj;
	JSONArray mArrayPersonal = null;
	ImageView send_image, image_pro, adv_delete_icon, adv_image;

	EditText input;
	TextView mTextViewBack, text_adv_changed, mTextViewAdd;
	ImageButton mImageButton;
	Button send;
	ListView lv;
	ChatAdapter chat_list_adapter;
	TextView mTextViewTitle;
	String inputTypetext, NamedeviceToken, JID, mString_name, mStringPhoneid,
			mStringNochangename, number, getfriend, mStringBase64, getfrdsname,
			personal_api_time, personal_api_pushkey, personal_api_idtag,
			personal_jidto, personal_jidfrom, testpromo_id, mString_Override,
			mStringOnlyOneSelectPromoId = "0", promoid, promo_image,
			promo_name;
	String messageSend;

	ArrayList<String> mArrayList_ToJid;
	ArrayList<String> mArrayList_FromJid;
	ArrayList<String> mArrayList_Time;
	ArrayList<String> mArrayList_Message;
	ArrayList<String> mArrayList_uniqueid;
	ArrayList<String> mArrayList_receive;
	ArrayList<String> mArrayList_promo_not;

	private Handler mHandler = new Handler();
	ArrayList<String[]> messages = new ArrayList<String[]>();
	ImageLoader imageLoader;
	DisplayImageOptions options;

	// for push notification parameter
	String to_notification, from_notification, messgae_notification,
			fromjid_notification, tojid_notification, time_notification,
			timestamp_notification, date_notification,
			localdatabase_notification, messgaepacketid_notification,
			memberid_notification, contacyuniqueid_notification,
			receiveornot_notification, bubblecolor_notification,
			fontsize_notification, username_notification,
			profileimage_notification, recentfromjid_notification,
			recenttojid_notification, promo_id_notification,
			notification_on_or_off;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_numberchatscreen);

		// xmpp
		activity = this;
		mConnectionDetector = new ConnectionDetector(this);
		config = new ConnectionConfiguration("64.235.48.26", 5222);
		connection = new XMPPConnection(config);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setReconnectionAllowed(true);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		config.setSecurityMode(SecurityMode.enabled);
		configure(ProviderManager.getInstance());
		config.setDebuggerEnabled(true);
		connection.DEBUG_ENABLED = true;
		config.setReconnectionAllowed(true);
		// database
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mArrayDATE = new ArrayList<String>();
		// chatting send images
		send_image = (ImageView) findViewById(R.id.send_image);
		adv_delete_icon = (ImageView) findViewById(R.id.adv_delete_icon);
		adv_image = (ImageView) findViewById(R.id.adv_image);
		text_adv_changed = (TextView) findViewById(R.id.text_adv_changed);
		mImageButton = (ImageButton) findViewById(R.id.img_speak);
		mTextViewAdd = (TextView) findViewById(R.id.add_plus);

		// imageloader
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).cacheInMemory()
				.cacheOnDisc().build();

		adv_delete_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				adv_on_off = false;
				adv_delete_icon.setVisibility(View.GONE);
				text_adv_changed.setVisibility(View.GONE);
				adv_image.setVisibility(View.GONE);
				mStringOnlyOneSelectPromoId = "0";
			}
		});
		send_image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mConnectionDetector.isConnectingToInternet()) {
					selectImage();
				} else {
					LogMessage.showDialog(NumberChatScreen.this, null,
							"No Internet Connection", null, "Ok");
				}
			}
		});
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mString_Override = sharedPreferences.getString(
				Constant.BUBBLE_SET_OVERRIDE_FIXED, "");
		mStringPhone = sharedPreferences.getString(Constant.PHONENO, "");
		notification_on_or_off = sharedPreferences.getString(
				Constant.SETTING_NOTIFICATION_ON_OFF, "");
		input = (EditText) findViewById(R.id.input);
		send = (Button) findViewById(R.id.send_clicked);
		lv = (ListView) findViewById(R.id.listview1);
		image_pro = (ImageView) findViewById(R.id.image_pro);
		mTextViewTitle = (TextView) findViewById(R.id.header_title);
		lv.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();

		setConnection(connection);
		mString_name = getIntent().getStringExtra("Chat_name_select");
		mStringPhoneid = getIntent().getStringExtra("phone_id");
		mStringNochangename = getIntent().getStringExtra("Chat_name_select");
		number = getIntent().getStringExtra("Chat_no_select");
		getfriend = getIntent().getStringExtra("Chat_name_select");
		mTextViewTitle.setText(getfriend.split("\\@")[0]);
		getfrdsname = mTextViewTitle.getText().toString();
		if (number.length() >= 10) {
			number = number.substring(number.length() - 10);
		} else {

		}

		image_pro.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showInputDialog_Done();

			}
		});
		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mConnectionDetector.isConnectingToInternet()) {
					JID = NamedeviceToken;

					String text = String.valueOf(input.getText().toString());
					inputTypetext = input.getText().toString();
					messgae_notification = input.getText().toString();

					if (connection.isConnected()) {
						if (mStringBase64 != null) {
							if (text.matches("")) {

								//
								JID = NamedeviceToken;// +

								Message msg = new Message(JID,
										Message.Type.chat);
								ChatManager chatManager = connection
										.getChatManager();
								ChatStateListener chatstat = new chatstate();
								Chat chat = chatManager.createChat(JID,
										chatstat);
								String time = currenttime();
								JSONObject json = new JSONObject();
								try {
									// //
									json.put("server_URL", "Not Uploaded");
									json.put("pushkey", "image");
									json.put("preview_Icon", mStringBase64);
									// //
								} catch (Exception e) {
									// //
								}
								// //
								msg.setBody(json.toString());
								if (connection != null) {
									// //
									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// //
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									// //
									roster = connection.getRoster();
									Presence availability = roster
											.getPresence(JID);

									if (adv_on_off) {
										String[] msg_Data = new String[6];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "1";
										messages.add(msg_Data);
									} else {
										String[] msg_Data = new String[6];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "0";
										messages.add(msg_Data);
									}

									// Log.e("COUNT:",
									// ""
									// + chat_list_adapter
									// .getCount());
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
									// //
									JSONObject objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();
									// //
									mJsonArray.put(json);
									// //
									// // // Finally add item arrays for "A"
									// and
									// // // "B" to
									// // // main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// // // TODO Auto-generated catch
										// block
										// // e.printStackTrace();
									}
									// //
									// // Log.e("CAMERA", "calling");
									if (adv_on_off) {
										String date = date();
										String timestamp = timestamp();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "1", date);
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING1 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										adv_delete_icon
												.setVisibility(View.GONE);
										text_adv_changed
												.setVisibility(View.GONE);
										adv_image.setVisibility(View.GONE);

									} else {
										String date = date();
										String timestamp = timestamp();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "0", date);
										// mTextViewChanged.setText("Off");
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING2 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										adv_delete_icon
												.setVisibility(View.GONE);
										text_adv_changed
												.setVisibility(View.GONE);
										adv_image.setVisibility(View.GONE);

									}

									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();
									// //
									mStringBase64 = null;
									send_image.setImageBitmap(null);
									send_image
											.setBackgroundResource(R.drawable.ic_camerachat);
									input.setText("");

								}

							} else {

								JID = NamedeviceToken;// +
								// Constant.XMPP_DNS_Name;

								// Log.e("SIMPLE TEXT MESSAGE JID: ", "" +
								// JID);

								Message msg = new Message(JID,
										Message.Type.chat);
								ChatManager chatManager = connection
										.getChatManager();
								ChatStateListener chatstat = new chatstate();
								Chat chat = chatManager.createChat(JID,
										chatstat);
								String time = currenttime();
								JSONObject json = new JSONObject();
								try {
									// //
									json.put("server_URL", "Not Uploaded");
									json.put("pushkey", "image");
									json.put("preview_Icon", mStringBase64);
									// //
								} catch (Exception e) {
									// //
								}
								// //
								msg.setBody(json.toString());
								if (connection != null) {
									// //
									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// //
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									// //
									roster = connection.getRoster();
									Presence availability = roster
											.getPresence(JID);
									if (adv_on_off) {
										String[] msg_Data = new String[6];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "1";
										messages.add(msg_Data);
									} else {
										String[] msg_Data = new String[6];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "0";
										messages.add(msg_Data);
									}

									// Log.e("COUNT:",
									// ""
									// + chat_list_adapter
									// .getCount());
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
									// //
									JSONObject objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();
									// //
									mJsonArray.put(json);
									// //
									// // // Finally add item arrays for "A"
									// and
									// // // "B" to
									// // // main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// // // TODO Auto-generated catch
										// block
										// // e.printStackTrace();
									}
									// //
									// // Log.e("CAMERA", "calling");
									if (adv_on_off) {
										String date = date();
										mDatasourceHandler.insertChathistory(
												message.getPacketID(),
												"username", toJID, fromJID,
												objMainList.toString(), time,
												"1", "0", date);
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING3 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = message
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										adv_delete_icon
												.setVisibility(View.GONE);
										text_adv_changed
												.setVisibility(View.GONE);
										adv_image.setVisibility(View.GONE);

										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "", getfrdsname,
										// date, time, timestamp);
									} else {
										String date = date();
										mDatasourceHandler.insertChathistory(
												message.getPacketID(),
												"username", toJID, fromJID,
												objMainList.toString(), time,
												"0", "0", date);

										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING4 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = message
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "", getfrdsname,
										// date, time, timestamp);
									}

									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();
									// //
									mStringBase64 = null;
									send_image.setImageBitmap(null);
									send_image
											.setBackgroundResource(R.drawable.ic_camerachat);
									input.setText("");
									// // Log.e("message sent to ", JID
									// // + "successfull");
									// //
									// Toast.makeText(getApplicationContext(),
									// // "Message Sent !",
									// // Toast.LENGTH_SHORT).show();
								}
								// text//

								// Log.e("connection.isConnected(): ", ""
								// + connection.isConnected());

								// Log.e("connection.isConnected(): ", ""
								// + connection.isConnected());
								JID = NamedeviceToken;// +
								// Log.e("SIMPLE TEXT MESSAGE JID: ", "" +
								// JID);

								// Log.e("Sending text ", "" + text + " to "
								// + JID);
								Message msg_ = new Message(JID,
										Message.Type.chat);
								ChatManager chat_Manager = connection
										.getChatManager();
								ChatStateListener chat_stat = new chatstate();
								Chat chat_ = chat_Manager.createChat(JID,
										chat_stat);
								JSONObject objMainList = new JSONObject();
								JSONArray mJsonArray = null;
								mJsonArray = new JSONArray();

								JSONObject json1 = new JSONObject();

								if (adv_on_off) {
									try {

										json1.put("pushkey", text);
										json1.put("_message_body_tag", text);
										json1.put("_promo_id_tag", promoid);
										json1.put("preview_Icon", "null");

									} catch (Exception e) {

									}
								} else {
									try {

										json1.put("pushkey", text);
										json1.put("_message_body_tag", text);
										json1.put("_promo_id_tag", "0");
										json1.put("preview_Icon", "null");

									} catch (Exception e) {

									}
								}

								// Log.e("msg.getPacketID(): ",
								// "" + msg.getPacketID());
								// Log.e("msg.getXmlns(): ",
								// "" + msg.getXmlns());

								// Log.e("msg.bodies: ", "" +
								// msg.getBodies());
								// Log.e("message send:  ",
								// "" + json.toString());
								messageSend = json1.toString();
								// Log.e("messageSend---->>", "" +
								// messageSend);
								msg_.setBody(messageSend);

								String time_ = currenttime();

								if (connection != null) {
									// Log.e("connection",
									// "" + connection.getHost());

									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// Log.e("msg", "" + msg);
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									roster = connection.getRoster();
									// Log.e("JID---", "" + JID);
									Presence availability = roster
											.getPresence(JID);

									if (text != null && !text.matches("\\s+")
											&& !text.matches("")) {

										if (adv_on_off) {
											String[] msg_Data = new String[8];
											msg_Data[0] = text;
											msg_Data[1] = "my";
											msg_Data[2] = "0";
											msg_Data[3] = msg.getPacketID();
											msg_Data[4] = "" + promo_image;
											msg_Data[6] = time_;
											msg_Data[5] = "1";
											msg_Data[7] = promoid;
											messages.add(msg_Data);
										} else {
											String[] msg_Data = new String[7];
											msg_Data[0] = text;
											msg_Data[1] = "my";
											msg_Data[2] = "0";
											msg_Data[3] = msg.getPacketID();
											msg_Data[4] = "" + promo_image;
											msg_Data[6] = time_;
											msg_Data[5] = "0";
											messages.add(msg_Data);
										}

										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();
										lv.setAdapter(chat_list_adapter);

									}
									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();

									input.setText("");
									// Log.e("message sent to ", JID
									// + "successfull");
									Toast.makeText(getApplicationContext(),
											"Message Sent !",
											Toast.LENGTH_SHORT).show();

									if (adv_on_off) {
										adv_delete_icon
												.setVisibility(View.GONE);
										text_adv_changed
												.setVisibility(View.GONE);
										adv_image.setVisibility(View.GONE);
										adv_on_off = false;
									}

									else {

									}

									mJsonArray.put(json);

									// Finally add item arrays for "A" and
									// "B"
									// to
									// main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									// Log.e("Calling time objMainList: ",
									// ""
									// + objMainList.toString());
									// /Log.e("adv_on_off: ", "" +
									// adv_on_off);

									if (adv_on_off) {
										String date = date();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"1", "0", date);
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING5 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "",
										// getfrdsname,
										// date, time, timestamp);
										adv_delete_icon
												.setVisibility(View.GONE);
										text_adv_changed
												.setVisibility(View.GONE);
										adv_image.setVisibility(View.GONE);
									} else {
										String date = date();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "0", date);
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING6 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "",
										// getfrdsname,
										// date, time, timestamp);

										if (adv_on_off) {
											adv_delete_icon
													.setVisibility(View.GONE);
											text_adv_changed
													.setVisibility(View.GONE);
											adv_image.setVisibility(View.GONE);
											mStringOnlyOneSelectPromoId = "0";
											adv_on_off = false;
										}

										else {

										}

									}

									// retrieveState_mode(
									// availability.getMode(),
									// availability.isAvailable(),
									// objMainList.toString());
								}

								// retrieveState_mode(
								// availability.getMode(),
								// availability.isAvailable(),
								// objMainList.toString());

								// mStringBase64 = null;

							}
						} else {

							mStringBase64 = null;
							if (text.matches("")) {
								LogMessage.showDialog(NumberChatScreen.this,
										null, "Please enter the message", null,
										"Ok");
							} else {

								// Log.e("connection.isConnected(): ", ""
								// + connection.isConnected());
								JID = NamedeviceToken;// +
								// Log.e("SIMPLE TEXT MESSAGE JID: ", "" +
								// JID);

								// Log.e("Sending text ", "" + text + " to "
								// + JID);
								Message msg = new Message(JID,
										Message.Type.chat);
								ChatManager chatManager = connection
										.getChatManager();
								ChatStateListener chatstat = new chatstate();
								Chat chat = chatManager.createChat(JID,
										chatstat);
								JSONObject objMainList = new JSONObject();
								JSONArray mJsonArray = null;
								mJsonArray = new JSONArray();

								JSONObject json = new JSONObject();

								if (adv_on_off) {
									try {

										json.put("pushkey", text);
										json.put("_message_body_tag", text);
										json.put("_promo_id_tag", promoid);
										json.put("preview_Icon", "null");

									} catch (Exception e) {

									}
								} else {
									try {

										json.put("pushkey", text);
										json.put("_message_body_tag", text);
										json.put("_promo_id_tag", "0");
										json.put("preview_Icon", "null");

									} catch (Exception e) {

									}
								}

								// Log.e("msg.getPacketID(): ",
								// "" + msg.getPacketID());
								// Log.e("msg.getXmlns(): ",
								// "" + msg.getXmlns());

								// Log.e("msg.bodies: ", "" +
								// msg.getBodies());
								// Log.e("message send:  ",
								// "" + json.toString());
								messageSend = json.toString();
								// Log.e("messageSend---->>", "" +
								// messageSend);
								msg.setBody(messageSend);

								String time = currenttime();

								if (connection != null) {
									// Log.e("connection",
									// "" + connection.getHost());

									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// Log.e("msg", "" + msg);
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									roster = connection.getRoster();
									// Log.e("JID---", "" + JID);
									Presence availability = roster
											.getPresence(JID);

									if (text != null && !text.matches("\\s+")
											&& !text.matches("")) {
										String date_sent = date();
										if (adv_on_off) {

											String[] msg_Data = new String[9];
											msg_Data[0] = text;
											msg_Data[1] = "my";
											msg_Data[2] = "0";
											msg_Data[3] = msg.getPacketID();
											msg_Data[4] = "" + promo_image;
											msg_Data[6] = time;
											msg_Data[5] = "1";
											msg_Data[7] = promoid;
											msg_Data[8] = date_sent;
											messages.add(msg_Data);
										} else {
											String[] msg_Data = new String[9];
											msg_Data[0] = text;
											msg_Data[1] = "my";
											msg_Data[2] = "0";
											msg_Data[3] = msg.getPacketID();
											msg_Data[4] = "" + promo_image;
											msg_Data[6] = time;
											msg_Data[5] = "0";
											msg_Data[8] = date_sent;
											messages.add(msg_Data);
										}

										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();
										lv.setAdapter(chat_list_adapter);

									}
									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();

									input.setText("");
									// Log.e("message sent to ", JID
									// + "successfull");
									Toast.makeText(getApplicationContext(),
											"Message Sent !",
											Toast.LENGTH_SHORT).show();

									if (adv_on_off) {
										adv_delete_icon
												.setVisibility(View.GONE);
										text_adv_changed
												.setVisibility(View.GONE);
										adv_image.setVisibility(View.GONE);
										mStringOnlyOneSelectPromoId = "0";
										adv_on_off = false;
									}

									else {

									}

									mJsonArray.put(json);

									// Finally add item arrays for "A" and
									// "B"
									// to
									// main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									// Log.e("Calling time objMainList: ",
									// ""
									// + objMainList.toString());
									// /Log.e("adv_on_off: ", "" +
									// adv_on_off);

									if (adv_on_off) {
										String date = date();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"1", "0", date);
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING9 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "",
										// getfrdsname,
										// date, time, timestamp);

									} else {
										String date = date();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "0", date);
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING10 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "",
										// getfrdsname,
										// date, time, timestamp);

										if (adv_on_off) {
											adv_delete_icon
													.setVisibility(View.GONE);
											text_adv_changed
													.setVisibility(View.GONE);
											adv_image.setVisibility(View.GONE);
											mStringOnlyOneSelectPromoId = "0";
											adv_on_off = false;
										}

										else {

										}

									}

									// retrieveState_mode(
									// availability.getMode(),
									// availability.isAvailable(),
									// objMainList.toString());
								}

							}

						}

					} else {

						LogMessage.showDialog(NumberChatScreen.this,
								"Waiting for Newtwork",
								"Please check your internet connection.", null,
								"Ok");

					}

				} else {
					LogMessage.showDialog(NumberChatScreen.this,
							"Waiting for Network",
							"Please Check your internet connection.", null,
							"Ok");
				}
				//
			}
		});

		mArrayList_ToJid = new ArrayList<String>();
		mArrayList_FromJid = new ArrayList<String>();
		mArrayList_Time = new ArrayList<String>();
		mArrayList_Message = new ArrayList<String>();
		mArrayList_uniqueid = new ArrayList<String>();
		mArrayList_promo_not = new ArrayList<String>();
		mArrayList_receive = new ArrayList<String>();

		mArrayList_ToJid.clear();
		mArrayList_FromJid.clear();
		mArrayList_Time.clear();
		mArrayList_Message.clear();
		mArrayList_uniqueid.clear();
		mArrayList_promo_not.clear();
		mArrayList_receive.clear();

		if (mConnectionDetector.isConnectingToInternet()) {
			new ChatHistory().execute();
		} else {
			LogMessage.showDialog(NumberChatScreen.this, null,
					"No Internet Connection", null, "Ok");
		}

		mTextViewBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mTextViewBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				s = "TT";
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		mImageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_info = new Intent(NumberChatScreen.this,
						SeeSponsoredandPersonal.class);

				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		mTextViewAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_info = new Intent(NumberChatScreen.this,
						SeeSponsoredandPersonal.class);

				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		username_notification = mTextViewTitle.getText().toString();
		from_notification = mStringPhone;
		to_notification = number;

	}

	public void testpromodata(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();
			imageLoader.displayImage(promo_image, adv_image, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									activity, R.anim.fade_in);

						}
					});
			text_adv_changed.setText(promo_name);

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (Constant.NAME_CHANGE_COUNT == 1) {
			Constant.NAME_CHANGE_COUNT = 0;
			mTextViewTitle.setText(Constant.NAME_CHANGE);
			Constant.NAME_CHANGE = "";

		}

		if (Constant.PROMO_OFF_NOW_SELECT_ONE == 1) {
			adv_on_off = true;
			Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
			mStringOnlyOneSelectPromoId = Constant._ID;
			Log.e("mStringOnlyOneSelectPromoId: ", ""
					+ mStringOnlyOneSelectPromoId);

			adv_delete_icon.setVisibility(View.VISIBLE);
			text_adv_changed.setVisibility(View.VISIBLE);
			adv_image.setVisibility(View.VISIBLE);
			testpromodata(mStringOnlyOneSelectPromoId);
			Constant._ID = "0";
		} else {
			adv_on_off = false;
			Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
		}

		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();
		PingManager.getInstanceFor(connection);

		DeliveryReceiptManager delievery_Manager = DeliveryReceiptManager
				.getInstanceFor(connection);
		delievery_Manager.setAutoReceiptsEnabled(true);
		delievery_Manager.enableAutoReceipts();

		delievery_Manager
				.addReceiptReceivedListener(new ReceiptReceivedListener() {
					@Override
					public void onReceiptReceived(String fromJid, String toJid,
							String receiptId) {
						// Log.e("Chat_Screen message sent confirmation:",
						// fromJid
						// + ", " + toJid + ", " + receiptId);
						loop: for (String[] msg_Data : messages) {
							if (msg_Data[3].equals(receiptId)) {
								msg_Data[2] = "1";
								boolean flag = mDatasourceHandler
										.UpdateReceive(receiptId);
								// Log.e("Pinging", "on Resumeflag:  " + flag);
								// update query implement--------------
								// Log.v("Delivery", "" + msg_Data[2]);
								mHandler.post(new Runnable() {
									public void run() {
										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();

									}
								});
								break loop;
							}

						}

					}

				});

	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			// s.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			// /Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public String countcheck(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public String historypromo_data(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = mCursor.getString(4).trim();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "https://imgrapp.com/release/uploads/promos/prosper-la.png";
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("dd-MMM/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public void setConnection(final XMPPConnection connection) {

		this.connection = connection;
		try {

			if (connection != null) {

				// Add a packet listener to get messages sent to us
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {

					@SuppressWarnings("unused")
					public void processPacket(Packet packet) {

						message = (Message) packet;

						String promoID = null;
						String mStringnewtojid = null;
						String mStringnewfromjid = null;
						JSONObject objMainList = null;
						String check = null;
						String time = currenttime();
						if (message.getBody() != null) {
							String fromName = StringUtils
									.parseBareAddress(message.getFrom().split(
											"\\@")[0]);
							mStringnewtojid = fromJID;
							mStringnewfromjid = fromName;

							s = NamedeviceToken.replace(Constant.XMPP_DNS_Name,
									"");
							if (fromName.equals(s)) {
								// String Body = null;
								try {
									JSONObject jsonObj = new JSONObject(message
											.getBody());

									objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();

									JSONObject json = new JSONObject();

									if (jsonObj.has("_message_body_tag")) {
										personaljsonObj = jsonObj;

										promoID = jsonObj
												.getString("_promo_id_tag");
										String date_sent = date();
										check = countcheck(promoID);
										Log.e("promoID: ", "" + promoID);
										if (promoID != null) {
											Log.e("NOT NULLpromoID: ", ""
													+ promoID);
											if (promoID.equals("0")) {
												// boolean
												// flags=ExecuteNewPromo(promoID)
												String path = historypromo_data(promoID);
												String[] msg_Data = new String[9];
												msg_Data[0] = "";
												msg_Data[4] = time;
												msg_Data[1] = "friend";
												msg_Data[2] = "0";
												msg_Data[3] = jsonObj
														.getString("pushkey");
												msg_Data[5] = jsonObj
														.getString("_promo_id_tag");
												msg_Data[6] = path;
												msg_Data[7] = promoID;
												msg_Data[8] = date_sent;
												messages.add(msg_Data);

												try {

													json.put(
															"pushkey",
															jsonObj.getString("pushkey"));
													json.put(
															"_message_body_tag",
															jsonObj.getString("_message_body_tag"));
													json.put(
															"_promo_id_tag",
															jsonObj.getString("_promo_id_tag"));
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}

												mJsonArray.put(json);

												try {
													objMainList.put("body",
															mJsonArray);
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												//
											} else {

												if (check != null) {
													if (check.equals("0")) {
														Log.e("API CALLING: ",
																""
																		+ promoID
																		+ "---check==="
																		+ check);
														personal_api_time = time;
														personal_api_pushkey = jsonObj
																.getString("pushkey");
														personal_api_idtag = jsonObj
																.getString("_promo_id_tag");
														personal_jidto = mStringnewtojid;
														personal_jidfrom = mStringnewfromjid;
														Log.e("personal_api_time: ",
																""
																		+ personal_api_time);
														Log.e("personal_api_pushkey: ",
																""
																		+ personal_api_pushkey);
														Log.e("personal_api_idtag: ",
																""
																		+ personal_api_idtag);
														Log.e("personal_jidto: ",
																""
																		+ personal_jidto);
														Log.e("personal_jidfrom: ",
																""
																		+ personal_jidfrom);
														testpromo_id = promoID;
														new ExecuteNewPromo()
																.execute();
														return;
													} else {
														Log.e("NOTAPI CALLING: ",
																""
																		+ promoID
																		+ "---check==="
																		+ check);
														String path = historypromo_data(promoID);
														String[] msg_Data = new String[9];
														msg_Data[0] = "";
														msg_Data[4] = time;
														msg_Data[1] = "friend";
														msg_Data[2] = "0";
														msg_Data[3] = jsonObj
																.getString("pushkey");
														msg_Data[5] = jsonObj
																.getString("_promo_id_tag");
														msg_Data[6] = path;
														msg_Data[7] = promoID;
														msg_Data[8] = date_sent;
														messages.add(msg_Data);

														try {

															json.put(
																	"pushkey",
																	jsonObj.getString("pushkey"));
															json.put(
																	"_message_body_tag",
																	jsonObj.getString("_message_body_tag"));
															json.put(
																	"_promo_id_tag",
																	jsonObj.getString("_promo_id_tag"));
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}

														mJsonArray.put(json);

														try {
															objMainList.put(
																	"body",
																	mJsonArray);
														} catch (JSONException e) {
															// TODO
															// Auto-generated
															// catch
															// block
															e.printStackTrace();
														}
													}
												}

											}
										} else {

											String path = historypromo_data(promoID);
											String[] msg_Data = new String[9];
											msg_Data[0] = "";
											msg_Data[4] = time;
											msg_Data[1] = "friend";
											msg_Data[2] = "0";
											msg_Data[3] = jsonObj
													.getString("pushkey");
											msg_Data[5] = jsonObj
													.getString("_promo_id_tag");
											msg_Data[6] = path;
											msg_Data[7] = promoID;
											msg_Data[8] = date_sent;
											messages.add(msg_Data);

											try {

												json.put("pushkey", jsonObj
														.getString("pushkey"));
												json.put(
														"_message_body_tag",
														jsonObj.getString("_message_body_tag"));
												json.put(
														"_promo_id_tag",
														jsonObj.getString("_promo_id_tag"));
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}

											mJsonArray.put(json);

											try {
												objMainList.put("body",
														mJsonArray);
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											//
										}

									} else {

										String[] msg_Data = new String[5];
										msg_Data[0] = "";
										msg_Data[4] = time;
										msg_Data[1] = "friend_pic";
										msg_Data[2] = "0";
										msg_Data[3] = jsonObj
												.getString("preview_Icon");

										messages.add(msg_Data);

										promoID = "0";

										try {

											json.put("pushkey", "image");

											json.put("_promo_id_tag", promoID);
											json.put("preview_Icon", jsonObj
													.getString("preview_Icon"));

										} catch (Exception e) {

										}
										//
										// Log.e("message send:  ",
										// "" + json.toString());

										mJsonArray.put(json);

										// Finally add item arrays for "A" and
										// "B"
										// to
										// main list with key
										try {
											objMainList.put("body", mJsonArray);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										// Log.e("objMainList: ",
										// "" + objMainList.toString());

										// mStringnewtojid = fromJID;
										// mStringnewfromjid = fromName;

									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								Log.e("mStringnewtojid: ", "" + mStringnewtojid);
								Log.e("mStringnewfromjid: ", ""
										+ mStringnewfromjid);
								// personal_jidto = mStringnewtojid;
								// personal_jidfrom = mStringnewfromjid;
								Log.e("SEEEEEEEEEEEEEEE", "SEEEEEEEEEEEEEEEEE");
								String date = date();
								mDatasourceHandler.insertChathistory(
										message.getPacketID(), "username",
										mStringnewtojid, mStringnewfromjid,
										objMainList.toString(), time, promoID,
										"0", date);
								String timestamp = timestamp();
								boolean flag = mDatasourceHandler
										.UpdateRecentReceive(mStringnewtojid,
												mStringnewfromjid,
												objMainList.toString(), time,
												date, timestamp, "0");
								Log.e("RECEIVE 1 flag:", "" + flag);
								if (!flag) {
									mDatasourceHandler.insertrecentMessage(
											objMainList.toString(),
											mStringnewfromjid, mStringnewtojid,
											"", getfrdsname, date, time,
											timestamp, "0");
								}

								mStringnewtojid = null;
								mStringnewfromjid = null;
								// Add the incoming message to the list view
								mHandler.post(new Runnable() {
									public void run() {
										// Log.e("Set connection COUNT:", ""
										// + chat_list_adapter.getCount());
										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();
										lv.setAdapter(chat_list_adapter);
									}
								});

							}
							//
							else {
								Log.e("", "CHECK");
								promoID = null;
								try {
									JSONObject jsonObj = new JSONObject(message
											.getBody());

									objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();

									JSONObject json = new JSONObject();
									Log.e("jsonObj: ", "" + jsonObj);

									if (jsonObj.has("_message_body_tag")) {
										Log.e(" ", "MESSAGE");
										promoID = jsonObj
												.getString("_promo_id_tag");
										if (promoID != null) {
											Log.e(" promoID", "promoID: "
													+ promoID);
											if (promoID.equals("0")) {
												Log.e(" promoID 0", "promoID 0");
												try {

													json.put(
															"pushkey",
															jsonObj.getString("pushkey"));
													json.put(
															"_message_body_tag",
															jsonObj.getString("_message_body_tag"));
													json.put(
															"_promo_id_tag",
															jsonObj.getString("_promo_id_tag"));
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}

												mJsonArray.put(json);

												try {
													objMainList.put("body",
															mJsonArray);
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												//

											}

											else {
												check = countcheck(promoID);
												// api calling
												Log.e("api calling: ", ""
														+ check);
												if (check != null) {
													if (check.equals("0")) {
														personal_api_time = time;
														personal_api_pushkey = jsonObj
																.getString("pushkey");
														personal_api_idtag = jsonObj
																.getString("_promo_id_tag");
														personal_jidto = mStringnewtojid;
														personal_jidfrom = mStringnewfromjid;
														new Execute_New_Promo()
																.execute(promoID);
														return;
													} else {
														try {

															json.put(
																	"pushkey",
																	jsonObj.getString("pushkey"));
															json.put(
																	"_message_body_tag",
																	jsonObj.getString("_message_body_tag"));
															json.put(
																	"_promo_id_tag",
																	jsonObj.getString("_promo_id_tag"));
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}

														mJsonArray.put(json);

														try {
															objMainList.put(
																	"body",
																	mJsonArray);
														} catch (JSONException e) {
															// TODO
															// Auto-generated
															// catch
															// block
															e.printStackTrace();
														}
													}
												}

											}

										} else {
											try {

												json.put("pushkey", jsonObj
														.getString("pushkey"));
												json.put(
														"_message_body_tag",
														jsonObj.getString("_message_body_tag"));
												json.put(
														"_promo_id_tag",
														jsonObj.getString("_promo_id_tag"));
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}

											mJsonArray.put(json);

											try {
												objMainList.put("body",
														mJsonArray);
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											//

										}
										Log.e("DATA objMainList: ", ""
												+ objMainList.toString());

									} else {
										// image show
										try {

											json.put("pushkey", "image");

											json.put("_promo_id_tag", "");
											json.put("preview_Icon", jsonObj
													.getString("preview_Icon"));
											promoID = "0";

										} catch (Exception e) {

										}

										mJsonArray.put(json);

										try {
											objMainList.put("body", mJsonArray);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								Log.e("ELSE", "NOTHING");
								Log.e("mStringnewtojid: ", "" + mStringnewtojid);
								Log.e("mStringnewfromjid: ", ""
										+ mStringnewfromjid);
								Log.e("objMainList.toString(): ", ""
										+ objMainList.toString());
								String date = date();
								String timestamp = timestamp();

								int history_count = mDatasourceHandler
										.HISTORYUpdateRecentReceiveCount(message
												.getPacketID());
								if (history_count == 0) {
									int countincrease = 0;
									countincrease = mDatasourceHandler
											.UpdateRecentReceiveCount(
													mStringnewtojid,
													mStringnewfromjid,
													objMainList.toString(),
													time, date, timestamp);
									String mString = "" + (countincrease + 1);

									boolean flag = mDatasourceHandler
											.UpdateRecentReceive(
													mStringnewtojid,
													mStringnewfromjid,
													objMainList.toString(),
													time, date, timestamp,
													mString);
									Log.e("RECEIVE 2 flag:", "" + flag);

									if (!flag) {
										mDatasourceHandler.insertrecentMessage(
												objMainList.toString(),
												mStringnewfromjid,
												mStringnewtojid, "", "", date,
												time, timestamp, "1");
									}
								}

								mDatasourceHandler.insertChathistory(
										message.getPacketID(), "username",
										mStringnewtojid, mStringnewfromjid,
										objMainList.toString(), time, promoID,
										"1", date);

								promoID = null;
								mStringnewtojid = null;
								mStringnewfromjid = null;

							}

							// }
						}
					}
				}, filter);
			}

		} catch (Exception e) {

		}
	}

	protected void selectImage() {

		final CharSequence[] options = { "Choose from Gallery", "Take Photo",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				NumberChatScreen.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	public void configure(ProviderManager pm) {
		// Private Data Storage
		pm.addIQProvider("query", "jabber:iq:private",
				new PrivateDataManager.PrivateDataIQProvider());
		// Time
		try {
			pm.addIQProvider("query", "jabber:iq:time",
					Class.forName("org.jivesoftware.smackx.packet.Time"));
		} catch (ClassNotFoundException e) {
			// Log.w("TestClient",
			// "Can't load class for org.jivesoftware.smackx.packet.Time");
		}

		// Roster Exchange
		pm.addExtensionProvider("x", "jabber:x:roster",
				new RosterExchangeProvider());

		// Message Events
		pm.addExtensionProvider("x", "jabber:x:event",
				new MessageEventProvider());

		// Chat State
		pm.addExtensionProvider("active",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("composing",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("paused",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("inactive",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("gone",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		// XHTML
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
				new XHTMLExtensionProvider());

		// Group Chat Invitations
		pm.addExtensionProvider("x", "jabber:x:conference",
				new GroupChatInvitation.Provider());

		// Service Discovery # Items
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());

		// Service Discovery # Info
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Data Forms
		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

		// MUC User
		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
				new MUCUserProvider());

		// MUC Admin
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
				new MUCAdminProvider());

		// MUC Owner
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
				new MUCOwnerProvider());

		// SharedGroupsInfo
		pm.addIQProvider("sharedgroup",
				"http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Delayed Delivery
		pm.addExtensionProvider("x", "jabber:x:delay",
				new DelayInformationProvider());

		// Version
		try {
			pm.addIQProvider("query", "jabber:iq:version",
					Class.forName("org.jivesoftware.smackx.packet.Version"));
		} catch (ClassNotFoundException e) {
			// Not sure what's happening here.
		}

		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

		// Offline Message Requests
		pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
				new OfflineMessageRequest.Provider());

		// Offline Message Indicator
		pm.addExtensionProvider("offline",
				"http://jabber.org/protocol/offline",
				new OfflineMessageInfo.Provider());

		// Last Activity
		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

		// User Search
		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

		// JEP-33: Extended Stanza Addressing
		pm.addExtensionProvider("addresses",
				"http://jabber.org/protocol/address",
				new MultipleAddressesProvider());

		// FileTransfer
		pm.addIQProvider("si", "http://jabber.org/protocol/si",
				new StreamInitiationProvider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());

		// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
		// IBBProviders.Open());

		// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
		// IBBProviders.Close());

		// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
		// new
		// IBBProviders.Data());

		// Privacy
		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());

		pm.addIQProvider("command", "http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider());
		pm.addExtensionProvider("malformed-action",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.MalformedActionError());
		pm.addExtensionProvider("bad-locale",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadLocaleError());
		pm.addExtensionProvider("bad-payload",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadPayloadError());
		pm.addExtensionProvider("bad-sessionid",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadSessionIDError());
		pm.addExtensionProvider("session-expired",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.SessionExpiredError());

		pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
				DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
		pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
				new DeliveryReceiptRequest().getNamespace(),
				new DeliveryReceiptRequest.Provider());
		/*
		 * pm.addExtensionProvider(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE,
		 * new ReadReceipt.Provider());
		 */
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		mImageCaptureUri = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), "tmp_"
				+ String.valueOf(System.currentTimeMillis()) + ".jpg"));

		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
				mImageCaptureUri);

		try {
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PICK_FROM_CAMERA);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}

	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
				// + IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case PICK_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				doCrop();

			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				mImageCaptureUri = data.getData();

				doCrop();

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(NumberChatScreen.this,
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(NumberChatScreen.this, "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case CROP_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				Bundle extras = data.getExtras();

				if (extras != null) {
					Bitmap photo = extras.getParcelable("data");

					send_image.setImageBitmap(photo);
					mStringBase64 = convert_image(photo);
				}

				File f = new File(mImageCaptureUri.getPath());

				if (f.exists())
					f.delete();
			}
			break;
		}

	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to

		byte[] byte_arr = stream.toByteArray();

		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		if (size == 0) {
			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 400);
			intent.putExtra("outputY", 400);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			if (size == 1) {
				Intent i = new Intent(intent);
				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);

					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(
						getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);
							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							getContentResolver().delete(mImageCaptureUri, null,
									null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class ExecuteNewPromo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			Log.e("id:", "doingbackground");
			Log.e("id:", "" + testpromo_id);
			url = JsonParserConnector.getPERSONALSponsored(testpromo_id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Log.e("id:", "" + testpromo_id);
			Log.e("id:", "dfsaghfdhfghfadhsdfhdfhsghdfsghghdfshhdsghdsgh");
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			// UI.hideProgressDialog();
			PersonalList(result);
		}

	}

	private void PersonalList(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				JSONArray mArray = jsonOBject.getJSONArray("data");
				for (int i = 0; i < mArray.length(); i++) {
					JSONObject sys = mArray.getJSONObject(i);
					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					// mStringsmodified_date = sys.getString("modified_date");
					mString_link_text = sys.getString("promo_link_text");
					mString_link_promo = sys.getString("promo_link");
					mString_header_promo_message = sys
							.getString("promo_message_header");
					Log.e("mStringspromo_name: ", "" + mStringspromo_name);
					Log.e("mStringspromo_image: ", "" + mStringspromo_image);
					Log.e("mString_link_text: ", "" + mString_link_text);
					Log.e("mString_header_promo_message: ", ""
							+ mString_header_promo_message);

					boolean flag = mDatasourceHandler.insertSponsored(
							mStringspromoid, "NO_Define", mStringspromo_name,
							"https://imgrapp.com/release/"
									+ mStringspromo_image, mString_link_promo,
							mString_link_text, mString_header_promo_message,
							"0", "", "1", "1");

					Log.e("flag", "" + flag);

					JSONObject json = new JSONObject();
					mArrayPersonal = new JSONArray();
					JSONObject objMainList = new JSONObject();

					String path = historypromo_data(mStringspromoid);
					String[] msg_Data = new String[8];
					msg_Data[0] = "";
					msg_Data[4] = personal_api_time;
					msg_Data[1] = "friend";
					msg_Data[2] = "0";
					msg_Data[3] = personal_api_pushkey;
					msg_Data[5] = personal_api_idtag;
					msg_Data[6] = path;
					msg_Data[7] = mStringspromoid;
					messages.add(msg_Data);

					try {

						json.put("pushkey",
								personaljsonObj.getString("pushkey"));
						json.put("_message_body_tag",
								personaljsonObj.getString("_message_body_tag"));
						json.put("_promo_id_tag",
								personaljsonObj.getString("_promo_id_tag"));
						json.put("preview_Icon", "null");

					} catch (Exception e) {

					}

					mArrayPersonal.put(json);
					Log.e("mArrayPersonal: ", "" + mArrayPersonal);
					try {
						objMainList.put("body", mArrayPersonal);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//

					Log.e("JSON OBJECT: ", "" + objMainList.toString());
					Log.e("personal_api_time: ", "" + personal_api_time);
					Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
					Log.e("message.getPacketID(): ", "" + message.getPacketID());
					Log.e("personal_jidto: ", "" + personal_jidto);

					String date = date();

					String timestamp = timestamp();

					int history_count = mDatasourceHandler
							.HISTORYUpdateRecentReceiveCount(message
									.getPacketID());
					if (history_count == 0) {
						int countincrease = 0;
						countincrease = mDatasourceHandler
								.UpdateRecentReceiveCount(personal_jidto,
										personal_jidfrom,
										objMainList.toString(),
										personal_api_time, date, timestamp);
						String mString = "" + (countincrease + 1);

						boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
								personal_jidto, personal_jidfrom,
								objMainList.toString(), personal_api_time,
								date, timestamp, mString);
						Log.e("RECEIVE 1 flag1:", "" + flag1);
						if (!flag1) {
							mDatasourceHandler.insertrecentMessage(
									objMainList.toString(), personal_jidfrom,
									personal_jidto, "", getfrdsname, date,
									personal_api_time, timestamp, "1");
						}

					}
					mDatasourceHandler.insertChathistory(message.getPacketID(),
							"username", personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time,
							mStringspromoid, "0", date);

					// boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
					// personal_jidto, personal_jidfrom,
					// objMainList.toString(), personal_api_time, date,
					// timestamp, "0");
					// Log.e("RECEIVE 1 flag1:", "" + flag1);
					// if (!flag1) {
					// mDatasourceHandler.insertrecentMessage(
					// objMainList.toString(), personal_jidfrom,
					// personal_jidto, "", getfrdsname, date,
					// personal_api_time, timestamp, "0");
					// }

					personal_jidfrom = null;
					personal_jidto = null;
					// Add the incoming message to the list view
					mHandler.post(new Runnable() {
						public void run() {
							// Log.e("Set connection COUNT:", ""
							// + chat_list_adapter.getCount());
							if (chat_list_adapter.getCount() >= 5) {
								lv.setStackFromBottom(true);
							} else {
								lv.setStackFromBottom(false);
							}
							chat_list_adapter.notifyDataSetChanged();
							lv.setAdapter(chat_list_adapter);
						}
					});
				}

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(NumberChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			UI.hideProgressDialog();
			Personal_List(result);
		}

	}

	private void Personal_List(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.insertSponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				Log.e("flag", "" + flag);

				JSONObject json = new JSONObject();
				mArrayPersonal = new JSONArray();
				JSONObject objMainList = new JSONObject();

				try {

					json.put("pushkey", personaljsonObj.getString("pushkey"));
					json.put("_message_body_tag",
							personaljsonObj.getString("_message_body_tag"));
					json.put("_promo_id_tag",
							personaljsonObj.getString("_promo_id_tag"));
					json.put("preview_Icon", "null");

				} catch (Exception e) {

				}

				mArrayPersonal.put(json);

				try {
					objMainList.put("body", mArrayPersonal);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//

				Log.e("if RECEVIVE", "calling");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				Log.e("ELSE", "NOTHING");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				String date = date();

				String timestamp = timestamp();

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(message.getPacketID());
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, objMainList.toString(),
									personal_api_time, date, timestamp);
					String mString = "" + (countincrease + 1);

					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time, date,
							timestamp, "0");
					Log.e("RECEIVE 1 flag1:", "" + flag1);
					if (!flag1) {
						mDatasourceHandler.insertrecentMessage(
								objMainList.toString(), personal_jidfrom,
								personal_jidto, "", getfrdsname, date,
								personal_api_time, timestamp, "0");
					}

				}

				mDatasourceHandler.insertChathistory(message.getPacketID(),
						"username", personal_jidto, personal_jidfrom,
						objMainList.toString(), personal_api_time,
						mStringspromoid, "1", date);

				// promoID = null;
				personal_jidto = null;
				personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	// chat history fetch from local and server side
	public class ChatHistory extends AsyncTask<String, Void, String> {
		public ChatHistory() {
		}

		@Override
		protected String doInBackground(String... params) {
			// Log.e("ME mStringPhone: ", "" + mStringPhone);
			// Log.e("USED SELECTED number: ", "" + number);
			String result = JsonParserConnector.ChatHistory("imgr",
					mStringPhone, number);

			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			try {
				Log.e("result: ", "" + result);
				Chat_Store(result);
			} catch (Exception e) {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(NumberChatScreen.this);
		}

	}

	public void Chat_Store(String strJson) {

		try {
			// /mArrayDATE = new ArrayList<String>();
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = null;
			try {
				jsonOBject = new JSONObject(strJson);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (jsonOBject.getBoolean("success")) {
					NamedeviceToken = jsonOBject.getString("devicetoken")
							+ Constant.XMPP_DNS_Name;

					Log.e("NamedeviceToken: ", "" + NamedeviceToken);
					s = NamedeviceToken.replace(Constant.XMPP_DNS_Name, "");
					Log.e("jsonOBject.getString(): ",
							"" + jsonOBject.getString("color"));
					addEntry(NamedeviceToken);
					Roster roster = connection.getRoster();
					Presence availability = roster.getPresence(NamedeviceToken);

					Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.manual);

					// String jid=userName+Constants.ADVANTLPC__SMACK;
					try {
						roster.createEntry(NamedeviceToken, NamedeviceToken,
								null);
					} catch (XMPPException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					Log.e("Availability: ", "" + availability);
					// try {
					// card.load(connection, NamedeviceToken);
					// } catch (XMPPException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					Log.e("mString_Override: ", "" + mString_Override);
					String mStringbubblecolor = null;
					if (mString_Override != null) {
						if (mString_Override.equals("true")) {
							mStringbubblecolor = "1";
							shared_Prefernces("blue");
						} else {
							if (jsonOBject.has("color")) {
								mStringbubblecolor = jsonOBject
										.getString("color");
							}
							if (mStringbubblecolor != null) {
								if (mStringbubblecolor.equals("1")) {
									shared_Prefernces("blue");
								} else if (mStringbubblecolor.equals("2")) {
									shared_Prefernces("green");
								} else if (mStringbubblecolor.equals("3")) {
									shared_Prefernces("purple");
								} else if (mStringbubblecolor.equals("0")) {
									shared_Prefernces("blue");
								}

							} else {
								shared_Prefernces("blue");
							}
						}

					} else {
						if (jsonOBject.has("color")) {
							mStringbubblecolor = jsonOBject.getString("color");
						}
						// String mStringbubblecolor =
						// card.getField("Bubble_Color");

						if (mStringbubblecolor != null) {
							if (mStringbubblecolor.equals("1")) {
								shared_Prefernces("blue");
							} else if (mStringbubblecolor.equals("2")) {
								shared_Prefernces("green");
							} else if (mStringbubblecolor.equals("3")) {
								shared_Prefernces("purple");
							} else if (mStringbubblecolor.equals("0")) {
								shared_Prefernces("blue");
							}

						} else {
							shared_Prefernces("blue");
						}
					}
					Log.e("VALUE mStringbubblecolor: ", "" + mStringbubblecolor);

					Log.e("chat_list_adapter chat_list_adapter: ", "1 CALLING");
					// chat_list_adapter
					// .registerDataSetObserver(new DataSetObserver() {
					// @Override
					// public void onChanged() {
					// super.onChanged();
					// lv.setSelection(chat_list_adapter.getCount() - 1);
					// }
					// });
					Log.e("chat_list_adapter chat_list_adapter: ", "2 CALLING");
					// / closed=======
					if (jsonOBject.has("from")) {
						fromJID = jsonOBject.getString("from");
						RECENTTOJID = jsonOBject.getString("from");
					}
					if (jsonOBject.has("to")) {
						toJID = jsonOBject.getString("to");
						RECENTFROMJID = jsonOBject.getString("to");
					}
					Log.e("DATABSE fromJID: ", "" + fromJID);
					Log.e("DATABSE toJID: ", "" + toJID);
					fromjid_notification = fromJID;
					tojid_notification = toJID;
					recentfromjid_notification = RECENTFROMJID;
					recenttojid_notification = RECENTTOJID;

					try {
						mDatasourceHandler.UpdateRecentReceiveCount(
								RECENTTOJID, RECENTFROMJID);
					} catch (Exception e) {
						Log.e("UPDATE TIME:", "" + e.getMessage());
					}
					// database fetch working
					try {
						mCursor = mDatasourceHandler.FetchChatHistory(toJID,
								fromJID);
						Log.e("Ans Chat history count: ",
								"" + mCursor.getCount());
					} catch (Exception e) {

						e.printStackTrace();
					}

					if (mCursor.getCount() != 0) {
						mArrayList_uniqueid.clear();
						mArrayList_ToJid.clear();
						mArrayList_FromJid.clear();
						mArrayList_Time.clear();
						mArrayList_Message.clear();
						mArrayList_receive.clear();
						mArrayList_promo_not.clear();
						mArrayDATE.clear();
						do {

							mArrayList_uniqueid
									.add(mCursor.getString(0).trim());
							Log.e("mCursor.getString(0).trim():", ""
									+ mCursor.getString(0).trim());
							mArrayList_ToJid.add(mCursor.getString(2).trim());
							Log.e("mCursor.getString(2).trim():", ""
									+ mCursor.getString(2).trim());
							mArrayList_FromJid.add(mCursor.getString(3).trim());
							Log.e("mCursor.getString(3).trim():", ""
									+ mCursor.getString(3).trim());
							mArrayList_Message.add(mCursor.getString(4).trim());
							Log.e("mCursor.getString(4).trim():", ""
									+ mCursor.getString(4).trim());
							mArrayList_Time.add(mCursor.getString(5).trim());
							Log.e("mCursor.getString(5).trim():", ""
									+ mCursor.getString(5).trim());
							mArrayList_receive.add(mCursor.getString(6).trim());
							mArrayDATE.add(mCursor.getString(9).trim());

							// mArrayList_promo_not.add(mCursor.getString(7).trim());
							// /Log.e("mCursor.getString(7).trim():", "" +
							// mCursor.getString(7).trim());

						} while (mCursor.moveToNext());

						mCursor.close();
					}
					Log.e("mArrayDATE:", "" + mArrayDATE);
					Log.e("mArrayList_uniqueid:", "" + mArrayList_uniqueid);
					Log.e("mArrayList_ToJid:", "" + mArrayList_ToJid);
					Log.e("mArrayList_FromJid:", "" + mArrayList_FromJid);
					Log.e("mArrayList_Message:", "" + mArrayList_Message);
					Log.e("mArrayList_Time:", "" + mArrayList_Time);
					Log.e("mArrayList_receive:", "" + mArrayList_receive);
					// Log.e("mArrayList_promo_not:", "" +
					// mArrayList_promo_not);
					chat_list_adapter = new ChatAdapter(NumberChatScreen.this,
							messages);

					if (mArrayList_uniqueid.size() == 0) {

					} else {

						for (int i = 0; i < mArrayList_uniqueid.size(); i++) {

							if (NamedeviceToken.equals(mArrayList_FromJid
									.get(i) + Constant.XMPP_DNS_Name)
									|| NamedeviceToken.equals(mArrayList_ToJid
											.get(i) + Constant.XMPP_DNS_Name)) {
								String time = mArrayList_Time.get(i);
								String mStringReceive = mArrayList_receive
										.get(i);
								String date_sent = mArrayDATE.get(i);
								// String mStringpromo =
								// mArrayList_promo_not.get(i);
								// Log.e("mStringpromo: ", "" + mStringpromo);
								if (NamedeviceToken.equals(mArrayList_FromJid
										.get(i) + Constant.XMPP_DNS_Name)) {

									String mStringBody = mArrayList_Message
											.get(i);
									// Log.e("mStringBody: ", "" + mStringBody);
									JSONObject json_OBject = new JSONObject(
											mStringBody);
									Log.e("HISTORY json_OBject: ", ""
											+ json_OBject);

									JSONArray mArray = json_OBject
											.getJSONArray("body");

									for (int j = 0; j < mArray.length(); j++) {
										JSONObject mJsonObject = mArray
												.getJSONObject(j);

										if (mJsonObject.getString("pushkey")
												.equals("image")) {

											String[] msg_Data = new String[6];
											msg_Data[0] = "";
											msg_Data[1] = "friend_pic";
											msg_Data[2] = "";
											msg_Data[3] = mJsonObject
													.getString("preview_Icon");
											msg_Data[4] = time;
											msg_Data[5] = date_sent;
											messages.add(msg_Data);
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
										} else {
											String[] msg_Data = new String[9];
											String promo_ID;
											if (mJsonObject
													.has("_promo_id_tag")) {
												promo_ID = mJsonObject
														.getString("_promo_id_tag");

											} else {
												promo_ID = "0";
											}

											// String
											// msendheader=promo_data_(mJsonObject
											// .getString("_promo_id_tag"));
											// Log.e("msendheader: ",
											// ""+msendheader);

											// String
											// link=promo_data_link(mJsonObject
											// .getString("_promo_id_tag"));
											// Log.e("msendheader: ",
											// ""+msendheader);
											Log.e("test: ", "" + promo_ID);
											String test;
											if (promo_ID.equals("0")) {
												test = "0";
											} else {
												test = "";
											}
											String path = historypromo_data(promo_ID);
											Log.e("test: ", "" + promo_ID);
											msg_Data[3] = mJsonObject
													.getString("pushkey");
											msg_Data[1] = "friend";
											msg_Data[2] = "";
											msg_Data[0] = "";
											msg_Data[4] = time;
											msg_Data[5] = test;
											msg_Data[6] = path;
											msg_Data[7] = promo_ID;
											msg_Data[8] = date_sent;

											messages.add(msg_Data);
											// Log.e("COUNT:",
											// ""
											// + chat_list_adapter
											// .getCount());
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
										}
									}
								}

								else {
									// Log.e("", "more condition not matched");
									String mStringBody = mArrayList_Message
											.get(i);
									JSONObject json_OBject = new JSONObject(
											mStringBody);
									JSONArray mArray = json_OBject
											.getJSONArray("body");

									for (int j = 0; j < mArray.length(); j++) {
										JSONObject mJsonObject = mArray
												.getJSONObject(j);
										if (mJsonObject.getString("pushkey")
												.equals("image")) {
											String[] msg_Data = new String[7];
											msg_Data[4] = time;
											msg_Data[0] = "";
											msg_Data[1] = "pic";
											msg_Data[2] = "";
											msg_Data[3] = mJsonObject
													.getString("preview_Icon");
											msg_Data[6] = date_sent;
											messages.add(msg_Data);
											// Log.e("COUNT:",
											// ""
											// + chat_list_adapter
											// .getCount());
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
										} else {
											String promo_ID;
											if (mJsonObject
													.has("_promo_id_tag")) {
												promo_ID = mJsonObject
														.getString("_promo_id_tag");

											} else {
												promo_ID = "0";
											}
											String test;
											if (promo_ID.equals("0")) {
												test = "0";
											} else {
												test = "";
											}
											String path = historypromo_data(promo_ID);
											// Log.e("path: ",
											// "" + path);
											// int path =
											// R.drawable.ic_launcher;
											String[] msg_Data = new String[9];
											msg_Data[6] = time;
											msg_Data[5] = test;
											msg_Data[3] = "";
											msg_Data[1] = "my";
											msg_Data[2] = mStringReceive;
											msg_Data[0] = mJsonObject
													.getString("pushkey");
											msg_Data[4] = path;
											msg_Data[7] = promo_ID;
											msg_Data[8] = date_sent;

											messages.add(msg_Data);
											Log.e("", "HERE");
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}

											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
											Log.e("", "LAST HERE");
										}

									}

								}

							} else {

							}
						}
					}

				} else {
					NamedeviceToken = jsonOBject.getString("devicetoken")
							+ Constant.XMPP_DNS_Name;

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) { // TODO: handle exception
			Log.e("CRASh: ", "" + e.getMessage());
			Log.e("CAUSE: ", "" + e.getCause());
			Log.e("String: ", "" + e.toString());
			Log.e("getLocalizedMessage: ", "" + e.getLocalizedMessage());

			ConnectionLost.showDialog(NumberChatScreen.this, null,
					"Chat history not show due to problem in connection.",
					null, "OK");
			Log.e("CRASh: ", "" + e.getMessage());
		}

	}

	public void shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.BUBBLE_SET_FREIND, mString);

		editor.commit();

	}

	protected void showInputDialog_Done() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.numberchat_pop_up, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final Button mButtonyes = (Button) v.findViewById(R.id.yes_dialog);

		// pinCode.setBackgroundColor(Color.parseColor("#ffffff"));
		pinDialog = new AlertDialog.Builder(NumberChatScreen.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();
						Intent intent_SavedMessageScreen = new Intent(
								NumberChatScreen.this,
								NumberSavedMessageScreen.class);

						intent_SavedMessageScreen.putExtra("to_jid", toJID);
						intent_SavedMessageScreen.putExtra("from_jid", fromJID);
						intent_SavedMessageScreen.putExtra("unknown_no",
								mTextViewTitle.getText().toString());
						startActivity(intent_SavedMessageScreen);
						overridePendingTransition(R.anim.fade_in,
								R.anim.fade_out);

					}
				});
			}
		});

		pinDialog.show();

	}

	private void retrieveState_mode(Mode mode, boolean available, String string) {
		// TODO Auto-generated method stub
		int userState = 0;
		/** 0 for offline, 1 for online, 2 for away,3 for busy */
		if (mode == Mode.dnd) {
			// /Log.e("Busy", "Busy" + mode);
			userState = 3;
		} else if (mode == Mode.away || mode == Mode.xa) {
			// Log.e("away", "away");
			userState = 2;
		} else if (available) {
			// Log.e("online", "online" + mode);
			Toast.makeText(getApplicationContext(),
					getfriend.split("\\@")[0] + "is    Online", 5000).show();
			userState = 1;
		} else {
			// Log.e("offline", "offline" + mode);

			new ExecuteChatNotification().execute();

		}
	}

	class ExecuteChatNotification extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// new_post_ChatNotificaton
			Log.e("to_notification", "" + to_notification);
			Log.e("from_notification", "" + from_notification);
			Log.e("messgae_notification", "" + messgae_notification);
			Log.e("fromjid_notification", "" + fromjid_notification);
			Log.e("tojid_notification", "" + tojid_notification);
			Log.e("time_notification", "" + time_notification);
			Log.e("", "" + timestamp_notification);
			Log.e("date_notification", "" + date_notification);
			Log.e("localdatabase_notification", "" + localdatabase_notification);
			Log.e("messgaepacketid_notification", ""
					+ messgaepacketid_notification);
			Log.e("username_notification", "" + username_notification);
			Log.e("recentfromjid_notification", "" + recentfromjid_notification);
			Log.e("recenttojid_notification", "" + recenttojid_notification);
			String url = JsonParserConnector.new_post_ChatNotificaton(
					to_notification, from_notification, messgae_notification,
					fromjid_notification, tojid_notification,
					time_notification, timestamp_notification,
					date_notification, localdatabase_notification,
					messgaepacketid_notification, "0", "0", "0", "0", "0",
					username_notification, "0", recentfromjid_notification,
					recenttojid_notification);

			// Log.e("Notification___", "" + url);

			return url;
		}

		// @Override
		// protected void onPre(String result) {
		// // TODO Auto-generated method stub
		// super.onPostExecute(result);
		// UI.showProgressDialog(ChatScreen.this);
		//
		// }
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(NumberChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				boolean flag = mDatasourceHandler
						.UpdateReceive(messgaepacketid_notification);
			}

		}

	}

	private void addEntry(String userName) {
		try {

			Presence subscribe = new Presence(Presence.Type.available);
			subscribe.setTo(userName);
			connection.sendPacket(subscribe);
			// Log.e("subscribe", "" + subscribe);
			// Log.e("userName", "" + userName);
		} catch (Exception e) {
			// Log.e("tag", "unable to add contact: ", e);
		}

	}
}
