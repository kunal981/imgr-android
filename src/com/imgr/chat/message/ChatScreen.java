package com.imgr.chat.message;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.ChatStateListener;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.BaseConvert;
import com.imgr.chat.R;
import com.imgr.chat.adapter.ChatAdapter;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.BlockAndMeaageActivity;
import com.imgr.chat.cropping.CropOption;
import com.imgr.chat.cropping.CropOptionAdapter;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.setting.SeeSponsoredandPersonal;
import com.imgr.chat.util.ConnectionLost;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;
import com.imgr.chat.xmpp.chatstate;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ChatScreen extends Activity {
	String mString_name, mStringOnlyOneSelectPromoId = "0";
	TextView mTextViewTitle, mTextView_text_adv_changed;
	SharedPreferences sharedPreferences, mSharedPreferences;
	Editor editor;
	String mStringPhone, mStringusername, mStringPassword, mStringHeader,
			mStringFooter, mStringPersonalSetting;
	String s;
	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;
	String personal_api_time, personal_api_pushkey, personal_api_idtag,
			personal_jidto, personal_jidfrom;
	JSONObject personaljsonObj;
	JSONArray mArrayPersonal = null;
	static final int MIN_DISTANCE = 150;
	ConnectionConfiguration config;
	String testpromo_id;
	XMPPConnection connection;
	EditText input;
	Button send;
	StickyListHeadersListView lv;
	// ListView lv;
	TextView tv, sender, mTextViewBack, mTextViewChanged;
	String getfriend, getfrdsname;
	String JID, mString_Override;
	String username;
	String text;
	String delivery;
	String number;
	Message message;
	ArrayAdapter<String> adapter;
	ArrayList<String[]> messages = new ArrayList<String[]>();
	ArrayList<String> mChatStores = null;
	private Handler mHandler = new Handler();
	ChatAdapter chat_list_adapter;
	static String NamedeviceToken;
	static HashMap<String, String> mHashMap;
	ConnectionDetector mConnectionDetector;
	// int count_test = 0;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	String mStringBase64 = null;
	String profileBase64;
	String notification_on_or_off;
	int check_count = 0;
	String fromJID = null, toJID = null;
	String RECENTFROMJID = null, RECENTTOJID = null;
	private static final int CROP_FROM_CAMERA = 3;
	private Uri mImageCaptureUri;
	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	// private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int PICK_FROM_CAMERA = 1;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "ImgrChat";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;
	ImageView mImageViewProfile, mImageViewAdv_on, mImageViewAdv_off,
			mImageViewPro, mImageViewProTop, image_adv_show_OFF,
			mImageView_adv_delete_icon, mImageView_adv_image;
	boolean adv_on_off;
	boolean sponsored_adv_on_off = true;
	boolean send_sponsored_adv_on_off = false;
	int[] imgIds = { R.drawable.first_promo, R.drawable.second_promo,
			R.drawable.third_promo, R.drawable.four_promo,
			R.drawable.five_promo, R.drawable.six_promo, R.drawable.seven_promo };
	String promo_image, promo_name, promoid;
	ArrayList<String> mArrayList_ToJid;
	ArrayList<String> mArrayList_FromJid;
	ArrayList<String> mArrayList_Time;
	ArrayList<String> mArrayList_Message;
	ArrayList<String> mArrayList_uniqueid;
	ArrayList<String> mArrayList_receive;
	ArrayList<String> mArrayList_promo_not;

	ArrayList<String> mArrayList_imageurl;
	ArrayList<String> mArrayList_promo_uniqueid;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListpromoname;

	ArrayList<String> mArrayDATE;

	ArrayList<String> newmArrayList_imageurl;
	ArrayList<String> newmArrayList_promo_uniqueid;
	ArrayList<String> newmArrayList_promo_uniqueid_personal;
	ArrayList<String> newmArrayList_promo_uniqueid_sponsored;
	ArrayList<String> newmArrayListisdelete;
	ArrayList<String> newmArrayListisactive;
	ArrayList<String> newmArrayListpromoname;
	Roster roster;
	String messageSend;
	String inputTypetext, mStringPhoneid, mStringNochangename,
			mStringPromoOnOFF, mStringAutorotate;

	ImageButton mImageButtonPlus;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	// promo show or
	boolean onlyOnePromo_select = false;
	boolean auto_rotate_Promo_OFF = false;

	VCard card;
	FrameLayout mFrameLayout_adv_view_off, mFrameLayout_adv_view;

	SwipeDismissTouchListener swipeListener;

	ImageButton mImageButton;
	TextView mTextViewAdd;
	String mStringSponsored_BarChanged;

	// new Frame layout Image view Text view Image button
	FrameLayout mFrameLayout_adv_view_OFF_OFF;
	ImageView image_adv_on_OFF, image_OFF_adv_show_OFF, image_adv_show_OFF_OFF,
			image_adv_off_OFF;
	TextView text_changed_OFF;
	ImageButton img_add_new_OFF;
	ArrayList<String> mArrayListisenabled;
	ArrayList<String> mArrayListtype;

	// for push notification parameter
	String to_notification, from_notification, messgae_notification,
			fromjid_notification, tojid_notification, time_notification,
			timestamp_notification, date_notification,
			localdatabase_notification, messgaepacketid_notification,
			memberid_notification, contacyuniqueid_notification,
			receiveornot_notification, bubblecolor_notification,
			fontsize_notification, username_notification,
			profileimage_notification, recentfromjid_notification,
			recenttojid_notification, unique_id_notification_user;

	String setPromo, setAutorotate, setSponsoredads, setpersonalads,
			setuniqueid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chatscreen);

		// try {
		swipeListener = new SwipeDismissTouchListener(this);
		activity = this;
		mConnectionDetector = new ConnectionDetector(this);
		config = new ConnectionConfiguration("64.235.48.26", 5222);
		connection = new XMPPConnection(config);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setReconnectionAllowed(true);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		config.setSecurityMode(SecurityMode.enabled);
		configure(ProviderManager.getInstance());
		config.setDebuggerEnabled(true);
		connection.DEBUG_ENABLED = true;
		config.setReconnectionAllowed(true);
		mFrameLayout_adv_view = (FrameLayout) findViewById(R.id.adv_view);
		mFrameLayout_adv_view_off = (FrameLayout) findViewById(R.id.adv_view_off);

		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mImageViewAdv_on = (ImageView) findViewById(R.id.image_adv_on);
		mImageViewAdv_off = (ImageView) findViewById(R.id.image_adv_off);
		mImageViewProfile = (ImageView) findViewById(R.id.send_image);
		mImageViewPro = (ImageView) findViewById(R.id.image_pro);
		mImageViewProTop = (ImageView) findViewById(R.id.image_adv_show);
		image_adv_show_OFF = (ImageView) findViewById(R.id.image_adv_show_OFF);
		mImageButtonPlus = (ImageButton) findViewById(R.id.img_add_new);
		mTextViewTitle = (TextView) findViewById(R.id.header_title);
		mTextViewChanged = (TextView) findViewById(R.id.text_changed);
		mTextView_text_adv_changed = (TextView) findViewById(R.id.text_adv_changed);
		mImageView_adv_delete_icon = (ImageView) findViewById(R.id.adv_delete_icon);
		mImageView_adv_image = (ImageView) findViewById(R.id.adv_image);
		// new change code=====
		mFrameLayout_adv_view_OFF_OFF = (FrameLayout) findViewById(R.id.adv_view_OFF_OFF);
		image_adv_on_OFF = (ImageView) findViewById(R.id.image_adv_on_OFF);
		image_OFF_adv_show_OFF = (ImageView) findViewById(R.id.image_OFF_adv_show_OFF);
		image_adv_show_OFF_OFF = (ImageView) findViewById(R.id.image_adv_show_OFF_OFF);
		image_adv_off_OFF = (ImageView) findViewById(R.id.image_adv_off_OFF);
		text_changed_OFF = (TextView) findViewById(R.id.text_changed_OFF);
		img_add_new_OFF = (ImageButton) findViewById(R.id.img_add_new_OFF);
		mImageButton = (ImageButton) findViewById(R.id.img_speak);
		mTextViewAdd = (TextView) findViewById(R.id.add_plus);
		// closed

		mArrayList_imageurl = new ArrayList<String>();
		mArrayDATE = new ArrayList<String>();
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mSharedPreferences = getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		sharedPrefernces();

		mStringPromoOnOFF = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "");
		mStringAutorotate = sharedPreferences.getString(
				Constant.SETTING_PROMO_AUTO_ROTATE, "");
		mStringSponsored_BarChanged = sharedPreferences.getString(
				Constant.SPONSORED_ADS, "");
		notification_on_or_off = sharedPreferences.getString(
				Constant.SETTING_NOTIFICATION_ON_OFF, "");

		mTextViewChanged.setOnTouchListener(swipeListener);

		mString_name = getIntent().getStringExtra("Chat_name_select");

		mStringNochangename = getIntent().getStringExtra("Chat_name_select");
		mTextViewBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mTextViewTitle.setText(mString_name);
		mChatStores = new ArrayList<String>();
		mArrayList_promo_uniqueid = new ArrayList<String>();
		mArrayList_ToJid = new ArrayList<String>();
		mArrayList_FromJid = new ArrayList<String>();
		mArrayList_Time = new ArrayList<String>();
		mArrayList_Message = new ArrayList<String>();
		mArrayList_uniqueid = new ArrayList<String>();
		mArrayList_promo_not = new ArrayList<String>();
		mArrayList_receive = new ArrayList<String>();
		mArrayListisdelete = new ArrayList<String>();
		mArrayListisactive = new ArrayList<String>();
		mArrayListpromoname = new ArrayList<String>();
		newmArrayListisdelete = new ArrayList<String>();
		newmArrayListisactive = new ArrayList<String>();
		newmArrayList_promo_uniqueid = new ArrayList<String>();
		newmArrayList_promo_uniqueid_personal = new ArrayList<String>();
		newmArrayList_promo_uniqueid_sponsored = new ArrayList<String>();
		mArrayListtype = new ArrayList<String>();
		newmArrayList_imageurl = new ArrayList<String>();
		newmArrayListpromoname = new ArrayList<String>();
		mArrayListisenabled = new ArrayList<String>();
		if (mArrayList_uniqueid.size() != 0 || mArrayList_FromJid.size() != 0
				|| mArrayList_Time.size() != 0
				|| mArrayList_Message.size() != 0
				|| mArrayList_ToJid.size() != 0) {
			mArrayList_uniqueid.clear();
			mArrayList_ToJid.clear();
			mArrayList_FromJid.clear();
			mArrayList_Time.clear();
			mArrayList_Message.clear();
			mArrayList_promo_not.clear();
			mArrayList_receive.clear();
			mArrayListisdelete.clear();
			mArrayList_promo_uniqueid.clear();
			mArrayListisactive.clear();
			newmArrayList_promo_uniqueid_personal.clear();
			newmArrayList_promo_uniqueid_sponsored.clear();
			newmArrayListisdelete.clear();
			newmArrayListisactive.clear();
			newmArrayList_promo_uniqueid.clear();
			newmArrayList_imageurl.clear();
			mArrayListpromoname.clear();
			mArrayListtype.clear();

			newmArrayListpromoname.clear();
		}

		mHashMap = new HashMap<String, String>();

		// promo fetch from local database
		mArrayListisenabled.clear();

		try {

			mCursor = mDatasourceHandler.All_FETCH_PROMO_DATA();

			check_count = mCursor.getCount();
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			do {
				mArrayList_promo_uniqueid.add(mCursor.getString(0).trim());

				mArrayList_imageurl.add(mCursor.getString(4).trim());
				mArrayListpromoname.add(mCursor.getString(5).trim());
				mArrayListisactive.add(mCursor.getString(6).trim());
				mArrayListisdelete.add(mCursor.getString(7).trim());
				mArrayListisenabled.add(mCursor.getString(8).trim());
				mArrayListtype.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
			Log.e("mArrayListisenabled: ", "" + mArrayListisenabled);
			// shuffleArrayList(mArrayList_imageurl);
			for (int i = 0; i < mArrayList_promo_uniqueid.size(); i++) {
				if (mArrayListisactive.get(i).equals("1")
						&& mArrayListisdelete.get(i).equals("0")
						&& mArrayListisenabled.get(i).equals("1")) {
					newmArrayList_imageurl.add(mArrayList_imageurl.get(i));
					newmArrayList_promo_uniqueid.add(mArrayList_promo_uniqueid
							.get(i));
					newmArrayListisdelete.add(mArrayListisdelete.get(i));
					newmArrayListpromoname.add(mArrayListpromoname.get(i));
					newmArrayListisactive.add(mArrayListisactive.get(i));
				}
			}

			for (int i = 0; i < mArrayList_promo_uniqueid.size(); i++) {
				if (mArrayListisactive.get(i).equals("1")
						&& mArrayListisdelete.get(i).equals("0")
						&& mArrayListisenabled.get(i).equals("1")
						&& mArrayListtype.get(i).equals("personal")) {

					newmArrayList_promo_uniqueid_personal
							.add(mArrayList_promo_uniqueid.get(i));

				}
			}
			Log.e("newmArrayList_promo_uniqueid_personal: ", ""
					+ newmArrayList_promo_uniqueid_personal.size());
			Collections.shuffle(newmArrayList_promo_uniqueid_personal);
			Collections.shuffle(newmArrayList_promo_uniqueid);
			for (int i = 0; i < mArrayList_promo_uniqueid.size(); i++) {
				if (mArrayListisactive.get(i).equals("1")
						&& mArrayListisdelete.get(i).equals("0")
						&& mArrayListisenabled.get(i).equals("1")
						&& mArrayListtype.get(i).equals("no_personal")) {

					newmArrayList_promo_uniqueid_sponsored
							.add(mArrayList_promo_uniqueid.get(i));

				}
			}

			Collections.shuffle(newmArrayList_promo_uniqueid_sponsored);

			for (int i = 0; i < newmArrayList_promo_uniqueid.size(); i++) {
				promodata(newmArrayList_promo_uniqueid.get(i));

			}
			if (newmArrayList_promo_uniqueid.size() == 0) {
				check_count = 0;
			}
		}

		else {

			promo_image = "https://imgrapp.com/release/uploads/promos/prosper-la.png";

		}

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).cacheInMemory()
				.cacheOnDisc().build();

		// imageLoader.displayImage(listFlag.get(position),
		// view.imgViewFlag);

		// /mImageViewProTop.setBackgroundResource(promo_image);

		mImageViewPro.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// if (mStringPhoneid.matches("0")) {
				// LogMessage.showDialog(ChatScreen.this, null,
				// "Please enter your phone number", null, "OK");
				// } else {
				Log.e("", "CLICK HERE");
				Intent mIntent_info = new Intent(ChatScreen.this,
						BlockAndMeaageActivity.class);
				mIntent_info.putExtra("name_select", mStringNochangename);
				mIntent_info.putExtra("phone_id", mStringPhoneid);
				mIntent_info.putExtra("phoneno_select", number);

				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
				// }

			}
		});

		mImageViewAdv_on.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Log.e("adv_on_off: on ", "" + adv_on_off);

				adv_on_off = false;
				mImageViewProTop.setVisibility(View.GONE);
				mImageViewAdv_on.setVisibility(View.GONE);
				mImageViewAdv_off.setVisibility(View.VISIBLE);
				image_adv_show_OFF.setVisibility(View.VISIBLE);
				image_adv_show_OFF.setBackgroundResource(R.drawable.ic_off);

				mTextViewChanged.setText("Off");
				mTextViewChanged.setTextColor(Color.parseColor("#B1B1B1"));
				sharedPrefernces(adv_on_off);

			}
		});
		mImageViewAdv_off.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Log.e("adv_on_off: off ", "" + adv_on_off);

				adv_on_off = true;
				mImageViewAdv_off.setVisibility(View.GONE);
				image_adv_show_OFF.setVisibility(View.GONE);
				mImageViewAdv_on.setVisibility(View.VISIBLE);
				mImageViewProTop.setVisibility(View.VISIBLE);
				mTextViewChanged.setVisibility(View.VISIBLE);
				mTextViewChanged.setTextColor(Color.parseColor("#000000"));
				if (check_count == 0) {
					mTextViewChanged.setText("No Promo");
					imageLoader.displayImage(promo_image, mImageViewProTop,
							options, new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
				} else {

					mTextViewChanged.setText(promo_name);

					imageLoader.displayImage(promo_image, mImageViewProTop,
							options, new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
				}

				sharedPrefernces(adv_on_off);

			}
		});
		mImageButtonPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent mIntent_info = new Intent(ChatScreen.this,
						SeeSponsoredandPersonal.class);
				mIntent_info.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);

			}
		});
		mTextViewAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_info = new Intent(ChatScreen.this,
						SeeSponsoredandPersonal.class);
				mIntent_info.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		mImageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_info = new Intent(ChatScreen.this,
						SeeSponsoredandPersonal.class);
				mIntent_info.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		mTextViewBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				s = "TT";

				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		// new code implement click event
		image_adv_on_OFF.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sponsored_adv_on_off = false;
				send_sponsored_adv_on_off = false;
				image_OFF_adv_show_OFF.setVisibility(View.GONE);
				image_adv_on_OFF.setVisibility(View.GONE);
				image_adv_off_OFF.setVisibility(View.VISIBLE);
				image_adv_show_OFF_OFF.setVisibility(View.VISIBLE);

				image_adv_show_OFF_OFF.setBackgroundResource(R.drawable.ic_off);

				text_changed_OFF.setText("Off");
				text_changed_OFF.setTextColor(Color.parseColor("#B1B1B1"));

			}
		});
		image_adv_off_OFF.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sponsored_adv_on_off = true;
				send_sponsored_adv_on_off = false;
				image_adv_off_OFF.setVisibility(View.GONE);
				image_adv_show_OFF_OFF.setVisibility(View.GONE);

				image_OFF_adv_show_OFF.setVisibility(View.VISIBLE);
				// image_OFF_adv_show_OFF.setBackgroundResource(R.drawable.ic_off);
				image_adv_on_OFF.setVisibility(View.VISIBLE);
				text_changed_OFF.setVisibility(View.VISIBLE);
				// image_adv_show_OFF_OFF.set
				image_adv_show_OFF_OFF
						.setBackgroundResource(R.color.transparent);
				// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

				text_changed_OFF.setText("");
				// text_changed_OFF.setTextColor(Color.parseColor("#B1B1B1"));

			}
		});
		img_add_new_OFF.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent mIntent_info = new Intent(ChatScreen.this,
						SeeSponsoredandPersonal.class);
				mIntent_info.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent_info);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		if (mHashMap != null) {
			mHashMap.clear();
		}

		input = (EditText) findViewById(R.id.input);
		send = (Button) findViewById(R.id.send_clicked);
		lv = (StickyListHeadersListView) findViewById(R.id.listview1);
		lv.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();

		setConnection(connection);
		card = new VCard();

		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mConnectionDetector.isConnectingToInternet()) {
					JID = NamedeviceToken;

					// Log.e("mStringPhone---", "" + mStringPhone);
					// Log.e("JID---", "" + JID);

					// Log.e("connection", "" + connection.isConnected());
					// Log.e("host", "" + connection.getHost());
					String text = String.valueOf(input.getText().toString());
					inputTypetext = input.getText().toString();

					if (connection.isConnected()) {

						if (mStringBase64 != null) {
							if (text.matches("")) {
								//
								JID = NamedeviceToken;// +
								// Constant.XMPP_DNS_Name;

								// Log.e("JID: ", "" + JID);
								// //
								// // Log.e("Sending text ", "" + text +
								// " to "
								// // + JID);
								Message msg = new Message(JID,
										Message.Type.chat);
								ChatManager chatManager = connection
										.getChatManager();
								ChatStateListener chatstat = new chatstate();
								Chat chat = chatManager.createChat(JID,
										chatstat);
								String time = currenttime();
								JSONObject json = new JSONObject();
								try {
									// //
									json.put("server_URL", "Not Uploaded");
									json.put("pushkey", "image");
									json.put("preview_Icon", mStringBase64);
									// //
								} catch (Exception e) {
									// //
								}
								// //
								msg.setBody(json.toString());
								if (connection != null) {
									// //
									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// //
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									// //
									roster = connection.getRoster();
									Presence availability = roster
											.getPresence(JID);
									String date_sent = date();
									if (adv_on_off) {
										String[] msg_Data = new String[7];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "1";
										msg_Data[6] = date_sent;
										messages.add(msg_Data);
									} else {
										String[] msg_Data = new String[7];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "0";
										msg_Data[6] = date_sent;
										messages.add(msg_Data);
									}

									// Log.e("COUNT:",
									// ""
									// + chat_list_adapter
									// .getCount());
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
									// //
									JSONObject objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();
									// //
									mJsonArray.put(json);
									// //
									// // // Finally add item arrays for "A"
									// and
									// // // "B" to
									// // // main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// // // TODO Auto-generated catch
										// block
										// // e.printStackTrace();
									}
									// //
									// // Log.e("CAMERA", "calling");
									if (adv_on_off) {
										String date = date();
										String timestamp = timestamp();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "1", date);
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING1 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}

									} else {
										String date = date();
										String timestamp = timestamp();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "0", date);
										mTextViewChanged.setText("Off");
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING2 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "", getfrdsname,
										// date, time, timestamp);
									}

									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();
									// //
									mStringBase64 = null;
									mImageViewProfile.setImageBitmap(null);
									mImageViewProfile
											.setBackgroundResource(R.drawable.ic_camerachat);
									input.setText("");
									// // Log.e("message sent to ", JID
									// // + "successfull");
									// //
									// Toast.makeText(getApplicationContext(),
									// // "Message Sent !",
									// // Toast.LENGTH_SHORT).show();
								}
							} else {
								JID = NamedeviceToken;// +
								// Constant.XMPP_DNS_Name;

								// Log.e("SIMPLE TEXT MESSAGE JID: ", "" +
								// JID);

								Message msg = new Message(JID,
										Message.Type.chat);
								ChatManager chatManager = connection
										.getChatManager();
								ChatStateListener chatstat = new chatstate();
								Chat chat = chatManager.createChat(JID,
										chatstat);
								String time = currenttime();
								JSONObject json = new JSONObject();
								try {
									// //
									json.put("server_URL", "Not Uploaded");
									json.put("pushkey", "image");
									json.put("preview_Icon", mStringBase64);
									// //
								} catch (Exception e) {
									// //
								}
								// //
								msg.setBody(json.toString());
								if (connection != null) {
									// //
									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// //
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									// //
									roster = connection.getRoster();
									Presence availability = roster
											.getPresence(JID);
									String date_sent = date();
									if (adv_on_off) {
										String[] msg_Data = new String[7];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "1";
										msg_Data[6] = date_sent;
										messages.add(msg_Data);
									} else {
										String[] msg_Data = new String[7];
										msg_Data[0] = text;
										msg_Data[1] = "pic";
										msg_Data[2] = "";
										msg_Data[3] = mStringBase64;
										msg_Data[4] = time;
										msg_Data[5] = "0";
										msg_Data[6] = date_sent;
										messages.add(msg_Data);
									}

									// Log.e("COUNT:",
									// ""
									// + chat_list_adapter
									// .getCount());
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
									// //
									JSONObject objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();
									// //
									mJsonArray.put(json);
									// //
									// // // Finally add item arrays for "A"
									// and
									// // // "B" to
									// // // main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// // // TODO Auto-generated catch
										// block
										// // e.printStackTrace();
									}
									// //
									// // Log.e("CAMERA", "calling");
									if (adv_on_off) {
										String date = date();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"1", "0", date);
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING3 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "", getfrdsname,
										// date, time, timestamp);
									} else {
										String date = date();
										mDatasourceHandler.insertChathistory(
												msg.getPacketID(), "username",
												toJID, fromJID,
												objMainList.toString(), time,
												"0", "0", date);
										mTextViewChanged.setText("Off");
										String timestamp = timestamp();
										boolean flag = mDatasourceHandler
												.UpdateRecentReceive(
														RECENTTOJID,
														RECENTFROMJID,
														objMainList.toString(),
														time, date, timestamp,
														"0");
										Log.e("SENDING4 flag:", "" + flag);
										if (!flag) {
											mDatasourceHandler.insertrecentMessage(
													objMainList.toString(),
													RECENTFROMJID, RECENTTOJID,
													"", getfrdsname, date,
													time, timestamp, "0");
										}
										messgaepacketid_notification = msg
												.getPacketID();
										time_notification = time;
										timestamp_notification = timestamp;
										localdatabase_notification = objMainList
												.toString();
										date_notification = date;
										if (notification_on_or_off != null) {
											if (notification_on_or_off
													.equals("true")) {
												retrieveState_mode(availability
														.getMode(),
														availability
																.isAvailable(),
														objMainList.toString());
											}
										}
										// retrieveState_mode(
										// availability.getMode(),
										// availability.isAvailable(),
										// objMainList.toString());
										// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
										// fromJID, toJID, "", getfrdsname,
										// date, time, timestamp);
									}

									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();
									// //
									mStringBase64 = null;
									mImageViewProfile.setImageBitmap(null);
									mImageViewProfile
											.setBackgroundResource(R.drawable.ic_camerachat);
									input.setText("");
									// // Log.e("message sent to ", JID
									// // + "successfull");
									// //
									// Toast.makeText(getApplicationContext(),
									// // "Message Sent !",
									// // Toast.LENGTH_SHORT).show();
								}
								// text//

								// Log.e("connection.isConnected(): ", ""
								// + connection.isConnected());

								// Log.e("connection.isConnected(): ", ""
								// + connection.isConnected());
								JID = NamedeviceToken;// +
								// Log.e("SIMPLE TEXT MESSAGE JID: ", "" +
								// JID);

								// Log.e("Sending text ", "" + text + " to "
								// + JID);
								Message msg_ = new Message(JID,
										Message.Type.chat);
								ChatManager chat_Manager = connection
										.getChatManager();
								ChatStateListener chat_stat = new chatstate();
								Chat chat_ = chat_Manager.createChat(JID,
										chat_stat);
								JSONObject objMainList = new JSONObject();
								JSONArray mJsonArray = null;
								mJsonArray = new JSONArray();

								JSONObject json1 = new JSONObject();
								if (mStringPromoOnOFF.equals("true")
										&& setPromo.equals("true")) {

									if (mStringSponsored_BarChanged
											.equals("true")
											&& setSponsoredads.equals("true")) {
										if (adv_on_off) {
											try {

												json1.put("pushkey", text);
												json1.put("_message_body_tag",
														text);
												json1.put("_promo_id_tag",
														promoid);
												json1.put("preview_Icon",
														"null");

											} catch (Exception e) {

											}
										} else {
											try {

												json1.put("pushkey", text);
												json1.put("_message_body_tag",
														text);
												json1.put("_promo_id_tag", "0");
												json1.put("preview_Icon",
														"null");

											} catch (Exception e) {

											}
										}

									} else {

										if (sponsored_adv_on_off) {
											if (send_sponsored_adv_on_off) {
												try {

													json1.put("pushkey", text);
													json1.put(
															"_message_body_tag",
															text);
													json1.put("_promo_id_tag",
															promoid);
													json1.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}
											} else {
												try {

													json1.put("pushkey", text);
													json1.put(
															"_message_body_tag",
															text);
													json1.put("_promo_id_tag",
															"0");
													json1.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}
											}
										} else {
											try {

												json1.put("pushkey", text);
												json1.put("_message_body_tag",
														text);
												json1.put("_promo_id_tag", "0");
												json1.put("preview_Icon",
														"null");

											} catch (Exception e) {

											}
										}

									}

								} else {
									try {
										if (mStringOnlyOneSelectPromoId
												.equals("0")) {
											json1.put("pushkey", text);
											json1.put("_message_body_tag", text);
											json1.put("_promo_id_tag", "0");
											json1.put("preview_Icon", "null");
										} else {
											json1.put("pushkey", text);
											json1.put("_message_body_tag", text);
											json1.put("_promo_id_tag",
													mStringOnlyOneSelectPromoId);
											json1.put("preview_Icon", "null");
										}

									} catch (Exception e) {

									}
								}

								// Log.e("msg.getPacketID(): ",
								// "" + msg.getPacketID());
								// Log.e("msg.getXmlns(): ",
								// "" + msg.getXmlns());

								// Log.e("msg.bodies: ", "" +
								// msg.getBodies());
								// Log.e("message send:  ",
								// "" + json.toString());
								messageSend = json1.toString();
								// Log.e("messageSend---->>", "" +
								// messageSend);
								msg_.setBody(messageSend);

								String time_ = currenttime();

								if (connection != null) {
									// Log.e("connection",
									// "" + connection.getHost());

									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// Log.e("msg", "" + msg);
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									roster = connection.getRoster();
									// Log.e("JID---", "" + JID);
									Presence availability = roster
											.getPresence(JID);

									if (text != null && !text.matches("\\s+")
											&& !text.matches("")) {
										if (mStringPromoOnOFF.equals("true")
												&& setPromo.equals("true")) {
											if (mStringSponsored_BarChanged
													.equals("true")
													&& setSponsoredads
															.equals("true")) {
												if (adv_on_off) {
													String[] msg_Data = new String[8];
													msg_Data[0] = text;
													msg_Data[1] = "my";
													msg_Data[2] = "0";
													msg_Data[3] = msg
															.getPacketID();
													msg_Data[4] = ""
															+ promo_image;
													msg_Data[6] = time_;
													msg_Data[5] = "1";
													msg_Data[7] = promoid;
													messages.add(msg_Data);
												} else {
													String[] msg_Data = new String[7];
													msg_Data[0] = text;
													msg_Data[1] = "my";
													msg_Data[2] = "0";
													msg_Data[3] = msg
															.getPacketID();
													msg_Data[4] = ""
															+ promo_image;
													msg_Data[6] = time_;
													msg_Data[5] = "0";
													messages.add(msg_Data);
												}
											} else {

												if (sponsored_adv_on_off) {
													if (send_sponsored_adv_on_off) {
														String[] msg_Data = new String[8];
														msg_Data[0] = text;
														msg_Data[1] = "my";
														msg_Data[2] = "0";
														msg_Data[3] = msg
																.getPacketID();
														msg_Data[4] = ""
																+ promo_image;
														msg_Data[6] = time_;
														msg_Data[5] = "1";
														msg_Data[7] = promoid;
														messages.add(msg_Data);
													} else {
														String[] msg_Data = new String[7];
														msg_Data[0] = text;
														msg_Data[1] = "my";
														msg_Data[2] = "0";
														msg_Data[3] = msg
																.getPacketID();
														msg_Data[4] = ""
																+ promo_image;
														msg_Data[6] = time_;
														msg_Data[5] = "0";
														messages.add(msg_Data);
													}
												} else {
													String[] msg_Data = new String[7];
													msg_Data[0] = text;
													msg_Data[1] = "my";
													msg_Data[2] = "0";
													msg_Data[3] = msg
															.getPacketID();
													msg_Data[4] = ""
															+ promo_image;
													msg_Data[6] = time_;
													msg_Data[5] = "0";
													messages.add(msg_Data);
												}
											}

										} else {
											String date_sent = date();
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												String[] msg_Data = new String[9];
												msg_Data[0] = text;
												msg_Data[1] = "my";
												msg_Data[2] = "0";
												msg_Data[3] = msg.getPacketID();
												msg_Data[4] = "" + promo_image;
												msg_Data[6] = time_;
												msg_Data[5] = "0";
												msg_Data[6] = date_sent;
												messages.add(msg_Data);
											} else {
												String[] msg_Data = new String[9];
												msg_Data[0] = text;
												msg_Data[1] = "my";
												msg_Data[2] = "0";
												msg_Data[3] = msg.getPacketID();
												msg_Data[4] = "" + promo_image;
												msg_Data[6] = time_;
												msg_Data[5] = "1";
												msg_Data[7] = mStringOnlyOneSelectPromoId;
												msg_Data[8] = date_sent;
												messages.add(msg_Data);
											}

										}

										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();
										lv.setAdapter(chat_list_adapter);

									}
									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();

									input.setText("");
									// Log.e("message sent to ", JID
									// + "successfull");
									Toast.makeText(getApplicationContext(),
											"Message Sent !",
											Toast.LENGTH_SHORT).show();
									if (check_count == 0) {
										promo_image = "https://imgrapp.com/release/uploads/promos/prosper-la.png";
									} else {
										if (auto_rotate_Promo_OFF) {
											Collections
													.shuffle(newmArrayList_promo_uniqueid);
											// Collections.shuffle(newmArrayListisactive);
											// Collections.shuffle(newmArrayListisdelete);
											// /Collections.shuffle(newmArrayListpromoname);
											for (int i = 0; i < newmArrayList_promo_uniqueid
													.size(); i++) {
												promodata(newmArrayList_promo_uniqueid
														.get(i));

											}
											mImageViewProfile
													.setImageBitmap(null);
										} else {

										}

									}

									imageLoader.displayImage(promo_image,
											mImageViewProTop, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
									if (mStringPromoOnOFF.equals("true")
											&& setPromo.equals("true")) {
										if (mStringSponsored_BarChanged
												.equals("true")
												&& setSponsoredads
														.equals("true")) {
											if (adv_on_off)
												mTextViewChanged
														.setText(promo_name);
											else
												mTextViewChanged.setText("Off");
										} else {
											if (sponsored_adv_on_off) {
												if (send_sponsored_adv_on_off) {
													text_changed_OFF
															.setText(promo_name);
												} else {
													text_changed_OFF
															.setText("");
												}
											} else {
												text_changed_OFF.setText("");
											}
										}

									} else {

									}

									mJsonArray.put(json);

									// Finally add item arrays for "A" and
									// "B"
									// to
									// main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									// Log.e("Calling time objMainList: ",
									// ""
									// + objMainList.toString());
									// /Log.e("adv_on_off: ", "" +
									// adv_on_off);
									if (mStringPromoOnOFF.equals("true")
											&& setPromo.equals("true")) {
										if (mStringSponsored_BarChanged
												.equals("true")
												&& setSponsoredads
														.equals("true")) {
											if (adv_on_off) {
												String date = date();
												mDatasourceHandler.insertChathistory(
														msg.getPacketID(),
														"username", toJID,
														fromJID,
														objMainList.toString(),
														time, "1", "0", date);
												String timestamp = timestamp();
												boolean flag = mDatasourceHandler
														.UpdateRecentReceive(
																RECENTTOJID,
																RECENTFROMJID,
																objMainList
																		.toString(),
																time, date,
																timestamp, "0");
												Log.e("SENDING5 flag:", ""
														+ flag);
												if (!flag) {
													mDatasourceHandler
															.insertrecentMessage(
																	objMainList
																			.toString(),
																	RECENTFROMJID,
																	RECENTTOJID,
																	"",
																	getfrdsname,
																	date, time,
																	timestamp,
																	"0");
												}
												messgaepacketid_notification = msg
														.getPacketID();
												time_notification = time;
												timestamp_notification = timestamp;
												localdatabase_notification = objMainList
														.toString();
												date_notification = date;
												if (notification_on_or_off != null) {
													if (notification_on_or_off
															.equals("true")) {
														retrieveState_mode(
																availability
																		.getMode(),
																availability
																		.isAvailable(),
																objMainList
																		.toString());
													}
												}
												// retrieveState_mode(availability
												// .getMode(),
												// availability
												// .isAvailable(),
												// objMainList.toString());
												// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
												// fromJID, toJID, "",
												// getfrdsname,
												// date, time, timestamp);

											} else {
												String date = date();
												mDatasourceHandler.insertChathistory(
														msg.getPacketID(),
														"username", toJID,
														fromJID,
														objMainList.toString(),
														time, "0", "0", date);
												String timestamp = timestamp();
												boolean flag = mDatasourceHandler
														.UpdateRecentReceive(
																RECENTTOJID,
																RECENTFROMJID,
																objMainList
																		.toString(),
																time, date,
																timestamp, "0");
												Log.e("SENDING6 flag:", ""
														+ flag);
												if (!flag) {
													mDatasourceHandler
															.insertrecentMessage(
																	objMainList
																			.toString(),
																	RECENTFROMJID,
																	RECENTTOJID,
																	"",
																	getfrdsname,
																	date, time,
																	timestamp,
																	"0");
												}
												messgaepacketid_notification = msg
														.getPacketID();
												time_notification = time;
												timestamp_notification = timestamp;
												localdatabase_notification = objMainList
														.toString();
												date_notification = date;
												if (notification_on_or_off != null) {
													if (notification_on_or_off
															.equals("true")) {
														retrieveState_mode(
																availability
																		.getMode(),
																availability
																		.isAvailable(),
																objMainList
																		.toString());
													}
												}
												// retrieveState_mode(availability
												// .getMode(),
												// availability
												// .isAvailable(),
												// objMainList.toString());
												// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
												// fromJID, toJID, "",
												// getfrdsname,
												// date, time, timestamp);
												adv_on_off = true;

												sharedPrefernces(adv_on_off);
												mImageViewAdv_off
														.setVisibility(View.GONE);
												image_adv_show_OFF
														.setVisibility(View.GONE);
												mImageViewAdv_on
														.setVisibility(View.VISIBLE);
												mImageViewProTop
														.setVisibility(View.VISIBLE);
												mTextViewChanged.setTextColor(Color
														.parseColor("#000000"));
												if (check_count == 0) {
													promo_image = "https://imgrapp.com/release/uploads/promos/prosper-la.png";
												} else {

													if (auto_rotate_Promo_OFF) {
														Collections
																.shuffle(newmArrayList_promo_uniqueid);
														// Collections.shuffle(newmArrayListisactive);
														// Collections.shuffle(newmArrayListisdelete);
														// /Collections.shuffle(newmArrayListpromoname);
														for (int i = 0; i < newmArrayList_promo_uniqueid
																.size(); i++) {
															promodata(newmArrayList_promo_uniqueid
																	.get(i));

														}
														mImageViewProfile
																.setImageBitmap(null);
													} else {

													}

												}

												imageLoader
														.displayImage(
																promo_image,
																mImageViewProTop,
																options,
																new SimpleImageLoadingListener() {
																	@Override
																	public void onLoadingComplete(
																			Bitmap loadedImage) {
																		Animation anim = AnimationUtils
																				.loadAnimation(
																						activity,
																						R.anim.fade_in);
																		// imgViewFlag.setAnimation(anim);
																		// anim.start();
																	}
																});

												if (adv_on_off)
													mTextViewChanged
															.setText(promo_name);
												else
													mTextViewChanged
															.setText("Off");
											}

											// single bubble visible and gone
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											} else {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											}

										} else {
											if (sponsored_adv_on_off) {
												if (send_sponsored_adv_on_off) {
													String date = date();
													mDatasourceHandler.insertChathistory(
															msg.getPacketID(),
															"username",
															toJID,
															fromJID,
															objMainList
																	.toString(),
															time, "1", "0",
															date);
													String timestamp = timestamp();
													boolean flag = mDatasourceHandler
															.UpdateRecentReceive(
																	RECENTTOJID,
																	RECENTFROMJID,
																	objMainList
																			.toString(),
																	time, date,
																	timestamp,
																	"0");
													Log.e("SENDING5 flag:", ""
															+ flag);
													if (!flag) {
														mDatasourceHandler
																.insertrecentMessage(
																		objMainList
																				.toString(),
																		RECENTFROMJID,
																		RECENTTOJID,
																		"",
																		getfrdsname,
																		date,
																		time,
																		timestamp,
																		"0");
													}
													messgaepacketid_notification = msg
															.getPacketID();
													time_notification = time;
													timestamp_notification = timestamp;
													localdatabase_notification = objMainList
															.toString();
													date_notification = date;
													if (notification_on_or_off != null) {
														if (notification_on_or_off
																.equals("true")) {
															retrieveState_mode(
																	availability
																			.getMode(),
																	availability
																			.isAvailable(),
																	objMainList
																			.toString());
														}
													}
													// retrieveState_mode(
													// availability
													// .getMode(),
													// availability
													// .isAvailable(),
													// objMainList
													// .toString());
													// need code here

													// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
													// fromJID, toJID, "",
													// getfrdsname,
													// date, time, timestamp);

												} else {

													String date = date();
													mDatasourceHandler.insertChathistory(
															msg.getPacketID(),
															"username",
															toJID,
															fromJID,
															objMainList
																	.toString(),
															time, "0", "0",
															date);
													String timestamp = timestamp();
													boolean flag = mDatasourceHandler
															.UpdateRecentReceive(
																	RECENTTOJID,
																	RECENTFROMJID,
																	objMainList
																			.toString(),
																	time, date,
																	timestamp,
																	"0");
													Log.e("SENDING6 flag:", ""
															+ flag);
													if (!flag) {
														mDatasourceHandler
																.insertrecentMessage(
																		objMainList
																				.toString(),
																		RECENTFROMJID,
																		RECENTTOJID,
																		"",
																		getfrdsname,
																		date,
																		time,
																		timestamp,
																		"0");
													}
													messgaepacketid_notification = msg
															.getPacketID();
													time_notification = time;
													timestamp_notification = timestamp;
													localdatabase_notification = objMainList
															.toString();
													date_notification = date;
													if (notification_on_or_off != null) {
														if (notification_on_or_off
																.equals("true")) {
															retrieveState_mode(
																	availability
																			.getMode(),
																	availability
																			.isAvailable(),
																	objMainList
																			.toString());
														}
													}
													// retrieveState_mode(
													// availability
													// .getMode(),
													// availability
													// .isAvailable(),
													// objMainList
													// .toString());
													// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
													// fromJID, toJID, "",
													// getfrdsname,
													// date, time, timestamp);

												}

											} else {
												String date = date();
												mDatasourceHandler.insertChathistory(
														msg.getPacketID(),
														"username", toJID,
														fromJID,
														objMainList.toString(),
														time, "0", "0", date);
												String timestamp = timestamp();
												boolean flag = mDatasourceHandler
														.UpdateRecentReceive(
																RECENTTOJID,
																RECENTFROMJID,
																objMainList
																		.toString(),
																time, date,
																timestamp, "0");
												Log.e("SENDING6 flag:", ""
														+ flag);
												if (!flag) {
													mDatasourceHandler
															.insertrecentMessage(
																	objMainList
																			.toString(),
																	RECENTFROMJID,
																	RECENTTOJID,
																	"",
																	getfrdsname,
																	date, time,
																	timestamp,
																	"0");
												}
												messgaepacketid_notification = msg
														.getPacketID();
												time_notification = time;
												timestamp_notification = timestamp;
												localdatabase_notification = objMainList
														.toString();
												date_notification = date;
												if (notification_on_or_off != null) {
													if (notification_on_or_off
															.equals("true")) {
														retrieveState_mode(
																availability
																		.getMode(),
																availability
																		.isAvailable(),
																objMainList
																		.toString());
													}
												}
												// retrieveState_mode(availability
												// .getMode(),
												// availability
												// .isAvailable(),
												// objMainList.toString());
												// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
												// fromJID, toJID, "",
												// getfrdsname,
												// date, time, timestamp);

											}
										}
									} else {
										if (mStringOnlyOneSelectPromoId
												.equals("0")) {
											String date = date();
											mDatasourceHandler.insertChathistory(
													msg.getPacketID(),
													"username", toJID, fromJID,
													objMainList.toString(),
													time, "0", "0", date);
											String timestamp = timestamp();
											boolean flag = mDatasourceHandler
													.UpdateRecentReceive(
															RECENTTOJID,
															RECENTFROMJID,
															objMainList
																	.toString(),
															time, date,
															timestamp, "0");
											Log.e("SENDING7 flag:", "" + flag);
											if (!flag) {
												mDatasourceHandler.insertrecentMessage(
														objMainList.toString(),
														RECENTFROMJID,
														RECENTTOJID, "",
														getfrdsname, date,
														time, timestamp, "0");
											}
											messgaepacketid_notification = msg
													.getPacketID();
											time_notification = time;
											timestamp_notification = timestamp;
											localdatabase_notification = objMainList
													.toString();
											date_notification = date;
											if (notification_on_or_off != null) {
												if (notification_on_or_off
														.equals("true")) {
													retrieveState_mode(
															availability
																	.getMode(),
															availability
																	.isAvailable(),
															objMainList
																	.toString());
												}
											}
											// retrieveState_mode(
											// availability.getMode(),
											// availability.isAvailable(),
											// objMainList.toString());
											// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
											// fromJID, toJID, "", getfrdsname,
											// date, time, timestamp);
											// sinle bubble visible and gone
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											} else {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											}
										} else {
											String date = date();
											mDatasourceHandler.insertChathistory(
													msg.getPacketID(),
													"username", toJID, fromJID,
													objMainList.toString(),
													time, "0", "0", date);
											String timestamp = timestamp();
											boolean flag = mDatasourceHandler
													.UpdateRecentReceive(
															RECENTTOJID,
															RECENTFROMJID,
															objMainList
																	.toString(),
															time, date,
															timestamp, "0");
											Log.e("SENDING8 flag:", "" + flag);
											if (!flag) {
												mDatasourceHandler.insertrecentMessage(
														objMainList.toString(),
														RECENTFROMJID,
														RECENTTOJID, "",
														getfrdsname, date,
														time, timestamp, "0");
											}
											messgaepacketid_notification = msg
													.getPacketID();
											time_notification = time;
											timestamp_notification = timestamp;
											localdatabase_notification = objMainList
													.toString();
											date_notification = date;
											if (notification_on_or_off != null) {
												if (notification_on_or_off
														.equals("true")) {
													retrieveState_mode(
															availability
																	.getMode(),
															availability
																	.isAvailable(),
															objMainList
																	.toString());
												}
											}
											// retrieveState_mode(
											// availability.getMode(),
											// availability.isAvailable(),
											// objMainList.toString());
											// sinle bubble visible and gone
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											} else {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											}
											// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
											// fromJID, toJID, "", getfrdsname,
											// date, time, timestamp);
										}

									}

								}

								// retrieveState_mode(
								// availability.getMode(),
								// availability.isAvailable(),
								// objMainList.toString());

								// mStringBase64 = null;
							}
						} else {
							mStringBase64 = null;
							if (text.matches("")) {
								LogMessage.showDialog(ChatScreen.this, null,
										"Please enter the message", null, "Ok");
							} else {

								JID = NamedeviceToken;// +

								Message msg = new Message(JID,
										Message.Type.chat);
								ChatManager chatManager = connection
										.getChatManager();
								ChatStateListener chatstat = new chatstate();
								Chat chat = chatManager.createChat(JID,
										chatstat);
								JSONObject objMainList = new JSONObject();
								JSONArray mJsonArray = null;
								mJsonArray = new JSONArray();

								JSONObject json = new JSONObject();
								if (mStringPromoOnOFF.equals("true")
										&& setPromo.equals("true")) {

									if (mStringSponsored_BarChanged
											.equals("true")
											&& setSponsoredads.equals("true")) {
										if (adv_on_off) {
											try {

												json.put("pushkey", text);
												json.put("_message_body_tag",
														text);
												json.put("_promo_id_tag",
														promoid);
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}
										} else {
											try {

												json.put("pushkey", text);
												json.put("_message_body_tag",
														text);
												json.put("_promo_id_tag", "0");
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}
										}

									} else {
										// 2 sept code implement
										if (mStringPersonalSetting
												.equals("true")
												&& setpersonalads
														.equals("true")) {
											if (newmArrayList_promo_uniqueid_personal
													.size() == 0) {
												try {

													json.put("pushkey", text);
													json.put(
															"_message_body_tag",
															text);
													json.put("_promo_id_tag",
															"0");
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}

											} else {
												if (sponsored_adv_on_off) {
													if (send_sponsored_adv_on_off) {
														try {

															json.put("pushkey",
																	text);
															json.put(
																	"_message_body_tag",
																	text);
															json.put(
																	"_promo_id_tag",
																	promoid);
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}
													} else {
														try {

															json.put("pushkey",
																	text);
															json.put(
																	"_message_body_tag",
																	text);
															json.put(
																	"_promo_id_tag",
																	"0");
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}
													}
												} else {
													try {

														json.put("pushkey",
																text);
														json.put(
																"_message_body_tag",
																text);
														json.put(
																"_promo_id_tag",
																"0");
														json.put(
																"preview_Icon",
																"null");

													} catch (Exception e) {

													}
												}
											}
										} else {
											if (sponsored_adv_on_off) {
												if (send_sponsored_adv_on_off) {
													try {

														json.put("pushkey",
																text);
														json.put(
																"_message_body_tag",
																text);
														json.put(
																"_promo_id_tag",
																promoid);
														json.put(
																"preview_Icon",
																"null");

													} catch (Exception e) {

													}
												} else {
													try {

														json.put("pushkey",
																text);
														json.put(
																"_message_body_tag",
																text);
														json.put(
																"_promo_id_tag",
																"0");
														json.put(
																"preview_Icon",
																"null");

													} catch (Exception e) {

													}
												}
											} else {
												try {

													json.put("pushkey", text);
													json.put(
															"_message_body_tag",
															text);
													json.put("_promo_id_tag",
															"0");
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}
											}
										}
										// closed

									}
								} else {
									try {
										if (mStringOnlyOneSelectPromoId
												.equals("0")) {
											json.put("pushkey", text);
											json.put("_message_body_tag", text);
											json.put("_promo_id_tag", "0");
											json.put("preview_Icon", "null");
										} else {
											json.put("pushkey", text);
											json.put("_message_body_tag", text);
											json.put("_promo_id_tag",
													mStringOnlyOneSelectPromoId);
											json.put("preview_Icon", "null");
										}

									} catch (Exception e) {

									}
								}

								messageSend = json.toString();

								msg.setBody(messageSend);

								String time = currenttime();

								if (connection != null) {
									// Log.e("connection",
									// "" + connection.getHost());

									DeliveryReceiptManager
											.addDeliveryReceiptRequest(msg);
									// Log.e("msg", "" + msg);
									// Log.e("connection",
									// "" + connection.isConnected());
									connection.sendPacket(msg);
									roster = connection.getRoster();
									// Log.e("JID---", "" + JID);
									Presence availability = roster
											.getPresence(JID);

									if (text != null && !text.matches("\\s+")
											&& !text.matches("")) {
										if (mStringPromoOnOFF.equals("true")
												&& setPromo.equals("true")) {
											if (mStringSponsored_BarChanged
													.equals("true")
													&& setSponsoredads
															.equals("true")) {
												String date_sent = date();
												if (adv_on_off) {
													String[] msg_Data = new String[9];
													msg_Data[0] = text;
													msg_Data[1] = "my";
													msg_Data[2] = "0";
													msg_Data[3] = msg
															.getPacketID();
													msg_Data[4] = ""
															+ promo_image;
													msg_Data[6] = time;
													msg_Data[5] = "1";
													msg_Data[7] = promoid;
													msg_Data[8] = date_sent;
													messages.add(msg_Data);
												} else {
													String[] msg_Data = new String[9];
													msg_Data[0] = text;
													msg_Data[1] = "my";
													msg_Data[2] = "0";
													msg_Data[3] = msg
															.getPacketID();
													msg_Data[4] = ""
															+ promo_image;
													msg_Data[6] = time;
													msg_Data[5] = "0";
													msg_Data[8] = date_sent;
													messages.add(msg_Data);
												}
											} else {
												// 2 sept cod implement
												if (mStringPersonalSetting
														.equals("true")
														&& setpersonalads
																.equals("true")) {
													if (newmArrayList_promo_uniqueid_personal
															.size() == 0) {
														String date_sent = date();
														String[] msg_Data = new String[9];
														msg_Data[0] = text;
														msg_Data[1] = "my";
														msg_Data[2] = "0";
														msg_Data[3] = msg
																.getPacketID();
														msg_Data[4] = ""
																+ promo_image;
														msg_Data[6] = time;
														msg_Data[5] = "0";
														msg_Data[8] = date_sent;
														messages.add(msg_Data);

													} else {
														if (sponsored_adv_on_off) {
															String date_sent = date();
															if (send_sponsored_adv_on_off) {
																String[] msg_Data = new String[9];
																msg_Data[0] = text;
																msg_Data[1] = "my";
																msg_Data[2] = "0";
																msg_Data[3] = msg
																		.getPacketID();
																msg_Data[4] = ""
																		+ promo_image;
																msg_Data[6] = time;
																msg_Data[5] = "1";
																msg_Data[7] = promoid;
																msg_Data[8] = date_sent;
																messages.add(msg_Data);
															} else {
																String[] msg_Data = new String[9];
																msg_Data[0] = text;
																msg_Data[1] = "my";
																msg_Data[2] = "0";
																msg_Data[3] = msg
																		.getPacketID();
																msg_Data[4] = ""
																		+ promo_image;
																msg_Data[6] = time;
																msg_Data[5] = "0";
																msg_Data[8] = date_sent;
																messages.add(msg_Data);
															}
														} else {
															String date_sent = date();
															String[] msg_Data = new String[9];
															msg_Data[0] = text;
															msg_Data[1] = "my";
															msg_Data[2] = "0";
															msg_Data[3] = msg
																	.getPacketID();
															msg_Data[4] = ""
																	+ promo_image;
															msg_Data[6] = time;
															msg_Data[5] = "0";
															msg_Data[8] = date_sent;
															messages.add(msg_Data);
														}
													}
												} else {
													if (sponsored_adv_on_off) {
														String date_sent = date();
														if (send_sponsored_adv_on_off) {
															String[] msg_Data = new String[9];
															msg_Data[0] = text;
															msg_Data[1] = "my";
															msg_Data[2] = "0";
															msg_Data[3] = msg
																	.getPacketID();
															msg_Data[4] = ""
																	+ promo_image;
															msg_Data[6] = time;
															msg_Data[5] = "1";
															msg_Data[7] = promoid;
															msg_Data[8] = date_sent;
															messages.add(msg_Data);
														} else {
															String[] msg_Data = new String[9];
															msg_Data[0] = text;
															msg_Data[1] = "my";
															msg_Data[2] = "0";
															msg_Data[3] = msg
																	.getPacketID();
															msg_Data[4] = ""
																	+ promo_image;
															msg_Data[6] = time;
															msg_Data[5] = "0";
															msg_Data[8] = date_sent;
															messages.add(msg_Data);
														}
													} else {
														String date_sent = date();
														String[] msg_Data = new String[9];
														msg_Data[0] = text;
														msg_Data[1] = "my";
														msg_Data[2] = "0";
														msg_Data[3] = msg
																.getPacketID();
														msg_Data[4] = ""
																+ promo_image;
														msg_Data[6] = time;
														msg_Data[5] = "0";
														msg_Data[8] = date_sent;
														messages.add(msg_Data);
													}
												}

												// cosed the code

											}

										} else {
											String date_sent = date();
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												String[] msg_Data = new String[9];
												msg_Data[0] = text;
												msg_Data[1] = "my";
												msg_Data[2] = "0";
												msg_Data[3] = msg.getPacketID();
												msg_Data[4] = "" + promo_image;
												msg_Data[6] = time;
												msg_Data[5] = "0";
												msg_Data[8] = date_sent;
												messages.add(msg_Data);
											} else {
												String[] msg_Data = new String[9];
												msg_Data[0] = text;
												msg_Data[1] = "my";
												msg_Data[2] = "0";
												msg_Data[3] = msg.getPacketID();
												msg_Data[4] = "" + promo_image;
												msg_Data[6] = time;
												msg_Data[5] = "1";
												msg_Data[7] = mStringOnlyOneSelectPromoId;
												msg_Data[8] = date_sent;
												messages.add(msg_Data);
											}

										}

										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();
										lv.setAdapter(chat_list_adapter);

									}
									inputTypetext = input.getText().toString();
									messgae_notification = input.getText()
											.toString();

									input.setText("");

									Toast.makeText(getApplicationContext(),
											"Message Sent !",
											Toast.LENGTH_SHORT).show();
									if (check_count == 0) {
										promo_image = "https://imgrapp.com/release/uploads/promos/prosper-la.png";
									} else {
										if (mStringSponsored_BarChanged
												.equals("true")
												&& setSponsoredads
														.equals("true")) {
											//

											if (mStringPersonalSetting
													.equals("true")
													&& setpersonalads
															.equals("true")) {
												if (auto_rotate_Promo_OFF) {

													Collections
															.shuffle(newmArrayList_promo_uniqueid);

													// Collections.shuffle(newmArrayListisactive);
													// Collections.shuffle(newmArrayListisdelete);
													// /Collections.shuffle(newmArrayListpromoname);
													for (int i = 0; i < newmArrayList_promo_uniqueid
															.size(); i++) {
														promodata(newmArrayList_promo_uniqueid
																.get(i));

													}
													mImageViewProfile
															.setImageBitmap(null);
												}

											} else {
												if (auto_rotate_Promo_OFF) {
													Collections
															.shuffle(newmArrayList_promo_uniqueid_sponsored);

													// Collections.shuffle(newmArrayListisactive);
													// Collections.shuffle(newmArrayListisdelete);
													// /Collections.shuffle(newmArrayListpromoname);
													for (int i = 0; i < newmArrayList_promo_uniqueid_sponsored
															.size(); i++) {
														promodata(newmArrayList_promo_uniqueid_sponsored
																.get(i));

													}
													mImageViewProfile
															.setImageBitmap(null);
												}

											}

										} else {
											if (auto_rotate_Promo_OFF) {
												if (mStringPersonalSetting
														.equals("true")
														&& setpersonalads
																.equals("true")) {
													if (newmArrayList_promo_uniqueid_personal
															.size() == 0) {

													} else {
														Collections
																.shuffle(newmArrayList_promo_uniqueid_personal);
														Log.e("ONLY PERSONAL",
																"ONLY PERSONAL");
														send_sponsored_adv_on_off = true;
														NEW_SPONSORED_PERSONAL_ONLY();
													}
												}

											}
										}

									}

									imageLoader.displayImage(promo_image,
											mImageViewProTop, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
									if (mStringPromoOnOFF.equals("true")
											&& setPromo.equals("true")) {
										if (mStringSponsored_BarChanged
												.equals("true")
												&& setSponsoredads
														.equals("true")) {
											if (adv_on_off)
												mTextViewChanged
														.setText(promo_name);
											else
												mTextViewChanged.setText("Off");
										} else {
											// 2 sept code
											if (mStringPersonalSetting
													.equals("true")
													&& setpersonalads
															.equals("true")) {
												if (newmArrayList_promo_uniqueid_personal
														.size() == 0) {

												} else {
													if (sponsored_adv_on_off) {
														if (send_sponsored_adv_on_off) {
															text_changed_OFF
																	.setText(promo_name);
														} else {
															text_changed_OFF
																	.setText("");
														}
													} else {
														text_changed_OFF
																.setText("");
													}
												}
											} else {
												if (sponsored_adv_on_off) {
													if (send_sponsored_adv_on_off) {
														text_changed_OFF
																.setText(promo_name);
													} else {
														text_changed_OFF
																.setText("");
													}
												} else {
													text_changed_OFF
															.setText("");
												}
											}
											// closed

										}

									} else {

									}

									mJsonArray.put(json);

									// Finally add item arrays for "A" and
									// "B"
									// to
									// main list with key
									try {
										objMainList.put("body", mJsonArray);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									// Log.e("Calling time objMainList: ",
									// ""
									// + objMainList.toString());
									// /Log.e("adv_on_off: ", "" +
									// adv_on_off);
									if (mStringPromoOnOFF.equals("true")
											&& setPromo.equals("true")) {

										if (mStringSponsored_BarChanged
												.equals("true")
												&& setSponsoredads
														.equals("true")) {
											if (adv_on_off) {
												String date = date();
												mDatasourceHandler.insertChathistory(
														msg.getPacketID(),
														"username", toJID,
														fromJID,
														objMainList.toString(),
														time, "1", "0", date);
												String timestamp = timestamp();
												boolean flag = mDatasourceHandler
														.UpdateRecentReceive(
																RECENTTOJID,
																RECENTFROMJID,
																objMainList
																		.toString(),
																time, date,
																timestamp, "0");
												Log.e("SENDING9 flag:", ""
														+ flag);
												if (!flag) {
													mDatasourceHandler
															.insertrecentMessage(
																	objMainList
																			.toString(),
																	RECENTFROMJID,
																	RECENTTOJID,
																	"",
																	getfrdsname,
																	date, time,
																	timestamp,
																	"0");
												}
												messgaepacketid_notification = msg
														.getPacketID();
												time_notification = time;
												timestamp_notification = timestamp;
												localdatabase_notification = objMainList
														.toString();
												date_notification = date;
												if (notification_on_or_off != null) {
													if (notification_on_or_off
															.equals("true")) {
														retrieveState_mode(
																availability
																		.getMode(),
																availability
																		.isAvailable(),
																objMainList
																		.toString());
													}
												}
												// retrieveState_mode(availability
												// .getMode(),
												// availability
												// .isAvailable(),
												// objMainList.toString());
												// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
												// fromJID, toJID, "",
												// getfrdsname,
												// date, time, timestamp);

											} else {
												String date = date();
												mDatasourceHandler.insertChathistory(
														msg.getPacketID(),
														"username", toJID,
														fromJID,
														objMainList.toString(),
														time, "0", "0", date);
												String timestamp = timestamp();
												boolean flag = mDatasourceHandler
														.UpdateRecentReceive(
																RECENTTOJID,
																RECENTFROMJID,
																objMainList
																		.toString(),
																time, date,
																timestamp, "0");
												Log.e("SENDING10 flag:", ""
														+ flag);
												if (!flag) {
													mDatasourceHandler
															.insertrecentMessage(
																	objMainList
																			.toString(),
																	RECENTFROMJID,
																	RECENTTOJID,
																	"",
																	getfrdsname,
																	date, time,
																	timestamp,
																	"0");
												}
												messgaepacketid_notification = msg
														.getPacketID();
												time_notification = time;
												timestamp_notification = timestamp;
												localdatabase_notification = objMainList
														.toString();
												date_notification = date;
												if (notification_on_or_off != null) {
													if (notification_on_or_off
															.equals("true")) {
														retrieveState_mode(
																availability
																		.getMode(),
																availability
																		.isAvailable(),
																objMainList
																		.toString());
													}
												}
												// retrieveState_mode(availability
												// .getMode(),
												// availability
												// .isAvailable(),
												// objMainList.toString());
												// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
												// fromJID, toJID, "",
												// getfrdsname,
												// date, time, timestamp);
												adv_on_off = true;

												sharedPrefernces(adv_on_off);
												mImageViewAdv_off
														.setVisibility(View.GONE);
												image_adv_show_OFF
														.setVisibility(View.GONE);
												mImageViewAdv_on
														.setVisibility(View.VISIBLE);
												mImageViewProTop
														.setVisibility(View.VISIBLE);
												mTextViewChanged.setTextColor(Color
														.parseColor("#000000"));
												if (check_count == 0) {
													promo_image = "https://imgrapp.com/release/uploads/promos/prosper-la.png";
												} else {

													if (auto_rotate_Promo_OFF) {
														Collections
																.shuffle(newmArrayList_promo_uniqueid);
														// Collections.shuffle(newmArrayListisactive);
														// Collections.shuffle(newmArrayListisdelete);
														// /Collections.shuffle(newmArrayListpromoname);
														for (int i = 0; i < newmArrayList_promo_uniqueid
																.size(); i++) {
															promodata(newmArrayList_promo_uniqueid
																	.get(i));

														}
														mImageViewProfile
																.setImageBitmap(null);
													} else {

													}

												}

												imageLoader
														.displayImage(
																promo_image,
																mImageViewProTop,
																options,
																new SimpleImageLoadingListener() {
																	@Override
																	public void onLoadingComplete(
																			Bitmap loadedImage) {
																		Animation anim = AnimationUtils
																				.loadAnimation(
																						activity,
																						R.anim.fade_in);
																		// imgViewFlag.setAnimation(anim);
																		// anim.start();
																	}
																});
												if (adv_on_off)
													mTextViewChanged
															.setText(promo_name);
												else
													mTextViewChanged
															.setText("Off");
											}
											// single bubble visible and gone
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											} else {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											}

										} else {
											// 2 sept code
											if (mStringPersonalSetting
													.equals("true")
													&& setpersonalads
															.equals("true")) {
												if (newmArrayList_promo_uniqueid_personal
														.size() == 0) {

													// need code here
													String date = date();
													mDatasourceHandler.insertChathistory(
															msg.getPacketID(),
															"username",
															toJID,
															fromJID,
															objMainList
																	.toString(),
															time, "0", "0",
															date);
													String timestamp = timestamp();
													boolean flag = mDatasourceHandler
															.UpdateRecentReceive(
																	RECENTTOJID,
																	RECENTFROMJID,
																	objMainList
																			.toString(),
																	time, date,
																	timestamp,
																	"0");
													Log.e("SENDING10 flag:", ""
															+ flag);
													if (!flag) {
														mDatasourceHandler
																.insertrecentMessage(
																		objMainList
																				.toString(),
																		RECENTFROMJID,
																		RECENTTOJID,
																		"",
																		getfrdsname,
																		date,
																		time,
																		timestamp,
																		"0");
													}
													messgaepacketid_notification = msg
															.getPacketID();
													time_notification = time;
													timestamp_notification = timestamp;
													localdatabase_notification = objMainList
															.toString();
													date_notification = date;

													if (notification_on_or_off != null) {
														if (notification_on_or_off
																.equals("true")) {
															retrieveState_mode(
																	availability
																			.getMode(),
																	availability
																			.isAvailable(),
																	objMainList
																			.toString());
														}
													}
													// retrieveState_mode(availability
													// .getMode(),
													// availability
													// .isAvailable(),
													// objMainList.toString());

												} else {
													if (sponsored_adv_on_off) {
														if (send_sponsored_adv_on_off) {

															String date = date();
															mDatasourceHandler
																	.insertChathistory(
																			msg.getPacketID(),
																			"username",
																			toJID,
																			fromJID,
																			objMainList
																					.toString(),
																			time,
																			"1",
																			"0",
																			date);
															String timestamp = timestamp();
															boolean flag = mDatasourceHandler
																	.UpdateRecentReceive(
																			RECENTTOJID,
																			RECENTFROMJID,
																			objMainList
																					.toString(),
																			time,
																			date,
																			timestamp,
																			"0");
															Log.e("SENDING9 flag:",
																	"" + flag);
															if (!flag) {
																mDatasourceHandler
																		.insertrecentMessage(
																				objMainList
																						.toString(),
																				RECENTFROMJID,
																				RECENTTOJID,
																				"",
																				getfrdsname,
																				date,
																				time,
																				timestamp,
																				"0");
															}
															messgaepacketid_notification = msg
																	.getPacketID();
															time_notification = time;
															timestamp_notification = timestamp;
															localdatabase_notification = objMainList
																	.toString();
															date_notification = date;

															if (notification_on_or_off != null) {
																if (notification_on_or_off
																		.equals("true")) {
																	retrieveState_mode(
																			availability
																					.getMode(),
																			availability
																					.isAvailable(),
																			objMainList
																					.toString());
																}
															}
															// retrieveState_mode(
															// availability
															// .getMode(),
															// availability
															// .isAvailable(),
															// objMainList
															// .toString());
															// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
															// fromJID, toJID,
															// "",
															// getfrdsname,
															// date, time,
															// timestamp);

														} else {
															// need code here
															String date = date();
															mDatasourceHandler
																	.insertChathistory(
																			msg.getPacketID(),
																			"username",
																			toJID,
																			fromJID,
																			objMainList
																					.toString(),
																			time,
																			"0",
																			"0",
																			date);
															String timestamp = timestamp();
															boolean flag = mDatasourceHandler
																	.UpdateRecentReceive(
																			RECENTTOJID,
																			RECENTFROMJID,
																			objMainList
																					.toString(),
																			time,
																			date,
																			timestamp,
																			"0");
															Log.e("SENDING10 flag:",
																	"" + flag);
															if (!flag) {
																mDatasourceHandler
																		.insertrecentMessage(
																				objMainList
																						.toString(),
																				RECENTFROMJID,
																				RECENTTOJID,
																				"",
																				getfrdsname,
																				date,
																				time,
																				timestamp,
																				"0");
															}
															messgaepacketid_notification = msg
																	.getPacketID();
															time_notification = time;
															timestamp_notification = timestamp;
															localdatabase_notification = objMainList
																	.toString();
															date_notification = date;

															if (notification_on_or_off != null) {
																if (notification_on_or_off
																		.equals("true")) {
																	retrieveState_mode(
																			availability
																					.getMode(),
																			availability
																					.isAvailable(),
																			objMainList
																					.toString());
																}
															}
															// retrieveState_mode(
															// availability
															// .getMode(),
															// availability
															// .isAvailable(),
															// objMainList
															// .toString());
														}

													} else {
														// need code here
														String date = date();
														mDatasourceHandler
																.insertChathistory(
																		msg.getPacketID(),
																		"username",
																		toJID,
																		fromJID,
																		objMainList
																				.toString(),
																		time,
																		"0",
																		"0",
																		date);
														String timestamp = timestamp();
														boolean flag = mDatasourceHandler
																.UpdateRecentReceive(
																		RECENTTOJID,
																		RECENTFROMJID,
																		objMainList
																				.toString(),
																		time,
																		date,
																		timestamp,
																		"0");
														Log.e("SENDING10 flag:",
																"" + flag);
														if (!flag) {
															mDatasourceHandler
																	.insertrecentMessage(
																			objMainList
																					.toString(),
																			RECENTFROMJID,
																			RECENTTOJID,
																			"",
																			getfrdsname,
																			date,
																			time,
																			timestamp,
																			"0");
														}
														messgaepacketid_notification = msg
																.getPacketID();
														time_notification = time;
														timestamp_notification = timestamp;
														localdatabase_notification = objMainList
																.toString();
														date_notification = date;

														if (notification_on_or_off != null) {
															if (notification_on_or_off
																	.equals("true")) {
																retrieveState_mode(
																		availability
																				.getMode(),
																		availability
																				.isAvailable(),
																		objMainList
																				.toString());
															}
														}
														// retrieveState_mode(availability
														// .getMode(),
														// availability
														// .isAvailable(),
														// objMainList.toString());

													}
												}
											} else {
												if (sponsored_adv_on_off) {
													if (send_sponsored_adv_on_off) {

														String date = date();
														mDatasourceHandler
																.insertChathistory(
																		msg.getPacketID(),
																		"username",
																		toJID,
																		fromJID,
																		objMainList
																				.toString(),
																		time,
																		"1",
																		"0",
																		date);
														String timestamp = timestamp();
														boolean flag = mDatasourceHandler
																.UpdateRecentReceive(
																		RECENTTOJID,
																		RECENTFROMJID,
																		objMainList
																				.toString(),
																		time,
																		date,
																		timestamp,
																		"0");
														Log.e("SENDING9 flag:",
																"" + flag);
														if (!flag) {
															mDatasourceHandler
																	.insertrecentMessage(
																			objMainList
																					.toString(),
																			RECENTFROMJID,
																			RECENTTOJID,
																			"",
																			getfrdsname,
																			date,
																			time,
																			timestamp,
																			"0");
														}
														messgaepacketid_notification = msg
																.getPacketID();
														time_notification = time;
														timestamp_notification = timestamp;
														localdatabase_notification = objMainList
																.toString();
														date_notification = date;

														if (notification_on_or_off != null) {
															if (notification_on_or_off
																	.equals("true")) {
																retrieveState_mode(
																		availability
																				.getMode(),
																		availability
																				.isAvailable(),
																		objMainList
																				.toString());
															}
														}
														// retrieveState_mode(
														// availability
														// .getMode(),
														// availability
														// .isAvailable(),
														// objMainList
														// .toString());
														// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
														// fromJID, toJID, "",
														// getfrdsname,
														// date, time,
														// timestamp);

													} else {
														// need code here
														String date = date();
														mDatasourceHandler
																.insertChathistory(
																		msg.getPacketID(),
																		"username",
																		toJID,
																		fromJID,
																		objMainList
																				.toString(),
																		time,
																		"0",
																		"0",
																		date);
														String timestamp = timestamp();
														boolean flag = mDatasourceHandler
																.UpdateRecentReceive(
																		RECENTTOJID,
																		RECENTFROMJID,
																		objMainList
																				.toString(),
																		time,
																		date,
																		timestamp,
																		"0");
														Log.e("SENDING10 flag:",
																"" + flag);
														if (!flag) {
															mDatasourceHandler
																	.insertrecentMessage(
																			objMainList
																					.toString(),
																			RECENTFROMJID,
																			RECENTTOJID,
																			"",
																			getfrdsname,
																			date,
																			time,
																			timestamp,
																			"0");
														}
														messgaepacketid_notification = msg
																.getPacketID();
														time_notification = time;
														timestamp_notification = timestamp;
														localdatabase_notification = objMainList
																.toString();
														date_notification = date;

														if (notification_on_or_off != null) {
															if (notification_on_or_off
																	.equals("true")) {
																retrieveState_mode(
																		availability
																				.getMode(),
																		availability
																				.isAvailable(),
																		objMainList
																				.toString());
															}
														}
														// retrieveState_mode(
														// availability
														// .getMode(),
														// availability
														// .isAvailable(),
														// objMainList
														// .toString());
													}

												} else {
													// need code here
													String date = date();
													mDatasourceHandler.insertChathistory(
															msg.getPacketID(),
															"username",
															toJID,
															fromJID,
															objMainList
																	.toString(),
															time, "0", "0",
															date);
													String timestamp = timestamp();
													boolean flag = mDatasourceHandler
															.UpdateRecentReceive(
																	RECENTTOJID,
																	RECENTFROMJID,
																	objMainList
																			.toString(),
																	time, date,
																	timestamp,
																	"0");
													Log.e("SENDING10 flag:", ""
															+ flag);
													if (!flag) {
														mDatasourceHandler
																.insertrecentMessage(
																		objMainList
																				.toString(),
																		RECENTFROMJID,
																		RECENTTOJID,
																		"",
																		getfrdsname,
																		date,
																		time,
																		timestamp,
																		"0");
													}
													messgaepacketid_notification = msg
															.getPacketID();
													time_notification = time;
													timestamp_notification = timestamp;
													localdatabase_notification = objMainList
															.toString();
													date_notification = date;

													if (notification_on_or_off != null) {
														if (notification_on_or_off
																.equals("true")) {
															retrieveState_mode(
																	availability
																			.getMode(),
																	availability
																			.isAvailable(),
																	objMainList
																			.toString());
														}
													}
													// retrieveState_mode(availability
													// .getMode(),
													// availability
													// .isAvailable(),
													// objMainList.toString());

												}
											}
											// closed

										}

									} else {
										if (mStringOnlyOneSelectPromoId
												.equals("0")) {
											String date = date();
											mDatasourceHandler.insertChathistory(
													msg.getPacketID(),
													"username", toJID, fromJID,
													objMainList.toString(),
													time, "0", "0", date);
											String timestamp = timestamp();
											boolean flag = mDatasourceHandler
													.UpdateRecentReceive(
															RECENTTOJID,
															RECENTFROMJID,
															objMainList
																	.toString(),
															time, date,
															timestamp, "0");
											Log.e("SENDING11 flag:", "" + flag);
											if (!flag) {
												mDatasourceHandler.insertrecentMessage(
														objMainList.toString(),
														RECENTFROMJID,
														RECENTTOJID, "",
														getfrdsname, date,
														time, timestamp, "0");
											}
											messgaepacketid_notification = msg
													.getPacketID();
											time_notification = time;
											timestamp_notification = timestamp;
											localdatabase_notification = objMainList
													.toString();
											date_notification = date;
											if (notification_on_or_off != null) {
												if (notification_on_or_off
														.equals("true")) {
													retrieveState_mode(
															availability
																	.getMode(),
															availability
																	.isAvailable(),
															objMainList
																	.toString());
												}
											}
											// retrieveState_mode(
											// availability.getMode(),
											// availability.isAvailable(),
											// objMainList.toString());
											// sinle bubble visible and gone
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											} else {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											}
											// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
											// fromJID, toJID, "", getfrdsname,
											// date, time, timestamp);
										} else {
											String date = date();
											mDatasourceHandler.insertChathistory(
													msg.getPacketID(),
													"username", toJID, fromJID,
													objMainList.toString(),
													time, "0", "0", date);
											String timestamp = timestamp();
											boolean flag = mDatasourceHandler
													.UpdateRecentReceive(
															RECENTTOJID,
															RECENTFROMJID,
															objMainList
																	.toString(),
															time, date,
															timestamp, "0");
											Log.e("SENDING12 flag:", "" + flag);
											if (!flag) {
												mDatasourceHandler.insertrecentMessage(
														objMainList.toString(),
														RECENTFROMJID,
														RECENTTOJID, "",
														getfrdsname, date,
														time, timestamp, "0");
											}
											messgaepacketid_notification = msg
													.getPacketID();
											time_notification = time;
											timestamp_notification = timestamp;
											localdatabase_notification = objMainList
													.toString();
											date_notification = date;
											if (notification_on_or_off != null) {
												if (notification_on_or_off
														.equals("true")) {
													retrieveState_mode(
															availability
																	.getMode(),
															availability
																	.isAvailable(),
															objMainList
																	.toString());
												}
											}
											// retrieveState_mode(
											// availability.getMode(),
											// availability.isAvailable(),
											// objMainList.toString());
											// sinle bubble visible and gone
											if (mStringOnlyOneSelectPromoId
													.equals("0")) {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											} else {
												mImageView_adv_delete_icon
														.setVisibility(View.GONE);
												mTextView_text_adv_changed
														.setVisibility(View.GONE);
												mImageView_adv_image
														.setVisibility(View.GONE);
												mStringOnlyOneSelectPromoId = "0";
											}
											// mDatasourceHandler.insertrecentMessage(objMainList.toString(),
											// fromJID, toJID, "", getfrdsname,
											// date, time, timestamp);
										}

									}

									// retrieveState_mode(
									// availability.getMode(),
									// availability.isAvailable(),
									// objMainList.toString());
								}

							}
						}

					} else {

						// new login_Existing_User().execute();

						ConnectionLost.showDialog(ChatScreen.this,
								"Waiting for Newtwork",
								"Please check your internet connection.", null,
								"OK");

						// LogMessage.showDialog(ChatScreen.this,
						// "Waiting for Newtwork",
						// "Please check your internet connection.", null,
						// "Ok");

					}

				} else {
					ConnectionLost.showDialog(ChatScreen.this,
							"Waiting for Newtwork",
							"Please check your internet connection.", null,
							"OK");
					// LogMessage.showDialog(ChatScreen.this,
					// "Waiting for Network",
					// "Please Check your internet connection.", null,
					// "Ok");
				}
				//
			}
		});

		Intent intent = getIntent();
		getfriend = intent.getStringExtra("Chat_name_select");
		number = intent.getStringExtra("Chat_no_select");
		if (number.length() >= 10) {
			number = number.substring(number.length() - 10);
		} else {
			// number=number;
		}
		// Log.e("number: ", "" + number);
		mTextViewTitle.setText(getfriend.split("\\@")[0]);
		getfrdsname = mTextViewTitle.getText().toString();
		username_notification = mTextViewTitle.getText().toString();

		// xmpp work

		connectivity_Manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		Connection.DEBUG_ENABLED = true;

		mStringPhone = sharedPreferences.getString(Constant.PHONENO, "");

		// Connection_Pool connection_Pool = Connection_Pool.getInstance();
		// connection = connection_Pool.getConnection();
		// setConnection(connection);

		if (mConnectionDetector.isConnectingToInternet()) {
			new ChatHistory().execute();
		} else {
			LogMessage.showDialog(ChatScreen.this, null,
					"No Internet Connection", null, "Ok");
		}

		mImageViewProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mConnectionDetector.isConnectingToInternet()) {
					selectImage();
				} else {
					LogMessage.showDialog(ChatScreen.this, null,
							"No Internet Connection", null, "Ok");
				}
			}
		});

		mImageView_adv_delete_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mImageView_adv_delete_icon.setVisibility(View.GONE);
				mTextView_text_adv_changed.setVisibility(View.GONE);
				mImageView_adv_image.setVisibility(View.GONE);
				mStringOnlyOneSelectPromoId = "0";

			}
		});
		mString_Override = sharedPreferences.getString(
				Constant.BUBBLE_SET_OVERRIDE_FIXED, "");
		// } catch (Exception ex) {
		// Log.e("", "CRASH ON CREATE");
		// /}

		// notification value

		from_notification = mStringPhone;
		to_notification = number;

	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public void promodata(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);
			// Log.e("mString_phone_no", id + "");
			// Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public void Personal_promo_data(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);
			// Log.e("mString_phone_no", id + "");
			// Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public void test_promo_data(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();
			imageLoader.displayImage(promo_image, mImageViewProTop, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									activity, R.anim.fade_in);

						}
					});
			mTextViewChanged.setText(promo_name);

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public void testpromodata(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();
			imageLoader.displayImage(promo_image, mImageView_adv_image,
					options, new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									activity, R.anim.fade_in);

						}
					});
			mTextView_text_adv_changed.setText(promo_name);

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public void Lstpromodata(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();
			mTextViewChanged.setText(promo_name);

			imageLoader.displayImage(promo_image, mImageViewProTop, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									activity, R.anim.fade_in);
							// imgViewFlag.setAnimation(anim);
							// anim.start();
						}
					});

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public void NEW_SPONSORED_PERSONAL_ONLY() {
		String id = null;
		if (newmArrayList_promo_uniqueid_personal.size() == 0) {
			Log.e("", "Nothing");
			send_sponsored_adv_on_off = false;
		} else {
			Collections.shuffle(newmArrayList_promo_uniqueid_personal);

			for (int i = 0; i < newmArrayList_promo_uniqueid_personal.size(); i++) {
				id = newmArrayList_promo_uniqueid_personal.get(i);

			}
			Log.e("id:", "" + id);
			try {

				mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

			} catch (Exception e) {

				e.printStackTrace();
			}
			Log.e("mCursor.getCount():", "" + mCursor.getCount());
			if (mCursor.getCount() != 0) {
				promoid = mCursor.getString(0).trim();
				promo_image = mCursor.getString(4).trim();
				promo_name = mCursor.getString(5).trim();

				if (sponsored_adv_on_off) {

					image_adv_off_OFF.setVisibility(View.GONE);
					image_adv_show_OFF_OFF.setVisibility(View.GONE);
					image_OFF_adv_show_OFF.setVisibility(View.VISIBLE);
					text_changed_OFF.setVisibility(View.VISIBLE);
					image_adv_on_OFF.setVisibility(View.VISIBLE);

					// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

					imageLoader.displayImage(promo_image,
							image_OFF_adv_show_OFF, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
					// text_changed_OFF.setText(promo_name);
				} else {

					image_OFF_adv_show_OFF.setVisibility(View.GONE);
					image_adv_on_OFF.setVisibility(View.GONE);
					image_adv_off_OFF.setVisibility(View.VISIBLE);
					text_changed_OFF.setVisibility(View.VISIBLE);
					image_adv_show_OFF_OFF.setVisibility(View.VISIBLE);

					// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

					imageLoader.displayImage(promo_image, image_adv_off_OFF,
							options, new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});

				}

				// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

				text_changed_OFF.setText(promo_name);

				// mTextViewChanged.setText(promo_name);
				//

			} else {
				// Log.e("", "Nothing data show");
			}
		}

	}

	public void NEW_SPONSORED(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();

			if (sponsored_adv_on_off) {

				image_adv_off_OFF.setVisibility(View.GONE);
				image_adv_show_OFF_OFF.setVisibility(View.GONE);
				image_OFF_adv_show_OFF.setVisibility(View.VISIBLE);
				text_changed_OFF.setVisibility(View.VISIBLE);
				image_adv_on_OFF.setVisibility(View.VISIBLE);

				// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

				imageLoader.displayImage(promo_image, image_OFF_adv_show_OFF,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils.loadAnimation(
										activity, R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});
			} else {

				image_OFF_adv_show_OFF.setVisibility(View.GONE);
				image_adv_on_OFF.setVisibility(View.GONE);
				image_adv_off_OFF.setVisibility(View.VISIBLE);
				text_changed_OFF.setVisibility(View.VISIBLE);
				image_adv_show_OFF_OFF.setVisibility(View.VISIBLE);

				// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

				imageLoader.displayImage(promo_image, image_adv_off_OFF,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils.loadAnimation(
										activity, R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});

			}

			// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);

			text_changed_OFF.setText(promo_name);

			// mTextViewChanged.setText(promo_name);
			//

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public String historypromo_data(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = mCursor.getString(4).trim();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "";
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public String countcheck(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public String count_check_update(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA_Update(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	private void addEntry(String userName) {
		try {

			Presence subscribe = new Presence(Presence.Type.available);
			subscribe.setTo(userName);
			connection.sendPacket(subscribe);
			// Log.e("subscribe", "" + subscribe);
			// Log.e("userName", "" + userName);
		} catch (Exception e) {
			// Log.e("tag", "unable to add contact: ", e);
		}

	}

	private void retrieveState_mode(Mode mode, boolean available, String string) {
		// TODO Auto-generated method stub
		int userState = 0;
		/** 0 for offline, 1 for online, 2 for away,3 for busy */
		if (mode == Mode.dnd) {
			// /Log.e("Busy", "Busy" + mode);
			userState = 3;
		} else if (mode == Mode.away || mode == Mode.xa) {
			// Log.e("away", "away");
			userState = 2;
		} else if (available) {
			Log.e("online", "online" + mode);
			Toast.makeText(getApplicationContext(),
					getfriend.split("\\@")[0] + "is    Online", 5000).show();
			userState = 1;
		} else {
			Log.e("offline", "offline" + mode);

			new ExecuteChatNotification().execute();
			// JsonParserConnector.new_post_ChatNotificaton(to, from, message,
			// fromjid, tojid, time, timestamp, date, localdatabase,
			// messagepacketid, memberid, Contactuniqueid, receiveornot,
			// bubblecolor, fontsize, string, profileimage)

		}
	}

	private String getDate(long timeStampStr) {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {
			// Log.e("timeStampStr: ", "" + timeStampStr);

			Date date = new Date(timeStampStr * 1000); // *1000 is to convert
			// seconds to
			// milliseconds
			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

			// the format
			// of your
			// date

			// sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
			// give a
			// timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
															// reference for
															// formating (see
															// comment at the
															// bottom

			String format = sdf.format(date);
			// Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	// shuffle images on click
	public void shuffleArray(int[] ar) {
		// TODO Auto-generated method stub

		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;

		}

	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			// s.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			// /Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public class ChatHistory extends AsyncTask<String, Void, String> {
		public ChatHistory() {
		}

		@Override
		protected String doInBackground(String... params) {
			// Log.e("ME mStringPhone: ", "" + mStringPhone);
			// Log.e("USED SELECTED number: ", "" + number);
			String result = JsonParserConnector.ChatHistory("imgr",
					mStringPhone, number);

			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			try {
				Log.e("result: ", "" + result);
				Chat_Store(result);
			} catch (Exception e) {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ChatScreen.this);
		}

	}

	public void Chat_Store(String strJson) {

		try {
			// /mArrayDATE = new ArrayList<String>();
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = null;
			try {
				jsonOBject = new JSONObject(strJson);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (jsonOBject.getBoolean("success")) {
					NamedeviceToken = jsonOBject.getString("devicetoken")
							+ Constant.XMPP_DNS_Name;

					// Log.e("NamedeviceToken: ", "" + NamedeviceToken);
					s = NamedeviceToken.replace(Constant.XMPP_DNS_Name, "");
					Log.e("jsonOBject.getString(): ",
							"" + jsonOBject.getString("color"));
					addEntry(NamedeviceToken);
					Roster roster = connection.getRoster();
					Presence availability = roster.getPresence(NamedeviceToken);

					Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.manual);

					// String jid=userName+Constants.ADVANTLPC__SMACK;
					try {
						roster.createEntry(NamedeviceToken, NamedeviceToken,
								null);
					} catch (XMPPException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// Log.e("Availability: ", "" + availability);
					// try {
					// card.load(connection, NamedeviceToken);
					// } catch (XMPPException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					// Log.e("mString_Override: ", "" + mString_Override);
					String mStringbubblecolor = null;
					if (mString_Override != null) {
						if (mString_Override.equals("true")) {
							mStringbubblecolor = "1";
							shared_Prefernces("blue");
						} else {
							if (jsonOBject.has("color")) {
								mStringbubblecolor = jsonOBject
										.getString("color");
							}
							if (mStringbubblecolor != null) {
								if (mStringbubblecolor.equals("1")) {
									shared_Prefernces("blue");
								} else if (mStringbubblecolor.equals("2")) {
									shared_Prefernces("green");
								} else if (mStringbubblecolor.equals("3")) {
									shared_Prefernces("purple");
								} else if (mStringbubblecolor.equals("0")) {
									shared_Prefernces("blue");
								}

							} else {
								shared_Prefernces("blue");
							}
						}

					} else {
						if (jsonOBject.has("color")) {
							mStringbubblecolor = jsonOBject.getString("color");
						}
						// String mStringbubblecolor =
						// card.getField("Bubble_Color");

						if (mStringbubblecolor != null) {
							if (mStringbubblecolor.equals("1")) {
								shared_Prefernces("blue");
							} else if (mStringbubblecolor.equals("2")) {
								shared_Prefernces("green");
							} else if (mStringbubblecolor.equals("3")) {
								shared_Prefernces("purple");
							} else if (mStringbubblecolor.equals("0")) {
								shared_Prefernces("blue");
							}

						} else {
							shared_Prefernces("blue");
						}
					}
					// Log.e("VALUE mStringbubblecolor: ", "" +
					// mStringbubblecolor);

					// Log.e("chat_list_adapter chat_list_adapter: ",
					// "1 CALLING");
					// chat_list_adapter
					// .registerDataSetObserver(new DataSetObserver() {
					// @Override
					// public void onChanged() {
					// super.onChanged();
					// lv.setSelection(chat_list_adapter.getCount() - 1);
					// }
					// });
					// Log.e("chat_list_adapter chat_list_adapter: ",
					// "2 CALLING");
					// / closed=======
					if (jsonOBject.has("from")) {
						fromJID = jsonOBject.getString("from");
						RECENTTOJID = jsonOBject.getString("from");
					}
					if (jsonOBject.has("to")) {
						toJID = jsonOBject.getString("to");
						RECENTFROMJID = jsonOBject.getString("to");
					}
					Log.e("DATABSE fromJID: ", "" + fromJID);
					// Log.e("DATABSE toJID: ", "" + toJID);
					fromjid_notification = fromJID;
					tojid_notification = toJID;
					recentfromjid_notification = RECENTFROMJID;
					recenttojid_notification = RECENTTOJID;

					try {
						mDatasourceHandler.UpdateRecentReceiveCount(
								RECENTTOJID, RECENTFROMJID);
					} catch (Exception e) {
						Log.e("UPDATE TIME:", "" + e.getMessage());
					}
					// database fetch working
					try {
						mCursor = mDatasourceHandler.FetchChatHistory(toJID,
								fromJID);
						Log.e("Ans Chat history count: ",
								"" + mCursor.getCount());
					} catch (Exception e) {

						e.printStackTrace();
					}

					if (mCursor.getCount() != 0) {
						mArrayList_uniqueid.clear();
						mArrayList_ToJid.clear();
						mArrayList_FromJid.clear();
						mArrayList_Time.clear();
						mArrayList_Message.clear();
						mArrayList_receive.clear();
						mArrayList_promo_not.clear();
						mArrayDATE.clear();
						do {

							mArrayList_uniqueid
									.add(mCursor.getString(0).trim());

							mArrayList_ToJid.add(mCursor.getString(2).trim());

							mArrayList_FromJid.add(mCursor.getString(3).trim());

							mArrayList_Message.add(mCursor.getString(4).trim());

							mArrayList_Time.add(mCursor.getString(5).trim());

							mArrayList_receive.add(mCursor.getString(6).trim());
							mArrayDATE.add(mCursor.getString(9).trim());

							// mArrayList_promo_not.add(mCursor.getString(7).trim());
							// /Log.e("mCursor.getString(7).trim():", "" +
							// mCursor.getString(7).trim());

						} while (mCursor.moveToNext());

						mCursor.close();
					}
					// Log.e("mArrayDATE:", "" + mArrayDATE);
					// Log.e("mArrayList_uniqueid:", "" + mArrayList_uniqueid);
					// Log.e("mArrayList_ToJid:", "" + mArrayList_ToJid);
					// Log.e("mArrayList_FromJid:", "" + mArrayList_FromJid);
					// Log.e("mArrayList_Message:", "" + mArrayList_Message);
					// Log.e("mArrayList_Time:", "" + mArrayList_Time);
					// Log.e("mArrayList_receive:", "" + mArrayList_receive);
					// Log.e("mArrayList_promo_not:", "" +
					// mArrayList_promo_not);
					chat_list_adapter = new ChatAdapter(ChatScreen.this,
							messages);

					if (mArrayList_uniqueid.size() == 0) {

					} else {

						for (int i = 0; i < mArrayList_uniqueid.size(); i++) {

							if (NamedeviceToken.equals(mArrayList_FromJid
									.get(i) + Constant.XMPP_DNS_Name)
									|| NamedeviceToken.equals(mArrayList_ToJid
											.get(i) + Constant.XMPP_DNS_Name)) {
								String time = mArrayList_Time.get(i);
								String mStringReceive = mArrayList_receive
										.get(i);
								String date_sent = mArrayDATE.get(i);
								// String mStringpromo =
								// mArrayList_promo_not.get(i);
								// Log.e("mStringpromo: ", "" + mStringpromo);
								if (NamedeviceToken.equals(mArrayList_FromJid
										.get(i) + Constant.XMPP_DNS_Name)) {

									String mStringBody = mArrayList_Message
											.get(i);
									// Log.e("mStringBody: ", "" + mStringBody);
									JSONObject json_OBject = new JSONObject(
											mStringBody);
									// Log.e("HISTORY json_OBject: ", ""
									// + json_OBject);

									JSONArray mArray = json_OBject
											.getJSONArray("body");

									for (int j = 0; j < mArray.length(); j++) {
										JSONObject mJsonObject = mArray
												.getJSONObject(j);

										if (mJsonObject.getString("pushkey")
												.equals("image")) {

											String[] msg_Data = new String[6];
											msg_Data[0] = "";
											msg_Data[1] = "friend_pic";
											msg_Data[2] = "";
											msg_Data[3] = mJsonObject
													.getString("preview_Icon");
											msg_Data[4] = time;
											msg_Data[5] = date_sent;
											messages.add(msg_Data);
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
										} else {
											String[] msg_Data = new String[9];
											String promo_ID;
											if (mJsonObject
													.has("_promo_id_tag")) {
												promo_ID = mJsonObject
														.getString("_promo_id_tag");

											} else {
												promo_ID = "0";
											}

											// String
											// msendheader=promo_data_(mJsonObject
											// .getString("_promo_id_tag"));
											// Log.e("msendheader: ",
											// ""+msendheader);

											// String
											// link=promo_data_link(mJsonObject
											// .getString("_promo_id_tag"));
											// Log.e("msendheader: ",
											// ""+msendheader);
											// Log.e("test: ", "" + promo_ID);
											String test;
											if (promo_ID.equals("0")) {
												test = "0";
											} else {
												test = "";
											}
											String path = historypromo_data(promo_ID);
											Log.e("test: ", "" + promo_ID);
											msg_Data[3] = mJsonObject
													.getString("pushkey");
											msg_Data[1] = "friend";
											msg_Data[2] = "";
											msg_Data[0] = "";
											msg_Data[4] = time;
											msg_Data[5] = test;
											msg_Data[6] = path;
											msg_Data[7] = promo_ID;
											msg_Data[8] = date_sent;

											messages.add(msg_Data);
											// Log.e("COUNT:",
											// ""
											// + chat_list_adapter
											// .getCount());
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
										}
									}
								}

								else {
									// Log.e("", "more condition not matched");
									String mStringBody = mArrayList_Message
											.get(i);
									JSONObject json_OBject = new JSONObject(
											mStringBody);
									JSONArray mArray = json_OBject
											.getJSONArray("body");

									for (int j = 0; j < mArray.length(); j++) {
										JSONObject mJsonObject = mArray
												.getJSONObject(j);
										if (mJsonObject.getString("pushkey")
												.equals("image")) {
											String[] msg_Data = new String[7];
											msg_Data[4] = time;
											msg_Data[0] = "";
											msg_Data[1] = "pic";
											msg_Data[2] = "";
											msg_Data[3] = mJsonObject
													.getString("preview_Icon");
											msg_Data[6] = date_sent;
											messages.add(msg_Data);
											// Log.e("COUNT:",
											// ""
											// + chat_list_adapter
											// .getCount());
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
										} else {
											String promo_ID;
											if (mJsonObject
													.has("_promo_id_tag")) {
												promo_ID = mJsonObject
														.getString("_promo_id_tag");

											} else {
												promo_ID = "0";
											}
											String test;
											if (promo_ID.equals("0")) {
												test = "0";
											} else {
												test = "";
											}
											String path = historypromo_data(promo_ID);
											// Log.e("path: ",
											// "" + path);
											// int path =
											// R.drawable.ic_launcher;
											String[] msg_Data = new String[9];
											msg_Data[6] = time;
											msg_Data[5] = test;
											msg_Data[3] = "";
											msg_Data[1] = "my";
											msg_Data[2] = mStringReceive;
											msg_Data[0] = mJsonObject
													.getString("pushkey");
											msg_Data[4] = path;
											msg_Data[7] = promo_ID;
											msg_Data[8] = date_sent;

											messages.add(msg_Data);
											// Log.e("", "HERE");
											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}

											chat_list_adapter
													.notifyDataSetChanged();
											lv.setAdapter(chat_list_adapter);
											// Log.e("", "LAST HERE");
										}

									}

								}

							} else {

							}
						}
					}

				} else {
					NamedeviceToken = jsonOBject.getString("devicetoken")
							+ Constant.XMPP_DNS_Name;

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) { // TODO: handle exception
			Log.e("CRASh: ", "" + e.getMessage());
			Log.e("CAUSE: ", "" + e.getCause());
			Log.e("String: ", "" + e.toString());
			Log.e("getLocalizedMessage: ", "" + e.getLocalizedMessage());

			ConnectionLost.showDialog(ChatScreen.this, null,
					"Chat history not show due to problem in connection.",
					null, "OK");
			Log.e("CRASh: ", "" + e.getMessage());
		}

	}

	public void ChatStore(String strJson) {

		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				NamedeviceToken = jsonOBject.getString("userdevicetoken")
						+ Constant.XMPP_DNS_Name;

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					// ChatStore mContact = new ChatStore();
					if (NamedeviceToken.equals(jsonOBjectContact
							.getString("fromJID"))
							|| NamedeviceToken.equals(jsonOBjectContact
									.getString("toJID"))) {
						if (NamedeviceToken.equals(jsonOBjectContact
								.getString("fromJID"))) {
							long l = Long.parseLong(jsonOBjectContact
									.getString("sentDate"));
							String time = getDate(l);
							JSONArray mArray = jsonOBjectContact
									.getJSONArray("body");
							for (int j = 0; j < mArray.length(); j++) {
								JSONObject mJsonObject = mArray
										.getJSONObject(j);

								if (mJsonObject.getString("pushkey").equals(
										"image")) {

									String[] msg_Data = new String[5];
									msg_Data[0] = time;
									msg_Data[1] = "friend_pic";
									msg_Data[2] = "0";
									msg_Data[3] = mJsonObject
											.getString("preview_Icon");
									msg_Data[4] = time;
									messages.add(msg_Data);
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
								} else {
									String[] msg_Data = new String[5];
									msg_Data[3] = mJsonObject
											.getString("pushkey");
									msg_Data[1] = "friend";
									msg_Data[2] = "0";
									msg_Data[0] = "";
									msg_Data[4] = time;

									messages.add(msg_Data);
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
								}

								// mHashMap.put("my",
								// /
								// mJsonObject.getString("_message_body_tag"));
							}
						} else {

							JSONArray mArray = jsonOBjectContact
									.getJSONArray("body");
							long l = Long.parseLong(jsonOBjectContact
									.getString("sentDate"));
							String time = getDate(l);
							for (int j = 0; j < mArray.length(); j++) {
								JSONObject mJsonObject = mArray
										.getJSONObject(j);
								if (mJsonObject.getString("pushkey").equals(
										"image")) {
									String[] msg_Data = new String[5];
									msg_Data[4] = time;
									msg_Data[0] = "";
									msg_Data[1] = "pic";
									msg_Data[2] = "0";
									msg_Data[3] = mJsonObject
											.getString("preview_Icon");
									messages.add(msg_Data);
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
								} else {
									String path = historypromo_data(mJsonObject
											.getString("_promo_id_tag"));
									// promodata(mJsonObject
									// .getString("_promo_id_tag"));
									// int path = R.drawable.ic_launcher;
									String[] msg_Data = new String[6];
									msg_Data[5] = time;
									msg_Data[3] = "";
									msg_Data[1] = "my";
									msg_Data[2] = "0";
									msg_Data[0] = mJsonObject
											.getString("pushkey");
									msg_Data[4] = path;

									messages.add(msg_Data);
									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();
									lv.setAdapter(chat_list_adapter);
								}

								// mHashMap.put("friend",
								// mJsonObject.getString("_message_body_tag"));
							}

						}
					} else {

					}

				}

			} else {
				NamedeviceToken = jsonOBject.getString("userdevicetoken")
						+ Constant.XMPP_DNS_Name;
				if (NamedeviceToken.equals("null")) {

				} else {

				}

			}

		} catch (JSONException e) {

			// Log.e("", e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mImgrContacts.size());
		// Log.e(TAG, "mImgrContacts: " + mImgrContacts);
		if (mHashMap == null) {

		} else {
			if (chat_list_adapter.getCount() >= 5) {
				lv.setStackFromBottom(true);
			} else {
				lv.setStackFromBottom(false);
			}
			lv.setAdapter(chat_list_adapter);
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		adv_on_off = sharedPreferences.getBoolean(Constant.on_off_ads, false);
		mStringPersonalSetting = sharedPreferences.getString(
				Constant.PERSONAL_ADS, "");
		Log.e("adv_on_off: ", "" + adv_on_off);
		if (adv_on_off) {
			mTextViewChanged.setText("No Promo");
			mTextViewChanged.setTextColor(Color.parseColor("#000000"));
			if (check_count == 0) {
				imageLoader.displayImage(promo_image, mImageViewProTop,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils.loadAnimation(
										activity, R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});
				mImageViewProTop.setVisibility(View.GONE);
				mTextViewChanged.setVisibility(View.GONE);
			} else {
				// Log.e("promo_image:", promo_image + "");
				mImageViewProTop.setVisibility(View.VISIBLE);
				mTextViewChanged.setVisibility(View.VISIBLE);
				mTextViewChanged.setText(promo_name);
				imageLoader.displayImage(promo_image, mImageViewProTop,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils.loadAnimation(
										activity, R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});
			}
			image_adv_show_OFF.setVisibility(View.GONE);
			mImageViewAdv_off.setVisibility(View.GONE);
			mImageViewAdv_on.setVisibility(View.VISIBLE);

		} else {
			mImageViewAdv_on.setVisibility(View.GONE);
			mImageViewProTop.setVisibility(View.GONE);
			image_adv_show_OFF.setVisibility(View.VISIBLE);
			image_adv_show_OFF.setBackgroundResource(R.drawable.ic_off);
			mTextViewChanged.setText("Off");
			mTextViewChanged.setTextColor(Color.parseColor("#B1B1B1"));

			mImageViewAdv_off.setVisibility(View.VISIBLE);
			// Log.e("HERE", "HERE");

		}

		mStringPhoneid = getIntent().getStringExtra("phone_id");
		Log.e("mStringPhoneid: ", "" + mStringPhoneid);
		unique_id_notification_user = mStringPhoneid;
		mCursor = mDatasourceHandler.User_Set_Fetch(mStringPhoneid);
		if (mCursor.getCount() != 0) {

			do {

				setPromo = mCursor.getString(0).trim();
				setAutorotate = mCursor.getString(1).trim();
				setSponsoredads = mCursor.getString(2).trim();
				setpersonalads = mCursor.getString(3).trim();
				setuniqueid = mCursor.getString(4).trim();

			} while (mCursor.moveToNext());

			mCursor.close();

		}
		// Log.e("TEST: ", "1" );

		if (mStringAutorotate != null) {
			if (mStringAutorotate.equals("true")
					&& setAutorotate.equals("true")) {
				auto_rotate_Promo_OFF = true;

			} else {
				auto_rotate_Promo_OFF = false;
			}
		} else {
			auto_rotate_Promo_OFF = false;
		}
		Log.e("auto_rotate_Promo_OFF: ", "" + auto_rotate_Promo_OFF);
		if (mStringPromoOnOFF.equals("true") && setPromo.equals("true")) {
			// // code start
			// // new if and else
			if (mStringSponsored_BarChanged.equals("true")
					&& setSponsoredads.equals("true")) {
				mFrameLayout_adv_view_off.setVisibility(View.GONE);
				mFrameLayout_adv_view.setVisibility(View.VISIBLE);
			} else {

				// 2 Sept

				mFrameLayout_adv_view.setVisibility(View.GONE);
				mFrameLayout_adv_view_off.setVisibility(View.GONE);
				mFrameLayout_adv_view_OFF_OFF.setVisibility(View.VISIBLE);
			}
			// // code//

		} else {

			mFrameLayout_adv_view.setVisibility(View.GONE);
			mFrameLayout_adv_view_off.setVisibility(View.VISIBLE);

		}
		if (mStringPromoOnOFF != null) {
			Log.e("TEST: ", "4");
			if (mStringPromoOnOFF.equals("true") && setPromo.equals("true")) {
				Log.e("TEST: ", "5");
				if (mStringSponsored_BarChanged.equals("true")
						&& setSponsoredads.equals("true")) {
					if (Constant.PROMO_OFF_NOW_SELECT_ONE == 1) {
						promoid = Constant._ID;
						Log.e("promoid: ", "" + promoid);
						Lstpromodata(promoid);
						Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
						Constant._ID = "0";
					} else {
						Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
					}
				} else {
					Log.e("TEST: ", "here");
					if (Constant.SPONSORED_PROMO_OFF_ == 1) {
						Constant.SPONSORED_PROMO_OFF_ = 0;
						promoid = Constant.SPONSORED_PROMO_OFF_ID;
						Log.e("promoid: ", "" + promoid);
						NEW_SPONSORED(promoid);
						send_sponsored_adv_on_off = true;
						Constant.SPONSORED_PROMO_OFF_ID = "0";

					} else {

						if (mStringPersonalSetting.equals("true")
								&& setpersonalads.equals("true")) {
							Log.e("TEST: ", "6");
							send_sponsored_adv_on_off = true;
							NEW_SPONSORED_PERSONAL_ONLY();
						} else {
							Log.e("TEST: ", "enter here");
							send_sponsored_adv_on_off = false;
							// image_adv_off_OFF.setVisibility(View.VISIBLE);
							// image_adv_show_OFF_OFF.setVisibility(View.VISIBLE);
							// image_OFF_adv_show_OFF.setVisibility(View.GONE);
							// image_adv_on_OFF.setVisibility(View.GONE);
							// text_changed_OFF.setVisibility(View.GONE);
						}
					}

				}

			} else {
				Log.e("TEST: ", "7");
				if (Constant.PROMO_OFF_NOW_SELECT_ONE == 1) {
					Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
					mStringOnlyOneSelectPromoId = Constant._ID;
					Log.e("mStringOnlyOneSelectPromoId: ", ""
							+ mStringOnlyOneSelectPromoId);
					mImageView_adv_delete_icon.setVisibility(View.VISIBLE);
					mTextView_text_adv_changed.setVisibility(View.VISIBLE);
					mImageView_adv_image.setVisibility(View.VISIBLE);
					testpromodata(mStringOnlyOneSelectPromoId);
					Constant._ID = "0";
				} else {
					Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
				}
			}
		} else {
			Log.e("TEST: ", "TEST");
			if (Constant.PROMO_OFF_NOW_SELECT_ONE == 1) {
				Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
				mStringOnlyOneSelectPromoId = Constant._ID;
				Log.e("mStringOnlyOneSelectPromoId: ", ""
						+ mStringOnlyOneSelectPromoId);
				mImageView_adv_delete_icon.setVisibility(View.VISIBLE);
				mTextView_text_adv_changed.setVisibility(View.VISIBLE);
				mImageView_adv_image.setVisibility(View.VISIBLE);
				testpromodata(mStringOnlyOneSelectPromoId);
				Constant._ID = "0";
			} else {
				Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
			}
		}

		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();
		PingManager.getInstanceFor(connection);

		DeliveryReceiptManager delievery_Manager = DeliveryReceiptManager
				.getInstanceFor(connection);
		delievery_Manager.setAutoReceiptsEnabled(true);
		delievery_Manager.enableAutoReceipts();

		delievery_Manager
				.addReceiptReceivedListener(new ReceiptReceivedListener() {
					@Override
					public void onReceiptReceived(String fromJid, String toJid,
							String receiptId) {
						// Log.e("Chat_Screen message sent confirmation:",
						// fromJid
						// + ", " + toJid + ", " + receiptId);
						loop: for (String[] msg_Data : messages) {
							if (msg_Data[3].equals(receiptId)) {
								msg_Data[2] = "1";
								boolean flag = mDatasourceHandler
										.UpdateReceive(receiptId);
								// Log.e("Pinging", "on Resumeflag:  " + flag);
								// update query implement--------------
								// Log.v("Delivery", "" + msg_Data[2]);
								mHandler.post(new Runnable() {
									public void run() {
										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();

									}
								});
								break loop;
							}

						}

					}

				});
		try {

			try {

				mCursor = mDatasourceHandler.FetchContact(number);
				// Log.e("mString_phone_no", number + "");
				// Log.e("value in cursor at starting", mCursor.getCount() +
				// "");
			} catch (Exception e) {

				e.printStackTrace();
			}

			if (mCursor.getCount() != 0) {

				do {
					profileBase64 = mCursor.getString(7).trim();

				} while (mCursor.moveToNext());

				mCursor.close();
			}
			if (profileBase64.equals("Null")) {

			} else {
				Bitmap mBitmap = ConvertToImage(profileBase64);
				mImageViewPro.setImageBitmap(mBitmap);
			}
		} catch (Exception e) {

		}

	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	public void setConnection(final XMPPConnection connection) {

		this.connection = connection;
		try {

			if (connection != null) {

				// Add a packet listener to get messages sent to us
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {

					@SuppressWarnings("unused")
					public void processPacket(Packet packet) {

						message = (Message) packet;
						// Log.e("setConnection:", "setConnection");
						// Log.e("fromJID "
						// ," from " + fromJID);
						// Log.e(" toJID ",
						// " from " + toJID);

						String promoID = null;
						String mStringnewtojid = null;
						String mStringnewfromjid = null;
						JSONObject objMainList = null;
						String check = null;
						String check_update = null;
						String time = currenttime();
						Log.e("sssssssss: ", "" + s);
						if (message.getBody() != null) {
							String fromName = StringUtils
									.parseBareAddress(message.getFrom().split(
											"\\@")[0]);
							mStringnewtojid = fromJID;
							mStringnewfromjid = fromName;
							Log.e(" Text mStringnewtojid ", ""
									+ mStringnewtojid + " mStringnewfromjid "
									+ mStringnewfromjid);

							if (fromName.equals(s)) {
								// String Body = null;
								try {
									JSONObject jsonObj = new JSONObject(message
											.getBody());

									objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();

									JSONObject json = new JSONObject();

									if (jsonObj.has("_message_body_tag")) {
										personaljsonObj = jsonObj;

										promoID = jsonObj
												.getString("_promo_id_tag");
										String date_sent = date();
										check = countcheck(promoID);
										check_update = count_check_update(promoID);
										Log.e("promoID: ", "" + promoID);
										Log.e("check: ", "" + check);
										if (promoID != null) {
											Log.e("NOT NULLpromoID: ", ""
													+ promoID);
											if (promoID.equals("0")) {
												// boolean
												// flags=ExecuteNewPromo(promoID)
												String path = historypromo_data(promoID);
												String[] msg_Data = new String[9];
												msg_Data[0] = "";
												msg_Data[4] = time;
												msg_Data[1] = "friend";
												msg_Data[2] = "0";
												msg_Data[3] = jsonObj
														.getString("pushkey");
												msg_Data[5] = jsonObj
														.getString("_promo_id_tag");
												msg_Data[6] = path;
												msg_Data[7] = promoID;
												msg_Data[8] = date_sent;
												messages.add(msg_Data);

												try {

													json.put(
															"pushkey",
															jsonObj.getString("pushkey"));
													json.put(
															"_message_body_tag",
															jsonObj.getString("_message_body_tag"));
													json.put(
															"_promo_id_tag",
															jsonObj.getString("_promo_id_tag"));
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}

												mJsonArray.put(json);

												try {
													objMainList.put("body",
															mJsonArray);
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												//
											} else {

												if (check != null) {
													if (check.equals("0")) {
														Log.e("API CALLING: ",
																""
																		+ promoID
																		+ "---check==="
																		+ check);
														personal_api_time = time;
														personal_api_pushkey = jsonObj
																.getString("pushkey");
														personal_api_idtag = jsonObj
																.getString("_promo_id_tag");
														personal_jidto = mStringnewtojid;
														personal_jidfrom = mStringnewfromjid;
														Log.e("personal_api_time: ",
																""
																		+ personal_api_time);
														Log.e("personal_api_pushkey: ",
																""
																		+ personal_api_pushkey);
														Log.e("personal_api_idtag: ",
																""
																		+ personal_api_idtag);
														Log.e("personal_jidto: ",
																""
																		+ personal_jidto);
														Log.e("personal_jidfrom: ",
																""
																		+ personal_jidfrom);
														testpromo_id = promoID;
														new ExecuteNewPromo()
																.execute();
														return;
													} else {
														// update API Changes
														if (check_update != null) {
															if (!check_update
																	.equals("0")) {
																Log.e("Update API CALLING: ",
																		""
																				+ promoID
																				+ "---check==="
																				+ check);
																personal_api_time = time;
																personal_api_pushkey = jsonObj
																		.getString("pushkey");
																personal_api_idtag = jsonObj
																		.getString("_promo_id_tag");
																personal_jidto = mStringnewtojid;
																personal_jidfrom = mStringnewfromjid;
																Log.e("personal_api_time: ",
																		""
																				+ personal_api_time);
																Log.e("personal_api_pushkey: ",
																		""
																				+ personal_api_pushkey);
																Log.e("personal_api_idtag: ",
																		""
																				+ personal_api_idtag);
																Log.e("personal_jidto: ",
																		""
																				+ personal_jidto);
																Log.e("personal_jidfrom: ",
																		""
																				+ personal_jidfrom);
																testpromo_id = promoID;
																new ExecuteNewPromoUpdate()
																		.execute();
																return;
															}
														}
														// SEE this result
														Log.e("NOTAPI CALLING: ",
																""
																		+ promoID
																		+ "---check==="
																		+ check);
														String path = historypromo_data(promoID);
														String[] msg_Data = new String[9];
														msg_Data[0] = "";
														msg_Data[4] = time;
														msg_Data[1] = "friend";
														msg_Data[2] = "0";
														msg_Data[3] = jsonObj
																.getString("pushkey");
														msg_Data[5] = jsonObj
																.getString("_promo_id_tag");
														msg_Data[6] = path;
														msg_Data[7] = promoID;
														msg_Data[8] = date_sent;
														messages.add(msg_Data);

														try {

															json.put(
																	"pushkey",
																	jsonObj.getString("pushkey"));
															json.put(
																	"_message_body_tag",
																	jsonObj.getString("_message_body_tag"));
															json.put(
																	"_promo_id_tag",
																	jsonObj.getString("_promo_id_tag"));
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}

														mJsonArray.put(json);

														try {
															objMainList.put(
																	"body",
																	mJsonArray);
														} catch (JSONException e) {
															// TODO
															// Auto-generated
															// catch
															// block
															e.printStackTrace();
														}
													}
												}

											}
										} else {
											// boolean
											// flags=ExecuteNewPromo(promoID)
											String path = historypromo_data(promoID);
											String[] msg_Data = new String[9];
											msg_Data[0] = "";
											msg_Data[4] = time;
											msg_Data[1] = "friend";
											msg_Data[2] = "0";
											msg_Data[3] = jsonObj
													.getString("pushkey");
											msg_Data[5] = jsonObj
													.getString("_promo_id_tag");
											msg_Data[6] = path;
											msg_Data[7] = promoID;
											msg_Data[8] = date_sent;
											messages.add(msg_Data);

											try {

												json.put("pushkey", jsonObj
														.getString("pushkey"));
												json.put(
														"_message_body_tag",
														jsonObj.getString("_message_body_tag"));
												json.put(
														"_promo_id_tag",
														jsonObj.getString("_promo_id_tag"));
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}

											mJsonArray.put(json);

											try {
												objMainList.put("body",
														mJsonArray);
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											//
										}

									} else {

										String date_sent = date();
										String[] msg_Data = new String[6];
										msg_Data[0] = "";
										msg_Data[4] = time;
										msg_Data[1] = "friend_pic";
										msg_Data[2] = "0";
										msg_Data[5] = date_sent;
										msg_Data[3] = jsonObj
												.getString("preview_Icon");

										messages.add(msg_Data);

										promoID = "0";

										try {

											json.put("pushkey", "image");

											json.put("_promo_id_tag", promoID);
											json.put("preview_Icon", jsonObj
													.getString("preview_Icon"));

										} catch (Exception e) {

										}
										//
										// Log.e("message send:  ",
										// "" + json.toString());

										mJsonArray.put(json);

										// Finally add item arrays for "A" and
										// "B"
										// to
										// main list with key
										try {
											objMainList.put("body", mJsonArray);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										// Log.e("objMainList: ",
										// "" + objMainList.toString());

										// mStringnewtojid = fromJID;
										// mStringnewfromjid = fromName;

									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								Log.e("mStringnewtojid: ", "" + mStringnewtojid);
								Log.e("mStringnewfromjid: ", ""
										+ mStringnewfromjid);
								// personal_jidto = mStringnewtojid;
								// personal_jidfrom = mStringnewfromjid;
								Log.e("SEEEEEEEEEEEEEEE", "SEEEEEEEEEEEEEEEEE");
								String date = date();
								mDatasourceHandler.insertChathistory(
										message.getPacketID(), "username",
										mStringnewtojid, mStringnewfromjid,
										objMainList.toString(), time, promoID,
										"0", date);
								String timestamp = timestamp();
								boolean flag = mDatasourceHandler
										.UpdateRecentReceive(mStringnewtojid,
												mStringnewfromjid,
												objMainList.toString(), time,
												date, timestamp, "0");
								Log.e("RECEIVE 1 flag:", "" + flag);
								if (!flag) {
									mDatasourceHandler.insertrecentMessage(
											objMainList.toString(),
											mStringnewfromjid, mStringnewtojid,
											"", getfrdsname, date, time,
											timestamp, "0");
								}

								mStringnewtojid = null;
								mStringnewfromjid = null;
								// Add the incoming message to the list view
								mHandler.post(new Runnable() {
									public void run() {
										// Log.e("Set connection COUNT:", ""
										// + chat_list_adapter.getCount());
										if (chat_list_adapter.getCount() >= 5) {
											lv.setStackFromBottom(true);
										} else {
											lv.setStackFromBottom(false);
										}
										chat_list_adapter
												.notifyDataSetChanged();
										lv.setAdapter(chat_list_adapter);
									}
								});

							}
							//
							else {
								Log.e("", "CHECK");
								promoID = null;
								try {
									JSONObject jsonObj = new JSONObject(message
											.getBody());

									objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();

									JSONObject json = new JSONObject();
									Log.e("jsonObj: ", "" + jsonObj);

									if (jsonObj.has("_message_body_tag")) {
										Log.e(" ", "MESSAGE");
										promoID = jsonObj
												.getString("_promo_id_tag");
										if (promoID != null) {
											Log.e(" promoID", "promoID: "
													+ promoID);
											if (promoID.equals("0")) {
												Log.e(" promoID 0", "promoID 0");
												try {

													json.put(
															"pushkey",
															jsonObj.getString("pushkey"));
													json.put(
															"_message_body_tag",
															jsonObj.getString("_message_body_tag"));
													json.put(
															"_promo_id_tag",
															jsonObj.getString("_promo_id_tag"));
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}

												mJsonArray.put(json);

												try {
													objMainList.put("body",
															mJsonArray);
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												//

											}

											else {
												check = countcheck(promoID);
												check_update = count_check_update(promoID);
												// api calling
												Log.e("api calling: ", ""
														+ check);
												if (check != null) {
													if (check.equals("0")) {
														personal_api_time = time;
														personal_api_pushkey = jsonObj
																.getString("pushkey");
														personal_api_idtag = jsonObj
																.getString("_promo_id_tag");
														personal_jidto = mStringnewtojid;
														personal_jidfrom = mStringnewfromjid;
														new Execute_New_Promo()
																.execute(promoID);
														return;
													} else {
														if (check_update != null) {
															if (!check_update
																	.equals("0")) {
																personal_api_time = time;
																personal_api_pushkey = jsonObj
																		.getString("pushkey");
																personal_api_idtag = jsonObj
																		.getString("_promo_id_tag");
																personal_jidto = mStringnewtojid;
																personal_jidfrom = mStringnewfromjid;
																new Execute_New_Promo_Update()
																		.execute(promoID);
																return;
															}
														}

														try {

															json.put(
																	"pushkey",
																	jsonObj.getString("pushkey"));
															json.put(
																	"_message_body_tag",
																	jsonObj.getString("_message_body_tag"));
															json.put(
																	"_promo_id_tag",
																	jsonObj.getString("_promo_id_tag"));
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}

														mJsonArray.put(json);

														try {
															objMainList.put(
																	"body",
																	mJsonArray);
														} catch (JSONException e) {
															// TODO
															// Auto-generated
															// catch
															// block
															e.printStackTrace();
														}
													}
												}

											}

										} else {
											try {

												json.put("pushkey", jsonObj
														.getString("pushkey"));
												json.put(
														"_message_body_tag",
														jsonObj.getString("_message_body_tag"));
												json.put(
														"_promo_id_tag",
														jsonObj.getString("_promo_id_tag"));
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}

											mJsonArray.put(json);

											try {
												objMainList.put("body",
														mJsonArray);
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											//

										}
										Log.e("DATA objMainList: ", ""
												+ objMainList.toString());

									} else {
										// image show
										try {

											json.put("pushkey", "image");

											json.put("_promo_id_tag", "");
											json.put("preview_Icon", jsonObj
													.getString("preview_Icon"));
											promoID = "0";

										} catch (Exception e) {

										}

										mJsonArray.put(json);

										try {
											objMainList.put("body", mJsonArray);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								Log.e("ELSE", "NOTHING");
								Log.e("mStringnewtojid: ", "" + mStringnewtojid);
								Log.e("mStringnewfromjid: ", ""
										+ mStringnewfromjid);
								Log.e("objMainList.toString(): ", ""
										+ objMainList.toString());
								String date = date();

								String timestamp = timestamp();
								int history_count = mDatasourceHandler
										.HISTORYUpdateRecentReceiveCount(message
												.getPacketID());
								if (history_count == 0) {
									int countincrease = 0;
									countincrease = mDatasourceHandler
											.UpdateRecentReceiveCount(
													mStringnewtojid,
													mStringnewfromjid,
													objMainList.toString(),
													time, date, timestamp);
									String mString = "" + (countincrease + 1);

									boolean flag = mDatasourceHandler
											.UpdateRecentReceive(
													mStringnewtojid,
													mStringnewfromjid,
													objMainList.toString(),
													time, date, timestamp,
													mString);
									Log.e("RECEIVE 2 flag:", "" + flag);

									if (!flag) {
										mDatasourceHandler.insertrecentMessage(
												objMainList.toString(),
												mStringnewfromjid,
												mStringnewtojid, "", "", date,
												time, timestamp, "1");
									}
								}
								mDatasourceHandler.insertChathistory(
										message.getPacketID(), "username",
										mStringnewtojid, mStringnewfromjid,
										objMainList.toString(), time, promoID,
										"1", date);

								// int countincrease=0;
								// countincrease=mDatasourceHandler.UpdateRecentReceiveCount(mStringnewtojid,
								// mStringnewfromjid,
								// objMainList.toString(), time,
								// date, timestamp);
								// Log.e("SIZE COUNT countincrease: ", "" +
								// countincrease);
								// String mString=""+(countincrease+1);
								//
								// Log.e("SIZE COUNT: ", "" + mString);
								// boolean flag = mDatasourceHandler
								// .UpdateRecentReceive(mStringnewtojid,
								// mStringnewfromjid,
								// objMainList.toString(), time,
								// date, timestamp, mString);
								// Log.e("RECEIVE 2 flag:", "" + flag);
								// if (!flag) {
								// mDatasourceHandler.insertrecentMessage(
								// objMainList.toString(),
								// mStringnewfromjid, mStringnewtojid,
								// "", "", date, time, timestamp, "1");
								// }

								promoID = null;
								mStringnewtojid = null;
								mStringnewfromjid = null;

							}

							// }
						}
					}
				}, filter);
			}

		} catch (Exception e) {

		}
	}

	public String promo_data_(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringHeader = mCursor.getString(3).trim();
				Log.e(" SSSSSSSSSSSSSSS mStringHeader: ", "   " + mStringHeader);
			} else {
				mStringHeader = "NO";
				Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringHeader;
	}

	public String promo_data_link(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringFooter = mCursor.getString(2).trim();
				Log.e(" SSSSSSSSSSSSSSS mStringHeader: ", "   " + mStringHeader);
			} else {
				mStringFooter = "NO";
				Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringFooter;
	}

	protected void selectImage() {

		final CharSequence[] options = { "Choose from Gallery", "Take Photo",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(ChatScreen.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		if (size == 0) {
			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 400);
			intent.putExtra("outputY", 400);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			if (size == 1) {
				Intent i = new Intent(intent);
				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);

					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(
						getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);
							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							getContentResolver().delete(mImageCaptureUri, null,
									null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case PICK_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				doCrop();

				// BitmapFactory.Options options = new BitmapFactory.Options();
				//
				// options.inJustDecodeBounds = true;
				//
				// BitmapFactory.decodeFile(fileUri.getPath(), options);
				//
				// options.inSampleSize = calculateInSampleSize(options, 100,
				// 100);
				//
				// // Decode bitmap with inSampleSize set
				// options.inJustDecodeBounds = false;
				//
				// Bitmap bitMap = BitmapFactory.decodeFile(fileUri.getPath(),
				// options);
				//
				// String Base64 = convert_image(bitMap);
				//
				// try {
				// if (mConnectionDetector.isConnectingToInternet()) {
				//
				// if (connection.isConnected()) {
				// Log.e("SINGLE:",
				// "CAMERA_CAPTURE_IMAGE_REQUEST_CODE");
				//
				// JID = NamedeviceToken;// +
				// // Constant.XMPP_DNS_Name;
				//
				// Log.e("JID: ", "" + JID);
				//
				// Log.e("Sending text ", "" + text + " to " + JID);
				// Message msg = new Message(JID, Message.Type.chat);
				// ChatManager chatManager = connection
				// .getChatManager();
				// ChatStateListener chatstat = new chatstate();
				// Chat chat = chatManager.createChat(JID, chatstat);
				// String time = currenttime();
				// JSONObject json = new JSONObject();
				// try {
				//
				// json.put("server_URL", "Not Uploaded");
				// json.put("pushkey", "image");
				// json.put("preview_Icon", Base64);
				//
				// } catch (Exception e) {
				//
				// }
				//
				// msg.setBody(json.toString());
				// if (connection != null) {
				//
				// DeliveryReceiptManager
				// .addDeliveryReceiptRequest(msg);
				//
				// Log.e("connection",
				// "" + connection.isConnected());
				// connection.sendPacket(msg);
				//
				// String[] msg_Data = new String[5];
				// msg_Data[0] = text;
				// msg_Data[1] = "pic";
				// msg_Data[2] = "";
				// msg_Data[3] = Base64;
				// msg_Data[4] = time;
				// messages.add(msg_Data);
				// chat_list_adapter.notifyDataSetChanged();
				// lv.setAdapter(chat_list_adapter);
				//
				// JSONObject objMainList = new JSONObject();
				// JSONArray mJsonArray = null;
				// mJsonArray = new JSONArray();
				//
				// mJsonArray.put(json);
				//
				// // Finally add item arrays for "A" and "B" to
				// // main list with key
				// try {
				// objMainList.put("body", mJsonArray);
				// } catch (JSONException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				//
				// Log.e("CAMERA", "calling");
				// mDatasourceHandler.insertChathistory(
				// "username", toJID, fromJID,
				// objMainList.toString(), time);
				//
				// input.setText("");
				// Log.e("message sent to ", JID + "successfull");
				// Toast.makeText(getApplicationContext(),
				// "Message Sent !", Toast.LENGTH_SHORT)
				// .show();
				//
				// } else {
				//
				// LogMessage.showDialog(ChatScreen.this, null,
				// "Connection Lost", null, "Ok");
				//
				// }
				// }
				//
				// } else {
				// LogMessage.showDialog(ChatScreen.this, null,
				// "No Internet Connection", null, "Ok");
				// }
				// } catch (Exception e) {
				// Log.e("Exception:", "" + e);
				// Log.e("", "On Send Click");
				// }
				//
				// } else if (resultCode == Activity.RESULT_CANCELED) {
				// // user cancelled Image capture
				// Toast.makeText(ChatScreen.this,
				// "User cancelled image capture",
				// Toast.LENGTH_SHORT).show();
				// } else {
				// // failed to capture image
				// Toast.makeText(ChatScreen.this,
				// "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
				// .show();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				mImageCaptureUri = data.getData();

				doCrop();
				// JSONObject obj_Main_List = null;
				// Uri selectedImageUri = data.getData();
				// Log.e("Gallery", "" + selectedImageUri);
				// String[] projection = { MediaColumns.DATA };
				// Cursor cursor = managedQuery(selectedImageUri, projection,
				// null, null, null);
				// int column_index = cursor
				// .getColumnIndexOrThrow(MediaColumns.DATA);
				// cursor.moveToFirst();
				//
				// String selectedImagePath = cursor.getString(column_index);
				// Log.e("selectedImagePath", "" + selectedImagePath);
				// BitmapFactory.Options options = new BitmapFactory.Options();
				//
				// options.inJustDecodeBounds = true;
				//
				// BitmapFactory.decodeFile(selectedImagePath, options);
				//
				// options.inSampleSize = calculateInSampleSize(options, 100,
				// 100);
				//
				// // Decode bitmap with inSampleSize set
				// options.inJustDecodeBounds = false;
				//
				// Bitmap bitMap = BitmapFactory.decodeFile(selectedImagePath,
				// options);
				//
				// String Base64 = convert_image(bitMap);
				//
				// try {
				// if (mConnectionDetector.isConnectingToInternet()) {
				//
				// Log.e("SINGLE:", "GALLERY_IMAGE_REQUEST_CODE");
				//
				// if (connection.isConnected()) {
				//
				// JID = NamedeviceToken;// +
				// // Constant.XMPP_DNS_Name;
				//
				// Log.e("JID: ", "" + JID);
				// // JID =
				// //
				// "8427378750_b300bb3e-8dfd-419c-b314-0e774270d48b@imgrapp.com";
				//
				// Log.e("Sending text ", "" + text + " to " + JID);
				// Message msg = new Message(JID, Message.Type.chat);
				// ChatManager chatManager = connection
				// .getChatManager();
				// ChatStateListener chatstat = new chatstate();
				// Chat chat = chatManager.createChat(JID, chatstat);
				// String time = currenttime();
				// JSONObject json_ = new JSONObject();
				// try {
				//
				// json_.put("server_URL", "Not Uploaded");
				// json_.put("pushkey", "image");
				// json_.put("preview_Icon", Base64);
				//
				// } catch (Exception e) {
				//
				// }
				//
				// msg.setBody(json_.toString());
				// if (connection != null) {
				//
				// DeliveryReceiptManager
				// .addDeliveryReceiptRequest(msg);
				//
				// Log.e("connection",
				// "" + connection.isConnected());
				// connection.sendPacket(msg);
				// if (selectedImagePath != null
				// && !selectedImagePath.matches("\\s+")
				// && !selectedImagePath.matches("")) {
				//
				// String[] msg_Data = new String[5];
				// msg_Data[0] = text;
				// msg_Data[1] = "pic";
				// msg_Data[2] = "";
				// msg_Data[3] = Base64;
				// msg_Data[4] = time;
				// messages.add(msg_Data);
				// chat_list_adapter.notifyDataSetChanged();
				// lv.setAdapter(chat_list_adapter);
				// obj_Main_List = new JSONObject();
				// JSONArray mJsonArray = null;
				// mJsonArray = new JSONArray();
				//
				// mJsonArray.put(json_);
				//
				// try {
				// obj_Main_List.put("body", mJsonArray);
				// } catch (JSONException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				//
				// }
				// Log.e("GALLERY", "calling");
				// mDatasourceHandler.insertChathistory(
				// "username", toJID, fromJID,
				// obj_Main_List.toString(), time);
				// input.setText("");
				// Log.e("message sent to ", JID + "successfull");
				// Toast.makeText(getApplicationContext(),
				// "Message Sent !", Toast.LENGTH_SHORT)
				// .show();
				//
				// } else {
				//
				// LogMessage.showDialog(ChatScreen.this, null,
				// "Connection Lost", null, "Ok");
				//
				// }
				// }
				//
				// } else {
				// LogMessage.showDialog(ChatScreen.this, null,
				// "No Internet Connection", null, "Ok");
				// }
				// } catch (Exception e) {
				// Log.e("", "On Send Click");
				// }

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(ChatScreen.this,
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(ChatScreen.this, "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case CROP_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				Bundle extras = data.getExtras();

				if (extras != null) {
					Bitmap photo = extras.getParcelable("data");

					mImageViewProfile.setImageBitmap(photo);
					mStringBase64 = convert_image(photo);
				}

				File f = new File(mImageCaptureUri.getPath());

				if (f.exists())
					f.delete();
			}
			break;
		}

	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFilePath(File mFile) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
				// + IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());

		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "CHAT_IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to

		byte[] byte_arr = stream.toByteArray();

		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		mImageCaptureUri = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), "tmp_"
				+ String.valueOf(System.currentTimeMillis()) + ".jpg"));

		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
				mImageCaptureUri);

		try {
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PICK_FROM_CAMERA);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}

		// Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		//
		// fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		//
		// intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		//
		// // start the image capture Intent
		// startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
				// + IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 6;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		// Log.e("Insample size", inSampleSize + "");
		return inSampleSize;
	}

	/*** end of onPostExecute ***/

	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {

			mStringusername = sharedPreferences.getString(
					Constant.USERNAME_XMPP, "");
			mStringPassword = sharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");
			// Log.e("mStringusername: ", "" + mStringusername);
			// Log.e("mStringPassword: ", "" + mStringPassword);
			UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected String doInBackground(String... params) {
			// Log.e("doInBackground", "doInBackground");

			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);
			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				// Log.e("connected", "server connect");

				// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
				// Log.e("" + mStringusername, "" + mStringPassword);
				connection.login(mStringusername, mStringPassword);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {
					// Log.e("XMPPChatDemoActivity",
					// "--------------------------------------");
					// Log.e("XMPPChatDemoActivity", "RosterEntry " + entry);
					// Log.e("XMPPChatDemoActivity", "User: " +
					// entry.getUser());
					// Log.e("XMPPChatDemoActivity", "Name: " +
					// entry.getName());
					// Log.e("XMPPChatDemoActivity",
					// "Status: " + entry.getStatus());
					// Log.e("XMPPChatDemoActivity", "Type: " +
					// entry.getType());
					Presence entryPresence = roster
							.getPresence(entry.getUser());

					// Log.e("XMPPChatDemoActivity", "Presence Status: "
					// + entryPresence.getStatus());
					// Log.e("XMPPChatDemoActivity", "Presence Type: "
					// + entryPresence.getMode());

					Presence.Type type = entryPresence.getType();
					if (type == Presence.Type.available) {
					}
					// Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
					// Log.e("XMPPChatDemoActivity", "Presence : " +
					// entryPresence);
				}
			} catch (XMPPException e) {
				// Log.e("Cannot connect to XMPP server with default admin username and password.",
				// "0");
				// Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			// Log.e("result", "" + result);
			if (result != null) {
				// Log.e("ConnectionId", "" + connection.getConnectionID());
				// Log.e("Servicename", "" + connection.getServiceName());
				// Log.e("User", "" + connection.getUser());

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getApplicationContext(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

	}

	// public class Verfication extends AsyncTask<String, Void, String> {
	// public Verfication() {
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	// String result = JsonParserConnector.PostVerfication(
	// mEditTextverfilycode.getText().toString(), "imgr",
	// deviceid, phone_str);
	// return result;
	// }
	//
	// @Override
	// protected void onCancelled() {
	//
	// super.onCancelled();
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// UI.hideProgressDialog();
	// try {
	// if (result.equals("true")) {
	//
	// getData();
	//
	// } else {
	// LogMessage.showDialog(VerificationActivity.this, null,
	// "Please enter valid activation code", null, "Ok");
	// }
	// } catch (Exception e) {
	// LogMessage.showDialog(VerificationActivity.this, null,
	// "Please enter valid activation code", null, "Ok");
	// }
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// UI.showProgressDialog(VerificationActivity.this);
	// }
	//
	// }

	class ExecuteChatNotification extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// new_post_ChatNotificaton
			Log.e("to_notification", "" + to_notification);
			Log.e("from_notification", "" + from_notification);
			Log.e("messgae_notification", "" + messgae_notification);
			Log.e("fromjid_notification", "" + fromjid_notification);
			Log.e("tojid_notification", "" + tojid_notification);
			Log.e("time_notification", "" + time_notification);
			Log.e("timestamp_notification", "" + timestamp_notification);
			Log.e("date_notification", "" + date_notification);
			Log.e("localdatabase_notification", "" + localdatabase_notification);
			Log.e("messgaepacketid_notification", ""
					+ messgaepacketid_notification);
			Log.e("username_notification", "" + username_notification);
			Log.e("recentfromjid_notification", "" + recentfromjid_notification);
			Log.e("recenttojid_notification", "" + recenttojid_notification);

			String url = JsonParserConnector.new_post_ChatNotificaton(
					to_notification, from_notification, messgae_notification,
					fromjid_notification, tojid_notification,
					time_notification, timestamp_notification,
					date_notification, localdatabase_notification,
					messgaepacketid_notification, "0",
					unique_id_notification_user, "1", "0", "0",
					username_notification, "0", recentfromjid_notification,
					recenttojid_notification);

			// Log.e("Notification___", "" + url);

			return url;
		}

		// @Override
		// protected void onPre(String result) {
		// // TODO Auto-generated method stub
		// super.onPostExecute(result);
		// UI.showProgressDialog(ChatScreen.this);
		//
		// }
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("Notification result: ", "" + result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				boolean flag = mDatasourceHandler
						.UpdateReceive(messgaepacketid_notification);
				Log.e("Notification flag: ", "" + flag);
			} else {
				Log.e("Notification result: ", "FALSE FALSE");
			}

		}

	}

	public void configure(ProviderManager pm) {
		// Private Data Storage
		pm.addIQProvider("query", "jabber:iq:private",
				new PrivateDataManager.PrivateDataIQProvider());
		// Time
		try {
			pm.addIQProvider("query", "jabber:iq:time",
					Class.forName("org.jivesoftware.smackx.packet.Time"));
		} catch (ClassNotFoundException e) {
			// Log.w("TestClient",
			// "Can't load class for org.jivesoftware.smackx.packet.Time");
		}

		// Roster Exchange
		pm.addExtensionProvider("x", "jabber:x:roster",
				new RosterExchangeProvider());

		// Message Events
		pm.addExtensionProvider("x", "jabber:x:event",
				new MessageEventProvider());

		// Chat State
		pm.addExtensionProvider("active",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("composing",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("paused",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("inactive",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("gone",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		// XHTML
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
				new XHTMLExtensionProvider());

		// Group Chat Invitations
		pm.addExtensionProvider("x", "jabber:x:conference",
				new GroupChatInvitation.Provider());

		// Service Discovery # Items
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());

		// Service Discovery # Info
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Data Forms
		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

		// MUC User
		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
				new MUCUserProvider());

		// MUC Admin
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
				new MUCAdminProvider());

		// MUC Owner
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
				new MUCOwnerProvider());

		// SharedGroupsInfo
		pm.addIQProvider("sharedgroup",
				"http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Delayed Delivery
		pm.addExtensionProvider("x", "jabber:x:delay",
				new DelayInformationProvider());

		// Version
		try {
			pm.addIQProvider("query", "jabber:iq:version",
					Class.forName("org.jivesoftware.smackx.packet.Version"));
		} catch (ClassNotFoundException e) {
			// Not sure what's happening here.
		}

		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

		// Offline Message Requests
		pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
				new OfflineMessageRequest.Provider());

		// Offline Message Indicator
		pm.addExtensionProvider("offline",
				"http://jabber.org/protocol/offline",
				new OfflineMessageInfo.Provider());

		// Last Activity
		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

		// User Search
		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

		// JEP-33: Extended Stanza Addressing
		pm.addExtensionProvider("addresses",
				"http://jabber.org/protocol/address",
				new MultipleAddressesProvider());

		// FileTransfer
		pm.addIQProvider("si", "http://jabber.org/protocol/si",
				new StreamInitiationProvider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());

		// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
		// IBBProviders.Open());

		// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
		// IBBProviders.Close());

		// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
		// new
		// IBBProviders.Data());

		// Privacy
		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());

		pm.addIQProvider("command", "http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider());
		pm.addExtensionProvider("malformed-action",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.MalformedActionError());
		pm.addExtensionProvider("bad-locale",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadLocaleError());
		pm.addExtensionProvider("bad-payload",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadPayloadError());
		pm.addExtensionProvider("bad-sessionid",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadSessionIDError());
		pm.addExtensionProvider("session-expired",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.SessionExpiredError());

		pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
				DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
		pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
				new DeliveryReceiptRequest().getNamespace(),
				new DeliveryReceiptRequest.Provider());
		/*
		 * pm.addExtensionProvider(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE,
		 * new ReadReceipt.Provider());
		 */
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		// Log.e("Pinging", "on Pause");
		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();
		PingManager.getInstanceFor(connection);
	}

	public void sharedPrefernces(boolean flag) {
		editor = sharedPreferences.edit();
		editor.putBoolean(Constant.on_off_ads, flag);

		editor.commit();

	}

	public void shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.BUBBLE_SET_FREIND, mString);

		editor.commit();

	}

	public void Sponsored_shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.SPONSORED_on_off_ads, mString);

		editor.commit();

	}

	public class SwipeDismissTouchListener implements View.OnTouchListener {

		static final String logTag = "ActivitySwipeDetector";
		private Activity activity;
		static final int MIN_DISTANCE = 100;
		private float downX, upX;

		public SwipeDismissTouchListener(Activity mActivity) {
			activity = mActivity;
		}

		public void onRightToLeftSwipe() {
			Log.i(logTag, "RightToLeftSwipe!");
			if (mStringPromoOnOFF.equals("true") && setPromo.equals("true")) {
				Log.e("TEST: ", "5");
				if (mStringSponsored_BarChanged.equals("true")
						&& setSponsoredads.equals("true")) {
					Collections.shuffle(newmArrayList_promo_uniqueid);

					for (int i = 0; i < newmArrayList_promo_uniqueid.size(); i++) {
						test_promo_data(newmArrayList_promo_uniqueid.get(i));

					}
				} else {
					if (mStringPersonalSetting.equals("true")
							&& setpersonalads.equals("true")) {
						NEW_SPONSORED_PERSONAL_ONLY();
					}
				}
			}
			// Toast.makeText(activity, "RightToLeftSwipe",
			// Toast.LENGTH_SHORT).show();
			// activity.doSomething();
		}

		public void onLeftToRightSwipe() {
			Log.i(logTag, "LeftToRightSwipe!");
			Log.i(logTag, "RightToLeftSwipe!");
			if (mStringPromoOnOFF.equals("true") && setPromo.equals("true")) {
				Log.e("TEST: ", "5");
				if (mStringSponsored_BarChanged.equals("true")
						&& setSponsoredads.equals("true")) {
					Collections.shuffle(newmArrayList_promo_uniqueid);

					for (int i = 0; i < newmArrayList_promo_uniqueid.size(); i++) {
						test_promo_data(newmArrayList_promo_uniqueid.get(i));

					}
				} else {
					if (mStringPersonalSetting.equals("true")
							&& setpersonalads.equals("true")) {
						NEW_SPONSORED_PERSONAL_ONLY();
					}
				}
			}

		}

		// public void onLeftToRightSwipe() {
		// Log.i(logTag, "LeftToRightSwipe!");
		// Toast.makeText(activity, "LeftToRightSwipe",
		// Toast.LENGTH_SHORT).show();
		// // activity.doSomething();
		// }

		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				downX = event.getX();
				return true;
			}
			case MotionEvent.ACTION_UP: {
				upX = event.getX();

				float deltaX = downX - upX;

				if (Math.abs(deltaX) > MIN_DISTANCE) {
					if (deltaX < 0) {
						this.onLeftToRightSwipe();
						return true;
					}
					if (deltaX > 0) {
						this.onRightToLeftSwipe();
						return true;
					}
				} else {
					Log.i(logTag, "Swipe was only " + Math.abs(deltaX)
							+ " long horizontally, need at least "
							+ MIN_DISTANCE);
					// return false; // We don't consume the event
				}

				return false; // no swipe horizontally and no swipe vertically
			}// case MotionEvent.ACTION_UP:
			}
			return false;
		}
	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class ExecuteNewPromo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			Log.e("id:", "doingbackground");
			Log.e("id:", "" + testpromo_id);
			url = JsonParserConnector.getPERSONALSponsored(testpromo_id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Log.e("id:", "" + testpromo_id);
			Log.e("id:", "dfsaghfdhfghfadhsdfhdfhsghdfsghghdfshhdsghdsgh");
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			// UI.hideProgressDialog();
			PersonalList(result);
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class ExecuteNewPromoUpdate extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			Log.e("id:", "doingbackground");
			Log.e("id:", "" + testpromo_id);
			url = JsonParserConnector.getPERSONALSponsored(testpromo_id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Log.e("id:", "" + testpromo_id);
			Log.e("id:", "dfsaghfdhfghfadhsdfhdfhsghdfsghghdfshhdsghdsgh");
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			// UI.hideProgressDialog();
			PersonalListUpdate(result);
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			UI.hideProgressDialog();
			Personal_List(result);
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo_Update extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			UI.hideProgressDialog();
			Personal_List_update(result);
		}

	}

	private void PersonalListUpdate(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				JSONArray mArray = jsonOBject.getJSONArray("data");
				for (int i = 0; i < mArray.length(); i++) {
					JSONObject sys = mArray.getJSONObject(i);
					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					// mStringsmodified_date = sys.getString("modified_date");
					mString_link_text = sys.getString("promo_link_text");
					mString_link_promo = sys.getString("promo_link");
					mString_header_promo_message = sys
							.getString("promo_message_header");
					Log.e("mStringspromo_name: ", "" + mStringspromo_name);
					Log.e("mStringspromo_image: ", "" + mStringspromo_image);
					Log.e("mString_link_text: ", "" + mString_link_text);
					Log.e("mString_header_promo_message: ", ""
							+ mString_header_promo_message);

					boolean flag = mDatasourceHandler.update_Sponsored(
							mStringspromoid, "NO_Define", mStringspromo_name,
							"https://imgrapp.com/release/"
									+ mStringspromo_image, mString_link_promo,
							mString_link_text, mString_header_promo_message,
							"0", "", "1", "1");

					Log.e("flag", "" + flag);

					JSONObject json = new JSONObject();
					mArrayPersonal = new JSONArray();
					JSONObject objMainList = new JSONObject();
					String date_sent = date();
					String path = historypromo_data(mStringspromoid);
					String[] msg_Data = new String[9];
					msg_Data[0] = "";
					msg_Data[4] = personal_api_time;
					msg_Data[1] = "friend";
					msg_Data[2] = "0";
					msg_Data[3] = personal_api_pushkey;
					msg_Data[5] = personal_api_idtag;
					msg_Data[6] = path;
					msg_Data[7] = mStringspromoid;
					msg_Data[8] = date_sent;
					messages.add(msg_Data);

					try {

						json.put("pushkey",
								personaljsonObj.getString("pushkey"));
						json.put("_message_body_tag",
								personaljsonObj.getString("_message_body_tag"));
						json.put("_promo_id_tag",
								personaljsonObj.getString("_promo_id_tag"));
						json.put("preview_Icon", "null");

					} catch (Exception e) {

					}

					mArrayPersonal.put(json);
					Log.e("mArrayPersonal: ", "" + mArrayPersonal);
					try {
						objMainList.put("body", mArrayPersonal);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//

					Log.e("JSON OBJECT: ", "" + objMainList.toString());
					Log.e("personal_api_time: ", "" + personal_api_time);
					Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
					Log.e("message.getPacketID(): ", "" + message.getPacketID());
					Log.e("personal_jidto: ", "" + personal_jidto);

					String date = date();
					String timestamp = timestamp();

					int history_count = mDatasourceHandler
							.HISTORYUpdateRecentReceiveCount(message
									.getPacketID());
					if (history_count == 0) {
						int countincrease = 0;
						countincrease = mDatasourceHandler
								.UpdateRecentReceiveCount(personal_jidto,
										personal_jidfrom,
										objMainList.toString(),
										personal_api_time, date, timestamp);
						String mString = "" + (countincrease + 1);

						boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
								personal_jidto, personal_jidfrom,
								objMainList.toString(), personal_api_time,
								date, timestamp, "0");
						Log.e("RECEIVE 1 flag1:", "" + flag1);
						if (!flag1) {
							mDatasourceHandler.insertrecentMessage(
									objMainList.toString(), personal_jidfrom,
									personal_jidto, "", getfrdsname, date,
									personal_api_time, timestamp, "0");
						}

					}

					mDatasourceHandler.insertChathistory(message.getPacketID(),
							"username", personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time,
							mStringspromoid, "0", date);

					personal_jidfrom = null;
					personal_jidto = null;
					// Add the incoming message to the list view
					mHandler.post(new Runnable() {
						public void run() {
							// Log.e("Set connection COUNT:", ""
							// + chat_list_adapter.getCount());
							if (chat_list_adapter.getCount() >= 5) {
								lv.setStackFromBottom(true);
							} else {
								lv.setStackFromBottom(false);
							}
							mArrayDATE.add(date());
							chat_list_adapter.notifyDataSetChanged();
							lv.setAdapter(chat_list_adapter);
						}
					});
				}

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	private void PersonalList(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				JSONArray mArray = jsonOBject.getJSONArray("data");
				for (int i = 0; i < mArray.length(); i++) {
					JSONObject sys = mArray.getJSONObject(i);
					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					// mStringsmodified_date = sys.getString("modified_date");
					mString_link_text = sys.getString("promo_link_text");
					mString_link_promo = sys.getString("promo_link");
					mString_header_promo_message = sys
							.getString("promo_message_header");
					Log.e("mStringspromo_name: ", "" + mStringspromo_name);
					Log.e("mStringspromo_image: ", "" + mStringspromo_image);
					Log.e("mString_link_text: ", "" + mString_link_text);
					Log.e("mString_header_promo_message: ", ""
							+ mString_header_promo_message);

					boolean flag = mDatasourceHandler.insertSponsored(
							mStringspromoid, "NO_Define", mStringspromo_name,
							"https://imgrapp.com/release/"
									+ mStringspromo_image, mString_link_promo,
							mString_link_text, mString_header_promo_message,
							"0", "", "1", "1");

					Log.e("flag", "" + flag);

					JSONObject json = new JSONObject();
					mArrayPersonal = new JSONArray();
					JSONObject objMainList = new JSONObject();
					String date_sent = date();
					String path = historypromo_data(mStringspromoid);
					String[] msg_Data = new String[9];
					msg_Data[0] = "";
					msg_Data[4] = personal_api_time;
					msg_Data[1] = "friend";
					msg_Data[2] = "0";
					msg_Data[3] = personal_api_pushkey;
					msg_Data[5] = personal_api_idtag;
					msg_Data[6] = path;
					msg_Data[7] = mStringspromoid;
					msg_Data[8] = date_sent;
					messages.add(msg_Data);

					try {

						json.put("pushkey",
								personaljsonObj.getString("pushkey"));
						json.put("_message_body_tag",
								personaljsonObj.getString("_message_body_tag"));
						json.put("_promo_id_tag",
								personaljsonObj.getString("_promo_id_tag"));
						json.put("preview_Icon", "null");

					} catch (Exception e) {

					}

					mArrayPersonal.put(json);
					Log.e("mArrayPersonal: ", "" + mArrayPersonal);
					try {
						objMainList.put("body", mArrayPersonal);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//

					Log.e("JSON OBJECT: ", "" + objMainList.toString());
					Log.e("personal_api_time: ", "" + personal_api_time);
					Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
					Log.e("message.getPacketID(): ", "" + message.getPacketID());
					Log.e("personal_jidto: ", "" + personal_jidto);

					String date = date();
					String timestamp = timestamp();

					int history_count = mDatasourceHandler
							.HISTORYUpdateRecentReceiveCount(message
									.getPacketID());
					if (history_count == 0) {
						int countincrease = 0;
						countincrease = mDatasourceHandler
								.UpdateRecentReceiveCount(personal_jidto,
										personal_jidfrom,
										objMainList.toString(),
										personal_api_time, date, timestamp);
						String mString = "" + (countincrease + 1);

						boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
								personal_jidto, personal_jidfrom,
								objMainList.toString(), personal_api_time,
								date, timestamp, "0");
						Log.e("RECEIVE 1 flag1:", "" + flag1);
						if (!flag1) {
							mDatasourceHandler.insertrecentMessage(
									objMainList.toString(), personal_jidfrom,
									personal_jidto, "", getfrdsname, date,
									personal_api_time, timestamp, "0");
						}

					}

					mDatasourceHandler.insertChathistory(message.getPacketID(),
							"username", personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time,
							mStringspromoid, "0", date);

					personal_jidfrom = null;
					personal_jidto = null;
					// Add the incoming message to the list view
					mHandler.post(new Runnable() {
						public void run() {
							// Log.e("Set connection COUNT:", ""
							// + chat_list_adapter.getCount());
							if (chat_list_adapter.getCount() >= 5) {
								lv.setStackFromBottom(true);
							} else {
								lv.setStackFromBottom(false);
							}
							mArrayDATE.add(date());
							chat_list_adapter.notifyDataSetChanged();
							lv.setAdapter(chat_list_adapter);
						}
					});
				}

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	private void Personal_List(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.insertSponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				Log.e("flag", "" + flag);

				JSONObject json = new JSONObject();
				mArrayPersonal = new JSONArray();
				JSONObject objMainList = new JSONObject();

				try {

					json.put("pushkey", personaljsonObj.getString("pushkey"));
					json.put("_message_body_tag",
							personaljsonObj.getString("_message_body_tag"));
					json.put("_promo_id_tag",
							personaljsonObj.getString("_promo_id_tag"));
					json.put("preview_Icon", "null");

				} catch (Exception e) {

				}

				mArrayPersonal.put(json);

				try {
					objMainList.put("body", mArrayPersonal);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//

				Log.e("if RECEVIVE", "calling");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				Log.e("ELSE", "NOTHING");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				String date = date();

				String timestamp = timestamp();

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(message.getPacketID());
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, objMainList.toString(),
									personal_api_time, date, timestamp);
					String mString = "" + (countincrease + 1);

					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time, date,
							timestamp, mString);
					Log.e("RECEIVE 2 flag:", "" + flag);
					if (!flag1) {
						mDatasourceHandler.insertrecentMessage(
								objMainList.toString(), personal_jidfrom,
								personal_jidto, "", "", date,
								personal_api_time, timestamp, "1");
					}

				}

				mDatasourceHandler.insertChathistory(message.getPacketID(),
						"username", personal_jidto, personal_jidfrom,
						objMainList.toString(), personal_api_time,
						mStringspromoid, "1", date);

				// promoID = null;
				personal_jidto = null;
				personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	private void Personal_List_update(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.update_Sponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				Log.e("flag", "" + flag);

				JSONObject json = new JSONObject();
				mArrayPersonal = new JSONArray();
				JSONObject objMainList = new JSONObject();

				try {

					json.put("pushkey", personaljsonObj.getString("pushkey"));
					json.put("_message_body_tag",
							personaljsonObj.getString("_message_body_tag"));
					json.put("_promo_id_tag",
							personaljsonObj.getString("_promo_id_tag"));
					json.put("preview_Icon", "null");

				} catch (Exception e) {

				}

				mArrayPersonal.put(json);

				try {
					objMainList.put("body", mArrayPersonal);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//

				Log.e("if RECEVIVE", "calling");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				Log.e("ELSE", "NOTHING");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				String date = date();

				String timestamp = timestamp();

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(message.getPacketID());
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, objMainList.toString(),
									personal_api_time, date, timestamp);
					String mString = "" + (countincrease + 1);

					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time, date,
							timestamp, mString);
					Log.e("RECEIVE 2 flag:", "" + flag);
					if (!flag1) {
						mDatasourceHandler.insertrecentMessage(
								objMainList.toString(), personal_jidfrom,
								personal_jidto, "", "", date,
								personal_api_time, timestamp, "1");
					}

				}

				mDatasourceHandler.insertChathistory(message.getPacketID(),
						"username", personal_jidto, personal_jidfrom,
						objMainList.toString(), personal_api_time,
						mStringspromoid, "1", date);

				// promoID = null;
				personal_jidto = null;
				personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	public void sharedPrefernces() {
		editor = mSharedPreferences.edit();
		editor.putString("Notification_Click_Message", "false");

		editor.commit();

	}

}
