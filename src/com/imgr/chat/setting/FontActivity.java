package com.imgr.chat.setting;

/**
 * author: amit agnihotri
 */
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;

public class FontActivity extends Activity {
	TextView mImageView_back, mTextViewNormal, mTextViewLarger;
	ImageView mImageViewNormal, mImageViewLarger;
	boolean flag = false;
	SharedPreferences sharedPreferences;
	Editor editor;
	String mStringfont;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_font);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringfont = sharedPreferences.getString(Constant.FONT_SET, "");
		mImageView_back = (TextView) findViewById(R.id.btn_back_loginpage);
		mTextViewNormal = (TextView) findViewById(R.id.txtTitle1);
		mTextViewLarger = (TextView) findViewById(R.id.txtTitle2);
		mImageViewNormal = (ImageView) findViewById(R.id.imageView1);
		mImageViewLarger = (ImageView) findViewById(R.id.imageView2);
		if (mStringfont.equals("normal")) {
			flag = false;
			mImageViewLarger.setVisibility(View.GONE);
			mImageViewNormal.setVisibility(View.VISIBLE);
		} else {
			flag = true;
			mImageViewNormal.setVisibility(View.GONE);
			mImageViewLarger.setVisibility(View.VISIBLE);
		}

		mTextViewNormal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sharedPrefernces("normal");
				flag = false;
				mImageViewLarger.setVisibility(View.GONE);
				mImageViewNormal.setVisibility(View.VISIBLE);

			}
		});
		mTextViewLarger.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sharedPrefernces("larger");
				flag = true;
				mImageViewNormal.setVisibility(View.GONE);
				mImageViewLarger.setVisibility(View.VISIBLE);

			}
		});

		mImageView_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
	}

	public void sharedPrefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.FONT_SET, mString);

		editor.commit();

	}
}
