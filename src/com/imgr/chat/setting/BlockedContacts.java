package com.imgr.chat.setting;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.imgr.chat.R;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.register.VerificationActivity;

public class BlockedContacts extends Activity {
	TextView txtBack, text_show_hide;
	private AppAdapter mAdapter;
	private SwipeMenuListView mListView;
	ArrayList<String> mArrayListName;
	ArrayList<String> mArrayListNameNew;
	ArrayList<String> mArrayListId;
	ArrayList<String> mArrayListIdNew;
	ArrayList<String> mArrayListBlockedContact;

	Button mButtonbtnAddnew;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	static int block = 0;
	AlertDialog pinDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blocked_contacts);
		mArrayListId = new ArrayList<String>();
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		text_show_hide = (TextView) findViewById(R.id.text_show_hide);
		mListView = (SwipeMenuListView) findViewById(R.id.listview_blocked);
		mButtonbtnAddnew = (Button) findViewById(R.id.btnAddnew);

		// mArrayListName.add("Amit");
		// mArrayListName.add("Shalvi");
		// mArrayListName.add("Pradeep");
		// mArrayListId.add("1");
		// mArrayListId.add("2");
		// mArrayListId.add("3");

		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		mButtonbtnAddnew.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(BlockedContacts.this,
						AddNewOpenImgrList.class);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		// step 1. create a MenuCreator
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item
				// SwipeMenuItem openItem = new SwipeMenuItem(
				// getApplicationContext());
				// // set item background
				// openItem.setBackground(new ColorDrawable(Color.rgb(0xC9,
				// 0xC9,
				// 0xCE)));
				// // set item width
				// openItem.setWidth(dp2px(90));
				// // set item title
				// openItem.setTitle("Open");
				// // set item title fontsize
				// openItem.setTitleSize(18);
				// // set item title font color
				// openItem.setTitleColor(Color.WHITE);
				// // add to menu
				// menu.addMenuItem(openItem);

				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						getApplicationContext());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				// deleteItem.set
				// deleteItem.setTitle("Unblock");
				// deleteItem.setTitleColor(Color.parseColor("#ffffff"));
				deleteItem.setIcon(R.drawable.ic_unblock);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		// set creator
		mListView.setMenuCreator(creator);

		// step 2. listener item click event
		mListView
				.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(int position,
							SwipeMenu menu, int index) {
						// ApplicationInfo item = mAppList.get(position);
						switch (index) {
						// case 0:
						// // open
						// open(item);
						// break;
						case 0:
							// delete
							// delete(item);
							mDatasourceHandler.UpdateBlockContact(
									mArrayListIdNew.get(position), "false");

							mArrayListNameNew.remove(position);
							mArrayListIdNew.remove(position);

							mAdapter.notifyDataSetChanged();
							if (mArrayListIdNew.size() == 0) {
								text_show_hide.setVisibility(View.VISIBLE);
								text_show_hide
										.setText("No blocked contact found.");
							}
							Send_again_popup_();
							break;
						}
						return false;
					}
				});

		// set SwipeListener
		mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

			@Override
			public void onSwipeStart(int position) {
				// swipe start
			}

			@Override
			public void onSwipeEnd(int position) {
				// swipe end
			}
		});

		// other setting
		// listView.setCloseInterpolator(new BounceInterpolator());

		// test item long click
		mListView
				.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						Toast.makeText(getApplicationContext(),
								position + " long click", Toast.LENGTH_SHORT)
								.show();
						return false;
					}
				});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			mArrayListName = new ArrayList<String>();
			mArrayListNameNew = new ArrayList<String>();
			mArrayListIdNew = new ArrayList<String>();
			mArrayListBlockedContact = new ArrayList<String>();
			mDatasourceHandler = new DatasourceHandler(this);
			mDatabaseHelper = new DatabaseHelper(this);
			mArrayListName.clear();
			mArrayListNameNew.clear();
			mArrayListId.clear();
			mArrayListIdNew.clear();

			if (block == 0) {
				block = 0;
				mCursor = mDatasourceHandler.ImgrContacts();
				Log.e("Local Fragment: ", mCursor.getCount() + "");

				if (mCursor.getCount() != 0) {
					mArrayListName.clear();
					mArrayListNameNew.clear();
					mArrayListId.clear();
					mArrayListIdNew.clear();
					do {

						mArrayListName.add(mCursor.getString(1).trim() + ""
								+ mCursor.getString(3).trim());

						mArrayListId.add(mCursor.getString(8).trim());
						mArrayListBlockedContact.add(mCursor.getString(9)
								.trim());

					} while (mCursor.moveToNext());

					mCursor.close();

					for (int i = 0; i < mArrayListName.size(); i++) {
						if (mArrayListBlockedContact.get(i).equals("true")) {
							mArrayListNameNew.add(mArrayListName.get(i));
							mArrayListIdNew.add(mArrayListId.get(i));
						}
					}
					Log.e("mArrayListNameNew: ", "" + mArrayListNameNew);
					if (mArrayListNameNew.size() != 0) {

						text_show_hide.setVisibility(View.GONE);
						mListView.setVisibility(View.VISIBLE);
						mAdapter = new AppAdapter();
						mListView.setAdapter(mAdapter);
					} else {
						text_show_hide.setVisibility(View.VISIBLE);
						mListView.setVisibility(View.GONE);
					}
				} else {
					mListView.setVisibility(View.GONE);
					text_show_hide.setVisibility(View.VISIBLE);

				}

			} else {
				block = 0;
				Send_again_popup();

			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	protected void Send_again_popup() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.send_again_popup, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.text_change);

		mTextView
				.setText("The Contact has been blocked.If you want to unblock go to Settings> Blocked Contacts");
		mTextView.setTypeface(null, Typeface.BOLD);
		pinDialog = new AlertDialog.Builder(BlockedContacts.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

						mCursor = mDatasourceHandler.ImgrContacts();
						Log.e("Local Fragment: ", mCursor.getCount() + "");

						if (mCursor.getCount() != 0) {
							mArrayListName.clear();
							mArrayListNameNew.clear();
							mArrayListId.clear();
							mArrayListIdNew.clear();
							do {

								mArrayListName.add(mCursor.getString(1).trim()
										+ "" + mCursor.getString(3).trim());

								mArrayListId.add(mCursor.getString(8).trim());
								mArrayListBlockedContact.add(mCursor.getString(
										9).trim());

							} while (mCursor.moveToNext());

							mCursor.close();

							for (int i = 0; i < mArrayListName.size(); i++) {
								if (mArrayListBlockedContact.get(i).equals(
										"true")) {
									mArrayListNameNew.add(mArrayListName.get(i));
									mArrayListIdNew.add(mArrayListId.get(i));
								}
							}
							Log.e("mArrayListNameNew: ", "" + mArrayListNameNew);
							if (mArrayListNameNew.size() != 0) {

								text_show_hide.setVisibility(View.GONE);
								mListView.setVisibility(View.VISIBLE);
								mAdapter = new AppAdapter();
								mListView.setAdapter(mAdapter);
							}
						}

					}
				});

			}
		});

		pinDialog.show();

	}

	protected void Send_again_popup_() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.send_again_popup, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final TextView mTextView = (TextView) v.findViewById(R.id.text_change);
		mTextView.setText("The Contact has been unblocked for you.");
		mTextView.setTypeface(null, Typeface.BOLD);
		pinDialog = new AlertDialog.Builder(BlockedContacts.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

			}
		});

		pinDialog.show();

	}

	class AppAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return mArrayListIdNew.size();
		}

		@Override
		public Object getItem(int position) {
			return mArrayListIdNew.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = View.inflate(getApplicationContext(),
						R.layout.item_list_blockcontact, null);
				new ViewHolder(convertView);
			}
			ViewHolder holder = (ViewHolder) convertView.getTag();

			holder.tv_name.setText(mArrayListNameNew.get(position));
			return convertView;
		}

		class ViewHolder {

			TextView tv_name;

			public ViewHolder(View view) {

				tv_name = (TextView) view.findViewById(R.id.tv_name);
				view.setTag(this);
			}
		}
	}

	private int dp2px(int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				getResources().getDisplayMetrics());
	}
}
