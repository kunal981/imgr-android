package com.imgr.chat.setting;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.adapter.EditSponsoredAdapter;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.FastListView;
import com.imgr.chat.model.ModelSponsoredSelect;
import com.imgr.chat.util.ProgressBar;
import com.imgr.chat.util.UI;

public class EditSponsoredAds extends Activity {
	// ListView mListView;
	FastListView mListView;

	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	Button mButton_selectall;
	TextView txtBack;
	private EditText searchBox;
	EditSponsoredAdapter mAdapter;
	ArrayList<String> mArrayListpromoname;
	ArrayList<String> mArrayListpromoid;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListisenabled;
	ArrayList<ModelSponsoredSelect> mSponsoredModels;
	ArrayList<String> mArrayListpersonal_or;

	ArrayList<String> contactname = new ArrayList<String>();
	ArrayList<String> contactnumber = new ArrayList<String>();
	String mStringPassword, deviceId;
	String result;
	ArrayList<String> mArrayListPHONE = new ArrayList<String>();
	Button mButtonInvite;
	SharedPreferences sharedPreferences;
	ArrayList<String> mArrayListpersonal_or_API;
	int API_HIT = 0;
	String Final_value, is_enable;
	int count = 0;

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.editsponsoredads);
		mListView = (FastListView) findViewById(R.id.list_View_IMGR);
		mButton_selectall = (Button) findViewById(R.id.selectall);
		mButtonInvite = (Button) findViewById(R.id.btn_invite);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mArrayListpromoname = new ArrayList<String>();
		mArrayListpromoid = new ArrayList<String>();
		mArrayListisdelete = new ArrayList<String>();
		mArrayListisactive = new ArrayList<String>();
		mArrayListisenabled = new ArrayList<String>();
		mArrayListpersonal_or = new ArrayList<String>();
		mArrayListpersonal_or_API = new ArrayList<String>();
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		deviceId = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceId:", "" + deviceId);
		mSponsoredModels = new ArrayList<ModelSponsoredSelect>();

		// fetchinfo();
		mButtonInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (API_HIT == 0) {
					finish();
					overridePendingTransition(R.anim.slide_out_left,
							R.anim.slide_in_right);
				} else {
					// mArrayListPHONE.clear();

					// if (API_HIT == 1) {
					/*
					 * // API_HIT = 1; //
					 * mButton_selectall.setText("Deselect All");
					 * mArrayListPHONE.clear(); mSponsoredModels.clear(); for
					 * (int i = 0; i < mArrayListpromoid.size(); i++) { if
					 * (mArrayListpersonal_or.get(i).equals( "no_personal")) {
					 * if (mArrayListisactive.get(i).equals("1") &&
					 * mArrayListisdelete.get(i) .equals("0")) {
					 * ModelSponsoredSelect mSponsoredSelect; if
					 * (mArrayListisenabled.get(i).equals("0")) {
					 * mSponsoredSelect = new ModelSponsoredSelect(
					 * mArrayListpromoid.get(i), mArrayListpromoname.get(i),
					 * mArrayListisenabled.get(i), mArrayListisdelete.get(i),
					 * mArrayListisactive.get(i), true); } else {
					 * mSponsoredSelect = new ModelSponsoredSelect(
					 * mArrayListpromoid.get(i), mArrayListpromoname.get(i),
					 * mArrayListisenabled.get(i), mArrayListisdelete.get(i),
					 * mArrayListisactive.get(i), true); }
					 * mSponsoredModels.add(mSponsoredSelect);
					 * 
					 * } } Log.e("SIZE:", "" + mSponsoredModels.size()); }
					 * 
					 * // mAdapter.notifyDataSetChanged(); } else { // API_HIT =
					 * 2; // mButton_selectall.setText("Select All");
					 * mArrayListPHONE.clear(); mSponsoredModels.clear(); for
					 * (int i = 0; i < mArrayListpromoid.size(); i++) { if
					 * (mArrayListpersonal_or.get(i).equals( "no_personal")) {
					 * if (mArrayListisactive.get(i).equals("1") &&
					 * mArrayListisdelete.get(i) .equals("0")) {
					 * ModelSponsoredSelect mSponsoredSelect; if
					 * (mArrayListisenabled.get(i).equals("0")) {
					 * mSponsoredSelect = new ModelSponsoredSelect(
					 * mArrayListpromoid.get(i), mArrayListpromoname.get(i),
					 * mArrayListisenabled.get(i), mArrayListisdelete.get(i),
					 * mArrayListisactive.get(i), false); } else {
					 * mSponsoredSelect = new ModelSponsoredSelect(
					 * mArrayListpromoid.get(i), mArrayListpromoname.get(i),
					 * mArrayListisenabled.get(i), mArrayListisdelete.get(i),
					 * mArrayListisactive.get(i), false); }
					 * mSponsoredModels.add(mSponsoredSelect);
					 * 
					 * } }
					 * 
					 * } Log.e("SIZE:", "" + mSponsoredModels.size());
					 * 
					 * // /mAdapter.notifyDataSetChanged(); }
					 */

					mArrayListPHONE.clear();
					for (int i = 0; i < mSponsoredModels.size(); i++) {
						mArrayListPHONE.add(mSponsoredModels.get(i)
								.getPromoId());
					}

					// for (ModelSponsoredSelect p : mAdapter.getBox()) {
					// if (p.box) {
					// result = "\n" + p.getPromoId();
					// mArrayListPHONE.add(p.getPromoId());
					// Log.e("BOX: ",""+p.box);
					//
					// }
					// }

					Log.e("FINAL mArrayListPHONE: ", "" + mArrayListPHONE);
					if (mArrayListPHONE.size() == 0) {
						mArrayListPHONE = mArrayListpromoid;
					}

					new Edit_Sponsored_Ads().execute(count + "", "" + count
							+ "");

				}

				// new
				// ExecutePullSponsored().execute();

			}
		});

		mButton_selectall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (mButton_selectall.getText().toString().equals("Select All")) {
					API_HIT = 1;
					mButton_selectall.setText("Deselect All");
					mArrayListPHONE.clear();
					mSponsoredModels.clear();
					for (int i = 0; i < mArrayListpromoid.size(); i++) {
						if (mArrayListpersonal_or.get(i).equals("no_personal")) {
							if (mArrayListisactive.get(i).equals("1")
									&& mArrayListisdelete.get(i).equals("0")) {
								ModelSponsoredSelect mSponsoredSelect;
								if (mArrayListisenabled.get(i).equals("0")) {
									mSponsoredSelect = new ModelSponsoredSelect(
											mArrayListpromoid.get(i),
											mArrayListpromoname.get(i),
											mArrayListisenabled.get(i),
											mArrayListisdelete.get(i),
											mArrayListisactive.get(i), true);
								} else {
									mSponsoredSelect = new ModelSponsoredSelect(
											mArrayListpromoid.get(i),
											mArrayListpromoname.get(i),
											mArrayListisenabled.get(i),
											mArrayListisdelete.get(i),
											mArrayListisactive.get(i), true);
								}
								mSponsoredModels.add(mSponsoredSelect);

							}
						}

					}
					Log.e("SIZE:", "" + mSponsoredModels.size());

					mAdapter.notifyDataSetChanged();

				} else {
					API_HIT = 2;
					mButton_selectall.setText("Select All");
					mArrayListPHONE.clear();
					mSponsoredModels.clear();
					for (int i = 0; i < mArrayListpromoid.size(); i++) {
						if (mArrayListpersonal_or.get(i).equals("no_personal")) {
							if (mArrayListisactive.get(i).equals("1")
									&& mArrayListisdelete.get(i).equals("0")) {
								ModelSponsoredSelect mSponsoredSelect;
								if (mArrayListisenabled.get(i).equals("0")) {
									mSponsoredSelect = new ModelSponsoredSelect(
											mArrayListpromoid.get(i),
											mArrayListpromoname.get(i),
											mArrayListisenabled.get(i),
											mArrayListisdelete.get(i),
											mArrayListisactive.get(i), false);
								} else {
									mSponsoredSelect = new ModelSponsoredSelect(
											mArrayListpromoid.get(i),
											mArrayListpromoname.get(i),
											mArrayListisenabled.get(i),
											mArrayListisdelete.get(i),
											mArrayListisactive.get(i), false);
								}
								mSponsoredModels.add(mSponsoredSelect);

							}
						}

					}
					Log.e("SIZE:", "" + mSponsoredModels.size());

					mAdapter.notifyDataSetChanged();
				}

			}
		});
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		searchBox = (EditText) findViewById(R.id.input_search_query);
		searchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					mAdapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@SuppressLint("DefaultLocale")
			@Override
			public void afterTextChanged(Editable s) {
				// searchString = searchBox.getText().toString().trim()
				// .toUpperCase();
				// searchBox.setText("");

			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			mCursor = mDatasourceHandler.SponsoredData();
			Log.e("SponsoredData count: ", mCursor.getCount() + "");

			if (mCursor.getCount() != 0) {
				mArrayListpromoname.clear();
				mArrayListpromoid.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				mArrayListpersonal_or.clear();
				do {

					mArrayListpromoid.add(mCursor.getString(0).trim());
					mArrayListisactive.add(mCursor.getString(1).trim());
					mArrayListisdelete.add(mCursor.getString(2).trim());
					mArrayListisenabled.add(mCursor.getString(3).trim());
					mArrayListpromoname.add(mCursor.getString(4).trim());
					mArrayListpersonal_or.add(mCursor.getString(5).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				Log.e("mArrayListpromoid: ", "" + mArrayListpromoid);

				for (int i = 0; i < mArrayListpromoid.size(); i++) {
					if (mArrayListpersonal_or.get(i).equals("no_personal")) {
						if (mArrayListisactive.get(i).equals("1")
								&& mArrayListisdelete.get(i).equals("0")) {
							ModelSponsoredSelect mSponsoredSelect;
							if (mArrayListisenabled.get(i).equals("0")) {
								mSponsoredSelect = new ModelSponsoredSelect(
										mArrayListpromoid.get(i),
										mArrayListpromoname.get(i),
										mArrayListisenabled.get(i),
										mArrayListisdelete.get(i),
										mArrayListisactive.get(i), false);
							} else {
								mSponsoredSelect = new ModelSponsoredSelect(
										mArrayListpromoid.get(i),
										mArrayListpromoname.get(i),
										mArrayListisenabled.get(i),
										mArrayListisdelete.get(i),
										mArrayListisactive.get(i), true);
							}
							mSponsoredModels.add(mSponsoredSelect);

						}

					}

				}
				mAdapter = new EditSponsoredAdapter(EditSponsoredAds.this,
						mArrayListpromoname, contactnumber, mSponsoredModels);

				mListView.setAdapter(mAdapter);

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public class EditALL extends AsyncTask<String, Void, String> {
		public EditALL() {
		}

		@Override
		protected String doInBackground(String... params) {
			String mString = JsonParserConnector.PostEditSponsored("imgr",
					mArrayListpromoid, mStringPassword, deviceId,
					mArrayListisenabled);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result JAMEEL : ", "" + result);
			UI.hideProgressDialog();
			update_list_sponsoreds(result);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(EditSponsoredAds.this);
		}

	}

	public void update_list_sponsoreds(String response) {
		try {
			Log.e("response JAMEEL: ", "" + response);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);

			}
		} catch (JSONException e) {

		}
	}

	/**
	 * Execute Sponsored Asynctask
	 */
	class ExecutePullSponsored extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.getSponsored("0", mStringPassword,
					deviceId);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(EditSponsoredAds.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Log.e("result----", "" + result);

			UI.hideProgressDialog();
			SponsornedPullList(result);

		}

	}

	private void SponsornedPullList(String response) {
		try {
			Log.e("response: ", "" + response);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				mArrayListpersonal_or_API.clear();
				mArrayListpromoid.clear();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					// SponsoredModel mContact = new SponsoredModel();
					String mStringspromoid = jsonOBjectContact
							.getString("promo_id");

					String mStringsis_enabled = jsonOBjectContact
							.getString("is_enabled");

					mArrayListpromoid.add(mStringspromoid);

					mArrayListpersonal_or_API.add(mStringsis_enabled);

				}

			} else {

			}

			mArrayListpromoid.removeAll(mArrayListPHONE);

			for (int i = 0; i < mArrayListpromoid.size(); i++) {
				mDatasourceHandler.UpdateAds(mArrayListpromoid.get(i), "0");
			}

			mCursor = mDatasourceHandler.SponsoredData();
			Log.e("SponsoredData count: ", mCursor.getCount() + "");

			if (mCursor.getCount() != 0) {

				mArrayListpromoid.clear();

				mArrayListisenabled.clear();

				do {

					mArrayListpromoid.add(mCursor.getString(0).trim());

					mArrayListisenabled.add(mCursor.getString(3).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				Log.e("mArrayListpromoid: ", "" + mArrayListpromoid);

				new EditALL().execute();

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Execute Sponsored Asynctask
	 */
	class Edit_Sponsored_Ads extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("doInBackground API_HIT: ", "" + API_HIT);
			if (API_HIT == 1) {
				is_enable = "1";
			} else {
				is_enable = "0";
			}
			int i = Integer.parseInt(params[0]);
			Final_value = mArrayListPHONE.get(i);
			Log.e("doInBackgroundis_enable: ", "" + is_enable);
			Log.e("doInBackground Final_value: ", "" + Final_value);
			String url = JsonParserConnector.get_SponsoredAds(is_enable,
					mStringPassword, deviceId, Final_value);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (count == 0) {
				ProgressBar.showProgressDialog(EditSponsoredAds.this);
			}

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result----", "" + result);

			count++;
			boolean test = false;
			JSONObject jsonOBject = null;
			try {
				jsonOBject = new JSONObject(result);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (jsonOBject.getBoolean("success")) {
					String message = jsonOBject.getString("message");
					Log.e("Final_value: ", "" + Final_value);
					if (API_HIT == 1) {
						Log.e("1 Final_value: ", "" + Final_value
								+ "---API_HIT-----" + API_HIT);
						// API_HIT=1;
						test = mDatasourceHandler.UpdateAds(Final_value, "1");
					} else {
						// API_HIT=1;
						Log.e("0 Final_value: ", "" + Final_value
								+ "---API_HIT-----" + API_HIT);
						test = mDatasourceHandler.UpdateAds(Final_value, "0");
					}
					// Log.e("SEEEEEEEEEEEEEE test: ", "" + test);
					if (count < mArrayListPHONE.size()) {
						new Edit_Sponsored_Ads().execute(count + "", "" + count
								+ "");
					} else {
						// Log.e("", "DONE");
						ProgressBar.hideProgressDialog();
						finish();
						overridePendingTransition(R.anim.slide_out_left,
								R.anim.slide_in_right);
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.e("test: ", "" + test);
			// SponsornedPullList(result);

		}

	}

}
