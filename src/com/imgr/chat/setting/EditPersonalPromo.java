package com.imgr.chat.setting;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.BaseConvert;
import com.imgr.chat.R;
import com.imgr.chat.adapter.SponsoredPersonalAdapter;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.cropping.CropOption;
import com.imgr.chat.cropping.CropOptionAdapter;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.SponsoredPersonalModel;
import com.imgr.chat.uploadfile.AndroidMultiPartEntity;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.NewLogMessage;
import com.imgr.chat.util.UI;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class EditPersonalPromo extends Activity {
	Button buttonCancel, btn_done, mButtonDelete;
	ImageView fixed, mImageViewOrignal;
	TextView header_title;
	String mStringtest = "1";

	private static final int CROP_FROM_CAMERA = 3;
	private Uri mImageCaptureUri;
	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	// private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int PICK_FROM_CAMERA = 1;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "ImgrChat";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;
	String mStringBase64, mStringusername;
	static String mStringType = "1";
	SharedPreferences sharedPreferences;

	String mStringPassword, deviceId;
	Editor editor;
	EditText promo_name, header_promo_message, link_promo, link_text;
	String mString_promo_name, mString_header_promo_message,
			mString_link_promo, mString_link_text;

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	String mStringPromoId, mStringImageUrl, mStringPromoName,
			mStringHeader_Message, mStringLink, mStringLinktext;

	AlertDialog pinDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editpersonalpromo);
		mButtonDelete = (Button) findViewById(R.id.btn_delete);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);

		fixed = (ImageView) findViewById(R.id.fixed);
		mImageViewOrignal = (ImageView) findViewById(R.id.promo_image_orignal);

		header_promo_message = (EditText) findViewById(R.id.header_promo_message);
		promo_name = (EditText) findViewById(R.id.promo_name);

		link_promo = (EditText) findViewById(R.id.link_promo);
		link_text = (EditText) findViewById(R.id.link_text);

		buttonCancel = (Button) findViewById(R.id.btnCancel);
		btn_done = (Button) findViewById(R.id.btn_done);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringusername = sharedPreferences.getString(Constant.USERNAME_XMPP,
				"");
		header_title = (TextView) findViewById(R.id.header_title);
		buttonCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		mButtonDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new ExecuteDeletePersonal().execute();
			}
		});

		btn_done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mString_promo_name = promo_name.getText().toString();
				mString_header_promo_message = header_promo_message.getText()
						.toString();
				mString_link_promo = link_promo.getText().toString();
				mString_link_text = link_text.getText().toString();
				if (mString_header_promo_message.equals("")) {
					mString_header_promo_message = "";
				}
				if (mString_link_promo.equals("")) {
					mString_link_promo = "";
					mString_link_text = "";
				}
				if (mString_link_text.equals("")) {
					if (!mString_link_promo.equals("")) {
						mString_link_text = "Tap here to learn more";
					} else {
						mString_link_text = "";
					}
				}
				if (mStringBase64 != null) {

				} else {
					mImageViewOrignal.setVisibility(View.VISIBLE);

					Bitmap bitmap = loadBitmapFromView(mImageViewOrignal);

					String mString = convert_image(bitmap);
					File mFile = savebitmap(mString);
					mStringType = "64";
					mStringBase64 = mFile.getAbsolutePath();
				}
				boolean receive = validation(mString_promo_name, mStringBase64);
				if (receive) {
					new ExecuteUpdatePromo().execute();
				}

			}
		});

		mImageViewOrignal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectImage();
			}
		});
		fixed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Send_again_popup();
			}
		});
		promo_name.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() == 25) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					LogMessage.showDialog(EditPersonalPromo.this, null,
							"Promo name should not exceed 35 characters", null,
							"OK");
				}
			}
		});

	}

	protected void Send_again_popup() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.new_promo_pop_up, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);

		pinDialog = new AlertDialog.Builder(EditPersonalPromo.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						// sharedPrefernces();
						pinDialog.cancel();

					}
				});

			}
		});

		pinDialog.show();

	}

	protected boolean validation(String mString_promo_name, String image) {
		// TODO Auto-generated method stub

		if (mString_promo_name.matches("")) {
			LogMessage.showDialog(EditPersonalPromo.this, null,
					"Please enter promo name", null, "OK");
			return false;
		}
		if (image.matches("")) {
			LogMessage.showDialog(EditPersonalPromo.this, null,
					"Please add some image for promo.", null, "OK");
			return false;
		}

		return true;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mStringType.equals("0")) {

			// mImageViewOrignal=null;
			imageLoader = ImageLoader.getInstance();
			options = new DisplayImageOptions.Builder()
					.showStubImage(R.drawable.ic_launcher)
					.showImageForEmptyUri(R.drawable.ic_launcher)
					.cacheInMemory().cacheOnDisc().build();
			final float scale = getResources().getDisplayMetrics().density;
			int padding_34dp = (int) (34 * scale + 0.5f);
			// imageLoader.displayImage(listFlag.get(position),
			// view.imgViewFlag);

			mImageViewOrignal.setVisibility(View.VISIBLE);
			imageLoader.displayImage(CustomGridViewGalleryActivity.path,
					mImageViewOrignal, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									EditPersonalPromo.this, R.anim.fade_in);
							// imgViewFlag.setAnimation(anim);
							// anim.start();
						}
					});

		} else {
			if (mStringtest.equals("1")) {
				mStringtest = "2";
				try {
					mStringPassword = sharedPreferences.getString(
							Constant.PASSWORD_XMPP, "");
					deviceId = Secure.getString(this.getContentResolver(),
							Secure.ANDROID_ID);
					Log.e("deviceId:", "" + deviceId);

					mStringPromoId = getIntent().getStringExtra("PROMO_ID");
					Log.e("mStringPromoId: ", "" + mStringPromoId);
					mCursor = mDatasourceHandler
							.FETCH_PROMO_DATA(mStringPromoId);
					Log.e("SponsoredData count: ", mCursor.getCount() + "");
					if (mCursor.getCount() != 0) {
						mStringHeader_Message = mCursor.getString(1).trim();

						mStringLink = mCursor.getString(2).trim();

						mStringLinktext = mCursor.getString(3).trim();
						mStringImageUrl = mCursor.getString(4).trim();
						mStringPromoName = mCursor.getString(5).trim();

					} else {

					}

					promo_name.setText(mStringPromoName);
					promo_name.setSelection(promo_name.getText().length());
					header_promo_message.setText(mStringHeader_Message);
					header_promo_message.setSelection(header_promo_message
							.getText().length());
					link_promo.setText(mStringLink);
					link_promo.setSelection(link_promo.getText().length());
					link_text.setText(mStringLinktext);
					link_text.setSelection(link_text.getText().length());
					// /btn_test_url_promo.setText(mStringTestPromoUrl);
					imageLoader = ImageLoader.getInstance();
					options = new DisplayImageOptions.Builder()
							.showStubImage(R.drawable.ic_launcher)
							.showImageForEmptyUri(R.drawable.ic_launcher)
							.cacheInMemory().cacheOnDisc().build();

					mImageViewOrignal.setVisibility(View.VISIBLE);
					imageLoader.displayImage(mStringImageUrl,
							mImageViewOrignal, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(
													getApplicationContext(),
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
					header_title.setText(mStringPromoName);
				} catch (Exception ex) {

				}
			}

		}

	}

	protected void selectImage() {

		final CharSequence[] options = { "Choose from IMGR",
				"Choose from existing", "Take Photo", "Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				EditPersonalPromo.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from existing")) {
					chooseFromGallery();

				} else if (options[item].equals("Choose from IMGR")) {
					mStringType = "0";
					Intent mIntent = new Intent(EditPersonalPromo.this,
							CustomGridViewGalleryActivity.class);
					startActivity(mIntent);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
				} else if (options[item].equals("Cancel")) {
					mStringType = "64";
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		mImageCaptureUri = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), "tmp_"
				+ String.valueOf(System.currentTimeMillis()) + ".jpg"));

		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
				mImageCaptureUri);

		try {
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PICK_FROM_CAMERA);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case PICK_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				doCrop();

			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				mImageCaptureUri = data.getData();

				doCrop();

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(EditPersonalPromo.this,
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(EditPersonalPromo.this, "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case CROP_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				Bundle extras = data.getExtras();

				mStringType = "64";
				if (extras != null) {

					Bitmap photo = extras.getParcelable("data");

					mImageViewOrignal.setVisibility(View.VISIBLE);

					mImageViewOrignal.setImageBitmap(photo);

					Bitmap bitmap = loadBitmapFromView(mImageViewOrignal);

					String mString = convert_image(bitmap);
					File mFile = savebitmap(mString);

					mStringBase64 = mFile.getAbsolutePath();

				}

				// File f = new File(mImageCaptureUri.getPath());
				// mStringBase64=f.getAbsolutePath();
				// if (f.exists())
				// f.delete();
			}
			break;
		}

	}

	public static Bitmap loadBitmapFromView(ImageView v) {
		Bitmap b = Bitmap.createBitmap(v.getLayoutParams().width,
				v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
		v.draw(c);
		return b;
	}

	// public static Bitmap loadBitmapFromView(ImageView v) {
	// Bitmap b = Bitmap.createBitmap(v.getLayoutParams().width,
	// v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
	// Canvas c = new Canvas(b);
	//
	// // v.layout(0, 0, v.getLayoutParams().width,
	// // v.getLayoutParams().height);
	// v.draw(c);
	// return b;
	// }

	public Bitmap toGrayscale(Bitmap bmpOriginal) {

		final float scale = getResources().getDisplayMetrics().density;
		int padding_34dp = (int) (34 * scale + 0.5f);
		Bitmap bmpGrayscale = Bitmap.createBitmap(padding_34dp, padding_34dp,
				Bitmap.Config.RGB_565);
		Canvas c = new Canvas(bmpGrayscale);

		c.drawBitmap(bmpOriginal, 0, 0, null);
		return bmpGrayscale;
	}

	public String BitMapToString(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		byte[] b = baos.toByteArray();
		String temp = Base64.encodeToString(b, Base64.DEFAULT);
		return temp;
	}

	public static void writeByteArraysToFile(String fileName, byte[] content)
			throws IOException {

		File file = new File(fileName);
		BufferedOutputStream writer = new BufferedOutputStream(
				new FileOutputStream(file));
		writer.write(content);
		writer.flush();
		writer.close();

	}

	private File savebitmap(String filename) {
		File file = null;
		// byte[] imgBytes = BaseConvert.encodeBytes(byte_arr);
		try {
			byte[] imgBytes = BaseConvert.decode(filename);
			file = new File(Environment.getExternalStorageDirectory()
					+ "/imgr.png");
			if (file.exists()) {
				file.delete();
				file = new File(Environment.getExternalStorageDirectory()
						+ "/imgr.png");
			} else {

			}

			FileOutputStream os = new FileOutputStream(file, true);
			os.write(imgBytes);
			os.flush();
			os.close();
			// BufferedOutputStream writer = new BufferedOutputStream(new
			// FileOutputStream(file));
			// writer.write(imgBytes);
			// writer.flush();
			// writer.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.e("", "" + file);

		// try
		// {
		// File file2 = new File(Environment.getExternalStorageDirectory() +
		// "/hello-5.wav");
		// FileOutputStream os = new FileOutputStream(file2, true);
		// os.write(decoded);
		// os.close();
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }

		return file;

	}

	public static String toHex(byte[] bytes) {
		BigInteger bi = new BigInteger(1, bytes);
		Log.e("-------------test---------: ",
				"" + String.format("%0" + (bytes.length << 1) + "X", bi));
		return String.format("%0" + (bytes.length << 1) + "X", bi);
	}

	public static byte[] bitmap_ToByteArray(Bitmap bm) {

		int intArray[];
		intArray = new int[bm.getWidth() * bm.getHeight()];
		// Create the buffer with the correct size
		int iBytes = bm.getWidth() * bm.getHeight() * 4;
		ByteBuffer buffer = ByteBuffer.allocate(iBytes);

		// Log.e("DBG", buffer.remaining()+""); -- Returns a correct number
		// based on dimensions
		// Copy to buffer and then into byte array
		bm.copyPixelsToBuffer(buffer);
		// Log.e("DBG", buffer.remaining()+""); -- Returns 0
		Log.e("DBG", intArray + "");
		Log.e("DBG", intArray.toString() + "");
		return buffer.array();
	}

	public byte[] bitmapToByteArray(Bitmap bitmap) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(bitmap.getByteCount());
		bitmap.copyPixelsToBuffer(byteBuffer);
		Log.e("--------------", "" + byteBuffer.toString());
		return byteBuffer.array();
	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to

		byte[] byte_arr = stream.toByteArray();

		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		if (size == 0) {
			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 400);
			intent.putExtra("outputY", 400);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			if (size == 1) {
				Intent i = new Intent(intent);
				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);

					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(
						getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);
							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							getContentResolver().delete(mImageCaptureUri, null,
									null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	/**
	 * Execute UpdatePromo Asynctask
	 */
	class ExecuteUpdatePromo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			if (mStringType.equals("64")) {
				url = uploadFile();
			} else {
				mStringBase64 = CustomGridViewGalleryActivity.path;
				mStringBase64 = mStringBase64.replace(
						"https://imgrapp.com/release/", "");
				Log.e("mString_promo_name: ", "" + mString_promo_name);
				Log.e("mStringBase64: ", "" + mStringBase64);
				Log.e("mString_header_promo_message: ", ""
						+ mString_header_promo_message);
				Log.e("mString_promo_name: ", "" + mString_promo_name);
				Log.e("mString_promo_name: ", "" + mString_promo_name);
				url = JsonParserConnector.UpdatePersonalPromoInfo(
						mString_promo_name, mStringBase64, mString_link_promo,
						mString_link_text, mString_header_promo_message,
						mStringType, mStringusername, mStringPromoId);

			}

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(EditPersonalPromo.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			UI.hideProgressDialog();
			PersonalList(result);
		}

	}

	private String uploadFile() {
		String responseString = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Constant.UPDATE_PERSONAL_PROMO);

		try {
			AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
					getApplicationContext());

			// File sourceFile = new File(mStringBase64);
			File sourceFile = new File(mStringBase64);
			Log.e("sourceFile: ", "" + sourceFile.getAbsolutePath());

			// Adding file data to http body

			// Extra parameters if you want to pass to server

			entity.addPart("apiKey", new StringBody("imgr"));
			entity.addPart("promo_image", new FileBody(sourceFile));
			entity.addPart("promo_id", new StringBody(mStringPromoId));
			entity.addPart("promo_name", new StringBody(mString_promo_name));
			entity.addPart("promo_link", new StringBody(mString_link_promo));
			entity.addPart("promo_link_text", new StringBody(mString_link_text));
			entity.addPart("promo_message_header", new StringBody(
					mString_header_promo_message));
			entity.addPart("username", new StringBody(mStringusername));

			httppost.setEntity(entity);
			Log.e("entity: ", "" + entity);
			// Making server call
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity r_entity = response.getEntity();
			Log.e("r_entity: ", "" + r_entity);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				// Server response
				responseString = EntityUtils.toString(r_entity);
				Log.e("responseString: ", "" + responseString);
			} else {
				responseString = "Error occurred! Http Status Code: "
						+ statusCode;
			}

		} catch (ClientProtocolException e) {
			responseString = e.toString();
		} catch (IOException e) {
			responseString = e.toString();
		}

		return responseString;

	}

	private void PersonalList(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null;

			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				Log.e("sys: ", "" + sys);
				mStringspromoid = sys.getString("promoId");
				mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				// mStringsmodified_date = sys.getString("modified_date");

				Log.e("mStringspromoid: ", "" + mStringspromoid);
				Log.e("mStringsusername: ", "" + mStringsusername);
				Log.e("mStringspromo_name: ", "" + mStringspromo_name);
				Log.e("mStringspromo_image: ", ""
						+ "https://imgrapp.com/release/" + mStringspromo_image);
				Log.e("mStringsmodified_date: ", "" + mStringsmodified_date);
				Log.e("mString_link_promo: ", "" + mString_link_promo);
				Log.e("mString_header_promo_message: ", ""
						+ mString_header_promo_message);
				Log.e("mString_link_text: ", "" + mString_link_text);

				boolean flag = mDatasourceHandler.updateSponsored(
						mStringspromoid, "personal", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0", "", "1", "1");

				Log.e("flag", "" + flag);
				if (mString_link_promo.equals("")) {
					New_again_popup();
				} else {
					NewLogMessage.showDialog(this, EditPersonalPromo.this,
							null, "Promo updated successfully", null, "OK");
				}

				// Constant.UPDATE_SUCCESSFULLY=1;
				// finish();
				// overridePendingTransition(R.anim.slide_out_left,
				// R.anim.slide_in_right);

			} else {

			}

			// mDatasourceHandler.insertPersonal(mStringspromoid, "null",
			// mStringspromo_name, mStringspromo_name, mString_link_promo,
			// mString_link_text, mString_header_promo_message, "0",
			// mStringsmodified_date, "1", "1", mStringType);

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	/**
	 * Execute Personal Asynctask
	 */
	class ExecuteDeletePersonal extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.DeletePersonalPromo(
					mStringPromoId, mStringPassword, deviceId);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(EditPersonalPromo.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result----", "" + result);

			UI.hideProgressDialog();
			DeleteList(result);

		}
	}

	private void DeleteList(String response) {
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				mDatasourceHandler.deletePromoRow(mStringPromoId);
				Constant.DELETEPROMO = "true";
				NewLogMessage.showDialog(this, EditPersonalPromo.this, null,
						"Delete Successfully", null, "OK");
				// finish();
				// overridePendingTransition(R.anim.slide_out_left,
				// R.anim.slide_in_right);

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	protected void New_again_popup() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(R.layout.popup_canot,
				null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);

		pinDialog = new AlertDialog.Builder(EditPersonalPromo.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						// sharedPrefernces();
						pinDialog.cancel();
						finish();
						overridePendingTransition(R.anim.slide_out_left,
								R.anim.slide_in_right);

					}
				});

			}
		});

		pinDialog.show();

	}

}
