package com.imgr.chat.setting;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.adapter.ImgrAdapter;
import com.imgr.chat.contact.BlockAndMeaageActivity;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.FastListView;
import com.imgr.chat.model.IMGR_Model;
import com.joooonho.SelectableRoundedImageView;

public class AddNewOpenImgrList extends Activity {

	FastListView mFastListView;
	ImgrAdapter mImgrAdapter;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;

	ArrayList<String> Imgrcontactunquieid = new ArrayList<String>();
	ArrayList<String> Imgrcontactlastname = new ArrayList<String>();
	ArrayList<String> Imgrcontactname = new ArrayList<String>();
	ArrayList<String> Imgrcontactnumber = new ArrayList<String>();
	ArrayList<String> ImgrMemberId = new ArrayList<String>();
	ArrayList<String> mArrayListBlockedContact;

	ArrayList<String> mArrayListBase64;
	ArrayList<IMGR_Model> arraylistImgr = new ArrayList<IMGR_Model>();
	TextView btn_back_loginpage;
	EditText input_search_query;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// block
		setContentView(R.layout.addnewopenimgrlist);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mArrayListBase64 = new ArrayList<String>();
		mArrayListBlockedContact = new ArrayList<String>();
		Imgrcontactnumber = new ArrayList<String>();
		Imgrcontactlastname = new ArrayList<String>();
		Imgrcontactunquieid = new ArrayList<String>();
		Imgrcontactname = new ArrayList<String>();
		ImgrMemberId = new ArrayList<String>();
		mFastListView = (FastListView) findViewById(R.id.list_IMGR);
		btn_back_loginpage = (TextView) findViewById(R.id.btn_back_loginpage);
		input_search_query = (EditText) findViewById(R.id.input_search_query);
		btn_back_loginpage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		input_search_query.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					mImgrAdapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			mCursor = mDatasourceHandler.ImgrContacts();
			Log.e("Local Fragment: ", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			Imgrcontactname.clear();
			Imgrcontactlastname.clear();
			Imgrcontactnumber.clear();
			mArrayListBase64.clear();
			Imgrcontactunquieid.clear();
			mArrayListBlockedContact.clear();
			// ImgrMemberId.clear();
			do {

				Imgrcontactname.add(mCursor.getString(1).trim());
				Imgrcontactnumber.add(mCursor.getString(2).trim());
				Imgrcontactlastname.add(mCursor.getString(3).trim());
				// ImgrMemberId.add(mCursor.getString(5).trim());
				mArrayListBase64.add(mCursor.getString(7).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}

		if (Imgrcontactname.size() == 0) {
			Toast.makeText(getApplicationContext(), "No Contacts",
					Toast.LENGTH_SHORT).show();
		} else {

			for (int i = 0; i < Imgrcontactnumber.size(); i++) {
				if (mArrayListBlockedContact.get(i).equals("true")) {

				} else {
					IMGR_Model wp = new IMGR_Model(Imgrcontactname.get(i),
							Imgrcontactlastname.get(i),
							Imgrcontactnumber.get(i), mArrayListBase64.get(i),
							Imgrcontactunquieid.get(i), Imgrcontactname.get(i)
									+ " " + Imgrcontactlastname.get(i), "");
					// Binds all strings into an array
					arraylistImgr.add(wp);
				}

			}
			Log.e("Imgrcontactnumber.size():", "" + Imgrcontactnumber.size());
			mImgrAdapter = new ImgrAdapter(getApplicationContext(),
					Imgrcontactname, Imgrcontactnumber, arraylistImgr);

			mFastListView.setAdapter(mImgrAdapter);

		}
	}

	public class ImgrAdapter extends BaseAdapter implements SectionIndexer {

		private ArrayList<String> contactnumber1;
		private LayoutInflater inflater = null;
		String select;
		Intent intent;
		private List<String> originalData = null;
		private List<String> filteredData = null;
		private Context context;
		private String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		private ItemFilter mFilter = new ItemFilter();

		private List<IMGR_Model> mPhoneModels = null;
		private ArrayList<IMGR_Model> arraylist;

		public ImgrAdapter(Context fragmentActivity,
				ArrayList<String> contactname, ArrayList<String> contactnumber,
				List<IMGR_Model> mPhoneModels) {
			// TODO Auto-generated constructor stub
			this.mPhoneModels = mPhoneModels;
			contactnumber1 = contactnumber;
			originalData = contactname;
			filteredData = contactname;
			context = fragmentActivity;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.arraylist = new ArrayList<IMGR_Model>();
			this.arraylist.addAll(mPhoneModels);

		}

		private void setSection(LinearLayout header, String label) {
			select = label.substring(0, 1).toUpperCase();
			TextView text = new TextView(context);
			// header.setBackgroundColor();
			text.setTextColor(Color.parseColor("#007AFF"));
			text.setText(select);
			text.setTextSize(20);
			text.setPadding(20, 0, 0, 0);
			text.setGravity(Gravity.CENTER_VERTICAL);
			header.addView(text);

		}

		public int getCount() {
			return mPhoneModels.size();
		}

		public IMGR_Model getItem(int position) {
			return mPhoneModels.get(position);
		}

		public long getItemId(int position) {
			return mPhoneModels.get(position).hashCode();
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView;
			rowView = inflater.inflate(R.layout.imgr_listrow, null);

			LinearLayout header = (LinearLayout) rowView
					.findViewById(R.id.section);
			SelectableRoundedImageView mImageViewProfile = (SelectableRoundedImageView) rowView
					.findViewById(R.id.imageView1);
			String name = mPhoneModels.get(position).getName();
			// String number = contactnumber1.get(position);
			char firstChar = name.toUpperCase().charAt(0);
			if (position == 0) {
				setSection(header, name);
			} else {
				String preLabel = mPhoneModels.get(position - 1).getName();
				char preFirstChar = preLabel.toUpperCase().charAt(0);
				if (firstChar != preFirstChar) {
					setSection(header, name);
				} else {
					header.setVisibility(View.GONE);
				}
			}
			mImageViewProfile.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

				}
			});

			final TextView textView = (TextView) rowView
					.findViewById(R.id.textView);
			// final TextView textView1 = (TextView)
			// rowView.findViewById(R.id.textView1);
			textView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mDatasourceHandler.UpdateBlockContact(
							mPhoneModels.get(position).getPhone(), "true");
					BlockedContacts.block = 1;
					finish();

				}
			});
			ImageView msg = (ImageView) rowView.findViewById(R.id.message);
			msg.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent mIntent_info = new Intent(AddNewOpenImgrList.this,
							BlockAndMeaageActivity.class);
					mIntent_info.putExtra("name_select",
							mPhoneModels.get(position).getName());
					mIntent_info.putExtra("phone_id", mPhoneModels
							.get(position).getPhone());
					mIntent_info.putExtra("phoneno_select",
							mPhoneModels.get(position).getNumber());

					startActivity(mIntent_info);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
				}

			});
			textView.setText(mPhoneModels.get(position).getName() + " "
					+ mPhoneModels.get(position).getLast());
			if (mPhoneModels.get(position).getBase64().equals("Null")) {
				mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
				mImageViewProfile.setOval(true);
				mImageViewProfile
						.setBackgroundResource(R.drawable.ic_contact_place);
			} else {
				Bitmap mBitmap = ConvertToImage(mPhoneModels.get(position)
						.getBase64());
				mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
				mImageViewProfile.setOval(true);
				mImageViewProfile.setImageBitmap(mBitmap);
			}

			return rowView;
		}

		public Bitmap ConvertToImage(String image) {
			try {
				byte[] imageAsBytes = Base64.decode(image.getBytes(),
						Base64.DEFAULT);

				Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
						imageAsBytes.length);

				return bitmap;
			} catch (Exception e) {
				return null;
			}
		}

		@Override
		public int getPositionForSection(int section) {
			// Log.e("ListView", "Get position for section");

			// Log.e("getCount(): ", "" + getCount());

			for (int i = 0; i < this.getCount(); i++) {
				// Log.e("indside: ", "" + this.getItem(i).getName());

				String item = this.getItem(i).getName();
				if (item.charAt(0) == sections.charAt(section))
					return i;
			}
			return 0;
		}

		@Override
		public int getSectionForPosition(int arg0) {
			Log.d("ListView", "Get section");
			return 0;
		}

		@Override
		public Object[] getSections() {

			String[] sectionsArr = new String[sections.length()];
			Log.e("sectionsArr: ", "" + sectionsArr);
			for (int i = 0; i < sections.length(); i++) {
				Log.e("sectionsArr: ", "" + sections.charAt(i));
				sectionsArr[i] = "" + sections.charAt(i);
			}

			return sectionsArr;
		}

		public Filter getFilter() {
			// TODO Auto-generated method stub
			return mFilter;
		}

		private class ItemFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				String filterString = constraint.toString().toLowerCase();

				FilterResults results = new FilterResults();

				final List<String> list = originalData;

				int count = list.size();
				final ArrayList<String> nlist = new ArrayList<String>(count);

				String filterableString;

				for (int i = 0; i < count; i++) {
					filterableString = list.get(i);
					if (filterableString.toLowerCase().contains(filterString)) {
						nlist.add(filterableString);
					}
				}

				results.values = nlist;
				results.count = nlist.size();

				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				filteredData = (ArrayList<String>) results.values;
				Log.e("", "" + filteredData);
				notifyDataSetChanged();
			}

		}

		// Filter Class
		public void filter(String charText) {
			charText = charText.toLowerCase(Locale.getDefault());
			mPhoneModels.clear();
			if (charText.length() == 0) {
				mPhoneModels.addAll(arraylist);
			} else {
				for (IMGR_Model wp : arraylist) {
					if (wp.getName().toLowerCase(Locale.getDefault())
							.contains(charText)) {
						mPhoneModels.add(wp);
					}
				}
			}
			notifyDataSetChanged();
		}

	}

}
