package com.imgr.chat.setting;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.adapter.SponsoredAdapter;
import com.imgr.chat.adapter.SponsoredChatAdapter;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.FastListView;
import com.imgr.chat.model.SponsoredModel;
import com.imgr.chat.setting.ViewSponsored.ExecutePullSponsored;
import com.imgr.chat.setting.ViewSponsored.ExecuteSponsored;
import com.imgr.chat.util.UI;

public class ViewSponsoredChat extends Activity {

	FastListView mListView;
	String mStringPassword, deviceId, mStringPromoOnOFF, mStringPhoneid;
	SharedPreferences sharedPreferences;

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	TextView txtBack;
	ArrayList<String> mArrayListpromoname;
	ArrayList<String> mArrayListpromoid;
	ArrayList<String> mArrayListImageUrl;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListisenabled;
	ArrayList<String> mArrayListpersonal_or;

	ArrayList<String> mArrayListheader;
	ArrayList<String> mArrayListlink_footer;
	ArrayList<String> mArrayListlink_text_footer;
	ArrayList<SponsoredModel> mSponsoredModels;
	SwipeRefreshLayout mSwipeRefreshLayout;
	SponsoredChatAdapter adapter;
	Button btn_edit;
	private EditText searchBox;

	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewsponsoredchat);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mListView = (FastListView) findViewById(R.id.listView);
		mArrayListpromoname = new ArrayList<String>();
		mArrayListpromoid = new ArrayList<String>();
		mArrayListisdelete = new ArrayList<String>();
		mArrayListisactive = new ArrayList<String>();
		mArrayListisenabled = new ArrayList<String>();
		mArrayListImageUrl = new ArrayList<String>();
		mArrayListpersonal_or = new ArrayList<String>();

		mArrayListheader = new ArrayList<String>();
		mArrayListlink_footer = new ArrayList<String>();
		mArrayListlink_text_footer = new ArrayList<String>();
		mStringPhoneid = getIntent().getStringExtra("phone_id");
		// btn_edit = (Button) findViewById(R.id.btn_edit);
		mSponsoredModels = new ArrayList<SponsoredModel>();
		/*
		 * btn_edit.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent mIntent = new Intent(ViewSponsoredChat.this,
		 * EditSponsoredAds.class); startActivity(mIntent);
		 * overridePendingTransition(R.anim.slide_in_left,
		 * R.anim.slide_out_right); } });
		 */
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container_all);
		mSwipeRefreshLayout.setColorSchemeResources(
				android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				new ExecutePullSponsored().execute();
			}
		});
		searchBox = (EditText) findViewById(R.id.input_search_query);
		searchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					adapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		deviceId = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceId:", "" + deviceId);
		mStringPromoOnOFF = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "");

		try {
			mCursor = mDatasourceHandler.SponsoredData();
			Log.e("SponsoredData count: ", mCursor.getCount() + "");

			if (mCursor.getCount() != 0) {
				mArrayListpromoname.clear();
				mArrayListpromoid.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				mArrayListpersonal_or.clear();
				do {

					mArrayListpromoid.add(mCursor.getString(0).trim());
					mArrayListisactive.add(mCursor.getString(1).trim());
					mArrayListisdelete.add(mCursor.getString(2).trim());
					mArrayListisenabled.add(mCursor.getString(3).trim());
					mArrayListpromoname.add(mCursor.getString(4).trim());
					mArrayListpersonal_or.add(mCursor.getString(5).trim());
					Log.e("Matched: ", "" + mCursor.getString(0).trim() + "--"
							+ mCursor.getString(8).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				Log.e("mArrayListpromoid: ", "" + mArrayListpromoid);

				for (int i = 0; i < mArrayListpromoid.size(); i++) {
					// Log.e("mArrayListisactive.get(i): ",
					// ""+mArrayListisactive.get(i)+"---------"+mArrayListisdelete.get(i));
					if (mArrayListpersonal_or.get(i).equals("no_personal")) {
						if (mArrayListisactive.get(i).equals("1")
								&& mArrayListisdelete.get(i).equals("0")) {

							SponsoredModel mSponsoredModel = new SponsoredModel(
									mArrayListpromoid.get(i),
									mArrayListisenabled.get(i),
									mArrayListpromoname.get(i),
									mArrayListisactive.get(i),
									mArrayListisdelete.get(i));
							mSponsoredModels.add(mSponsoredModel);
						}
					}

				}
				Log.e("Size: ", " " + mSponsoredModels.size());

				adapter = new SponsoredChatAdapter(ViewSponsoredChat.this,
						mArrayListpromoname, mArrayListpromoid,
						mArrayListisactive, mSponsoredModels, mStringPhoneid);

				mListView.setAdapter(adapter);

			} else {
				new ExecuteSponsored().execute();
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/**
	 * Execute Sponsored Asynctask
	 */
	class ExecuteSponsored extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.getSponsored("0", mStringPassword,
					deviceId);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ViewSponsoredChat.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result----", "" + result);

			UI.hideProgressDialog();
			SponsornedList(result);

		}

	}

	private void SponsornedList(String response) {
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				mArrayListpromoid.clear();
				mArrayListpromoname.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				mArrayListImageUrl.clear();
				mArrayListlink_text_footer.clear();
				mArrayListheader.clear();
				mArrayListlink_footer.clear();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					// SponsoredModel mContact = new SponsoredModel();
					String mStringspromoid = jsonOBjectContact
							.getString("promo_id");
					String mStringspromo_type = jsonOBjectContact
							.getString("promo_type");

					String mStringspromo_name = jsonOBjectContact
							.getString("promo_name");
					String mStringspromo_image = jsonOBjectContact
							.getString("promo_image");

					String mStringspromo_link = jsonOBjectContact
							.getString("promo_link");

					String mStringspromo_link_text = jsonOBjectContact
							.getString("promo_link_text");
					String mStringspromo_message_header = jsonOBjectContact
							.getString("promo_message_header");
					String mStringsis_deleted = jsonOBjectContact
							.getString("is_deleted");
					String mStringsmodified_date = jsonOBjectContact
							.getString("modified_date");
					String mStringsisActive = jsonOBjectContact
							.getString("isActive");
					String mStringsis_enabled = jsonOBjectContact
							.getString("is_enabled");

					mArrayListpromoid.add(mStringspromoid);
					mArrayListpromoname.add(mStringspromo_name);
					mArrayListisdelete.add(mStringsis_deleted);
					mArrayListisactive.add(mStringsisActive);
					mArrayListisenabled.add(mStringsis_enabled);
					mArrayListImageUrl.add(mStringspromo_image);
					mArrayListlink_text_footer.add(mStringspromo_link_text);
					mArrayListheader.add(mStringspromo_message_header);
					mArrayListlink_footer.add(mStringspromo_link);

				}

			} else {

			}

			for (int i = 0; i < mArrayListpromoid.size(); i++) {
				Log.e("", "" + mArrayListlink_text_footer);
				mDatasourceHandler.insertSponsored(mArrayListpromoid.get(i),
						"no_personal", mArrayListpromoname.get(i),
						mArrayListImageUrl.get(i),
						mArrayListlink_footer.get(i),
						mArrayListlink_text_footer.get(i),
						mArrayListheader.get(i), mArrayListisdelete.get(i), "",
						mArrayListisactive.get(i), mArrayListisenabled.get(i));
				mArrayListpersonal_or.add("no_personal");
			}

			mCursor = mDatasourceHandler.SponsoredData();
			Log.e("SponsoredData count: ", mCursor.getCount() + "");

			if (mCursor.getCount() != 0) {
				mArrayListpromoname.clear();
				mArrayListpromoid.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				do {

					mArrayListpromoid.add(mCursor.getString(0).trim());
					mArrayListisactive.add(mCursor.getString(1).trim());
					mArrayListisdelete.add(mCursor.getString(2).trim());
					mArrayListisenabled.add(mCursor.getString(3).trim());
					mArrayListpromoname.add(mCursor.getString(4).trim());
					mArrayListpersonal_or.add(mCursor.getString(5).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				Log.e("mArrayListpromoid: ", "" + mArrayListpromoid);

				for (int i = 0; i < mArrayListpromoid.size(); i++) {
					// Log.e("mArrayListisactive.get(i): ",
					// ""+mArrayListisactive.get(i)+"---------"+mArrayListisdelete.get(i));
					if (mArrayListpersonal_or.get(i).equals("no_personal")) {
						if (mArrayListisactive.get(i).equals("1")
								&& mArrayListisdelete.get(i).equals("0")) {
							SponsoredModel mSponsoredModel = new SponsoredModel(
									mArrayListpromoid.get(i),
									mArrayListisenabled.get(i),
									mArrayListpromoname.get(i),
									mArrayListisactive.get(i),
									mArrayListisdelete.get(i));
							mSponsoredModels.add(mSponsoredModel);
						}
					}

				}
			}
			Log.e("Size: ", " " + mSponsoredModels.size());

			adapter = new SponsoredChatAdapter(ViewSponsoredChat.this,
					mArrayListpromoname, mArrayListpromoid, mArrayListisactive,
					mSponsoredModels, mStringPhoneid);

			mListView.setAdapter(adapter);

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	/**
	 * Execute Sponsored Asynctask
	 */
	class ExecutePullSponsored extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.getSponsored("0", mStringPassword,
					deviceId);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ViewSponsored.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result----", "" + result);
			mSwipeRefreshLayout.setRefreshing(false);
			// UI.hideProgressDialog();
			SponsornedPullList(result);

		}

	}

	private void SponsornedPullList(String response) {
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				mArrayListpromoid.clear();
				mArrayListpromoname.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				mArrayListImageUrl.clear();
				mArrayListlink_text_footer.clear();
				mArrayListheader.clear();
				mArrayListlink_footer.clear();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					// SponsoredModel mContact = new SponsoredModel();
					String mStringspromoid = jsonOBjectContact
							.getString("promo_id");
					String mStringspromo_type = jsonOBjectContact
							.getString("promo_type");

					String mStringspromo_name = jsonOBjectContact
							.getString("promo_name");
					String mStringspromo_image = jsonOBjectContact
							.getString("promo_image");

					String mStringspromo_link = jsonOBjectContact
							.getString("promo_link");

					String mStringspromo_link_text = jsonOBjectContact
							.getString("promo_link_text");
					String mStringspromo_message_header = jsonOBjectContact
							.getString("promo_message_header");
					String mStringsis_deleted = jsonOBjectContact
							.getString("is_deleted");
					String mStringsmodified_date = jsonOBjectContact
							.getString("modified_date");
					String mStringsisActive = jsonOBjectContact
							.getString("isActive");
					String mStringsis_enabled = jsonOBjectContact
							.getString("is_enabled");

					mArrayListpromoid.add(mStringspromoid);
					mArrayListpromoname.add(mStringspromo_name);
					mArrayListisdelete.add(mStringsis_deleted);
					mArrayListisactive.add(mStringsisActive);
					mArrayListisenabled.add(mStringsis_enabled);
					mArrayListImageUrl.add(mStringspromo_image);
					mArrayListlink_text_footer.add(mStringspromo_link_text);
					mArrayListheader.add(mStringspromo_message_header);
					mArrayListlink_footer.add(mStringspromo_link);

				}

			} else {

			}

			for (int i = 0; i < mArrayListpromoid.size(); i++) {
				mDatasourceHandler.PullSponsoredUnquie(
						mArrayListpromoid.get(i), "no_personal",
						mArrayListpromoname.get(i), mArrayListImageUrl.get(i),
						mArrayListlink_footer.get(i),
						mArrayListlink_text_footer.get(i),
						mArrayListheader.get(i), mArrayListisdelete.get(i), "",
						mArrayListisactive.get(i), mArrayListisenabled.get(i));

				mArrayListpersonal_or.add("no_personal");
			}

			mCursor = mDatasourceHandler.SponsoredData();
			Log.e("SponsoredData count: ", mCursor.getCount() + "");

			if (mCursor.getCount() != 0) {
				mArrayListpromoname.clear();
				mArrayListpromoid.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				do {

					mArrayListpromoid.add(mCursor.getString(0).trim());
					mArrayListisactive.add(mCursor.getString(1).trim());
					mArrayListisdelete.add(mCursor.getString(2).trim());
					mArrayListisenabled.add(mCursor.getString(3).trim());
					mArrayListpromoname.add(mCursor.getString(4).trim());
					mArrayListpersonal_or.add(mCursor.getString(5).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				Log.e("mArrayListpromoid: ", "" + mArrayListpromoid);

				for (int i = 0; i < mArrayListpromoid.size(); i++) {
					// Log.e("mArrayListisactive.get(i): ",
					// ""+mArrayListisactive.get(i)+"---------"+mArrayListisdelete.get(i));
					if (mArrayListpersonal_or.get(i).equals("no_personal")) {
						if (mArrayListisactive.get(i).equals("1")
								&& mArrayListisdelete.get(i).equals("0")) {
							SponsoredModel mSponsoredModel = new SponsoredModel(
									mArrayListpromoid.get(i),
									mArrayListisenabled.get(i),
									mArrayListpromoname.get(i),
									mArrayListisactive.get(i),
									mArrayListisdelete.get(i));
							mSponsoredModels.add(mSponsoredModel);
						}
					}

				}
			}
			Log.e("Size: ", " " + mSponsoredModels.size());

			// adapter = new SponsoredAdapter(ViewSponsored.this,
			// mArrayListpromoname, mArrayListpromoid, mArrayListisactive,
			// mSponsoredModels);

			mListView.setAdapter(adapter);
			adapter.notifyDataSetChanged();

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

}
