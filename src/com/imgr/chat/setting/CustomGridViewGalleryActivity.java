package com.imgr.chat.setting;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.imgr.chat.R;
import com.imgr.chat.SplashActivity;
import com.imgr.chat.adapter.GridviewAdapter;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.model.CustomGalleryModel;
import com.imgr.chat.model.IMGR_Model;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class CustomGridViewGalleryActivity extends Activity {
	/** Called when the activity is first created. */
	TextView mImageView_back;
	private GridviewAdapter mAdapter;
	private ArrayList<String> listCountry;
	private ArrayList<String> listFlag;
	public static String path;
	private GridView gridView;
	ArrayList<CustomGalleryModel> arraylistgallerymodel = new ArrayList<CustomGalleryModel>();
	// check internet connection
	ConnectionDetector mConnectionDetector;

	ArrayList<String> mArrayListpromoname;
	ArrayList<String> mArrayListpromoid;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListisenabled;

	ArrayList<String> mArrayListUrl;

	String mStringusername;
	SharedPreferences sharedPreferences;
	Editor editor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mConnectionDetector = new ConnectionDetector(this);
		mImageView_back = (TextView) findViewById(R.id.btn_back_loginpage);
		mArrayListpromoname = new ArrayList<String>();
		mArrayListpromoid = new ArrayList<String>();
		mArrayListisdelete = new ArrayList<String>();
		mArrayListisactive = new ArrayList<String>();
		mArrayListisenabled = new ArrayList<String>();
		mArrayListUrl = new ArrayList<String>();
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringusername = sharedPreferences.getString(Constant.USERNAME_XMPP,
				"");
		listFlag = new ArrayList<String>();
		listFlag.clear();
		mImageView_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NewPromoActivity.mStringType = "1";
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		gridView = (GridView) findViewById(R.id.gridView1);

		// Implement On Item click listener
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// mAdapter.g
				// path=arraylistgallerymodel.get(position);
				// path = mAdapter.getItem(position);
				// Toast.makeText(CustomGridViewGalleryActivity.this,
				// mAdapter.getItem(position), Toast.LENGTH_SHORT).show();

			}
		});
		if (mConnectionDetector.isConnectingToInternet()) {
			new ExecuteGallery().execute();
		} else {
			LogMessage.showDialog(CustomGridViewGalleryActivity.this, null,
					"No Internet Connection", null, "Ok");
			finish();
		}

	}

	/**
	 * Execute Gallery Asynctask
	 */
	class ExecuteGallery extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.getGalleryImage(mStringusername);

			// Log.e("url----", "" + url);
			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(CustomGridViewGalleryActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			PersonalList(result);
		}

	}

	private void PersonalList(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null;
			arraylistgallerymodel.clear();
			listCountry = new ArrayList<String>();
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject sys = jArray.getJSONObject(i);
					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					mStringsmodified_date = sys.getString("modified_date");
					String mStringsis_deleted = sys.getString("is_deleted");
					// String mStringsisActive = sys
					// .getString("is_active");
					String mStringsis_enabled = sys.getString("is_enabled");

					if (mStringsis_enabled.equals("1")
							&& mStringsis_deleted.equals("0")) {
						// Log.e("mStringspromoid: ", "" + mStringspromoid);
						// Log.e("mStringsusername: ", "" + mStringsusername);
						// Log.e("mStringspromo_name: ", "" +
						// mStringspromo_name);
						// Log.e("mStringspromo_image: ", ""
						// + "https://imgrapp.com/release/"
						// + mStringspromo_image);
						// Log.e("mStringsmodified_date: ", ""
						// + mStringsmodified_date);
						listFlag.add("https://imgrapp.com/release/"
								+ mStringspromo_image);
						CustomGalleryModel mCustomGalleryModel = new CustomGalleryModel(
								mStringspromo_name,
								"https://imgrapp.com/release/"
										+ mStringspromo_image);
						arraylistgallerymodel.add(mCustomGalleryModel);
					}

				}

			} else {

			}
			Collections.sort(arraylistgallerymodel);

			mAdapter = new GridviewAdapter(this, listFlag,
					arraylistgallerymodel);
			// mAdapter.

			// Set custom adapter to gridview

			gridView.setAdapter(mAdapter);

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

}