package com.imgr.chat.setting;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.util.LogMessage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class OpendetailPersonalPromo extends Activity {

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	Cursor mCursor;
	TextView txtBack, promo_name, promo_header_message, promo_link_url,
			promo_link_text, header_title;
	String mStringPromoId, mStringImageUrl, mStringPromoName,
			mStringHeader_Message, mStringLink, mStringLinktext,
			mStringTestPromoUrl;
	ImageView promo_image_orignal;
	Button btn_test_url_promo, btn_edit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.open_detail_personal_promo);
		btn_edit = (Button) findViewById(R.id.btn_edit);
		promo_image_orignal = (ImageView) findViewById(R.id.promo_image_orignal);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		btn_test_url_promo = (Button) findViewById(R.id.btn_test_url_promo);

		promo_name = (TextView) findViewById(R.id.promo_name);
		promo_header_message = (TextView) findViewById(R.id.promo_header_message);
		promo_link_url = (TextView) findViewById(R.id.promo_link_url);
		promo_link_text = (TextView) findViewById(R.id.promo_link_text);
		header_title = (TextView) findViewById(R.id.header_title);

		btn_edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(OpendetailPersonalPromo.this,
						EditPersonalPromo.class);
				mIntent.putExtra("PROMO_ID", mStringPromoId);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);

			}
		});

		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		btn_test_url_promo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// String mString=btn_test_url_promo.getText().toString();
				if (mStringTestPromoUrl.equals("No Link")) {
					LogMessage.showDialog(OpendetailPersonalPromo.this, null,
							"No Link", null, "Ok");
				} else {
					if (!mStringTestPromoUrl.startsWith("https://")
							&& !mStringTestPromoUrl.startsWith("http://")) {
						mStringTestPromoUrl = "http://" + mStringTestPromoUrl;
					}
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(mStringTestPromoUrl));
					startActivity(i);
				}
			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constant.DELETEPROMO.equals("true")) {
			Constant.DELETEPROMO = "false";
			finish();
			overridePendingTransition(R.anim.slide_out_left,
					R.anim.slide_in_right);
		} else {
			mDatabaseHelper = new DatabaseHelper(this);
			mDatasourceHandler = new DatasourceHandler(this);
			try {
				mStringPromoId = getIntent().getStringExtra("PROMO_ID");
				Log.e("mStringPromoId: ", "" + mStringPromoId);
				mCursor = mDatasourceHandler.FETCH_PROMO_DATA(mStringPromoId);
				Log.e("SponsoredData count: ", mCursor.getCount() + "");
				if (mCursor.getCount() != 0) {
					mStringHeader_Message = mCursor.getString(1).trim();

					mStringLink = mCursor.getString(2).trim();
					mStringTestPromoUrl = mCursor.getString(2).trim();
					mStringLinktext = mCursor.getString(3).trim();
					mStringImageUrl = mCursor.getString(4).trim();
					mStringPromoName = mCursor.getString(5).trim();

				} else {

				}

				Log.e("mStringLink: ", mStringLink + "");
				Log.e("mStringTestPromoUrl: ", mStringTestPromoUrl + "");
				Log.e("mStringLinktext: ", mStringLinktext + "");
				Log.e("mStringImageUrl: ", mStringImageUrl + "");
				Log.e("mStringPromoName: ", mStringPromoName + "");
				if (mStringHeader_Message.equals("")) {
					// mStringHeader_Message = "No Promo Header";
				}
				if (mStringLink.equals("")) {
					// mStringLink = "No Link";
				}
				if (mStringTestPromoUrl.equals("")) {
					// mStringTestPromoUrl = "No Link";
				}
				if (mStringPromoName.equals("")) {
					// mStringPromoName = "No Promo Name";
				}
				promo_name.setText(mStringPromoName);
				promo_header_message.setText(mStringHeader_Message);
				promo_link_url.setText(mStringLink);
				promo_link_text.setText(mStringLinktext);

				imageLoader = ImageLoader.getInstance();
				options = new DisplayImageOptions.Builder()
						.showStubImage(R.drawable.ic_launcher)
						.showImageForEmptyUri(R.drawable.ic_launcher)
						.cacheInMemory().cacheOnDisc().build();
				imageLoader.displayImage(mStringImageUrl, promo_image_orignal,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils
										.loadAnimation(getApplicationContext(),
												R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});
				header_title.setText(mStringPromoName);
				// if(Constant.UPDATE_SUCCESSFULLY==1){
				// Constant.UPDATE_SUCCESSFULLY=0;
				// LogMessage.showDialog(OpendetailPersonalPromo.this, null,
				// "Promo updated successfully", null, "OK");
				// }
				// else{
				// Constant.UPDATE_SUCCESSFULLY=0;
				// }

			} catch (Exception ex) {

			}
		}

	}

}
