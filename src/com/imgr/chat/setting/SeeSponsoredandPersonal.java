package com.imgr.chat.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;

public class SeeSponsoredandPersonal extends Activity {
	TextView mTextViewBack, mTextViewSponsored, mTextViewPersonal;
	String mStringPhoneid;
	ImageView imageView2, imageView3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seesponsoredandpersonal);
		mTextViewBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mTextViewSponsored = (TextView) findViewById(R.id.txtTitle);
		mTextViewPersonal = (TextView) findViewById(R.id.mTextViewPersonal);

		imageView2 = (ImageView) findViewById(R.id.imageView2);
		imageView3 = (ImageView) findViewById(R.id.imageView3);
		mStringPhoneid = getIntent().getStringExtra("phone_id");
		imageView3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SeeSponsoredandPersonal.this,
						ViewSponsoredPersonalchat.class);
				mIntent.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		mTextViewBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		imageView2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SeeSponsoredandPersonal.this,
						ViewSponsoredChat.class);
				mIntent.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		mTextViewSponsored.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SeeSponsoredandPersonal.this,
						ViewSponsoredChat.class);
				mIntent.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		mTextViewPersonal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SeeSponsoredandPersonal.this,
						ViewSponsoredPersonalchat.class);
				mIntent.putExtra("phone_id", mStringPhoneid);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (Constant.PROMO_OFF_NOW_SELECT_ONE == 1) {
			finish();

			overridePendingTransition(R.anim.slide_out_left,
					R.anim.slide_in_right);
		} else if (Constant.SPONSORED_PROMO_OFF_ == 1) {
			finish();

			overridePendingTransition(R.anim.slide_out_left,
					R.anim.slide_in_right);
		} else {
			Constant.PROMO_OFF_NOW_SELECT_ONE = 0;
		}
	}

}
