package com.imgr.chat.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;

/**
 * author: amit agnihotri
 */
public class SettingPromoActivity extends Activity {
	TextView mImageView_back;
	Button button_promo;
	LinearLayout mLinearLayout_two, mLinearLayout_fourth;

	Switch switch_sponsored, switch_promons_personal,
			switch_auto_rotate_promons;
	SharedPreferences sharedPreferences;
	Editor editor;
	String mStringSponsored, mStringPersonal, mStringAutorotate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_promo);
		mImageView_back = (TextView) findViewById(R.id.btn_back_loginpage);
		mLinearLayout_two = (LinearLayout) findViewById(R.id.linear_two);
		mLinearLayout_fourth = (LinearLayout) findViewById(R.id.linear_fourth);
		button_promo = (Button) findViewById(R.id.button_promo_create);
		switch_sponsored = (Switch) findViewById(R.id.switch_sponsored);
		switch_promons_personal = (Switch) findViewById(R.id.switch_promons_personal);
		switch_auto_rotate_promons = (Switch) findViewById(R.id.switch_auto_rotate_promons);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringSponsored = sharedPreferences.getString(Constant.SPONSORED_ADS,
				"");
		mStringPersonal = sharedPreferences
				.getString(Constant.PERSONAL_ADS, "");
		mStringAutorotate = sharedPreferences.getString(
				Constant.SETTING_PROMO_AUTO_ROTATE, "");
		if (mStringSponsored.equals("true")) {
			switch_sponsored.setChecked(true);
			mLinearLayout_two.setVisibility(View.VISIBLE);
		} else {
			switch_sponsored.setChecked(false);
			mLinearLayout_two.setVisibility(View.GONE);
		}
		if (mStringPersonal.equals("true")) {
			switch_promons_personal.setChecked(true);
			mLinearLayout_fourth.setVisibility(View.VISIBLE);
		} else {
			switch_promons_personal.setChecked(false);
			mLinearLayout_fourth.setVisibility(View.GONE);
		}

		if (mStringAutorotate != null) {
			if (mStringAutorotate.equals("true")) {
				switch_auto_rotate_promons.setChecked(true);
			} else {
				switch_auto_rotate_promons.setChecked(false);
			}
		} else {
			switch_auto_rotate_promons.setChecked(false);
			shared_Prefernces_Auto_Rotate("false");
		}
		switch_auto_rotate_promons
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							shared_Prefernces_Auto_Rotate("true");
						} else {
							shared_Prefernces_Auto_Rotate("false");
						}

					}
				});

		switch_sponsored
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mLinearLayout_two.setVisibility(View.VISIBLE);
							shared_Prefernces_Sponsorned("true");
						} else {
							mLinearLayout_two.setVisibility(View.GONE);
							shared_Prefernces_Sponsorned("false");
						}

					}
				});
		switch_promons_personal
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mLinearLayout_fourth.setVisibility(View.VISIBLE);
							shared_Prefernces_Personal("true");
						} else {
							mLinearLayout_fourth.setVisibility(View.GONE);
							shared_Prefernces_Personal("false");
						}

					}
				});
		mImageView_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		mLinearLayout_two.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SettingPromoActivity.this,
						ViewSponsored.class);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		mLinearLayout_fourth.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SettingPromoActivity.this,
						ViewPersonalPromo.class);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		button_promo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SettingPromoActivity.this,
						NewPromoActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
				finish();
			}
		});

	}

	public void shared_Prefernces_Sponsorned(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.SPONSORED_ADS, mString);

		editor.commit();

	}

	public void shared_Prefernces_Auto_Rotate(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.SETTING_PROMO_AUTO_ROTATE, mString);

		editor.commit();

	}

	public void shared_Prefernces_Personal(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.PERSONAL_ADS, mString);

		editor.commit();

	}

}
