package com.imgr.chat.setting;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smackx.packet.VCard;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;

public class BubbleColorActivity extends Activity {
	TextView txtBack, mTextView_blue, mTextView_green, mTextView_purple,
			textchanges, text_show_hide;
	ImageView mImageView_blue, mImageView_green, mImageView_purple;
	SharedPreferences sharedPreferences;
	Editor editor;
	FrameLayout mFrameLayout_back;
	String mString_bubble_select, mString_Override, BUUBLE_color_set;
	VCard card;
	Switch textView_overide_bubble;
	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;

	ConnectionConfiguration config;

	XMPPConnection connection;
	String mStringPassword, deviceid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bubble_color);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		deviceid = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceid:", "" + deviceid);
		textView_overide_bubble = (Switch) findViewById(R.id.textView_overide_bubble);
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		mTextView_blue = (TextView) findViewById(R.id.text_blue);
		mTextView_green = (TextView) findViewById(R.id.text_green);
		mTextView_purple = (TextView) findViewById(R.id.text_purple);
		textchanges = (TextView) findViewById(R.id.textchanges);

		// imageview define

		config = new ConnectionConfiguration("64.235.48.26", 5222);
		connection = new XMPPConnection(config);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setReconnectionAllowed(true);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		config.setSecurityMode(SecurityMode.enabled);
		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();

		// setConnection(connection);
		config.setReconnectionAllowed(true);
		card = new VCard();

		mImageView_blue = (ImageView) findViewById(R.id.imageView_blue);
		mImageView_green = (ImageView) findViewById(R.id.imageView_green);
		mImageView_purple = (ImageView) findViewById(R.id.imageView_purple);

		mFrameLayout_back = (FrameLayout) findViewById(R.id.frame_back);

		mString_bubble_select = sharedPreferences.getString(
				Constant.BUBBLE_SET, "");
		mString_Override = sharedPreferences.getString(
				Constant.BUBBLE_SET_OVERRIDE_FIXED, "");

		Log.e("mString_bubble_select: ", "" + mString_bubble_select);
		if (mString_bubble_select.contains("blue")) {
			mImageView_green.setVisibility(View.GONE);
			mImageView_purple.setVisibility(View.GONE);
			mImageView_blue.setVisibility(View.VISIBLE);
			mFrameLayout_back
					.setBackgroundResource(R.drawable.bubble_blue_settings);
			textchanges.setText("I am Blue bubble");
			// sharedPrefernces("blue");

		} else if (mString_bubble_select.contains("green")) {
			mImageView_blue.setVisibility(View.GONE);

			mImageView_purple.setVisibility(View.GONE);
			mImageView_green.setVisibility(View.VISIBLE);
			mFrameLayout_back
					.setBackgroundResource(R.drawable.bubble_green_settings);
			textchanges.setText("I am Green bubble");

		} else if (mString_bubble_select.contains("purple")) {
			mImageView_blue.setVisibility(View.GONE);
			mImageView_green.setVisibility(View.GONE);
			mImageView_purple.setVisibility(View.VISIBLE);
			mFrameLayout_back
					.setBackgroundResource(R.drawable.bubble_purple_setting);
			textchanges.setText("I am Purple bubble");

		} else {
			mImageView_green.setVisibility(View.GONE);
			mImageView_purple.setVisibility(View.GONE);
			mImageView_blue.setVisibility(View.VISIBLE);
			mFrameLayout_back
					.setBackgroundResource(R.drawable.bubble_blue_settings);
			sharedPrefernces("blue");
			textchanges.setText("I am Blue Bubble");
		}

		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		// click event textview choose color
		mTextView_blue.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("1 mString_Override: ", "" + mString_Override);
				if (mString_Override != null) {
					if (mString_Override.equals("true")) {
						BUUBLE_color_set = "1";

						// card.setField("Bubble_Color", "1");
						//
						// try {
						// card.save(connection);
						// } catch (XMPPException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

					} else {
						BUUBLE_color_set = "1";
						new BubbleColorSet().execute();
						// card.setField("Bubble_Color", "1");
						// try {
						// card.save(connection);
						// } catch (XMPPException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
					}
				} else {
					BUUBLE_color_set = "1";
					new BubbleColorSet().execute();
					// card.setField("Bubble_Color", "1");
					// try {
					// card.save(connection);
					// } catch (XMPPException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
				}

				// card.setField("Bubble_Color", "1");
				// try {
				// card.save(connection);
				// } catch (XMPPException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				mImageView_green.setVisibility(View.GONE);
				mImageView_purple.setVisibility(View.GONE);
				mImageView_blue.setVisibility(View.VISIBLE);
				mFrameLayout_back
						.setBackgroundResource(R.drawable.bubble_blue_settings);
				textchanges.setText("I am Blue Bubble");
				sharedPrefernces("blue");
			}
		});

		mTextView_green.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("2 mString_Override: ", "" + mString_Override);
				if (mString_Override != null) {
					if (mString_Override.equals("true")) {
						BUUBLE_color_set = "1";
						// card.setField("Bubble_Color", "1");
						// try {
						// card.save(connection);
						// } catch (XMPPException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

					} else {

						BUUBLE_color_set = "2";
						new BubbleColorSet().execute();
						// card.setField("Bubble_Color", "2");
						// try {
						// card.save(connection);
						// } catch (XMPPException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
					}
				} else {
					BUUBLE_color_set = "2";
					new BubbleColorSet().execute();
					// card.setField("Bubble_Color", "2");
					// try {
					// card.save(connection);
					// } catch (XMPPException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
				}

				// card.setField("Bubble_Color", "2");
				// try {
				// card.save(connection);
				// } catch (XMPPException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				mImageView_blue.setVisibility(View.GONE);

				mImageView_purple.setVisibility(View.GONE);
				mImageView_green.setVisibility(View.VISIBLE);
				mFrameLayout_back
						.setBackgroundResource(R.drawable.bubble_green_settings);
				textchanges.setText("I am Green Bubble");
				sharedPrefernces("green");

			}
		});
		mTextView_purple.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("3 mString_Override: ", "" + mString_Override);
				if (mString_Override != null) {
					if (mString_Override.equals("true")) {
						BUUBLE_color_set = "1";
						// card.setField("Bubble_Color", "1");
						// try {
						// card.save(connection);
						// } catch (XMPPException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

					} else {
						BUUBLE_color_set = "3";
						new BubbleColorSet().execute();
						// card.setField("Bubble_Color", "3");
						// try {
						// card.save(connection);
						// } catch (XMPPException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
					}
				} else {
					BUUBLE_color_set = "3";
					new BubbleColorSet().execute();
					// card.setField("Bubble_Color", "3");
					// try {
					// card.save(connection);
					// } catch (XMPPException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
				}

				// card.setField("Bubble_Color", "3");
				// try {
				// card.save(connection);
				// } catch (XMPPException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				mImageView_blue.setVisibility(View.GONE);
				mImageView_green.setVisibility(View.GONE);
				mImageView_purple.setVisibility(View.VISIBLE);
				mFrameLayout_back
						.setBackgroundResource(R.drawable.bubble_purple_setting);
				textchanges.setText("I am Purple Bubble");
				sharedPrefernces("purple");

			}
		});

		textView_overide_bubble
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub

						if (isChecked) {
							BUUBLE_color_set = "1";
							// new BubbleColorSet().execute();
							// card.setField("Bubble_Color", "1");
							// try {
							// card.save(connection);
							// } catch (XMPPException e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
							mString_Override = "true";
							// sharedPrefernces("blue");
							shared_Prefernces("blue", "true");
						} else {
							mString_bubble_select = sharedPreferences
									.getString(Constant.BUBBLE_SET, "");
							if (mString_bubble_select.contains("blue")) {
								BUUBLE_color_set = "1";
								new BubbleColorSet().execute();
								// card.setField("Bubble_Color", "1");
								// try {
								// card.save(connection);
								// } catch (XMPPException e) {
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }

							} else if (mString_bubble_select.contains("green")) {
								BUUBLE_color_set = "2";
								new BubbleColorSet().execute();
								// card.setField("Bubble_Color", "2");
								// try {
								// card.save(connection);
								// } catch (XMPPException e) {
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }

							} else if (mString_bubble_select.contains("purple")) {
								BUUBLE_color_set = "3";
								new BubbleColorSet().execute();
								// card.setField("Bubble_Color", "3");
								// try {
								// card.save(connection);
								// } catch (XMPPException e) {
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }

							} else {
								BUUBLE_color_set = "1";
								new BubbleColorSet().execute();
								// card.setField("Bubble_Color", "1");
								// try {
								// card.save(connection);
								// } catch (XMPPException e) {
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }
								sharedPrefernces("blue");

							}

							mString_Override = "false";
							shared_Prefernces("blue", "false");
						}
					}
				});
		Log.e("mString_Override: ", "" + mString_Override);
		if (mString_Override != null) {
			if (mString_Override.equals("true")) {
				textView_overide_bubble.setChecked(true);
				textView_overide_bubble.setSelected(true);
				shared_Prefernces("blue", "true");
				BUUBLE_color_set = "1";
				sharedPrefernces("blue");
				// card.setField("Bubble_Color", "1");
				// try {
				// card.save(connection);
				// } catch (XMPPException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
			} else {
				shared_Prefernces("blue", "false");
			}
		} else {
			textView_overide_bubble.setSelected(false);
			shared_Prefernces("blue", "false");
		}

	}

	public void sharedPrefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.BUBBLE_SET, mString);

		editor.commit();

	}

	public void shared_Prefernces(String mString, String mString2) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.BUBBLE_SET_OVERRIDE, mString);
		editor.putString(Constant.BUBBLE_SET_OVERRIDE_FIXED, mString2);

		editor.commit();

	}

	/**
	 * Execute BubbleColorSet Async task
	 */
	class BubbleColorSet extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.PostBubbleColor(BUUBLE_color_set,
					mStringPassword + "_" + deviceid);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(BubbleColorActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result----", "" + result);
			Bubble_Chat_Store(result);
			UI.hideProgressDialog();

		}

		public void Bubble_Chat_Store(String strJson) {
			try {
				JSONObject jsonOBject = new JSONObject(strJson);
				if (jsonOBject.getBoolean("success")) {
					Log.e("", "SUCCESS");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
