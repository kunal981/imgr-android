package com.imgr.chat.connector;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.imgr.chat.JsonParser.JSONARRAY;
import com.imgr.chat.JsonParser.JSONParserPost;
import com.imgr.chat.JsonParser.SocketConnection;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.model.AllContactModel;
import com.imgr.chat.model.SingleModelAdd;
import com.imgr.chat.model.ChatStore;
import com.imgr.chat.util.ImgrContact;

public class JsonParserConnector {
	static String mString_success;
	private static final String TAG = "JSONParser handler";
	static JSONObject jsonobject;

	public static String PostRegisterPhone(String apiKey, String appVersion,
			String countryCode, String deviceType, String phonenumber,
			String os, String deviceid) {
		try {
			String Url = Constant.Register_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			if (deviceid != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceid));
			}
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs
					.add(new BasicNameValuePair("appVersion", appVersion));
			nameValuePairs.add(new BasicNameValuePair("countryCode",
					countryCode));

			nameValuePairs
					.add(new BasicNameValuePair("deviceType", deviceType));
			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));
			nameValuePairs.add(new BasicNameValuePair("os", os));
			Log.e("nameValuePairs: ", "" + nameValuePairs);
			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			Log.e("jsonobject: ", "" + jsonobject);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}
		Log.e("mString_success: ", "" + mString_success);
		return mString_success;
	}

	public static String PostPersonalInfo(String apiKey, String appVersion,
			String countryCode, String deviceType, String phonenumber,
			String os, String deviceid, String Email, String mStringfirstname,
			String mStringlastname, String mStringBase, String mStringExt) {
		try {
			String Url = Constant.USER_INFO_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			if (deviceid != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceid));
			}
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs
					.add(new BasicNameValuePair("appVersion", appVersion));
			nameValuePairs.add(new BasicNameValuePair("countryCode",
					countryCode));

			nameValuePairs.add(new BasicNameValuePair("firstname",
					mStringfirstname));
			nameValuePairs.add(new BasicNameValuePair("nickname",
					mStringlastname));
			nameValuePairs.add(new BasicNameValuePair("email", Email));
			nameValuePairs
					.add(new BasicNameValuePair("image_path", mStringBase));
			nameValuePairs.add(new BasicNameValuePair("image_extension",
					mStringExt));

			nameValuePairs
					.add(new BasicNameValuePair("deviceType", deviceType));
			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));
			nameValuePairs.add(new BasicNameValuePair("os", os));
			Log.e("nameValuePairs: ", "" + nameValuePairs);
			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			Log.e("jsonobject: ", "" + jsonobject);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}
		Log.e("mString_success: ", "" + mString_success);
		return mString_success;
	}

	public static String PostVerfication(String activationcode, String apiKey,
			String deviceId, String phonenumber) {
		try {
			String Url = Constant.verifyRegistration_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("activationcode",
					activationcode));
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			if (deviceId != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceId));
			}

			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			// Log.e("jsonobject: ", "" + jsonobject);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

			// for (int i = 0; i < jsonArray.length(); i++) {
			// try {
			// JSONObject jsonObject = jsonArray.getJSONObject(i);
			// String name = jsonObject.getString("_name");
			// String id = jsonObject.getString("_gameid");
			// String time = jsonObject.getString("_time");
			// String image = jsonObject.getString("_image");
			// String ownerid = jsonObject.getString("_owner_id");
			// String player_count = jsonObject.getString("_players");
			// String mark_image = jsonObject
			// .getString("_marked_image");
			// String thumb = jsonObject
			// .getString("_original_image_thumb");
			// // Product p = new Product(name, time, image);
			// // product_arraylist.add(p);
			// mArrayList_player_count.add(player_count);
			// mArrayList_gameid.add(ownerid);
			// mArrayList_id.add(id);
			// mArrayList_name.add("Game" + (i + 1));
			// mArrayList_time.add(time);
			// mArrayList_images.add(image);
			// mArrayList_mark_image.add(mark_image);
			// mArrayList_thumb.add(thumb);
			//
			// } catch (JSONException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String PostImgrContact(String apiKey,
			ArrayList<String> contact_phonenumber) {
		JSONObject jsonobject = null;
		Log.e("FIRST : ", "" + contact_phonenumber.size());
		Log.e("DATA  : ", "" + contact_phonenumber);
		try {
			int totalElements = contact_phonenumber.size();
			Log.e("totalElements : ", "" + totalElements);
			String Url = Constant.IMGERContact;
			Log.e("Url: ", "" + Url);
			ArrayList<String> newphone = new ArrayList<String>();
			newphone.clear();

			for (int i = 0; i < totalElements; i++) {

				newphone.add(contact_phonenumber.get(i).replace("[(", "")
						.replace(")", "").replace(" ", "").replace(")]", "")
						.replace("(", "").replace("-", "").replace("]", "")
						.replace("[", ""));

			}
			Log.e("newphone newphone: ", "" + newphone.size());
			int total_Elements = newphone.size();
			Log.e("total_Elements : ", "" + total_Elements);
			contact_phonenumber.clear();
			for (int i = 0; i < total_Elements; i++) {
				if (newphone.get(i).length() >= 10) {

					contact_phonenumber.add(newphone.get(i).substring(
							newphone.get(i).length() - 10));
				} else {

					contact_phonenumber.add(newphone.get(i));
				}

			}
			Log.e("New phonenumber: ", "" + contact_phonenumber.size());

			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs
					.add(new BasicNameValuePair("phonenumbers",
							contact_phonenumber.toString().replace("[(", "")
									.replace(")", "").replace(" ", "")
									.replace(")]", "").replace("(", "")
									.replace("-", "").replace("]", "")
									.replace("[", "")));
			Log.e("New nameValuePairs: ", "" + nameValuePairs);

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}

		return jsonobject.toString();
	}

	public static String SINGLE_PostImgrContact(String apiKey,
			String phonenumber) {
		JSONObject jsonobject = null;

		try {
			Log.e("Older phonenumber: ", "" + phonenumber);
			String Url = Constant.IMGERContact;

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phonenumbers",
					phonenumber));
			Log.e("New nameValuePairs: ", "" + nameValuePairs);

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}
		// Log.e("see jsonobject: ", "" + jsonobject);
		return jsonobject.toString();
	}

	public static String OWNID(String response) {

		String MEMBERID = null;
		try {

			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {

				JSONArray jArray = jsonOBject.getJSONArray("data");
				Log.e("jArray.length(): ", "" + jArray.length());

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);

					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					if (mStringsImgrUser.equals("0")) {
						MEMBERID = "";
					} else {
						MEMBERID = (jsonOBjectContact.getString("memberId"));
					}

				}

			} else {

			}

		} catch (JSONException e) {

			Log.e(TAG, e.getMessage());
		}

		return MEMBERID;

	}

	public static String PostEditSponsored(String apiKey,
			ArrayList<String> phonenumber, String username, String deviceid,
			ArrayList<String> promo_isenabled) {
		JSONObject json_object = null;

		try {

			String Url = Constant.PromoStatus_EDIT;
			Log.e("Url: ", "" + Url);
			/*
			 * Log.e("isen: ", "" + promo_isenabled.toString().replace("[(", "")
			 * .replace(")", "").replace(" ", "") .replace(")]",
			 * "").replace("(", "") .replace("-", "").replace("]", "")
			 * .replace("[", "")); Log.e("id: ", "" +
			 * phonenumber.toString().replace("[(", "").replace(")", "")
			 * .replace(" ", "").replace(")]", "") .replace("(",
			 * "").replace("-", "").replace("]", "") .replace("[", ""));
			 */

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("username", username
					+ "_" + deviceid));
			nameValuePairs.add(new BasicNameValuePair("is_enable",
					promo_isenabled.toString().replace("[(", "")
							.replace(")", "").replace(" ", "")
							.replace(")]", "").replace("(", "")
							.replace("-", "")));

			nameValuePairs.add(new BasicNameValuePair("promo_id", phonenumber
					.toString().replace("[(", "").replace(")", "")
					.replace(" ", "").replace(")]", "").replace("(", "")
					.replace("-", "")));
			Log.e("New nameValuePairs: ", "" + nameValuePairs);

			json_object = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}
		// Log.e("see jsonobject: ", "" + jsonobject);
		return json_object.toString();
	}

	public static String AllContact(String apiKey,
			ArrayList<String> phonenumber, ArrayList<String> contactname) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.IMGERContact;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phonenumbers",
					phonenumber.toString().replace("[(", "").replace(")", "")
							.replace(" ", "").replace(")]", "")
							.replace("(", "").replace("-", "").replace("]", "")
							.replace("[", "")));
			// Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);
			// Log.e("jsonobject: ", "" + jsonobject);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static List<AllContactModel> ModelAllContactNormalUser(
			String strJson, ArrayList<String> phonenumber,
			ArrayList<String> contactname, ArrayList<String> contactlastname,
			ArrayList<String> mArrayListContactId) {

		List<AllContactModel> mContactModels = null;
		try {

			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mContactModels = new ArrayList<AllContactModel>();

				JSONArray jArray = jsonOBject.getJSONArray("data");
				// Log.e("jArray.length(): ", "" + jArray);

				Log.e("jArray.length(): ", "" + jArray.length());
				Log.e("phonenumber: ", "" + phonenumber.size());
				Log.e("contactname: ", "" + contactname.size());
				Log.e("mArrayListContactId: ", "" + mArrayListContactId.size());

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					final AllContactModel mContact = new AllContactModel();
					mContact.setPhonenumber(jsonOBjectContact
							.getString("phonenumber").replace("[(", "")
							.replace(")", "").replace(" ", "")
							.replace(")]", "").replace("(", "")
							.replace("-", "").replace("]", "").replace("[", ""));

					mContact.setPhoneid(mArrayListContactId.get(i));
					mContact.setMemberId(jsonOBjectContact
							.getString("memberId"));

					mContact.setFirstName(contactname.get(i));
					mContact.setLastName(contactlastname.get(i));
					mContact.setJid(jsonOBjectContact.getString("jid"));
					mContact.setIsImgrUser(jsonOBjectContact
							.getString("isImgrUser"));
					mContact.setBase64(jsonOBjectContact.getString("image"));

					mContactModels.add(mContact);

				}

			} else {
				mContactModels = new ArrayList<AllContactModel>();
			}

		} catch (JSONException e) {
			mContactModels = new ArrayList<AllContactModel>();
			Log.e(TAG, e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mContactModels.size());

		return mContactModels;

	}

	public static List<AllContactModel> ModelIMGRContact(String strJson,
			ArrayList<String> phonenumber, ArrayList<String> contactname,
			ArrayList<String> mArrayListContactid, ArrayList<String> contactlast) {

		List<AllContactModel> mContactModels = null;
		Log.e("Model: ", "" + phonenumber.size());
		// mContactModels.clear();
		try {

			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mContactModels = new ArrayList<AllContactModel>();

				JSONArray jArray = jsonOBject.getJSONArray("data");
				Log.e("Model: ", "" + jArray.length());
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					AllContactModel mContact = new AllContactModel();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setPhonenumber(phonenumber.get(i)
								.replace("[(", "").replace(")", "")
								.replace(" ", "").replace(")]", "")
								.replace("(", "").replace("-", "")
								.replace("]", "").replace("[", ""));
						// mContact.setPhonenumber(jsonOBjectContact
						// .getString("phonenumber").replace("[(", "")
						// .replace(")", "").replace(" ", "")
						// .replace(")]", "").replace("(", "")
						// .replace("-", "").replace("]", "")
						// .replace("[", ""));
						mContact.setFirstName(contactname.get(i));
						mContact.setLastName(contactlast.get(i));
						mContact.setPhoneid(mArrayListContactid.get(i));
						Log.e("memberId: ",
								"" + jsonOBjectContact.getString("memberId"));

						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mContact.setBase64(jsonOBjectContact.getString("image"));

						mContactModels.add(mContact);
					}

				}

			} else {
				mContactModels = new ArrayList<AllContactModel>();
			}

		} catch (JSONException e) {
			mContactModels = new ArrayList<AllContactModel>();
			Log.e(TAG, e.getMessage());
		}
		Log.e(TAG, "mImgrContacts.SIZE: " + mContactModels.size());

		return mContactModels;

	}

	public static List<AllContactModel> FirstModelIMGRContact(String strJson,
			ArrayList<String> phonenumber, ArrayList<String> contactname,
			ArrayList<String> contactlast, ArrayList<String> mArrayListContactid) {

		List<AllContactModel> mContactModels = null;
		try {

			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mContactModels = new ArrayList<AllContactModel>();

				JSONArray jArray = jsonOBject.getJSONArray("data");
				Log.e("jArray.length(): ", "" + jArray.length());

				Log.e("contactname: ", "" + contactname.size());
				Log.e("contactlast: ", "" + contactlast.size());
				Log.e("mArrayListContactid: ", "" + mArrayListContactid.size());
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					AllContactModel mContact = new AllContactModel();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");
					// Log.e("IMGR TEST mStringsImgrUser: ", "" +
					// mStringsImgrUser);

					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setFirstName(contactname.get(i));
						mContact.setLastName(contactlast.get(i));
						mContact.setPhonenumber(jsonOBjectContact
								.getString("phonenumber").replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", ""));
						mContact.setPhoneid(mArrayListContactid.get(i));
						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mContact.setBase64(jsonOBjectContact.getString("image"));

						mContactModels.add(mContact);
					}

				}

			} else {
				mContactModels = new ArrayList<AllContactModel>();
			}

		} catch (JSONException e) {
			mContactModels = new ArrayList<AllContactModel>();
			Log.e(TAG, e.getMessage());
		}
		Log.e(TAG, "mImgrContacts.SIZE: " + mContactModels.size());

		return mContactModels;

	}

	public static List<ImgrContact> PostImgrContact(String strJson) {

		List<ImgrContact> mImgrContacts = null;
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mImgrContacts = new ArrayList<ImgrContact>();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					ImgrContact mContact = new ImgrContact();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					String mStringsph = jsonOBjectContact
							.getString("phonenumber").replace("[(", "")
							.replace(")", "").replace(" ", "")
							.replace(")]", "").replace("(", "")
							.replace("-", "").replace("]", "").replace("[", "");
					// Log.e("mStringsph: ", "" + mStringsph);
					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setPhonenumber(jsonOBjectContact
								.getString("phonenumber"));
						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mImgrContacts.add(mContact);
					}

				}

			} else {
				mImgrContacts = new ArrayList<ImgrContact>();
			}

		} catch (JSONException e) {
			mImgrContacts = new ArrayList<ImgrContact>();
			Log.e(TAG, e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mImgrContacts.size());
		// Log.e(TAG, "mImgrContacts: " + mImgrContacts);

		return mImgrContacts;

	}

	public static String AddContactPhone(String apiKey, String phonenumber) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.IMGERContact;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phonenumbers",
					phonenumber.toString().replace("[(", "").replace(")", "")
							.replace(" ", "").replace(")]", "")
							.replace("(", "").replace("-", "").replace("]", "")
							.replace("[", "")));
			// Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static List<SingleModelAdd> SingleContactupdate(String strJson) {

		List<SingleModelAdd> mSingleModelAdds = null;
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mSingleModelAdds = new ArrayList<SingleModelAdd>();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					SingleModelAdd mContact = new SingleModelAdd();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setPhonenumber(jsonOBjectContact
								.getString("phonenumber").replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", ""));
						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mContact.setBase64(jsonOBjectContact.getString("image"));
						mSingleModelAdds.add(mContact);
					}

				}

			} else {
				mSingleModelAdds = new ArrayList<SingleModelAdd>();
			}

		} catch (JSONException e) {
			mSingleModelAdds = new ArrayList<SingleModelAdd>();
			Log.e(TAG, e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mImgrContacts.size());
		// Log.e(TAG, "mImgrContacts: " + mImgrContacts);

		return mSingleModelAdds;

	}

	public static String ChatHistory(String apiKey, String fromnumber,
			String tophone) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.CHAT_HISTORY;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("from", fromnumber));
			nameValuePairs.add(new BasicNameValuePair("to", tophone));
			// mStringPhone

			Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);
			Log.e("jsonobject: ", "" + jsonobject);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String PushNotification(String deviceId, String phonenumber) {
		try {
			String Url = Constant.verifyRegistration_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			if (deviceId != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceId));
			}

			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			// Log.e("jsonobject: ", "" + jsonobject);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

			// for (int i = 0; i < jsonArray.length(); i++) {
			// try {
			// JSONObject jsonObject = jsonArray.getJSONObject(i);
			// String name = jsonObject.getString("_name");
			// String id = jsonObject.getString("_gameid");
			// String time = jsonObject.getString("_time");
			// String image = jsonObject.getString("_image");
			// String ownerid = jsonObject.getString("_owner_id");
			// String player_count = jsonObject.getString("_players");
			// String mark_image = jsonObject
			// .getString("_marked_image");
			// String thumb = jsonObject
			// .getString("_original_image_thumb");
			// // Product p = new Product(name, time, image);
			// // product_arraylist.add(p);
			// mArrayList_player_count.add(player_count);
			// mArrayList_gameid.add(ownerid);
			// mArrayList_id.add(id);
			// mArrayList_name.add("Game" + (i + 1));
			// mArrayList_time.add(time);
			// mArrayList_images.add(image);
			// mArrayList_mark_image.add(mark_image);
			// mArrayList_thumb.add(thumb);
			//
			// } catch (JSONException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String getDevice(String apiKey, String username,
			String deviceToken, String deviceId) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.DeviceToken + "deviceToken=" + deviceToken + "&"
				+ "apiKey=imgr" + "&" + "username=" + username + "&"
				+ "deviceId=" + deviceId;
		Log.e("Url-->>", "" + Url);

		String result = jsonarray.getJSONFromUrl(Url);

		Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			mString_success = jsonobject.getString("success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (mString_success.equals("true")) {

		} else {
			try {
				mString_success = jsonobject.getString("error");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return mString_success;
	}

	public static String getSponsored(String ispersonal, String username,
			String deviceId) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.SponsoredAds + "apiKey=imgr&isPersonal="
				+ ispersonal + "&username=" + username + "_" + deviceId;
		Log.e("Url-->>", "" + Url);

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// try {
		// mString_success = jsonobject.getString("success");
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// if (mString_success.equals("true")) {
		//
		// } else {
		// try {
		// mString_success = jsonobject.getString("error");
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }

		return jsonobject.toString();
	}

	public static String getPERSONALSponsored(String id) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.PERSONAL_PROMO_CHAT_FETCH + id;
		Log.e("Url-->>", "" + Url);

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// try {
		// mString_success = jsonobject.getString("success");
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// if (mString_success.equals("true")) {
		//
		// } else {
		// try {
		// mString_success = jsonobject.getString("error");
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }

		return jsonobject.toString();
	}

	public static String get_SponsoredAds(String isenabled, String username,
			String deviceId, String promoid) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.GET_PromoStatus_EDIT + "apiKey=imgr&is_enable="
				+ isenabled + "&promo_id=" + promoid + "&username=" + username
				+ "_" + deviceId;
		Log.e("Url-->>", "" + Url);

		String result = jsonarray.getJSONFromUrl(Url);

		Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String post_ChatNotificaton(String to, String message,
			String from, String localdatabase, String username, String fromJID,
			String toJID) {
		try {
			String Url = Constant.CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("to", to));

			nameValuePairs.add(new BasicNameValuePair("message", message));
			nameValuePairs.add(new BasicNameValuePair("localdatabase",
					localdatabase));

			nameValuePairs.add(new BasicNameValuePair("username", username));

			nameValuePairs.add(new BasicNameValuePair("from", from));
			nameValuePairs.add(new BasicNameValuePair("fromJID", fromJID));
			nameValuePairs.add(new BasicNameValuePair("toJID", toJID));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			Log.e("jsonobject:- ", "" + jsonobject);
			mString_success = "";
			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String new_post_ChatNotificaton(String to, String from,
			String message, String fromjid, String tojid, String time,
			String timestamp, String date, String localdatabase,
			String messagepacketid, String memberid, String Contactuniqueid,
			String receiveornot, String bubblecolor, String fontsize,
			String username, String profileimage, String recentfromjid,
			String recenttojid) {
		try {

			Log.e("to_notification", "" + to);
			Log.e("from_notification", "" + from);
			Log.e("messgae_notification", "" + message);
			Log.e("fromjid_notification", "" + fromjid);
			Log.e("tojid_notification", "" + tojid);
			Log.e("time_notification", "" + time);
			Log.e("", "" + timestamp);
			Log.e("date_notification", "" + date);
			Log.e("localdatabase_notification", "" + localdatabase);
			Log.e("messgaepacketid_notification", "" + messagepacketid);
			Log.e("username_notification", "" + username);
			Log.e("recentfromjid_notification", "" + recentfromjid);
			Log.e("recenttojid_notification", "" + recenttojid);

			String Url = Constant.CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("to", to));
			nameValuePairs.add(new BasicNameValuePair("from", from));
			nameValuePairs.add(new BasicNameValuePair("message", message));
			nameValuePairs.add(new BasicNameValuePair("fromJID", fromjid));
			nameValuePairs.add(new BasicNameValuePair("toJID", tojid));

			nameValuePairs.add(new BasicNameValuePair("time", time));
			nameValuePairs.add(new BasicNameValuePair("timestamp", timestamp));
			nameValuePairs.add(new BasicNameValuePair("date", date));
			nameValuePairs.add(new BasicNameValuePair("localdatabase",
					localdatabase));
			nameValuePairs.add(new BasicNameValuePair("messsgepacketId",
					messagepacketid));
			nameValuePairs.add(new BasicNameValuePair("memberid", memberid));
			nameValuePairs.add(new BasicNameValuePair("contactuniqueid",
					Contactuniqueid));
			nameValuePairs.add(new BasicNameValuePair("receiveornot",
					receiveornot));
			nameValuePairs.add(new BasicNameValuePair("bubblecolor",
					bubblecolor));
			nameValuePairs.add(new BasicNameValuePair("fontsize", fontsize));
			nameValuePairs.add(new BasicNameValuePair("username", username));
			nameValuePairs.add(new BasicNameValuePair("profilepicture",
					profileimage));
			nameValuePairs.add(new BasicNameValuePair("recenttojid",
					recenttojid));
			nameValuePairs.add(new BasicNameValuePair("recentfromjid",
					recentfromjid));
			Log.e("nameValuePairs:- ", "" + nameValuePairs);
			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);

			mString_success = "";
			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String post_receive_ChatNotificaton(String to) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.AFTER_CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("to", to));

			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String PersonalPromoInfo(String promo_name,
			String promo_image, String promo_link, String promo_link_text,
			String promo_message_header, String finaltype, String username) {
		try {

			String Url = Constant.CREATE_PERSONAL_PROMO;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs
					.add(new BasicNameValuePair("promo_name", promo_name));

			nameValuePairs.add(new BasicNameValuePair("promo_image_url",
					promo_image));

			nameValuePairs
					.add(new BasicNameValuePair("promo_link", promo_link));
			nameValuePairs.add(new BasicNameValuePair("promo_link_text",
					promo_link_text));
			nameValuePairs.add(new BasicNameValuePair("promo_message_header",
					promo_message_header));
			nameValuePairs.add(new BasicNameValuePair("username", username));

			Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);
			Log.e("jsonobject: ", "" + jsonobject);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String UpdatePersonalPromoInfo(String promo_name,
			String promo_image, String promo_link, String promo_link_text,
			String promo_message_header, String finaltype, String username,
			String promoid) {
		try {

			String Url = Constant.UPDATE_PERSONAL_PROMO;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("promo_id", promoid));
			nameValuePairs
					.add(new BasicNameValuePair("promo_name", promo_name));

			nameValuePairs.add(new BasicNameValuePair("promo_image_url",
					promo_image));

			nameValuePairs
					.add(new BasicNameValuePair("promo_link", promo_link));
			nameValuePairs.add(new BasicNameValuePair("promo_link_text",
					promo_link_text));
			nameValuePairs.add(new BasicNameValuePair("promo_message_header",
					promo_message_header));
			nameValuePairs.add(new BasicNameValuePair("username", username));

			Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);
			Log.e("jsonobject: ", "" + jsonobject);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String DeletePersonalPromo(String promoid, String username,
			String deviceId) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.DELETE_PERSONAL_PROMO + "apiKey=imgr&promo_id="
				+ promoid + "&username=" + username + "_" + deviceId;
		Log.e("Url-->>", "" + Url);

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// try {
		// mString_success = jsonobject.getString("success");
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// if (mString_success.equals("true")) {
		//
		// } else {
		// try {
		// mString_success = jsonobject.getString("error");
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }

		return jsonobject.toString();
	}

	public static String getGalleryImage(String username) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.PROMO_IMAGES_GALLERY + username;
		Log.e("Url-->>", "" + Url);

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String PostMessageList(String apiKey, String phonenumber) {
		JSONObject jsonobject = null;

		try {

			String Url = Constant.RECENT_MESSAGE_LIST;
			Log.e("Url: ", "" + Url);

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phoneno", phonenumber
					.toString().replace("[(", "").replace(")", "")
					.replace(" ", "").replace(")]", "").replace("(", "")
					.replace("-", "").replace("]", "").replace("[", "")));
			Log.e("New nameValuePairs: ", "" + nameValuePairs);

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}
		// Log.e("see jsonobject: ", "" + jsonobject);
		return jsonobject.toString();
	}

	public static String PostBubbleColor(String colorcode, String jid) {
		JSONObject jsonobject = null;

		try {

			String Url = Constant.BUBBLE_COLOR;
			Log.e("Url: ", "" + Url);

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs
					.add(new BasicNameValuePair("bubble_color", colorcode));
			nameValuePairs.add(new BasicNameValuePair("jid", jid));
			Log.e("New nameValuePairs: ", "" + nameValuePairs);

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}

		return jsonobject.toString();
	}

}
