package com.imgr.chat.home;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.search.UserSearch;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.adapter.ImgrAdapter;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.InviteFreindActivity;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.AllContactModel;
import com.imgr.chat.model.ContactPhoneModel;
import com.imgr.chat.model.ExampleContactItem;
import com.imgr.chat.model.IMGR_Model;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;
import com.ngohung.widget.ContactItemInterface;

public class Contactimgr extends Fragment {
	// ListView mListView;
	FastListView mFastListView;

	ConnectionDetector mConnectionDetector;

	boolean inSearchMode = false;
	private EditText searchBox;

	List<ContactItemInterface> contactList;
	List<ContactItemInterface> filterList;
	// ListView mListView;
	View V;

	Button mButton_invite;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	SwipeRefreshLayout mSwipeLayout_Imgr;
	ArrayList<String> contactname = new ArrayList<String>();
	ArrayList<String> contactlastname = new ArrayList<String>();
	ArrayList<String> contactnumber = new ArrayList<String>();
	ArrayList<String> isIMGR_array = new ArrayList<String>();
	ArrayList<String> base64_array = new ArrayList<String>();

	ArrayList<String> country_code = new ArrayList<String>();

	ArrayList<String> Imgrcontactname = new ArrayList<String>();

	ArrayList<String> ImgrMemberId = new ArrayList<String>();
	ArrayList<String> NewImgrMemberId = new ArrayList<String>();

	ArrayList<String> Imgrcontactunquieid = new ArrayList<String>();
	ArrayList<String> Imgrcontactlastname = new ArrayList<String>();
	ArrayList<String> Imgrcontactnumber = new ArrayList<String>();

	ArrayList<String> ImgrcontactnameNEW = new ArrayList<String>();

	ArrayList<String> ImgrcontactunquieidNEW = new ArrayList<String>();
	ArrayList<String> ImgrcontactlastnameNEW = new ArrayList<String>();
	ArrayList<String> ImgrcontactnumberNEW = new ArrayList<String>();

	ArrayList<String> ImgrcontactunquieidNEW_DELETE = new ArrayList<String>();

	ArrayList<String> mArrayListBlockedContact;
	ImgrAdapter mImgrAdapter;
	List<AllContactModel> mImgrContacts = null;

	TextView mRadioButton_one, mRadioButton_two;
	boolean flag = true;

	ArrayList<ContactPhoneModel> arraylist = new ArrayList<ContactPhoneModel>();

	ArrayList<IMGR_Model> arraylistImgr = new ArrayList<IMGR_Model>();
	FragmentTransaction t;
	FragmentManager my_fragment_manager;
	String mStringPassword, deviceid, mStringusername;
	SharedPreferences sharedPreferences;
	Editor editor;

	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;

	ConnectionConfiguration config;
	XMPPConnection connection;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		V = inflater.inflate(R.layout.contact_imgr, container, false);
		sharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		deviceid = Secure.getString(getActivity().getContentResolver(),
				Secure.ANDROID_ID);
		mStringusername = sharedPreferences.getString(Constant.USERNAME_XMPP,
				"");
		Log.e("deviceid:", "" + deviceid);

		mConnectionDetector = new ConnectionDetector(getActivity());
		mButton_invite = (Button) V.findViewById(R.id.invite_people);
		mDatabaseHelper = new DatabaseHelper(getActivity());
		mDatasourceHandler = new DatasourceHandler(getActivity());
		contactList = new ArrayList<ContactItemInterface>();
		filterList = new ArrayList<ContactItemInterface>();
		base64_array = new ArrayList<String>();
		Imgrcontactunquieid = new ArrayList<String>();
		mArrayListBlockedContact = new ArrayList<String>();
		mRadioButton_one = (TextView) V.findViewById(R.id.button_one);
		mRadioButton_two = (TextView) V.findViewById(R.id.button_two);
		Imgrcontactname = new ArrayList<String>();
		Imgrcontactlastname = new ArrayList<String>();
		Imgrcontactnumber = new ArrayList<String>();
		ImgrMemberId = new ArrayList<String>();

		mRadioButton_two.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AllContact mAllContact = new AllContact();
				changeFragment(mAllContact);
			}
		});

		mSwipeLayout_Imgr = (SwipeRefreshLayout) V
				.findViewById(R.id.swipe_imgr);
		mFastListView = (FastListView) V.findViewById(R.id.list_IMGR);
		// mFastListView.setFastScrollEnabled(true);

		mSwipeLayout_Imgr.setColorSchemeResources(
				android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		mSwipeLayout_Imgr.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				if (mConnectionDetector.isConnectingToInternet()) {
					imgrfetchContacts();
				} else {
					mSwipeLayout_Imgr.setRefreshing(false);
					LogMessage.showDialog(getActivity(), null,
							"No Internet Connection", null, "Ok");
				}

			}
		});

		searchBox = (EditText) V.findViewById(R.id.input_search_query);
		searchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					mImgrAdapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		mButton_invite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(getActivity(),
						InviteFreindActivity.class);

				startActivity(mIntent);
				getActivity().overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		if (MenuActivity.mString_IMGR.equals("1")) {
			MenuActivity.mString_IMGR = "2";
			if (mConnectionDetector.isConnectingToInternet()) {
				mRadioButton_one.setTypeface(null, Typeface.BOLD);
				mRadioButton_two.setTypeface(null, Typeface.NORMAL);
				new MEMBERIDCONTACT().execute();
			} else {
				LogMessage.showDialog(getActivity(), null,
						"No Internet Connection", null, "Ok");
			}

		} else {
			mRadioButton_one.setTypeface(null, Typeface.BOLD);
			mRadioButton_two.setTypeface(null, Typeface.NORMAL);
			LocalimgrfetchContacts();
		}
		return V;
	}

	public void changeFragment(Fragment targetFragment) {

		my_fragment_manager = getActivity().getSupportFragmentManager();
		t = my_fragment_manager.beginTransaction();
		t.replace(R.id.menu_frame_page, targetFragment, "fragment");
		t.commit();

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e("", "ONRESUME");
		if (Constant.HANDLE_BACK_CONATCT_ADD == 2) {
			Constant.HANDLE_BACK_CONATCT_ADD = 0;
			Log.e("CASLLING", "cdscdsc  ONRESUME");
			LocalimgrfetchContacts();
		} else if (Constant.HANDLE_BACK_CONATCT_SCREEN_ONLY == 1) {
			Constant.HANDLE_BACK_CONATCT_SCREEN_ONLY = 0;
			LocalimgrfetchContacts();
		} else if (Constant.HANDLE_BACK_IMGRFRDS == 1) {
			Constant.HANDLE_BACK_IMGRFRDS = 0;
			LocalimgrfetchContacts();
		} else {
			Constant.HANDLE_BACK_IMGRFRDS = 0;
			Constant.HANDLE_BACK_CONATCT_ADD = 0;
		}
		if (Constant.CONNECTION_LOST == 1) {
			Constant.CONNECTION_LOST = 0;
			// connectivity_Manager =
			// (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
			activity = getActivity();
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			SASLAuthentication.registerSASLMechanism("PLAIN",
					SASLPlainMechanism.class);
			connection.DEBUG_ENABLED = true;
			config.setSASLAuthenticationEnabled(true);
			connection = new XMPPConnection(config);
			mStringusername = sharedPreferences.getString(
					Constant.USERNAME_XMPP, "");
			mStringPassword = sharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");
			Log.e("mStringusername: ", "" + mStringusername);
			Log.e("mStringPassword: ", "" + mStringPassword);
			if (mConnectionDetector.isConnectingToInternet()) {
				new login_Existing_User().execute();
			} else {
				LogMessage.showDialog(getActivity(), null,
						"No Internet Connection", null, "Ok");

			}
		}
	}

	public void imgrfetchContacts() {
		contactname.clear();
		contactnumber.clear();
		contactList.clear();
		contactlastname.clear();
		Imgrcontactlastname.clear();
		Imgrcontactname.clear();
		Imgrcontactnumber.clear();
		base64_array.clear();
		Imgrcontactunquieid.clear();
		mArrayListBlockedContact.clear();
		ImgrcontactnameNEW.clear();
		ImgrcontactnumberNEW.clear();
		ImgrcontactlastnameNEW.clear();
		ImgrcontactunquieidNEW.clear();
		// mImgrContacts.clear();

		String phoneNumber = null;
		String email = null;

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;

		StringBuffer output = new StringBuffer();

		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
				null);

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {

				String contact_id = cursor
						.getString(cursor.getColumnIndex(_ID));
				String name = cursor.getString(cursor
						.getColumnIndex(DISPLAY_NAME));

				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
						.getColumnIndex(HAS_PHONE_NUMBER)));

				if (hasPhoneNumber > 0) {

					output.append("\n First Name:" + name);

					// Query and loop for every phone number of the contact
					Cursor phoneCursor = contentResolver.query(
							PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor
								.getColumnIndex(NUMBER));
						output.append("\n Phone number:" + phoneNumber);

						if (name.equals("")) {

						} else {

							if (mStringPassword.equals(phoneNumber
									.replace("[(", "").replace(")", "")
									.replace(" ", "").replace(")]", "")
									.replace("(", "").replace("-", "")
									.replace("]", "").replace("[", "")
									.replace("*", "").replace(" ", "")
									.replace("+1", "").replace("+91", ""))) {

							} else {
								contactList.add(new ExampleContactItem(name,
										name));
								// Log.e("name: ", "" + name);
								try {
									String[] splited = name.split(" ");

									// Log.e("splited: ",
									// ""+splited.length);

									if (splited.length == 2) {
										contactname.add(splited[0]);
										contactlastname.add(splited[1]);
									}

									else if (splited.length == 1) {
										contactname.add(splited[0]);
										contactlastname.add("");
									} else {
										// Log.e("nothing: ", "Nothing");
										contactname.add(name);
										contactlastname.add("");
									}
								} catch (Exception e) {

								}
								// Log.e("contact_id: ", "" + contact_id);
								Imgrcontactunquieid.add(contact_id);
								contactnumber.add(phoneNumber.replace("[(", "")
										.replace(")", "").replace(" ", "")
										.replace(")]", "").replace("(", "")
										.replace("-", "").replace("]", "")
										.replace("[", "").replace("*", ""));
								ImgrcontactnumberNEW.add(phoneNumber
										.replace("[(", "").replace(")", "")
										.replace(" ", "").replace(")]", "")
										.replace("(", "").replace("-", "")
										.replace("]", "").replace("[", ""));
								// Imgrcontactunquieid.add(contact_id);
							}

						}

					}

					phoneCursor.close();

					// Query and loop for every email of the contact
					Cursor emailCursor = contentResolver.query(
							EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (emailCursor.moveToNext()) {

						email = emailCursor.getString(emailCursor
								.getColumnIndex(DATA));

						output.append("\nEmail:" + email);

					}

					emailCursor.close();
				}
				// cursor.close();
				output.append("\n");
			}

			if (contactnumber.size() == 0) {
				Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
						.show();
			} else {

				try {
					mCursor = mDatasourceHandler.AllContacts();
					Log.e("Local Fragment: ", mCursor.getCount() + "");
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (mCursor.getCount() != 0) {
					ImgrcontactunquieidNEW_DELETE.clear();
					do {

						ImgrcontactunquieidNEW_DELETE.add(mCursor.getString(8)
								.trim());

					} while (mCursor.moveToNext());

					mCursor.close();
				}
				ImgrcontactunquieidNEW_DELETE.removeAll(Imgrcontactunquieid);
				// Log.e("ImgrcontactunquieidNEW_DELETE: ", ""
				// + ImgrcontactunquieidNEW_DELETE);
				if (ImgrcontactunquieidNEW_DELETE.size() != 0) {
					for (int i = 0; i < ImgrcontactunquieidNEW_DELETE.size(); i++) {
						mDatasourceHandler
								.deleteRow(ImgrcontactunquieidNEW_DELETE.get(i));
					}
				} else {
					ImgrcontactunquieidNEW_DELETE.clear();
				}

				for (int i = 0; i < contactnumber.size(); i++) {

					mDatasourceHandler.AllContactsLocalUnquie(
							contactnumber.get(i).replace("[(", "")
									.replace(")", "").replace(" ", "")
									.replace(")]", "").replace("(", "")
									.replace("-", "").replace("]", "")
									.replace("[", ""), contactname.get(i),
							contactlastname.get(i), Imgrcontactunquieid.get(i));

				}
			}

			if (contactnumber.size() == 0) {
				Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
						.show();
			} else {

				for (int i = 0; i < contactnumber.size(); i++) {

					mDatasourceHandler.AllContactsUnquie(contactnumber.get(i)
							.replace("[(", "").replace(")", "")
							.replace(" ", "").replace(")]", "")
							.replace("(", "").replace("-", "").replace("]", "")
							.replace("[", ""), contactname.get(i),
							contactlastname.get(i), null, null, "0", "",
							"Null", Imgrcontactunquieid.get(i), "false");

				}

			}

			new IMGRCONTACT().execute();

		}
	}

	public void LocalimgrfetchContacts() {
		try {

			contactname.clear();
			contactnumber.clear();
			contactList.clear();
			contactlastname.clear();
			base64_array.clear();
			mArrayListBlockedContact.clear();
			Imgrcontactunquieid.clear();
			ImgrcontactnameNEW.clear();
			ImgrcontactnumberNEW.clear();
			ImgrcontactlastnameNEW.clear();
			ImgrcontactunquieidNEW.clear();

			try {
				mCursor = mDatasourceHandler.ImgrContacts();
				Log.e("Next Local Fragment: ", mCursor.getCount() + "");
			} catch (Exception e) {

				e.printStackTrace();
			}

			if (mCursor.getCount() != 0) {
				Imgrcontactname.clear();
				Imgrcontactnumber.clear();
				Imgrcontactlastname.clear();
				base64_array.clear();
				Imgrcontactunquieid.clear();
				mArrayListBlockedContact.clear();
				ImgrMemberId.clear();

				do {

					Imgrcontactname.add(mCursor.getString(1).trim());
					Imgrcontactnumber.add(mCursor.getString(2).trim());
					Imgrcontactlastname.add(mCursor.getString(3));
					ImgrMemberId.add(mCursor.getString(5));
					base64_array.add(mCursor.getString(7).trim());
					// Log.e("", "HERE");
					Imgrcontactunquieid.add(mCursor.getString(8).trim());
					mArrayListBlockedContact.add(mCursor.getString(9).trim());
					// Log.e("last", "HERE");

				} while (mCursor.moveToNext());

				mCursor.close();
			}

			if (Imgrcontactname.size() == 0) {
				Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
						.show();
			} else {
				arraylistImgr.clear();
				for (int i = 0; i < Imgrcontactnumber.size(); i++) {
					if (mArrayListBlockedContact.get(i).equals("true")) {

					} else {
						IMGR_Model wp = new IMGR_Model(Imgrcontactname.get(i),
								Imgrcontactlastname.get(i),
								Imgrcontactnumber.get(i), base64_array.get(i),
								Imgrcontactunquieid.get(i),
								Imgrcontactname.get(i) + " "
										+ Imgrcontactlastname.get(i),
								ImgrMemberId.get(i));
						// Binds all strings into an array
						arraylistImgr.add(wp);
					}

				}
				Log.e("Imgrcontactnumber.size():",
						"" + Imgrcontactnumber.size());
				Log.e("arraylistImgr.size():", "" + arraylistImgr.size());
				mImgrAdapter = new ImgrAdapter(getActivity(), Imgrcontactname,
						Imgrcontactnumber, arraylistImgr);

				mFastListView.setAdapter(mImgrAdapter);
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	// for OWN MEMBER ID FETCH:

	public class MEMBERIDCONTACT extends AsyncTask<String, Void, String> {
		public MEMBERIDCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {
			String mString = JsonParserConnector.SINGLE_PostImgrContact("imgr",
					mStringPassword);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			String ownId = JsonParserConnector.OWNID(result);
			OWN_Prefernces(ownId);
			UI.hideProgressDialog();
			FirstimgrfetchContacts();

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(getActivity());
		}

	}

	public void OWN_Prefernces(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.YOUR_ID_PROMO, mString);

		editor.commit();

	}

	public void FirstimgrfetchContacts() {
		contactname.clear();
		contactnumber.clear();
		contactList.clear();
		contactlastname.clear();
		base64_array.clear();
		mArrayListBlockedContact.clear();
		String phoneNumber = null;
		String email = null;
		ImgrMemberId.clear();
		// ContactsContract.Data.CONTACT_ID

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;

		StringBuffer output = new StringBuffer();

		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
				null);

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {

				String contact_id = cursor
						.getString(cursor.getColumnIndex(_ID));
				String name = cursor.getString(cursor
						.getColumnIndex(DISPLAY_NAME));

				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
						.getColumnIndex(HAS_PHONE_NUMBER)));

				if (hasPhoneNumber > 0) {

					output.append("\n First Name:" + name);

					// Query and loop for every phone number of the contact
					Cursor phoneCursor = contentResolver.query(
							PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor
								.getColumnIndex(NUMBER));
						output.append("\n Phone number:" + phoneNumber);

						if (name.equals("")) {

						} else {

							if (mStringPassword.equals(phoneNumber
									.replace("[(", "").replace(")", "")
									.replace(" ", "").replace(")]", "")
									.replace("(", "").replace("-", "")
									.replace("]", "").replace("[", "")
									.replace("*", "").replace(" ", "")
									.replace("+1", "").replace("+91", ""))) {

							} else {
								contactList.add(new ExampleContactItem(name,
										name));
								Log.e("name: ", "" + name);
								try {
									String[] splited = name.split(" ");

									// Log.e("splited: ", ""+splited.length);

									if (splited.length == 2) {
										contactname.add(splited[0]);
										contactlastname.add(splited[1]);
									}

									else if (splited.length == 1) {
										contactname.add(splited[0]);
										contactlastname.add("");
									} else {
										// Log.e("nothing: ", "Nothing");
										contactname.add(name);
										contactlastname.add("");
									}
								} catch (Exception e) {

								}
								Log.e("contact_id: ", "" + contact_id);
								Imgrcontactunquieid.add(contact_id);
								contactnumber.add(phoneNumber.replace("[(", "")
										.replace(")", "").replace(" ", "")
										.replace(")]", "").replace("(", "")
										.replace("-", "").replace("]", "")
										.replace("[", "").replace("*", ""));
							}

						}

					}

					phoneCursor.close();

					// Query and loop for every email of the contact
					Cursor emailCursor = contentResolver.query(
							EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (emailCursor.moveToNext()) {

						email = emailCursor.getString(emailCursor
								.getColumnIndex(DATA));

						output.append("\nEmail:" + email);

					}

					emailCursor.close();
				}
				// cursor.close();
				output.append("\n");
			}

			if (contactnumber.size() == 0) {
				Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
						.show();
			} else {

				for (int i = 0; i < contactnumber.size(); i++) {
					mDatasourceHandler.FIRST_AllContactsUnquie(
							contactnumber.get(i).replace("[(", "")
									.replace(")", "").replace(" ", "")
									.replace(")]", "").replace("(", "")
									.replace("-", "").replace("]", "")
									.replace("[", ""), contactname.get(i),
							contactlastname.get(i), null, null, "0", "",
							"Null", Imgrcontactunquieid.get(i), "false");

					// mDatasourceHandler.AllContactsUnquie(contactnumber.get(i)
					// .replace("[(", "").replace(")", "")
					// .replace(" ", "").replace(")]", "")
					// .replace("(", "").replace("-", "").replace("]", "")
					// .replace("[", ""), contactname.get(i),
					// contactlastname.get(i), null, null, "0", "",
					// "Null", Imgrcontactunquieid.get(i), "false");

				}

				try {
					mCursor = mDatasourceHandler.AllContacts();
					// Log.e("value in cursor at starting", mCursor.getCount() +
					// "");
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (mCursor.getCount() != 0) {
					contactname.clear();
					contactnumber.clear();
					contactlastname.clear();
					isIMGR_array.clear();
					Imgrcontactunquieid.clear();
					mArrayListBlockedContact.clear();
					do {
						isIMGR_array.add(mCursor.getString(0).trim());
						contactname.add(mCursor.getString(1).trim());
						contactnumber.add(mCursor.getString(2).trim());
						contactlastname.add(mCursor.getString(3).trim());
						Imgrcontactunquieid.add(mCursor.getString(8).trim());
						mArrayListBlockedContact.add(mCursor.getString(9)
								.trim());

					} while (mCursor.moveToNext());

					mCursor.close();
				}

				new FIRSTIMGRCONTACT().execute();
			}

		}
	}

	public class IMGRCONTACT extends AsyncTask<String, Void, String> {
		public IMGRCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("pull contactnumber: ", "" + contactnumber);
			String mString = JsonParserConnector.PostImgrContact("imgr",
					contactnumber);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("POST: ", "" + ImgrcontactnumberNEW);
			mImgrContacts = JsonParserConnector.ModelIMGRContact(result,
					ImgrcontactnumberNEW, contactname, Imgrcontactunquieid,
					contactlastname);

			Imgrcontact(mImgrContacts);

			mSwipeLayout_Imgr.setRefreshing(false);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

	}

	public class FIRSTIMGRCONTACT extends AsyncTask<String, Void, String> {
		public FIRSTIMGRCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {
			String mString = JsonParserConnector.PostImgrContact("imgr",
					contactnumber);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			mImgrContacts = JsonParserConnector.FirstModelIMGRContact(result,
					contactnumber, contactname, contactlastname,
					Imgrcontactunquieid);

			FIRSTImgrcontact(mImgrContacts);

			// Log.e("mImgrContacts: ", "" + mImgrContacts.size());
			UI.hideProgressDialog();
			// mSwipeLayout_Imgr.setRefreshing(false);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(getActivity());
		}

	}

	private void FIRSTImgrcontact(List<AllContactModel> mAllContactModels) {
		Log.e("Fragment first: ", "" + mAllContactModels.size());
		for (int i = 0; i < mAllContactModels.size(); i++) {
			final AllContactModel item = mAllContactModels.get(i);
			Log.e("memberid: ", ""
					+ item.getMemberId()
					+ "------ph====="
					+ item.getPhonenumber().replace("#", "").replace("*", "")
							.replace("-", "").replace("(", "").replace(")", "")
							.replace(" ", ""));
			mDatasourceHandler.AllContactsNoPullUnquie(item.getPhonenumber()
					.replace("#", "").replace("*", "").replace("-", "")
					.replace("(", "").replace(")", "").replace(" ", ""),
					item.getFirstName(), item.getLastName(), item.getJid(),
					item.getMemberId(), item.getIsImgrUser(), "",
					item.getBase64(), item.getPhoneid());

		}

		try {
			mCursor = mDatasourceHandler.ImgrContacts();
			Log.e("Fragment first: ", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			Imgrcontactname.clear();
			Imgrcontactnumber.clear();
			Imgrcontactlastname.clear();
			base64_array.clear();
			Imgrcontactunquieid.clear();
			mArrayListBlockedContact.clear();
			ImgrMemberId.clear();
			do {

				Imgrcontactname.add(mCursor.getString(1).trim());
				Imgrcontactnumber.add(mCursor.getString(2).trim());
				Imgrcontactlastname.add(mCursor.getString(3).trim());
				ImgrMemberId.add(mCursor.getString(5).trim());
				base64_array.add(mCursor.getString(7).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}

		if (Imgrcontactname.size() == 0) {
			Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			ImgrcontactnameNEW.clear();
			// ImgrcontactnumberNEW.clear();
			ImgrcontactlastnameNEW.clear();
			ImgrcontactunquieidNEW.clear();
			arraylistImgr.clear();
			for (int i = 0; i < Imgrcontactnumber.size(); i++) {
				if (mArrayListBlockedContact.get(i).equals("true")) {

				} else {
					IMGR_Model wp = new IMGR_Model(Imgrcontactname.get(i),
							Imgrcontactlastname.get(i),
							Imgrcontactnumber.get(i), base64_array.get(i),
							Imgrcontactunquieid.get(i), Imgrcontactname.get(i)
									+ " " + Imgrcontactlastname.get(i),
							ImgrMemberId.get(i));

					// Binds all strings into an array
					arraylistImgr.add(wp);
					mDatasourceHandler.SingleUser_set("true", "true", "true",
							"true", Imgrcontactunquieid.get(i));
					// ImgrcontactnumberNEW.add(Imgrcontactnumber.get(i));
					// ImgrcontactnameNEW

				}

			}

			mImgrAdapter = new ImgrAdapter(getActivity(), Imgrcontactname,
					Imgrcontactnumber, arraylistImgr);

			mFastListView.setAdapter(mImgrAdapter);
			mImgrAdapter.notifyDataSetChanged();
		}

	}

	private void Imgrcontact(List<AllContactModel> mAllContactModels) {

		ArrayList<String> NEWPhone = new ArrayList<String>();
		ArrayList<String> NEWName = new ArrayList<String>();
		ArrayList<String> NEWuniqueID = new ArrayList<String>();
		ArrayList<String> NEWlastname = new ArrayList<String>();
		NEWPhone.clear();
		NEWName.clear();
		NEWuniqueID.clear();
		NEWlastname.clear();
		for (int i = 0; i < mAllContactModels.size(); i++) {
			final AllContactModel item = mAllContactModels.get(i);

			Log.e("Fragment getPhoneid: ", item.getPhoneid()
					+ "---first---:"
					+ item.getFirstName()
					+ "---last----:"
					+ item.getMemberId()
					+ "----------"
					+ item.getPhonenumber().replace("#", "").replace("*", "")
							.replace("-", "").replace("(", "").replace(")", "")
							.replace(" ", ""));

			mDatasourceHandler.UpdateImgrContact(
					item.getPhonenumber().replace("#", "").replace("*", "")
							.replace("-", "").replace("(", "").replace(")", "")
							.replace(" ", ""), item.getJid(),
					item.getMemberId(), item.getIsImgrUser(), item.getBase64(),
					item.getPhoneid(), item.getFirstName(), item.getLastName());
			NEWPhone.add(item.getPhonenumber().replace("#", "")
					.replace("*", "").replace("-", "").replace("(", "")
					.replace(")", "").replace(" ", ""));
			NEWName.add(item.getFirstName());
			NEWlastname.add(item.getLastName());
			NEWuniqueID.add(item.getPhoneid());
		}

		ArrayList<String> unique = new ArrayList<String>();
		unique.clear();

		//
		unique.addAll(Imgrcontactunquieid);
		Log.e("Before new", "" + NEWuniqueID.size());
		Log.e("Before imgr", "" + Imgrcontactunquieid.size());

		unique.removeAll(NEWuniqueID);
		Log.e("Remove unqie:", "" + unique.size());

		for (int i = 0; i < unique.size(); i++) {
			mDatasourceHandler.UpdateNoImgrContact("0", unique.get(i));
		}

		try {
			mCursor = mDatasourceHandler.ImgrContacts();
			Log.e("Fragment Database: ", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			Imgrcontactname.clear();
			Imgrcontactnumber.clear();
			Imgrcontactlastname.clear();
			Imgrcontactunquieid.clear();
			mArrayListBlockedContact.clear();
			ImgrMemberId.clear();
			do {

				Imgrcontactname.add(mCursor.getString(1).trim());
				Imgrcontactnumber.add(mCursor.getString(2).trim());
				Imgrcontactlastname.add(mCursor.getString(3));
				ImgrMemberId.add(mCursor.getString(5));
				base64_array.add(mCursor.getString(7).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}

		if (Imgrcontactname.size() == 0) {
			Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			Log.e("ImgrMemberId: ", "" + ImgrMemberId);
			arraylistImgr.clear();
			ImgrcontactnameNEW.clear();
			// /ImgrcontactnumberNEW.clear();
			ImgrcontactlastnameNEW.clear();
			ImgrcontactunquieidNEW.clear();
			for (int i = 0; i < Imgrcontactnumber.size(); i++) {
				if (mArrayListBlockedContact.get(i).equals("true")) {

				} else {
					IMGR_Model wp = new IMGR_Model(Imgrcontactname.get(i),
							Imgrcontactlastname.get(i),
							Imgrcontactnumber.get(i), base64_array.get(i),
							Imgrcontactunquieid.get(i), Imgrcontactname.get(i)
									+ " " + Imgrcontactlastname.get(i),
							ImgrMemberId.get(i));
					ImgrcontactnameNEW.add(Imgrcontactname.get(i));
					ImgrcontactlastnameNEW.add(Imgrcontactnumber.get(i));

					arraylistImgr.add(wp);
					mDatasourceHandler.SingleUser_set("true", "true", "true",
							"true", Imgrcontactunquieid.get(i));
				}

			}
			Log.e("Imgrcontactunquieid: ", "" + Imgrcontactunquieid);
			Log.e("arraylistImgr: ", "" + arraylistImgr.size());
			mImgrAdapter = new ImgrAdapter(getActivity(), ImgrcontactnameNEW,
					ImgrcontactlastnameNEW, arraylistImgr);

			mFastListView.setAdapter(mImgrAdapter);
			// mImgrAdapter.notifyDataSetChanged();
		}

	}

	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {
			UI.showProgressDialog(getActivity());
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("doInBackground", "doInBackground");
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			connection = new XMPPConnection(config);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			configure(ProviderManager.getInstance());
			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);
			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				Log.e("connected", "server connect");

				// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
				Log.e("" + mStringusername, "" + mStringPassword);
				connection.login(mStringusername, mStringPassword);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {
					Log.e("XMPPChatDemoActivity",
							"--------------------------------------");
					Log.e("XMPPChatDemoActivity", "RosterEntry " + entry);
					Log.e("XMPPChatDemoActivity", "User: " + entry.getUser());
					Log.e("XMPPChatDemoActivity", "Name: " + entry.getName());
					Log.e("XMPPChatDemoActivity",
							"Status: " + entry.getStatus());
					Log.e("XMPPChatDemoActivity", "Type: " + entry.getType());
					Presence entryPresence = roster
							.getPresence(entry.getUser());

					Log.e("XMPPChatDemoActivity", "Presence Status: "
							+ entryPresence.getStatus());
					Log.e("XMPPChatDemoActivity", "Presence Type: "
							+ entryPresence.getMode());

					Presence.Type type = entryPresence.getType();
					if (type == Presence.Type.available)
						Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
					Log.e("XMPPChatDemoActivity", "Presence : " + entryPresence);

				}
			} catch (XMPPException e) {
				Log.e("Cannot connect to XMPP server with default admin username and password.",
						"0");
				Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			Log.e("result", "" + result);
			if (result != null) {
				Log.e("ConnectionId", "" + connection.getConnectionID());
				Log.e("Servicename", "" + connection.getServiceName());
				Log.e("User", "" + connection.getUser());

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getActivity(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

		/*** end of onPostExecute ***/

		public void configure(ProviderManager pm) {
			// Private Data Storage
			pm.addIQProvider("query", "jabber:iq:private",
					new PrivateDataManager.PrivateDataIQProvider());
			// Time
			try {
				pm.addIQProvider("query", "jabber:iq:time",
						Class.forName("org.jivesoftware.smackx.packet.Time"));
			} catch (ClassNotFoundException e) {
				Log.w("TestClient",
						"Can't load class for org.jivesoftware.smackx.packet.Time");
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Roster Exchange
			pm.addExtensionProvider("x", "jabber:x:roster",
					new RosterExchangeProvider());

			// Message Events
			pm.addExtensionProvider("x", "jabber:x:event",
					new MessageEventProvider());

			// Chat State
			pm.addExtensionProvider("active",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("composing",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("paused",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("inactive",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("gone",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			// XHTML
			pm.addExtensionProvider("html",
					"http://jabber.org/protocol/xhtml-im",
					new XHTMLExtensionProvider());

			// Group Chat Invitations
			pm.addExtensionProvider("x", "jabber:x:conference",
					new GroupChatInvitation.Provider());

			// Service Discovery # Items
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());

			// Service Discovery # Info
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Data Forms
			pm.addExtensionProvider("x", "jabber:x:data",
					new DataFormProvider());

			// MUC User
			pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
					new MUCUserProvider());

			// MUC Admin
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
					new MUCAdminProvider());

			// MUC Owner
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
					new MUCOwnerProvider());

			// SharedGroupsInfo
			pm.addIQProvider("sharedgroup",
					"http://www.jivesoftware.org/protocol/sharedgroup",
					new SharedGroupsInfo.Provider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Delayed Delivery
			pm.addExtensionProvider("x", "jabber:x:delay",
					new DelayInformationProvider());

			// Version
			try {
				pm.addIQProvider("query", "jabber:iq:version",
						Class.forName("org.jivesoftware.smackx.packet.Version"));
			} catch (ClassNotFoundException e) {
				// Not sure what's happening here.
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Offline Message Requests
			pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
					new OfflineMessageRequest.Provider());

			// Offline Message Indicator
			pm.addExtensionProvider("offline",
					"http://jabber.org/protocol/offline",
					new OfflineMessageInfo.Provider());

			// Last Activity
			pm.addIQProvider("query", "jabber:iq:last",
					new LastActivity.Provider());

			// User Search
			pm.addIQProvider("query", "jabber:iq:search",
					new UserSearch.Provider());

			// JEP-33: Extended Stanza Addressing
			pm.addExtensionProvider("addresses",
					"http://jabber.org/protocol/address",
					new MultipleAddressesProvider());

			// FileTransfer
			pm.addIQProvider("si", "http://jabber.org/protocol/si",
					new StreamInitiationProvider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());

			// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
			// IBBProviders.Open());

			// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
			// IBBProviders.Close());

			// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
			// new
			// IBBProviders.Data());

			// Privacy
			pm.addIQProvider("query", "jabber:iq:privacy",
					new PrivacyProvider());

			pm.addIQProvider("command", "http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider());
			pm.addExtensionProvider("malformed-action",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.MalformedActionError());
			pm.addExtensionProvider("bad-locale",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadLocaleError());
			pm.addExtensionProvider("bad-payload",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadPayloadError());
			pm.addExtensionProvider("bad-sessionid",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadSessionIDError());
			pm.addExtensionProvider("session-expired",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.SessionExpiredError());

			pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
					DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
			pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
					new DeliveryReceiptRequest().getNamespace(),
					new DeliveryReceiptRequest.Provider());
			/*
			 * pm.addExtensionProvider(ReadReceipt.ELEMENT,
			 * ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
			 */
		}

	}
}
