package com.imgr.chat.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.setting.BlockedContacts;
import com.imgr.chat.setting.BubbleColorActivity;
import com.imgr.chat.setting.FontActivity;
import com.imgr.chat.setting.SettingProfileActivity;
import com.imgr.chat.setting.SettingPromoActivity;
import com.imgr.chat.views.BetterPopupWindow;

/**
 * author: amit agnihotri
 */
public class SettingsFragment extends Fragment implements OnClickListener {
	View V;
	TextView mTextView, mTextView_senond, mTextView_openurl;
	ImageView mImageView_arrow_first, mImageView_second;
	LinearLayout mLinearLayout_first, mLinearLayout_second, mLinearLayout_five,
			mLinearLayout_third, mLinearLayout_fourth;
	Intent mIntent;
	Button mButtonShare;
	DemoPopupWindow dw;
	Switch mSwitchPromo, switch_notification;
	String mStringPromoOnOFF, mStringNotification;
	SharedPreferences sharedPreferences;
	Editor editor;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		V = inflater.inflate(R.layout.settings, container, false);
		sharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);
		mStringPromoOnOFF = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "");
		mStringNotification = sharedPreferences.getString(
				Constant.SETTING_NOTIFICATION_ON_OFF, "");
		mButtonShare = (Button) V.findViewById(R.id.share_setting);
		switch_notification = (Switch) V.findViewById(R.id.switch_notification);
		mSwitchPromo = (Switch) V.findViewById(R.id.switch_promo);
		mTextView = (TextView) V.findViewById(R.id.txtTitle1);
		mImageView_arrow_first = (ImageView) V.findViewById(R.id.imageView1);
		mLinearLayout_first = (LinearLayout) V.findViewById(R.id.first_layout);
		mTextView_senond = (TextView) V.findViewById(R.id.txtTitle2);
		mTextView_openurl = (TextView) V.findViewById(R.id.openurl);
		mImageView_second = (ImageView) V.findViewById(R.id.imageView2);
		mLinearLayout_second = (LinearLayout) V
				.findViewById(R.id.second_layout);
		mLinearLayout_third = (LinearLayout) V.findViewById(R.id.third_layout);
		mLinearLayout_fourth = (LinearLayout) V.findViewById(R.id.four_layout);
		mLinearLayout_five = (LinearLayout) V.findViewById(R.id.five_layout);

		mTextView.setOnClickListener(SettingsFragment.this);
		mImageView_arrow_first.setOnClickListener(SettingsFragment.this);
		mLinearLayout_first.setOnClickListener(SettingsFragment.this);
		mTextView_openurl.setOnClickListener(SettingsFragment.this);
		mTextView_senond.setOnClickListener(SettingsFragment.this);
		mImageView_second.setOnClickListener(SettingsFragment.this);
		mLinearLayout_second.setOnClickListener(SettingsFragment.this);
		mLinearLayout_five.setOnClickListener(SettingsFragment.this);
		mLinearLayout_third.setOnClickListener(SettingsFragment.this);
		mLinearLayout_fourth.setOnClickListener(SettingsFragment.this);
		mButtonShare.setOnClickListener(SettingsFragment.this);
		if (mStringNotification != null) {
			if (mStringNotification.equals("true")) {
				switch_notification.setChecked(true);
			}
		}

		switch_notification
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							shared_Prefernces("true");
						} else {
							shared_Prefernces("false");
						}

					}
				});

		mSwitchPromo.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					sharedPrefernces("true");
				} else {
					sharedPrefernces("false");
				}

			}
		});
		if (mStringPromoOnOFF.equals("true")) {
			mSwitchPromo.setChecked(true);

		} else {
			mSwitchPromo.setChecked(false);

		}
		return V;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.txtTitle1:
			mIntent = new Intent(getActivity(), SettingProfileActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.imageView1:
			mIntent = new Intent(getActivity(), SettingProfileActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.first_layout:
			mIntent = new Intent(getActivity(), SettingProfileActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.second_layout:
			mIntent = new Intent(getActivity(), SettingPromoActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.third_layout:
			mIntent = new Intent(getActivity(), BubbleColorActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.four_layout:
			mIntent = new Intent(getActivity(), BlockedContacts.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.five_layout:
			mIntent = new Intent(getActivity(), FontActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.imageView2:
			mIntent = new Intent(getActivity(), SettingPromoActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.txtTitle2:
			mIntent = new Intent(getActivity(), SettingPromoActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.openurl:

			break;

		case R.id.share_setting:
			dw = new DemoPopupWindow(v, getActivity());
			dw.showLikeQuickAction(0, 0);
			break;

		default:
			break;
		}

	}

	// ************** Class for pop-up window **********************
	/**
	 * The Class DemoPopupWindow.
	 */
	private class DemoPopupWindow extends BetterPopupWindow {

		/**
		 * Instantiates a new demo popup window.
		 * 
		 * @param anchor
		 *            the anchor
		 * @param cnt
		 *            the cnt
		 */
		public DemoPopupWindow(View anchor, Context cnt) {
			super(anchor);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cellalert24.Views.BetterPopupWindow#onCreate()
		 */
		@Override
		protected void onCreate() {
			// inflate layout
			LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			ViewGroup root = (ViewGroup) inflater.inflate(
					R.layout.share_choose_popup, null);

			TextView txtmail = (TextView) root.findViewById(R.id.txtMail);
			TextView txtmessage = (TextView) root.findViewById(R.id.txtMessage);
			Button mButton = (Button) root.findViewById(R.id.cancelBtn);

			txtmail.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();
					sendEmail();

				}
			});
			txtmessage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();
					Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
					smsVIntent.setType("vnd.android-dir/mms-sms");
					// smsVIntent.putExtra("address", mString_phone_no);
					smsVIntent
							.putExtra(
									"sms_body",
									"Check out imgr instant messaging app.Download it today from http://imgr.im/app");

					startActivity(smsVIntent);

				}
			});
			mButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();

				}
			});

			this.setContentView(root);
		}

	}

	protected void sendEmail() {
		// Log.e("Send email", "" + mStringEmail);
		String[] TO = {};
		String[] CC = { "" };
		Intent emailIntent = new Intent(Intent.ACTION_SEND);

		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.setType("text/plain");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		emailIntent.putExtra(Intent.EXTRA_CC, CC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "IMGR Android Application");
		emailIntent
				.putExtra(
						Intent.EXTRA_TEXT,
						"Check out imgr instant messaging app.Download it today from http://imgr.im/app");

		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			// finish();
			Log.i("Finished sending email...", "");
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(getActivity(),
					"There is no email client installed.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	public void sharedPrefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.SETTING_MAIN_PROMO_ON_OFF, mString);

		editor.commit();

	}

	public void shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.SETTING_NOTIFICATION_ON_OFF, mString);

		editor.commit();

	}

}
