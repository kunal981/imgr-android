package com.imgr.chat.home;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.imgr.chat.R;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.message.ChatScreen;
import com.imgr.chat.message.NumberChatScreen;

import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;

/**
 * author: amit agnihotri
 */
public class MessageFragment extends Fragment {
	View V;
	SwipeMenuListView mListView;
	ArrayList<String[]> messages = new ArrayList<String[]>();
	Message message;
	JSONArray mArrayPersonal = null;
	JSONObject personaljsonObj;
	String promoID;
	String personal_api_time, personal_api_pushkey, personal_api_idtag,
			personal_jidto, personal_jidfrom;

	private Handler mHandler = new Handler();

	SharedPreferences sharedPreferences, mSharedPreferences;
	Editor editor;
	SwipeRefreshLayout mSwipeLayout_message;
	String mStringusername;
	ArrayList<String> mArrayListFirstName;
	ArrayList<String> mArrayListFirstName_new;
	ArrayList<String> mArrayListLastName;
	ArrayList<String> mArrayListProficePic;
	ArrayList<String> mArrayListMessage;
	ArrayList<String> mArrayListTime;
	ArrayList<String> mArrayListDate;
	ArrayList<String> NEWmArrayListTime;
	ArrayList<String> mArrayListJid;
	ArrayList<String> mArrayListPhoneno;
	ArrayList<String> mArrayListuniqueId;
	ConnectionDetector mConnectionDetector;

	ArrayList<String> mArrayListTestTo;
	ArrayList<String> mArrayListTestFrom;
	ArrayList<String> Imgrcontactnumber;
	ArrayList<String> Imgrcontactname;
	ArrayList<String> Imgrcontactunquieid;
	ArrayList<String> NewImgrcontactnumber;
	ArrayList<String> mArrayListTestMessage;
	ArrayList<String> NewmArrayListTestMessage;
	ArrayList<String> mArrayListTestDate;
	ArrayList<String> mArrayListTestTime;
	ArrayList<String> mArrayListTestToJID;
	ArrayList<String> mArrayListTestFromJID;
	ArrayList<String> NEWmArrayListTestToJID;
	ArrayList<String> AllNEWmArrayListTestToJID;

	ArrayList<String> FINALNewImgrcontactnumber;
	ArrayList<String> FINALNewImgrcontactname;
	ArrayList<String> FINALNewImgrcontactuniqueid;
	ArrayList<String> MemberId;
	ArrayList<String> NewMemberId;
	ArrayList<String> ChatCount;

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor, mCursor2;
	String mStringPassword, deviceId;
	FrameLayout mFrameLayout_no_message;
	AlertDialog pinDialog;
	int pos;
	MessageScreenAdapter message_Adapter;
	ArrayList<String> mArrayListTOTEST;
	boolean list_set = false;

	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;

	ConnectionConfiguration config;
	XMPPConnection connection;
	String mString_notification;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		V = inflater.inflate(R.layout.message, container, false);
		mListView = (SwipeMenuListView) V.findViewById(R.id.listView1);
		mFrameLayout_no_message = (FrameLayout) V
				.findViewById(R.id.frame_no_message);
		sharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);
		mSharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		mConnectionDetector = new ConnectionDetector(getActivity());

		mSwipeLayout_message = (SwipeRefreshLayout) V
				.findViewById(R.id.swipe_imgr);

		mSwipeLayout_message.setColorSchemeResources(
				android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		mSwipeLayout_message.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				if (mConnectionDetector.isConnectingToInternet()) {
					refresh_screen();
					// new Handler().postDelayed(new Runnable() {
					// @Override
					// public void run() {
					//
					// }
					// }, 4000);

				} else {
					mSwipeLayout_message.setRefreshing(false);
					LogMessage.showDialog(getActivity(), null,
							"No Internet Connection", null, "Ok");
				}

			}
		});

		return V;

	}

	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {
			UI.showProgressDialog(getActivity());
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("doInBackground", "doInBackground");
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			connection = new XMPPConnection(config);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			configure(ProviderManager.getInstance());
			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);
			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				Log.e("connected", "server connect");

				// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
				Log.e("" + mStringusername, "" + mStringPassword);
				connection.login(mStringusername, mStringPassword);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {

					Presence entryPresence = roster
							.getPresence(entry.getUser());

					Presence.Type type = entryPresence.getType();
					if (type == Presence.Type.available)
						Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
					Log.e("XMPPChatDemoActivity", "Presence : " + entryPresence);

				}
			} catch (XMPPException e) {
				Log.e("Cannot connect to XMPP server with default admin username and password.",
						"0");
				Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			Log.e("result", "" + result);
			if (result != null) {
				Log.e("ConnectionId", "" + connection.getConnectionID());
				Log.e("Servicename", "" + connection.getServiceName());
				Log.e("User", "" + connection.getUser());

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getActivity(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

		/*** end of onPostExecute ***/

		public void configure(ProviderManager pm) {
			// Private Data Storage
			pm.addIQProvider("query", "jabber:iq:private",
					new PrivateDataManager.PrivateDataIQProvider());
			// Time
			try {
				pm.addIQProvider("query", "jabber:iq:time",
						Class.forName("org.jivesoftware.smackx.packet.Time"));
			} catch (ClassNotFoundException e) {
				Log.w("TestClient",
						"Can't load class for org.jivesoftware.smackx.packet.Time");
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Roster Exchange
			pm.addExtensionProvider("x", "jabber:x:roster",
					new RosterExchangeProvider());

			// Message Events
			pm.addExtensionProvider("x", "jabber:x:event",
					new MessageEventProvider());

			// Chat State
			pm.addExtensionProvider("active",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("composing",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("paused",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("inactive",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("gone",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			// XHTML
			pm.addExtensionProvider("html",
					"http://jabber.org/protocol/xhtml-im",
					new XHTMLExtensionProvider());

			// Group Chat Invitations
			pm.addExtensionProvider("x", "jabber:x:conference",
					new GroupChatInvitation.Provider());

			// Service Discovery # Items
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());

			// Service Discovery # Info
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Data Forms
			pm.addExtensionProvider("x", "jabber:x:data",
					new DataFormProvider());

			// MUC User
			pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
					new MUCUserProvider());

			// MUC Admin
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
					new MUCAdminProvider());

			// MUC Owner
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
					new MUCOwnerProvider());

			// SharedGroupsInfo
			pm.addIQProvider("sharedgroup",
					"http://www.jivesoftware.org/protocol/sharedgroup",
					new SharedGroupsInfo.Provider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Delayed Delivery
			pm.addExtensionProvider("x", "jabber:x:delay",
					new DelayInformationProvider());

			// Version
			try {
				pm.addIQProvider("query", "jabber:iq:version",
						Class.forName("org.jivesoftware.smackx.packet.Version"));
			} catch (ClassNotFoundException e) {
				// Not sure what's happening here.
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Offline Message Requests
			pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
					new OfflineMessageRequest.Provider());

			// Offline Message Indicator
			pm.addExtensionProvider("offline",
					"http://jabber.org/protocol/offline",
					new OfflineMessageInfo.Provider());

			// Last Activity
			pm.addIQProvider("query", "jabber:iq:last",
					new LastActivity.Provider());

			// User Search
			pm.addIQProvider("query", "jabber:iq:search",
					new UserSearch.Provider());

			// JEP-33: Extended Stanza Addressing
			pm.addExtensionProvider("addresses",
					"http://jabber.org/protocol/address",
					new MultipleAddressesProvider());

			// FileTransfer
			pm.addIQProvider("si", "http://jabber.org/protocol/si",
					new StreamInitiationProvider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());

			// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
			// IBBProviders.Open());

			// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
			// IBBProviders.Close());

			// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
			// new
			// IBBProviders.Data());

			// Privacy
			pm.addIQProvider("query", "jabber:iq:privacy",
					new PrivacyProvider());

			pm.addIQProvider("command", "http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider());
			pm.addExtensionProvider("malformed-action",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.MalformedActionError());
			pm.addExtensionProvider("bad-locale",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadLocaleError());
			pm.addExtensionProvider("bad-payload",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadPayloadError());
			pm.addExtensionProvider("bad-sessionid",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadSessionIDError());
			pm.addExtensionProvider("session-expired",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.SessionExpiredError());

			pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
					DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
			pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
					new DeliveryReceiptRequest().getNamespace(),
					new DeliveryReceiptRequest.Provider());
			/*
			 * pm.addExtensionProvider(ReadReceipt.ELEMENT,
			 * ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
			 */
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// useHandler();
		if (Constant.CONNECTION_LOST == 1) {
			Constant.CONNECTION_LOST = 0;
			// connectivity_Manager =
			// (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
			activity = getActivity();
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			SASLAuthentication.registerSASLMechanism("PLAIN",
					SASLPlainMechanism.class);
			connection.DEBUG_ENABLED = true;
			config.setSASLAuthenticationEnabled(true);
			connection = new XMPPConnection(config);
			mStringusername = sharedPreferences.getString(
					Constant.USERNAME_XMPP, "");
			mStringPassword = sharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");

			if (mConnectionDetector.isConnectingToInternet()) {
				new login_Existing_User().execute();
			} else {
				LogMessage.showDialog(getActivity(), null,
						"No Internet Connection", null, "Ok");

			}
		}
		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection();

		setConnection(connection);

		// deliver work

		PingManager.getInstanceFor(connection);

		DeliveryReceiptManager delievery_Manager = DeliveryReceiptManager
				.getInstanceFor(connection);
		delievery_Manager.setAutoReceiptsEnabled(true);
		delievery_Manager.enableAutoReceipts();

		delievery_Manager
				.addReceiptReceivedListener(new ReceiptReceivedListener() {
					@Override
					public void onReceiptReceived(String fromJid, String toJid,
							String receiptId) {
						// Log.e("Chat_Screen message sent confirmation:",
						// fromJid
						// + ", " + toJid + ", " + receiptId);
						loop: for (String[] msg_Data : messages) {
							if (msg_Data[3].equals(receiptId)) {
								msg_Data[2] = "1";
								boolean flag = mDatasourceHandler
										.UpdateReceive(receiptId);
								// update query implement--------------
								Log.e("Pinging", "on Resumeflag:  " + flag);

								break loop;
							}

						}

					}

				});

		ChatCount = new ArrayList<String>();
		mArrayListFirstName = new ArrayList<String>();
		mArrayListFirstName_new = new ArrayList<String>();
		mArrayListMessage = new ArrayList<String>();
		mArrayListTime = new ArrayList<String>();
		mArrayListDate = new ArrayList<String>();
		mArrayListJid = new ArrayList<String>();
		NewmArrayListTestMessage = new ArrayList<String>();
		mDatasourceHandler = new DatasourceHandler(getActivity());
		mDatabaseHelper = new DatabaseHelper(getActivity());
		mArrayListuniqueId = new ArrayList<String>();
		AllNEWmArrayListTestToJID = new ArrayList<String>();
		mArrayListMessage = new ArrayList<String>();
		mArrayListTime = new ArrayList<String>();
		NEWmArrayListTime = new ArrayList<String>();
		mArrayListJid = new ArrayList<String>();
		mArrayListTOTEST = new ArrayList<String>();
		mArrayListuniqueId = new ArrayList<String>();

		Imgrcontactnumber = new ArrayList<String>();
		NewImgrcontactnumber = new ArrayList<String>();
		MemberId = new ArrayList<String>();
		NewMemberId = new ArrayList<String>();

		Imgrcontactunquieid = new ArrayList<String>();
		deviceId = Secure.getString(getActivity().getContentResolver(),
				Secure.ANDROID_ID);

		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");

		// listview variable declear
		// step 1. create a MenuCreator
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {

				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));

				deleteItem.setIcon(R.drawable.ic_delete_message);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		// set creator
		mListView.setMenuCreator(creator);

		// step 2. listener item click event
		mListView
				.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(int position,
							SwipeMenu menu, int index) {
						// ApplicationInfo item = mAppList.get(position);
						switch (index) {

						case 0:
							pos = position;

							delete_popup();
							break;
						}
						return false;
					}
				});

		// set SwipeListener
		mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

			@Override
			public void onSwipeStart(int position) {
				// swipe start
			}

			@Override
			public void onSwipeEnd(int position) {
				// swipe end
			}
		});
		mListView
				.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						Toast.makeText(getActivity(), position + " long click",
								Toast.LENGTH_SHORT).show();
						return false;
					}
				});

		try {
			mCursor = mDatasourceHandler.FETCH_RECENT();
			if (mCursor.getCount() != 0) {
				mArrayListFirstName.clear();
				mArrayListMessage.clear();
				mArrayListTime.clear();
				mArrayListDate.clear();
				mArrayListJid.clear();
				NewmArrayListTestMessage.clear();
				mArrayListFirstName_new.clear();

				do {

					mArrayListDate.add(mCursor.getString(0).trim());
					mArrayListFirstName.add(mCursor.getString(1).trim());
					mArrayListJid.add(mCursor.getString(2).trim());
					mArrayListTOTEST.add(mCursor.getString(3).trim());
					mArrayListMessage.add(mCursor.getString(5).trim());
					mArrayListTime.add(mCursor.getString(6).trim());
					ChatCount.add(mCursor.getString(8).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				// Message decode section open=======

				for (int j = 0; j < mArrayListMessage.size(); j++) {
					String mStringBody = mArrayListMessage.get(j);
					// Log.e("mStringBody: ", "" + mStringBody);
					JSONObject json_OBject = null;
					try {
						json_OBject = new JSONObject(mStringBody);
					} catch (JSONException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					// Log.e("json_OBject: ", "" + json_OBject);

					JSONArray mArray = null;
					try {
						mArray = json_OBject.getJSONArray("body");
					} catch (JSONException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

					for (int z = 0; z < mArray.length(); z++) {
						JSONObject mJsonObject = null;
						try {
							mJsonObject = mArray.getJSONObject(z);
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						try {
							if (mJsonObject.getString("pushkey")
									.equals("image")) {
								NewmArrayListTestMessage.add("Image");
							} else {

								try {
									String promo_ID = mJsonObject
											.getString("_promo_id_tag");
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								String message_ = mJsonObject
										.getString("pushkey");
								NewmArrayListTestMessage.add(mJsonObject
										.getString("pushkey"));

							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
				mArrayListPhoneno = new ArrayList<String>();
				mArrayListPhoneno.clear();
				AllNEWmArrayListTestToJID.clear();
				for (int i = 0; i < mArrayListJid.size(); i++) {
					// if(mArrayListFirstName.get(i).equals("")){
					// mArrayListFirstName.get(i).replace("",mArrayListJid.get(i).split("_")[0]);
					// }
					mArrayListPhoneno.add(mArrayListJid.get(i).split("_")[0]);
					AllNEWmArrayListTestToJID.add(mArrayListJid.get(i).split(
							"_")[0]);
				}

				// mDatasourceHandler = new DatasourceHandler(getActivity());
				// mDatabaseHelper = new DatabaseHelper(getActivity());

				try {
					mCursor = mDatasourceHandler.ImgrContacts();

				} catch (Exception e) {

					e.printStackTrace();
				}

				if (mCursor.getCount() != 0) {

					NewImgrcontactnumber.clear();

					// mArrayListFirstName.clear();
					mArrayListuniqueId.clear();
					MemberId.clear();
					mArrayListFirstName_new.clear();

					Imgrcontactnumber.clear();
					mArrayListFirstName.clear();
					Imgrcontactunquieid.clear();

					do {

						// mArrayListFirstName.add(mCursor.getString(1).trim());
						Imgrcontactnumber.add(mCursor.getString(2).trim());

						MemberId.add(mCursor.getString(5));

						Imgrcontactunquieid.add(mCursor.getString(8).trim());

					} while (mCursor.moveToNext());

					mCursor.close();

					for (int i = 0; i < Imgrcontactnumber.size(); i++) {
						NewImgrcontactnumber
								.add(Imgrcontactnumber.get(i).substring(
										Imgrcontactnumber.get(i).length() - 10));
					}
				}

				NewMemberId.clear();
				mArrayListuniqueId.clear();
				mArrayListFirstName_new.clear();
				for (int i = 0; i < AllNEWmArrayListTestToJID.size(); i++) {
					Cursor mCursor = mDatasourceHandler
							.FETCH_(AllNEWmArrayListTestToJID.get(i));
					if (mCursor.getCount() != 0) {

						if (mCursor.moveToFirst()) {
							mArrayListFirstName_new.add(mCursor.getString(1)
									.trim());

							NewMemberId.add(mCursor.getString(5));

							mArrayListuniqueId.add(mCursor.getString(8).trim());

						}

						mCursor.close();

					} else {
						NewMemberId.add("0");
						mArrayListuniqueId.add("0");
						mArrayListFirstName_new.add("");

						mCursor.close();
					}

				}

			}
		} catch (Exception e) {
			Log.e("MESSAGE CRASH:", "" + e.getMessage());
		}
		if (mArrayListJid.size() == 0) {

			list_set = false;
			mListView.setVisibility(View.GONE);
			// mSwipeLayout_message.setVisibility(View.GONE);
			mFrameLayout_no_message.setVisibility(View.VISIBLE);

		} else {
			list_set = true;
			mFrameLayout_no_message.setVisibility(View.GONE);
			// mSwipeLayout_message.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.VISIBLE);

			// Message decode section closed
			message_Adapter = new MessageScreenAdapter(getActivity(),
					mArrayListFirstName_new, mArrayListFirstName_new,
					mArrayListFirstName_new, mArrayListPhoneno, mArrayListJid,
					NewmArrayListTestMessage, mArrayListTime,
					mArrayListuniqueId, ChatCount, mArrayListDate);
			mListView.setAdapter(message_Adapter);
		}

		mString_notification = mSharedPreferences.getString(
				"Notification_Click_Message", "");
		if (mString_notification != null) {
			if (mString_notification.equals("true")) {
				String phone_id = null;
				String Chat_name_select = null;
				String Chat_no_select = mSharedPreferences.getString(
						"Notification_no", "");

				Cursor mCursor = mDatasourceHandler.FETCH_(Chat_no_select
						.split("_")[0]);
				if (mCursor.getCount() != 0) {

					if (mCursor.moveToFirst()) {
						Chat_name_select = mCursor.getString(1).trim();
						NewMemberId.add(mCursor.getString(5));

						phone_id = mCursor.getString(8).trim();

					}

					mCursor.close();

				} else {
					// NewMemberId.add("0");
					Chat_name_select = mSharedPreferences.getString(
							"Notification_Username", "");
					phone_id = "0";
					// mArrayListuniqueId.add("0");
					// mArrayListFirstName_new.add("");

					mCursor.close();
				}

				if (phone_id.equals("0")) {
					Intent mIntent_info = new Intent(getActivity(),
							NumberChatScreen.class);

					mIntent_info.putExtra("Chat_name_select",
							Chat_no_select.split("_")[0]);

					mIntent_info.putExtra("Chat_no_select",
							Chat_no_select.split("_")[0]);
					mIntent_info.putExtra("phone_id", phone_id);

					getActivity().startActivity(mIntent_info);
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);

				} else {
					Intent mIntent_info = new Intent(getActivity(),
							ChatScreen.class);
					if (Chat_name_select.equals("No Name")) {
						mIntent_info.putExtra("Chat_name_select", phone_id);
					} else {
						mIntent_info.putExtra("Chat_name_select",
								Chat_name_select);
					}

					mIntent_info.putExtra("Chat_no_select",
							Chat_no_select.split("_")[0]);
					mIntent_info.putExtra("phone_id", phone_id);

					getActivity().startActivity(mIntent_info);
					getActivity().overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
				}

			}
		}

	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	// custom class working===========
	public class MessageScreenAdapter extends BaseAdapter {
		Context ctx;
		LayoutInflater inflater;
		ArrayList<String> mArrayList_first = new ArrayList<String>();
		ArrayList<String> mArrayList_last = new ArrayList<String>();
		ArrayList<String> mArrayList_profice = new ArrayList<String>();
		ArrayList<String> mArrayList_phoneno = new ArrayList<String>();
		ArrayList<String> mArrayList_jid = new ArrayList<String>();
		ArrayList<String> mArrayList_message = new ArrayList<String>();
		ArrayList<String> mArrayList_time = new ArrayList<String>();
		ArrayList<String> mArrayList_uniqID = new ArrayList<String>();
		ArrayList<String> mArrayList_count = new ArrayList<String>();
		ArrayList<String> mArrayList_date = new ArrayList<String>();

		public MessageScreenAdapter(Context mContext,
				ArrayList<String> mArrayfirstname,
				ArrayList<String> mArraylastname,
				ArrayList<String> profilecimage, ArrayList<String> phoneno,
				ArrayList<String> Jid, ArrayList<String> message,
				ArrayList<String> time, ArrayList<String> uniqueid,
				ArrayList<String> count, ArrayList<String> date) {
			ctx = mContext;
			// mImageLoader = new ImageLoader(getActivity());

			this.mArrayList_first = mArrayfirstname;
			this.mArrayList_last = mArraylastname;
			this.mArrayList_profice = profilecimage;
			this.mArrayList_phoneno = phoneno;
			this.mArrayList_jid = Jid;
			this.mArrayList_message = message;
			this.mArrayList_time = time;
			this.mArrayList_uniqID = uniqueid;
			this.mArrayList_count = count;
			this.mArrayList_date = date;

		}

		public int getCount() {

			return mArrayList_phoneno.size();
		}

		public Object getItem(int position) {

			return position;
		}

		public long getItemId(int position) {

			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {

			ViewHolder holder;
			View vi = convertView;
			if (inflater == null)
				inflater = (LayoutInflater) ctx
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				vi = inflater.inflate(R.layout.customlist, null);
				holder = new ViewHolder();
				holder.mFrameLayout = (FrameLayout) vi
						.findViewById(R.id.frameclick);
				holder.unread_layout = (FrameLayout) vi
						.findViewById(R.id.unread_layout);
				holder.unread_message = (TextView) vi
						.findViewById(R.id.unread_message);
				holder.text = (TextView) vi.findViewById(R.id.custo);
				holder.t1 = (TextView) vi.findViewById(R.id.desc);
				holder.imag = (ImageView) vi.findViewById(R.id.video);
				holder.time = (TextView) vi.findViewById(R.id.time);
				vi.setTag(holder);
			} else {
				holder = (ViewHolder) vi.getTag();
			}

			holder.text.setId(position);
			holder.t1.setId(position);
			holder.imag.setId(position);
			holder.unread_message.setId(position);

			if (mArrayList_count.get(position).equals("0")) {

				holder.unread_layout.setVisibility(View.GONE);
			} else {

				holder.unread_layout.setVisibility(View.VISIBLE);
				holder.unread_message.setText(mArrayList_count.get(position));

			}

			if (mArrayList_first.size() == 0) {
				holder.text.setText(mArrayList_phoneno.get(position));
			} else {

				if (mArrayList_first.get(position).equals("")) {
					holder.text.setText(mArrayList_phoneno.get(position));
				} else if (mArrayList_first.get(position).matches("No Name")) {
					holder.text.setText(mArrayList_phoneno.get(position));
				} else if (mArrayList_first.get(position).matches("No")) {
					holder.text.setText(mArrayList_phoneno.get(position));
				}

				else {
					holder.text.setText(mArrayList_first.get(position));
				}
			}

			try {
				holder.t1.setText(mArrayList_message.get(position));
				String date_matched = date();
				if (date_matched.matches(mArrayList_date.get(position))) {
					holder.time.setText(mArrayList_time.get(position));
				} else {
					holder.time.setText(mArrayList_date.get(position)
							.split("/")[0]);
				}

				holder.imag.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Friend_Prefernces(NewMemberId.get(position));
						if (mArrayList_first.get(position).matches("")) {

							Intent mIntent_info = new Intent(getActivity(),
									NumberChatScreen.class);

							mIntent_info.putExtra("Chat_name_select",
									mArrayList_phoneno.get(position));

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);

						} else if (mArrayList_first.get(position).matches(
								"No Name")) {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info
									.putExtra("Chat_name_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else if (mArrayList_first.get(position).matches("No")) {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info
									.putExtra("Chat_name_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info.putExtra("Chat_name_select",
									mArrayList_first.get(position));

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
							// mIntent_info.putExtra("Chat_name_select",
							// mArrayList_phoneno.get(position));
						}

					}
				});

				holder.t1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Friend_Prefernces(NewMemberId.get(position));
						if (mArrayList_first.get(position).matches("")) {

							Intent mIntent_info = new Intent(getActivity(),
									NumberChatScreen.class);

							mIntent_info.putExtra("Chat_name_select",
									mArrayList_phoneno.get(position));

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);

						} else if (mArrayList_first.get(position).matches(
								"No Name")) {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info
									.putExtra("Chat_name_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else if (mArrayList_first.get(position).matches("No")) {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info
									.putExtra("Chat_name_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info.putExtra("Chat_name_select",
									mArrayList_first.get(position));

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
							// mIntent_info.putExtra("Chat_name_select",
							// mArrayList_phoneno.get(position));
						}

					}
				});

				holder.time.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Friend_Prefernces(NewMemberId.get(position));

						if (mArrayList_first.get(position).matches("")) {

							Intent mIntent_info = new Intent(getActivity(),
									NumberChatScreen.class);

							mIntent_info.putExtra("Chat_name_select",
									mArrayList_phoneno.get(position));

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
							// mIntent_info.putExtra("Chat_name_select",
							// mArrayList_phoneno.get(position));
						} else if (mArrayList_first.get(position).matches(
								"No Name")) {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info
									.putExtra("Chat_name_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else if (mArrayList_first.get(position).matches("No")) {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info
									.putExtra("Chat_name_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else {
							Intent mIntent_info = new Intent(getActivity(),
									ChatScreen.class);

							mIntent_info.putExtra("Chat_name_select",
									mArrayList_first.get(position));

							mIntent_info
									.putExtra("Chat_no_select",
											mArrayList_phoneno.get(position)
													.split("_")[0]);
							mIntent_info.putExtra("phone_id",
									mArrayList_uniqID.get(position));

							getActivity().startActivity(mIntent_info);
							getActivity().overridePendingTransition(
									R.anim.slide_in_left,
									R.anim.slide_out_right);
							// mIntent_info.putExtra("Chat_name_select",
							// mArrayList_phoneno.get(position));
						}

					}
				});

			} catch (Exception e) {

			}
			return vi;

		}
	}

	public class ViewHolder {
		public TextView text, t1, time, unread_message;
		public ImageView imag;
		public FrameLayout mFrameLayout, unread_layout;

	}

	protected void refresh_screen() {
		ChatCount = new ArrayList<String>();
		mArrayListFirstName = new ArrayList<String>();
		mArrayListFirstName_new = new ArrayList<String>();
		mArrayListMessage = new ArrayList<String>();
		mArrayListTime = new ArrayList<String>();
		mArrayListDate = new ArrayList<String>();
		mArrayListJid = new ArrayList<String>();
		NewmArrayListTestMessage = new ArrayList<String>();
		mDatasourceHandler = new DatasourceHandler(getActivity());
		mDatabaseHelper = new DatabaseHelper(getActivity());
		mArrayListuniqueId = new ArrayList<String>();
		AllNEWmArrayListTestToJID = new ArrayList<String>();
		mArrayListMessage = new ArrayList<String>();
		mArrayListTime = new ArrayList<String>();
		NEWmArrayListTime = new ArrayList<String>();
		mArrayListJid = new ArrayList<String>();
		mArrayListTOTEST = new ArrayList<String>();
		mArrayListuniqueId = new ArrayList<String>();

		Imgrcontactnumber = new ArrayList<String>();
		NewImgrcontactnumber = new ArrayList<String>();
		MemberId = new ArrayList<String>();
		NewMemberId = new ArrayList<String>();

		Imgrcontactunquieid = new ArrayList<String>();
		deviceId = Secure.getString(getActivity().getContentResolver(),
				Secure.ANDROID_ID);

		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");

		try {
			mCursor = mDatasourceHandler.FETCH_RECENT();
			if (mCursor.getCount() != 0) {
				mArrayListFirstName.clear();
				mArrayListMessage.clear();
				mArrayListTime.clear();
				mArrayListJid.clear();
				NewmArrayListTestMessage.clear();
				mArrayListFirstName_new.clear();
				mArrayListDate.clear();

				do {
					mArrayListDate.add(mCursor.getString(0).trim());
					mArrayListFirstName.add(mCursor.getString(1).trim());
					mArrayListJid.add(mCursor.getString(2).trim());
					mArrayListTOTEST.add(mCursor.getString(3).trim());
					mArrayListMessage.add(mCursor.getString(5).trim());
					mArrayListTime.add(mCursor.getString(6).trim());
					ChatCount.add(mCursor.getString(8).trim());

				} while (mCursor.moveToNext());

				mCursor.close();

				// Message decode section open=======

				for (int j = 0; j < mArrayListMessage.size(); j++) {
					String mStringBody = mArrayListMessage.get(j);
					// Log.e("mStringBody: ", "" + mStringBody);
					JSONObject json_OBject = null;
					try {
						json_OBject = new JSONObject(mStringBody);
					} catch (JSONException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					// Log.e("json_OBject: ", "" + json_OBject);

					JSONArray mArray = null;
					try {
						mArray = json_OBject.getJSONArray("body");
					} catch (JSONException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

					for (int z = 0; z < mArray.length(); z++) {
						JSONObject mJsonObject = null;
						try {
							mJsonObject = mArray.getJSONObject(z);
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						try {
							if (mJsonObject.getString("pushkey")
									.equals("image")) {
								NewmArrayListTestMessage.add("Image");
							} else {

								try {
									String promo_ID = mJsonObject
											.getString("_promo_id_tag");
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								String message_ = mJsonObject
										.getString("pushkey");
								NewmArrayListTestMessage.add(mJsonObject
										.getString("pushkey"));

							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
				mArrayListPhoneno = new ArrayList<String>();
				mArrayListPhoneno.clear();
				AllNEWmArrayListTestToJID.clear();
				for (int i = 0; i < mArrayListJid.size(); i++) {

					mArrayListPhoneno.add(mArrayListJid.get(i).split("_")[0]);
					AllNEWmArrayListTestToJID.add(mArrayListJid.get(i).split(
							"_")[0]);
				}

				try {
					mCursor = mDatasourceHandler.ImgrContacts();

				} catch (Exception e) {

					e.printStackTrace();
				}

				if (mCursor.getCount() != 0) {

					NewImgrcontactnumber.clear();

					// mArrayListFirstName.clear();
					mArrayListuniqueId.clear();
					MemberId.clear();
					mArrayListFirstName_new.clear();

					Imgrcontactnumber.clear();
					mArrayListFirstName.clear();
					Imgrcontactunquieid.clear();

					do {

						// mArrayListFirstName.add(mCursor.getString(1).trim());
						Imgrcontactnumber.add(mCursor.getString(2).trim());

						MemberId.add(mCursor.getString(5));

						Imgrcontactunquieid.add(mCursor.getString(8).trim());

					} while (mCursor.moveToNext());

					mCursor.close();

					for (int i = 0; i < Imgrcontactnumber.size(); i++) {
						NewImgrcontactnumber
								.add(Imgrcontactnumber.get(i).substring(
										Imgrcontactnumber.get(i).length() - 10));
					}
				}

				NewMemberId.clear();
				mArrayListuniqueId.clear();
				mArrayListFirstName_new.clear();
				for (int i = 0; i < AllNEWmArrayListTestToJID.size(); i++) {
					Cursor mCursor = mDatasourceHandler
							.FETCH_(AllNEWmArrayListTestToJID.get(i));
					if (mCursor.getCount() != 0) {

						if (mCursor.moveToFirst()) {
							mArrayListFirstName_new.add(mCursor.getString(1)
									.trim());
							NewMemberId.add(mCursor.getString(5));

							mArrayListuniqueId.add(mCursor.getString(8).trim());

						}

						mCursor.close();

					} else {
						NewMemberId.add("0");
						mArrayListuniqueId.add("0");
						mArrayListFirstName_new.add("");

						mCursor.close();
					}

				}

			}
		} catch (Exception e) {
			Log.e("MESSAGE CRASH:", "" + e.getMessage());
		}
		if (mArrayListJid.size() == 0) {

			list_set = false;
			mListView.setVisibility(View.GONE);
			// mSwipeLayout_message.setVisibility(View.GONE);
			mFrameLayout_no_message.setVisibility(View.VISIBLE);

		} else {
			list_set = true;
			mFrameLayout_no_message.setVisibility(View.GONE);
			// mSwipeLayout_message.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.VISIBLE);

			// Message decode section closed
			message_Adapter = new MessageScreenAdapter(getActivity(),
					mArrayListFirstName_new, mArrayListFirstName_new,
					mArrayListFirstName_new, mArrayListPhoneno, mArrayListJid,
					NewmArrayListTestMessage, mArrayListTime,
					mArrayListuniqueId, ChatCount, mArrayListDate);
			mListView.setAdapter(message_Adapter);
		}
		mSwipeLayout_message.setRefreshing(false);

	}

	private int dp2px(int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				getResources().getDisplayMetrics());
	}

	protected void delete_popup() {

		final View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.delete_pop_up_message, null);

		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final Button mButtonyes = (Button) v.findViewById(R.id.yes_dialog);

		pinDialog = new AlertDialog.Builder(getActivity()).setView(v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						boolean flag1 = mDatasourceHandler.deleteMesage(
								mStringPassword + "_" + deviceId,
								mArrayListJid.get(pos));
						boolean flag = mDatasourceHandler.deleteHistoryMesage(
								mStringPassword + "_" + deviceId,
								mArrayListJid.get(pos));

						mArrayListFirstName_new.remove(pos);
						mArrayListJid.remove(pos);
						NewmArrayListTestMessage.remove(pos);
						mArrayListTime.remove(pos);
						mArrayListuniqueId.remove(pos);

						message_Adapter.notifyDataSetChanged();
						if (mArrayListJid.size() == 0) {
							list_set = false;
							mListView.setVisibility(View.GONE);
							// mSwipeLayout_message.setVisibility(View.GONE);
							mFrameLayout_no_message.setVisibility(View.VISIBLE);
						} else {
							list_set = true;
						}

						pinDialog.cancel();

					}
				});
			}
		});

		pinDialog.show();

	}

	public void useHandler() {
		mHandler = new Handler();
		mHandler.postDelayed(mRunnable, 1000);
	}

	private Runnable mRunnable = new Runnable() {

		@Override
		public void run() {

			/** Do something **/

			mHandler.postDelayed(mRunnable, 1000);
		}
	};

	public void Friend_Prefernces(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.FRIEND_ID_PROMO, mString);

		editor.commit();

	}

	public void setConnection(final XMPPConnection connection) {

		this.connection = connection;
		try {

			if (connection != null) {

				// Add a packet listener to get messages sent to us
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {

					public void processPacket(Packet packet) {

						try {
							String mStringnewtojid = null;
							String mStringnewfromjid = null;
							JSONObject objMainList = null;
							String check = null;
							String check_update = null;
							String time = currenttime();
							// 4 sept
							message = (Message) packet;
							if (message.getBody() != null) {
								String fromName = StringUtils
										.parseBareAddress(message.getFrom()
												.split("\\@")[0]);
								mStringnewtojid = mStringPassword + "_"
										+ deviceId;
								mStringnewfromjid = fromName;
								Log.e("message.getBody(): ",
										"" + message.getBody());

								promoID = null;
								try {
									JSONObject jsonObj = new JSONObject(message
											.getBody());

									objMainList = new JSONObject();
									JSONArray mJsonArray = null;
									mJsonArray = new JSONArray();

									JSONObject json = new JSONObject();

									if (jsonObj.has("_message_body_tag")) {

										promoID = jsonObj
												.getString("_promo_id_tag");
										if (promoID != null) {

											if (promoID.equals("0")) {

												try {

													json.put(
															"pushkey",
															jsonObj.getString("pushkey"));
													json.put(
															"_message_body_tag",
															jsonObj.getString("_message_body_tag"));
													json.put(
															"_promo_id_tag",
															jsonObj.getString("_promo_id_tag"));
													json.put("preview_Icon",
															"null");

												} catch (Exception e) {

												}

												mJsonArray.put(json);

												try {
													objMainList.put("body",
															mJsonArray);
												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												//

											}

											else {
												check = countcheck(promoID);
												check_update = count_check_update(promoID);
												// api calling

												if (check != null) {
													if (check.equals("0")) {
														personal_api_time = time;
														personal_api_pushkey = jsonObj
																.getString("pushkey");
														personal_api_idtag = jsonObj
																.getString("_promo_id_tag");
														personal_jidto = mStringnewtojid;
														personal_jidfrom = mStringnewfromjid;
														new Execute_New_Promo()
																.execute(promoID);
														return;
													} else {
														if (check_update != null) {
															if (!check_update
																	.equals("0")) {
																personal_api_time = time;
																personal_api_pushkey = jsonObj
																		.getString("pushkey");
																personal_api_idtag = jsonObj
																		.getString("_promo_id_tag");
																personal_jidto = mStringnewtojid;
																personal_jidfrom = mStringnewfromjid;
																new Execute_New_Promo_Update()
																		.execute(promoID);
																return;
															}
														}
														try {

															json.put(
																	"pushkey",
																	jsonObj.getString("pushkey"));
															json.put(
																	"_message_body_tag",
																	jsonObj.getString("_message_body_tag"));
															json.put(
																	"_promo_id_tag",
																	jsonObj.getString("_promo_id_tag"));
															json.put(
																	"preview_Icon",
																	"null");

														} catch (Exception e) {

														}

														mJsonArray.put(json);

														try {
															objMainList.put(
																	"body",
																	mJsonArray);
														} catch (JSONException e) {
															// TODO
															// Auto-generated
															// catch
															// block
															e.printStackTrace();
														}
													}
												}

											}

										} else {
											try {

												json.put("pushkey", jsonObj
														.getString("pushkey"));
												json.put(
														"_message_body_tag",
														jsonObj.getString("_message_body_tag"));
												json.put(
														"_promo_id_tag",
														jsonObj.getString("_promo_id_tag"));
												json.put("preview_Icon", "null");

											} catch (Exception e) {

											}

											mJsonArray.put(json);

											try {
												objMainList.put("body",
														mJsonArray);
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											//

										}

									} else {
										// image show
										try {

											json.put("pushkey", "image");

											json.put("_promo_id_tag", "");
											json.put("preview_Icon", jsonObj
													.getString("preview_Icon"));
											promoID = "0";

										} catch (Exception e) {

										}

										mJsonArray.put(json);

										try {
											objMainList.put("body", mJsonArray);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								String date = date();

								String timestamp = timestamp();
								int history_count = mDatasourceHandler
										.HISTORYUpdateRecentReceiveCount(message
												.getPacketID());
								if (history_count == 0) {
									int countincrease = 0;
									countincrease = mDatasourceHandler
											.UpdateRecentReceiveCount(
													mStringnewtojid,
													mStringnewfromjid,
													objMainList.toString(),
													time, date, timestamp);
									String mString = "" + (countincrease + 1);

									boolean flag = mDatasourceHandler
											.UpdateRecentReceive(
													mStringnewtojid,
													mStringnewfromjid,
													objMainList.toString(),
													time, date, timestamp,
													mString);

									if (!flag) {
										mDatasourceHandler.insertrecentMessage(
												objMainList.toString(),
												mStringnewfromjid,
												mStringnewtojid, "", "", date,
												time, timestamp, "1");
									}
								}
								mDatasourceHandler.insertChathistory(
										message.getPacketID(), "username",
										mStringnewtojid, mStringnewfromjid,
										objMainList.toString(), time, promoID,
										"1", date);

								promoID = null;
								mStringnewtojid = null;
								mStringnewfromjid = null;

							}

							// CLOSED
							ChatCount = new ArrayList<String>();
							mArrayListFirstName = new ArrayList<String>();
							mArrayListFirstName_new = new ArrayList<String>();
							mArrayListMessage = new ArrayList<String>();
							mArrayListTime = new ArrayList<String>();
							mArrayListDate = new ArrayList<String>();
							mArrayListJid = new ArrayList<String>();
							NewmArrayListTestMessage = new ArrayList<String>();
							mDatasourceHandler = new DatasourceHandler(
									getActivity());
							mDatabaseHelper = new DatabaseHelper(getActivity());
							mArrayListuniqueId = new ArrayList<String>();
							AllNEWmArrayListTestToJID = new ArrayList<String>();
							mArrayListMessage = new ArrayList<String>();
							mArrayListTime = new ArrayList<String>();
							NEWmArrayListTime = new ArrayList<String>();
							mArrayListJid = new ArrayList<String>();
							mArrayListTOTEST = new ArrayList<String>();
							mArrayListuniqueId = new ArrayList<String>();

							Imgrcontactnumber = new ArrayList<String>();
							NewImgrcontactnumber = new ArrayList<String>();
							MemberId = new ArrayList<String>();
							NewMemberId = new ArrayList<String>();

							Imgrcontactunquieid = new ArrayList<String>();
							deviceId = Secure.getString(getActivity()
									.getContentResolver(), Secure.ANDROID_ID);

							mStringPassword = sharedPreferences.getString(
									Constant.PASSWORD_XMPP, "");

							try {
								mCursor = mDatasourceHandler.FETCH_RECENT();
								if (mCursor.getCount() != 0) {
									mArrayListFirstName.clear();
									mArrayListMessage.clear();
									mArrayListTime.clear();
									mArrayListJid.clear();
									NewmArrayListTestMessage.clear();
									mArrayListFirstName_new.clear();
									mArrayListDate.clear();

									do {
										mArrayListDate.add(mCursor.getString(0)
												.trim());
										mArrayListFirstName.add(mCursor
												.getString(1).trim());
										mArrayListJid.add(mCursor.getString(2)
												.trim());
										mArrayListTOTEST.add(mCursor.getString(
												3).trim());
										mArrayListMessage.add(mCursor
												.getString(5).trim());
										mArrayListTime.add(mCursor.getString(6)
												.trim());
										ChatCount.add(mCursor.getString(8)
												.trim());

									} while (mCursor.moveToNext());

									mCursor.close();

									// Message decode section open=======

									for (int j = 0; j < mArrayListMessage
											.size(); j++) {
										String mStringBody = mArrayListMessage
												.get(j);
										// Log.e("mStringBody: ", "" +
										// mStringBody);
										JSONObject json_OBject = null;
										try {
											json_OBject = new JSONObject(
													mStringBody);
										} catch (JSONException e2) {
											// TODO Auto-generated catch block
											e2.printStackTrace();
										}
										// Log.e("json_OBject: ", "" +
										// json_OBject);

										JSONArray mArray = null;
										try {
											mArray = json_OBject
													.getJSONArray("body");
										} catch (JSONException e2) {
											// TODO Auto-generated catch block
											e2.printStackTrace();
										}

										for (int z = 0; z < mArray.length(); z++) {
											JSONObject mJsonObject = null;
											try {
												mJsonObject = mArray
														.getJSONObject(z);
											} catch (JSONException e1) {
												// TODO Auto-generated catch
												// block
												e1.printStackTrace();
											}

											try {
												if (mJsonObject.getString(
														"pushkey").equals(
														"image")) {
													NewmArrayListTestMessage
															.add("Image");
												} else {

													try {
														String promo_ID = mJsonObject
																.getString("_promo_id_tag");
													} catch (JSONException e) {
														// TODO Auto-generated
														// catch block
														e.printStackTrace();
													}
													String message_ = mJsonObject
															.getString("pushkey");
													NewmArrayListTestMessage
															.add(mJsonObject
																	.getString("pushkey"));

												}
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}

									}
									mArrayListPhoneno = new ArrayList<String>();
									mArrayListPhoneno.clear();
									AllNEWmArrayListTestToJID.clear();
									for (int i = 0; i < mArrayListJid.size(); i++) {

										mArrayListPhoneno.add(mArrayListJid
												.get(i).split("_")[0]);
										AllNEWmArrayListTestToJID
												.add(mArrayListJid.get(i)
														.split("_")[0]);
									}

									try {
										mCursor = mDatasourceHandler
												.ImgrContacts();

									} catch (Exception e) {

										e.printStackTrace();
									}

									if (mCursor.getCount() != 0) {

										NewImgrcontactnumber.clear();

										// mArrayListFirstName.clear();
										mArrayListuniqueId.clear();
										MemberId.clear();
										mArrayListFirstName_new.clear();

										Imgrcontactnumber.clear();
										mArrayListFirstName.clear();
										Imgrcontactunquieid.clear();

										do {

											// mArrayListFirstName.add(mCursor.getString(1).trim());
											Imgrcontactnumber.add(mCursor
													.getString(2).trim());

											MemberId.add(mCursor.getString(5));

											Imgrcontactunquieid.add(mCursor
													.getString(8).trim());

										} while (mCursor.moveToNext());

										mCursor.close();

										for (int i = 0; i < Imgrcontactnumber
												.size(); i++) {
											NewImgrcontactnumber
													.add(Imgrcontactnumber
															.get(i)
															.substring(
																	Imgrcontactnumber
																			.get(i)
																			.length() - 10));
										}
									}

									NewMemberId.clear();
									mArrayListuniqueId.clear();
									mArrayListFirstName_new.clear();
									for (int i = 0; i < AllNEWmArrayListTestToJID
											.size(); i++) {
										Cursor mCursor = mDatasourceHandler
												.FETCH_(AllNEWmArrayListTestToJID
														.get(i));
										if (mCursor.getCount() != 0) {

											if (mCursor.moveToFirst()) {
												mArrayListFirstName_new
														.add(mCursor.getString(
																1).trim());
												NewMemberId.add(mCursor
														.getString(5));

												mArrayListuniqueId.add(mCursor
														.getString(8).trim());

											}

											mCursor.close();

										} else {
											NewMemberId.add("0");
											mArrayListuniqueId.add("0");
											mArrayListFirstName_new.add("");

											mCursor.close();
										}

									}

								}
							} catch (Exception e) {
								Log.e("MESSAGE CRASH:", "" + e.getMessage());
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						mHandler.post(new Runnable() {

							public void run() {
								if (list_set) {

									// message_Adapter.notifyDataSetChanged();
									message_Adapter = new MessageScreenAdapter(
											getActivity(),
											mArrayListFirstName_new,
											mArrayListFirstName_new,
											mArrayListFirstName_new,
											mArrayListPhoneno, mArrayListJid,
											NewmArrayListTestMessage,
											mArrayListTime, mArrayListuniqueId,
											ChatCount, mArrayListDate);
									mListView.setAdapter(message_Adapter);

								} else {

									if (mArrayListJid.size() == 0) {

										mListView.setVisibility(View.GONE);
										// mSwipeLayout_message.setVisibility(View.GONE);
										mFrameLayout_no_message
												.setVisibility(View.VISIBLE);

									} else {
										mFrameLayout_no_message
												.setVisibility(View.GONE);
										// mSwipeLayout_message.setVisibility(View.VISIBLE);
										mListView.setVisibility(View.VISIBLE);

									}

								}

							}
						});

					}

				}, filter);
			}

		} catch (Exception e) {

		}
	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			UI.hideProgressDialog();
			Personal_List(result);
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo_Update extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(getActivity());
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			UI.hideProgressDialog();
			Personal_List_update(result);
		}

	}

	private void Personal_List(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.insertSponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				JSONObject json = new JSONObject();
				mArrayPersonal = new JSONArray();
				JSONObject objMainList = new JSONObject();

				try {

					json.put("pushkey", personaljsonObj.getString("pushkey"));
					json.put("_message_body_tag",
							personaljsonObj.getString("_message_body_tag"));
					json.put("_promo_id_tag",
							personaljsonObj.getString("_promo_id_tag"));
					json.put("preview_Icon", "null");

				} catch (Exception e) {

				}

				mArrayPersonal.put(json);

				try {
					objMainList.put("body", mArrayPersonal);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//

				String date = date();

				String timestamp = timestamp();

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(message.getPacketID());
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, objMainList.toString(),
									personal_api_time, date, timestamp);
					String mString = "" + (countincrease + 1);

					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time, date,
							timestamp, mString);

					if (!flag1) {
						mDatasourceHandler.insertrecentMessage(
								objMainList.toString(), personal_jidfrom,
								personal_jidto, "", "", date,
								personal_api_time, timestamp, "1");
					}

				}

				mDatasourceHandler.insertChathistory(message.getPacketID(),
						"username", personal_jidto, personal_jidfrom,
						objMainList.toString(), personal_api_time,
						mStringspromoid, "1", date);

				// promoID = null;
				personal_jidto = null;
				personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	private void Personal_List_update(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.update_Sponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				JSONObject json = new JSONObject();
				mArrayPersonal = new JSONArray();
				JSONObject objMainList = new JSONObject();

				try {

					json.put("pushkey", personaljsonObj.getString("pushkey"));
					json.put("_message_body_tag",
							personaljsonObj.getString("_message_body_tag"));
					json.put("_promo_id_tag",
							personaljsonObj.getString("_promo_id_tag"));
					json.put("preview_Icon", "null");

				} catch (Exception e) {

				}

				mArrayPersonal.put(json);

				try {
					objMainList.put("body", mArrayPersonal);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//

				String date = date();

				String timestamp = timestamp();

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(message.getPacketID());
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, objMainList.toString(),
									personal_api_time, date, timestamp);
					String mString = "" + (countincrease + 1);

					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time, date,
							timestamp, mString);

					if (!flag1) {
						mDatasourceHandler.insertrecentMessage(
								objMainList.toString(), personal_jidfrom,
								personal_jidto, "", "", date,
								personal_api_time, timestamp, "1");
					}

				}

				mDatasourceHandler.insertChathistory(message.getPacketID(),
						"username", personal_jidto, personal_jidfrom,
						objMainList.toString(), personal_api_time,
						mStringspromoid, "1", date);

				// promoID = null;
				personal_jidto = null;
				personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	public String countcheck(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			// s.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			// /Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public String count_check_update(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA_Update(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

}
