package com.imgr.chat.home;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;

public class FastListView extends ListView {

	private Context ctx;

	private static int indWidth = 25;
	private String[] sections;
	String[] sectionsArr;
	private float scaledWidth;
	private float sx;
	private int indexSize;
	private String section;
	private boolean showLetter = true;
	private Handler listHandler;
	private static String sections_ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public FastListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		ctx = context;
	}

	public FastListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		ctx = context;
	}

	public FastListView(Context context, String keyList) {
		super(context);
		ctx = context;
	}

	public Object[] getSections() {

		sectionsArr = new String[sections_.length()];

		// Log.e("sectionsArr: ", "" + sectionsArr);
		for (int i = 0; i < sections_.length(); i++) {
			sectionsArr[i] = "" + sections_.charAt(i);
			// Log.e("sectionsArr: ", "" + sections_.charAt(i));
		}

		return sectionsArr;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		// Log.e("onDraw", "onDraw");
		getSections();
		sections = sectionsArr;
		// Log.e("sections: ", "" + sections);
		scaledWidth = indWidth * getSizeInPixel(ctx);
		sx = this.getWidth() - this.getPaddingRight() - scaledWidth;
		// Log.e("sx", "" + sx);
		Paint p = new Paint();
		p.setColor(Color.WHITE);
		p.setAlpha(100);

		canvas.drawRect(sx, this.getPaddingTop(), sx + scaledWidth,
				this.getHeight() - this.getPaddingBottom(), p);
		indexSize = (this.getHeight() - this.getPaddingTop() - getPaddingBottom())
				/ sections.length;
		//
		Paint textPaint = new Paint();
		textPaint.setColor(Color.DKGRAY);
		textPaint.setTextSize(scaledWidth / 2);

		for (int i = 0; i < sections.length; i++)
			canvas.drawText(sections[i], sx + textPaint.getTextSize() / 2,
					getPaddingTop() + indexSize * (i + 1), textPaint);
		// We draw the letter in the middle
		if (showLetter & section != null && !section.equals("")) {
			Paint textPaint2 = new Paint();
			textPaint2.setColor(Color.DKGRAY);
			textPaint2.setTextSize(2 * indWidth);
			canvas.drawText(section, getWidth() / 2, getHeight() / 2,
					textPaint2);
		} else {

		}

	}

	private static float getSizeInPixel(Context ctx) {
		return ctx.getResources().getDisplayMetrics().density;
	}

	// @Override
	// public void setAdapter(ListAdapter adapter) {
	// super.setAdapter(adapter);
	//
	// if (adapter instanceof SectionIndexer){
	// sections = (String[]) ((SectionIndexer) adapter).getSections();
	// Log.e("sections: ", ""+sections);
	// }
	//
	// }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		showLetter = true;
		float x = event.getX();
		// Log.e("onTouchEvent", "" + x);
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			// Log.e("ACTION_DOWN", "ACTION_DOWN");
			if (x < sx) {
				showLetter = false;
				listHandler = new ListHandler();
				listHandler.sendEmptyMessageDelayed(0, 1);
				return super.onTouchEvent(event);
			} else {
				// We touched the index bar
				float y = event.getY() - this.getPaddingTop()
						- getPaddingBottom();
				int currentPosition = (int) Math.floor(y / indexSize);
				section = sections[currentPosition];
				this.setSelection(((SectionIndexer) getAdapter())
						.getPositionForSection(currentPosition));
				listHandler = new ListHandler();
				listHandler.sendEmptyMessageDelayed(0, 100);
			}
			break;

		case MotionEvent.ACTION_MOVE:
			// Log.e("ACTION_MOVE", "ACTION_MOVE");
			if (x < sx) {
				showLetter = false;
				listHandler = new ListHandler();
				listHandler.sendEmptyMessageDelayed(0, 1);
				return super.onTouchEvent(event);

			} else {
				float y = event.getY();
				int currentPosition = (int) Math.floor(y / indexSize);
				section = sections[currentPosition];
				this.setSelection(((SectionIndexer) getAdapter())
						.getPositionForSection(currentPosition));
				listHandler = new ListHandler();
				listHandler.sendEmptyMessageDelayed(0, 100);
			}
			break;

		case MotionEvent.ACTION_UP:

			// Log.e("ACTION_UP", "ACTION_UP");
			if (x < sx) {
				showLetter = false;
				listHandler = new ListHandler();
				listHandler.sendEmptyMessageDelayed(0, 1);
				return super.onTouchEvent(event);

			} else {
				listHandler = new ListHandler();
				listHandler.sendEmptyMessageDelayed(0, 100);
			}
			break;

		}

		return true;
	}

	private class ListHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			showLetter = false;
			FastListView.this.invalidate();
		}
	}
}
