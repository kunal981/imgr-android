package com.imgr.chat.util;

import com.imgr.chat.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class NewLogMessage {
	public static void showDialog(final Activity activity, Context context,
			String title, String Message, String text, String text_) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog
				.setTitle(title)
				.setMessage(Message)
				.setCancelable(false)
				.setPositiveButton(text, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				})
				.setNegativeButton(text_,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
								activity.finish();
								activity.overridePendingTransition(
										R.anim.slide_out_left,
										R.anim.slide_in_right);

							}
						});

		AlertDialog dialog = alertDialog.create();
		dialog.show();

	}

}
