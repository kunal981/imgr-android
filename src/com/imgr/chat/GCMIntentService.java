package com.imgr.chat;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;

public class GCMIntentService extends GCMBaseIntentService {
	SharedPreferences mSharedPreferences;
	String mString, mString1, mString2, mString3, mString4, mString5,
			mString_toJID, mString_fromJID, to_messgae, testpromo_id,
			personal_jidfrom, personal_jidto;
	int coun;
	Editor editor;
	JSONObject mJsonObject;
	int count_test = 0;
	DatabaseHelper mDatabaseHelper;
	Context mContext;
	Cursor mCursor;
	DatasourceHandler mDatasourceHandler;

	String to_receive = null, from_receive = null, message_receive = null,
			fromJID_receive = null, toJID_receive = null, time_receive = null,
			timestamp_receive = null, date_receive = null,
			localdatabase_receive = null, messagepacketId_receive = null,
			receiveornot_receive = null, bubblecolor_receive = null,
			fontsize_receive = null, username_receive = null,
			profilepicture_receive = null, recenttojid_receive = null,
			recentfromjid_receive = null, contactuniqueid_receive = null;

	public GCMIntentService() {
		super(Constant.GCM_PROJECT_NO);
	}

	/**
	 * Method called on device registered
	 **/

	@Override
	protected void onRegistered(Context context, String registrationId) {
		// TODO Auto-generated method stub
		Log.e("registrationId: ", "" + registrationId);
		mSharedPreferences = this.getSharedPreferences(Constant.IMGRCHATGCM,
				Context.MODE_PRIVATE);
		editor = mSharedPreferences.edit();
		editor.putString(Constant.GCM_ID, registrationId);
		editor.commit();

	}

	/**
	 * Method called on device un registred
	 * */

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		// TODO Auto-generated method stub

	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {

		mString = intent.getExtras().getString("message");
		mContext = context;
		mSharedPreferences = this.getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		mDatasourceHandler = new DatasourceHandler(this);
		mDatabaseHelper = new DatabaseHelper(this);

		if (mString != null) {
			try {
				mJsonObject = new JSONObject(mString);
				mString2 = mJsonObject.getString("message");
				to_messgae = mJsonObject.getString("to");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			generateNotification_chat_message(context, mString2);
			new ExecuteChatNotificationSaved().execute(to_messgae);
		} else {

		}

	}

	/**
	 * Method called on Error
	 * */

	@Override
	protected void onError(Context context, String errorId) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Received error: " + errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message
	 * Chat side.
	 */

	// Chat notification coming from Chat screen
	public void generateNotification_chat_message(Context context,
			String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		String title = context.getString(R.string.app_name);

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		Intent notificationIntent = new Intent(context, NewSplash.class);
		mSharedPreferences = this.getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		editor = mSharedPreferences.edit();
		editor.putString("Notification_Click_Message", "true");

		editor.commit();

		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		notification.defaults |= Notification.DEFAULT_SOUND;

		// notification.sound = Uri.parse("android.resource://" +
		// context.getPackageName() + "your_sound_file_name.mp3");

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify((int) System.currentTimeMillis(),
				notification);
		// notificationManager.cancelAll();

	}

	class ExecuteChatNotificationSaved extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = JsonParserConnector
					.post_receive_ChatNotificaton(params[0]);

			// Log.e("Notification___", "" + url);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// UI.hideProgressDialog();
			Log.e("result: ", "" + result);
			JSONObject jsonobject1 = null;
			JSONObject jsonobject = null;
			try {
				jsonobject1 = new JSONObject(result);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String mString_success = null;
			try {
				mString_success = jsonobject1.getString("success");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (mString_success.equals("true")) {
				try {

					jsonobject = jsonobject1.getJSONObject("data");
					to_receive = jsonobject.getString("to");
					from_receive = jsonobject.getString("from");

					message_receive = jsonobject.getString("message");
					fromJID_receive = jsonobject.getString("fromJID");
					toJID_receive = jsonobject.getString("toJID");

					time_receive = jsonobject.getString("time");
					timestamp_receive = jsonobject.getString("timestamp");
					date_receive = jsonobject.getString("date");
					localdatabase_receive = jsonobject
							.getString("localdatabase");

					messagepacketId_receive = jsonobject
							.getString("messsgepacketId");
					Log.e("mString_success: ", "" + mString_success);
					String contactuniqueid_receive = jsonobject
							.getString("contactunqiueid");

					receiveornot_receive = jsonobject.getString("receiveornot");
					bubblecolor_receive = jsonobject.getString("bubblecolor");
					fontsize_receive = jsonobject.getString("fontsize");
					username_receive = jsonobject.getString("username");

					// String profilepicture_receive = jsonobject
					// .getString("profilepicture");
					recenttojid_receive = jsonobject.getString("recenttojid");
					recentfromjid_receive = jsonobject
							.getString("recentfromjid");

					// Log.e("to_receive:", "" + to_receive);
					// Log.e("from_receive:", "" + from_receive);
					// Log.e("message_receive:", "" + message_receive);
					// Log.e("fromJID_receive:", "" + fromJID_receive);
					// Log.e("toJID_receive:", "" + toJID_receive);
					// Log.e("time_receive:", "" + time_receive);
					// Log.e("timestamp_receive:", "" + timestamp_receive);
					// Log.e("date_receive:", "" + date_receive);
					// Log.e("localdatabase_receive:", "" +
					// localdatabase_receive);
					// Log.e("messagepacketId_receive:", ""
					// + messagepacketId_receive);
					editor = mSharedPreferences.edit();
					editor.putString("Notification_no", fromJID_receive);
					editor.putString("Notification_Username", username_receive);
					editor.putString("Notification_contactuniqueid_receive",
							contactuniqueid_receive);

					editor.commit();
					/*
					 * Log.e("contactuniqueid_receive:", "" +
					 * contactuniqueid_receive);
					 */
					// Log.e("receiveornot_receive:", "" +
					// receiveornot_receive);
					// Log.e("bubblecolor_receive:", "" + bubblecolor_receive);
					// Log.e("fontsize_receive:", "" + fontsize_receive);
					// Log.e("username_receive:", "" + username_receive);
					// Log.e("profilepicture_receive:", ""
					// + profilepicture_receive);
					// Log.e("recenttojid_receive:", "" + recenttojid_receive);
					// Log.e("recentfromjid_receive:", "" +
					// recentfromjid_receive);
					String promoID = null;
					String mStringnewtojid = null;
					String mStringnewfromjid = null;
					JSONObject objMainList = null;
					String check = null;

					mStringnewtojid = toJID_receive;
					mStringnewfromjid = fromJID_receive;
					// Log.e(" Text Recieved ", "" + message.getBody()
					// + " from " + fromName + "-----fromjid----"
					// + fromJID + "----tojid------" + toJID);

					// Log.e("", "CHECK");
					promoID = null;
					try {
						JSONObject jsonObj = new JSONObject(
								localdatabase_receive);

						objMainList = new JSONObject();
						// JSONArray mJsonArray = null;
						// mJsonArray = new JSONArray();

						// JSONObject json = new JSONObject();
						// Log.e("jsonObj: ", "" + jsonObj);

						if (jsonObj.has("_message_body_tag")) {
							Log.e(" ", "MESSAGE");
							promoID = jsonObj.getString("_promo_id_tag");
							if (promoID != null) {
								Log.e(" promoID", "promoID: " + promoID);
								if (promoID.equals("0")) {

								}

								else {
									check = countcheck(promoID);
									// api calling
									Log.e("api calling: ", "" + check);
									if (check != null) {
										if (check.equals("0")) {
											personal_jidfrom = mStringnewfromjid;
											personal_jidto = mStringnewtojid;

											new Execute_New_Promo()
													.execute(promoID);
											return;
										} else {

										}
									}

								}

							} else {

							}
							// Log.e("DATA objMainList: ",
							// "" + objMainList.toString());

						} else {

						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// Log.e("ELSE", "NOTHING");
					// Log.e("mStringnewtojid: ", "" + mStringnewtojid);
					// Log.e("mStringnewfromjid: ", "" + mStringnewfromjid);
					// Log.e("localdatabase_receive: ", "" +
					// localdatabase_receive);

					int history_count = mDatasourceHandler
							.HISTORYUpdateRecentReceiveCount(messagepacketId_receive);
					if (history_count == 0) {
						int countincrease = 0;
						countincrease = mDatasourceHandler
								.UpdateRecentReceiveCount(mStringnewtojid,
										mStringnewfromjid,
										localdatabase_receive, time_receive,
										date_receive, timestamp_receive);
						String mString = "" + (countincrease + 1);
						coun = Integer.parseInt(mString);

						boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
								mStringnewtojid, mStringnewfromjid,
								localdatabase_receive, time_receive,
								date_receive, timestamp_receive, mString);
						Log.e("RECEIVE 1 flag1:", "" + flag1);
						if (!flag1) {
							mDatasourceHandler.insertrecentMessage(
									localdatabase_receive, mStringnewfromjid,
									mStringnewtojid, "", username_receive,
									date_receive, time_receive,
									timestamp_receive, "1");
						}

					}

					mDatasourceHandler.insertChathistory(
							messagepacketId_receive, "username",
							mStringnewtojid, mStringnewfromjid,
							localdatabase_receive, time_receive, promoID, "1",
							date_receive);

					setBadge(mContext, coun);

					// count_test++;
					// int countincrease = mDatasourceHandler
					// .UpdateRecentReceiveCount(mStringnewtojid,
					// mStringnewfromjid, localdatabase_receive,
					// time_receive, date_receive,
					// timestamp_receive);
					// String mString = "" + countincrease + 1;
					//
					// Log.e("COUNT: ", "" + mString);
					// boolean flag = mDatasourceHandler.UpdateRecentReceive(
					// mStringnewtojid, mStringnewfromjid,
					// localdatabase_receive, time_receive, date_receive,
					// timestamp_receive, mString);
					// Log.e("RECEIVE 2 flag:", "" + flag);
					// if (!flag) {
					// mDatasourceHandler.insertrecentMessage(
					// localdatabase_receive, mStringnewfromjid,
					// mStringnewtojid, "", username_receive,
					// date_receive, time_receive, timestamp_receive,
					// "1");
					// }

					promoID = null;
					mStringnewtojid = null;
					mStringnewfromjid = null;

					/*
					 * mDatasourceHandler.insertChathistory(
					 * messagepacketId_receive, "username", toJID_receive,
					 * fromJID_receive, localdatabase_receive, time_receive,
					 * "0", "1", date_receive); // String timestamp =
					 * timestamp(); // count_test++; // String mString = "" +
					 * count_test; Log.e("COUNT: ", "" + mString); boolean flag
					 * = mDatasourceHandler.UpdateRecentReceive( toJID_receive,
					 * fromJID_receive, localdatabase_receive, time_receive,
					 * date_receive, timestamp_receive, "0");
					 * Log.e("RECEIVE 2 flag:", "" + flag); if (!flag) {
					 * mDatasourceHandler.insertrecentMessage(
					 * localdatabase_receive, fromJID_receive, toJID_receive,
					 * "", username_receive, date_receive, time_receive,
					 * timestamp_receive, "1"); }
					 */

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

			}

		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result: ", "" + result);
			// UI.hideProgressDialog();
			Personal_List(result);
		}

	}

	private void Personal_List(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.insertSponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				Log.e("flag", "" + flag);

				//

				Log.e("if RECEVIVE", "calling");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);
				Log.e("ELSE", "NOTHING");
				Log.e("mStringnewtojid: ", "" + personal_jidto);
				Log.e("mStringnewfromjid: ", "" + personal_jidfrom);

				// count_test++;

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(messagepacketId_receive);
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, localdatabase_receive,
									time_receive, date_receive,
									timestamp_receive);
					String mString = "" + (countincrease + 1);
					coun = Integer.parseInt(mString);
					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							localdatabase_receive, time_receive, date_receive,
							timestamp_receive, mString);
					Log.e("RECEIVE 1 flag1:", "" + flag1);
					if (!flag1) {
						mDatasourceHandler.insertrecentMessage(
								localdatabase_receive, personal_jidfrom,
								personal_jidto, "", username_receive,
								date_receive, time_receive, timestamp_receive,
								"1");
					}

				}

				mDatasourceHandler.insertChathistory(messagepacketId_receive,
						"username", personal_jidto, personal_jidfrom,
						localdatabase_receive, time_receive, mStringspromoid,
						"1", date_receive);

				// String mString = "" + count_test;
				// boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
				// personal_jidto, personal_jidfrom,
				// localdatabase_receive, time_receive, date_receive,
				// timestamp_receive, mString);
				// Log.e("RECEIVE 2 flag:", "" + flag);
				// if (!flag1) {
				// mDatasourceHandler.insertrecentMessage(
				// localdatabase_receive, personal_jidfrom,
				// personal_jidto, "", username_receive, date_receive,
				// time_receive, timestamp_receive, "1");
				// }
				//
				// // promoID = null;
				// personal_jidto = null;
				// personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	public String countcheck(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public static void setBadge(Context context, int count) {
		String launcherClassName = getLauncherClassName(context);
		if (launcherClassName == null) {
			return;
		}
		Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
		intent.putExtra("badge_count", count);
		intent.putExtra("badge_count_package_name", context.getPackageName());
		intent.putExtra("badge_count_class_name", launcherClassName);
		context.sendBroadcast(intent);
	}

	public static String getLauncherClassName(Context context) {

		PackageManager pm = context.getPackageManager();

		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);

		List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
		for (ResolveInfo resolveInfo : resolveInfos) {
			String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
			if (pkgName.equalsIgnoreCase(context.getPackageName())) {
				String className = resolveInfo.activityInfo.name;
				return className;
			}
		}
		return null;
	}

}
