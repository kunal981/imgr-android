package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.BlockAndMeaageActivity;
import com.imgr.chat.message.ChatScreen;
import com.imgr.chat.model.IMGR_Model;
import com.joooonho.SelectableRoundedImageView;

public class ImgrAdapter extends BaseAdapter implements SectionIndexer {

	private ArrayList<String> contactnumber1;
	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;
	private static String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private ItemFilter mFilter = new ItemFilter();

	private List<IMGR_Model> mPhoneModels = null;
	private ArrayList<IMGR_Model> arraylist;
	SharedPreferences sharedPreferences;
	Editor editor;

	public ImgrAdapter(FragmentActivity fragmentActivity,
			ArrayList<String> contactname, ArrayList<String> contactnumber,
			List<IMGR_Model> mPhoneModels) {
		// TODO Auto-generated constructor stub
		this.mPhoneModels = mPhoneModels;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		context = fragmentActivity;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.arraylist = new ArrayList<IMGR_Model>();
		this.arraylist.addAll(mPhoneModels);
		sharedPreferences = fragmentActivity.getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	public IMGR_Model getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	private void setSection(LinearLayout header, String label) {
		select = label.substring(0, 1).toUpperCase();
		TextView text = new TextView(context);
		// header.setBackgroundColor();
		text.setTextColor(Color.parseColor("#007AFF"));
		text.setText(select);
		text.setTextSize(20);
		text.setPadding(20, 0, 0, 0);
		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.imgr_listrow, null);

		LinearLayout header = (LinearLayout) rowView.findViewById(R.id.section);
		SelectableRoundedImageView mImageViewProfile = (SelectableRoundedImageView) rowView
				.findViewById(R.id.imageView1);
		String name;
		if (mPhoneModels.get(position).getName().equals("")) {
			name = "No Name";
		} else {
			name = mPhoneModels.get(position).getName();
		}

		// String number = contactnumber1.get(position);
		char firstChar = name.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, name);
		} else {
			char preFirstChar;
			String preLabel = null;
			if (mPhoneModels.get(position - 1).getName().equals("")) {
				// preLabel = mPhoneModels.get(position - 1).getName();
			} else {
				preLabel = mPhoneModels.get(position - 1).getName();
			}

			preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {
				setSection(header, name);
			} else {
				header.setVisibility(View.GONE);
			}
		}
		mImageViewProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Friend_Prefernces(mPhoneModels.get(position).getMemberId());
				Intent mIntent_info = new Intent(context, ChatScreen.class);
				mIntent_info.putExtra("Chat_name_select",
						mPhoneModels.get(position).getName());
				mIntent_info.putExtra("Chat_no_select",
						mPhoneModels.get(position).getNumber());

				context.startActivity(mIntent_info);
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);
			}
		});

		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);
		// final TextView textView1 = (TextView)
		// rowView.findViewById(R.id.textView1);
		textView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Friend_Prefernces(mPhoneModels.get(position).getMemberId());
				Intent mIntent_info = new Intent(context, ChatScreen.class);
				mIntent_info.putExtra("Chat_name_select",
						mPhoneModels.get(position).getName());
				mIntent_info.putExtra("Chat_no_select",
						mPhoneModels.get(position).getNumber());
				mIntent_info.putExtra("phone_id", mPhoneModels.get(position)
						.getPhone());
				context.startActivity(mIntent_info);
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);
				// LogMessage.showDialog(context, null,
				// "This feature availble in next module.", null, "Ok");

			}
		});
		ImageView msg = (ImageView) rowView.findViewById(R.id.message);
		msg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constant.HANDLE_BACK_CONATCT_ADD = 2;
				Intent mIntent_info = new Intent(context,
						BlockAndMeaageActivity.class);
				mIntent_info.putExtra("name_select", mPhoneModels.get(position)
						.getName());
				mIntent_info.putExtra("phone_id", mPhoneModels.get(position)
						.getPhone());
				mIntent_info.putExtra("phoneno_select",
						mPhoneModels.get(position).getNumber());

				context.startActivity(mIntent_info);
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);
			}

		});
		textView.setText(mPhoneModels.get(position).getName() + " "
				+ mPhoneModels.get(position).getLast());
		if (mPhoneModels.get(position).getBase64().equals("Null")) {
			mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
			mImageViewProfile.setOval(true);
			mImageViewProfile
					.setBackgroundResource(R.drawable.ic_contact_place);
		} else {
			Bitmap mBitmap = ConvertToImage(mPhoneModels.get(position)
					.getBase64());
			mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
			mImageViewProfile.setOval(true);
			mImageViewProfile.setImageBitmap(mBitmap);
		}

		return rowView;
	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public int getPositionForSection(int section) {
		// Log.e("ListView", "Get position for section");

		// Log.e("getCount(): ", "" + getCount());

		for (int i = 0; i < this.getCount(); i++) {
			// Log.e("indside: ", "" + this.getItem(i).getName());

			String item = this.getItem(i).getName();
			if (item.charAt(0) == sections.charAt(section))
				return i;
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int arg0) {
		Log.d("ListView", "Get section");
		return 0;
	}

	@Override
	public Object[] getSections() {

		String[] sectionsArr = new String[sections.length()];
		Log.e("sectionsArr: ", "" + sectionsArr);
		for (int i = 0; i < sections.length(); i++) {
			Log.e("sectionsArr: ", "" + sections.charAt(i));
			sectionsArr[i] = "" + sections.charAt(i);
		}

		return sectionsArr;
	}

	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final List<IMGR_Model> list = mPhoneModels;

			int count = list.size();
			final ArrayList<String> nlist = new ArrayList<String>(count);

			String filterableString;

			for (int i = 0; i < count; i++) {
				filterableString = list.get(i).getName();
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(filterableString);
				}
			}

			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredData = (ArrayList<String>) results.values;
			Log.e("", "" + filteredData);
			notifyDataSetChanged();
		}

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mPhoneModels.clear();
		if (charText.length() == 0) {
			mPhoneModels.addAll(arraylist);
		} else {
			for (IMGR_Model wp : arraylist) {
				if (wp.getFullName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mPhoneModels.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}

	public void Friend_Prefernces(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.FRIEND_ID_PROMO, mString);

		editor.commit();

	}

}