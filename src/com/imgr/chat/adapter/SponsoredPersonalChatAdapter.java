package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.ModelPersonalSponsoredSelect;
import com.imgr.chat.model.SponsoredPersonalModel;
import com.imgr.chat.setting.OpendetailPersonalPromo;
import com.imgr.chat.util.UI;

public class SponsoredPersonalChatAdapter extends BaseAdapter implements
		SectionIndexer {

	@SuppressWarnings("unused")
	private ArrayList<String> contactnumber1;
	@SuppressWarnings("unused")
	private ArrayList<String> ISIMGR;
	LinearLayout header;

	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;

	private List<SponsoredPersonalModel> mPhoneModels = null;
	private ArrayList<SponsoredPersonalModel> arraylist;

	private static String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	// private static String sections = "abcdefghijklmnoprrstuvwxyz";
	private ItemFilter mFilter = new ItemFilter();

	String mStringPromoOnOFF;
	String mStringSponsored_BarChanged;
	SharedPreferences sharedPreferences;
	String PhoneID;
	Cursor mCursor;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	String setPromo, setAutorotate, setSponsoredads, setpersonalads,
			setuniqueid;

	public SponsoredPersonalChatAdapter(Activity fragmentActivity,
			ArrayList<String> contactname, ArrayList<String> contactnumber,
			ArrayList<String> isIMGR,
			List<SponsoredPersonalModel> mPhoneModels, String phoneid) {
		// TODO Auto-generated constructor stub
		this.mPhoneModels = mPhoneModels;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		ISIMGR = isIMGR;
		context = fragmentActivity;
		this.PhoneID = phoneid;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.arraylist = new ArrayList<SponsoredPersonalModel>();
		this.arraylist.addAll(mPhoneModels);
		sharedPreferences = context.getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);

		mStringPromoOnOFF = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "");
		mStringSponsored_BarChanged = sharedPreferences.getString(
				Constant.SPONSORED_ADS, "");
		mDatabaseHelper = new DatabaseHelper(context);
		mDatasourceHandler = new DatasourceHandler(context);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	// public String getItem(int position) {
	// return filteredData.get(position);
	// }

	public SponsoredPersonalModel getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	@SuppressLint("DefaultLocale")
	private void setSection(LinearLayout header, String label) {

		select = label.substring(0, 1).toUpperCase();

		TextView text = new TextView(context);

		text.setTextColor(Color.parseColor("#007AFF"));
		text.setText(select);
		text.setTextSize(20);
		text.setPadding(20, 0, 0, 0);

		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);

	}

	@SuppressLint("DefaultLocale")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.custom_personal_sponsored_chat,
				null);

		header = (LinearLayout) rowView.findViewById(R.id.section);
		String name = mPhoneModels.get(position).getpromoname();
		// String number = contactnumber1.get(position);
		char firstChar = name.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, name);
		} else {
			String preLabel = mPhoneModels.get(position - 1).getpromoname();
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {

				setSection(header, name);
			} else {

				header.setVisibility(View.GONE);
			}
		}
		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);

		textView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mStringPromoOnOFF.equals("true")) {

					mCursor = mDatasourceHandler.User_Set_Fetch(PhoneID);
					if (mCursor.getCount() != 0) {

						do {

							setPromo = mCursor.getString(0).trim();
							setAutorotate = mCursor.getString(1).trim();
							setSponsoredads = mCursor.getString(2).trim();
							setpersonalads = mCursor.getString(3).trim();
							setuniqueid = mCursor.getString(4).trim();

						} while (mCursor.moveToNext());

						mCursor.close();

					}

					if (mStringSponsored_BarChanged.equals("true")
							&& setSponsoredads.equals("true")) {
						Constant.PROMO_OFF_NOW_SELECT_ONE = 1;
						Constant._ID = mPhoneModels.get(position).getPromoId();
						((Activity) context).finish();

						((Activity) context).overridePendingTransition(
								R.anim.slide_out_left, R.anim.slide_in_right);
					} else {
						Constant.SPONSORED_PROMO_OFF_ = 1;
						Constant.SPONSORED_PROMO_OFF_ID = mPhoneModels.get(
								position).getPromoId();
						((Activity) context).finish();

						((Activity) context).overridePendingTransition(
								R.anim.slide_out_left, R.anim.slide_in_right);
					}

				} else {
					Constant.PROMO_OFF_NOW_SELECT_ONE = 1;
					Constant._ID = mPhoneModels.get(position).getPromoId();
					((Activity) context).finish();

					((Activity) context).overridePendingTransition(
							R.anim.slide_out_left, R.anim.slide_in_right);
				}
			}
		});

		if (mPhoneModels.get(position).getEnabled().equals("0")) {
			textView.setText(mPhoneModels.get(position).getpromoname());
			textView.setTextColor(Color.RED);
		} else {
			textView.setText(mPhoneModels.get(position).getpromoname());
			textView.setTextColor(Color.BLACK);
		}

		return rowView;
	}

	@Override
	public int getPositionForSection(int section) {

		for (int i = 0; i < this.getCount(); i++) {

			String item = this.getItem(i).getpromoname();

			if (item.charAt(0) == sections.charAt(section))
				return i;
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int arg0) {

		return 0;
	}

	@Override
	public Object[] getSections() {

		String[] sectionsArr = new String[sections.length()];

		Log.e("sectionsArr: ", "" + sectionsArr);
		for (int i = 0; i < sections.length(); i++) {
			sectionsArr[i] = "" + sections.charAt(i);
			Log.e("sectionsArr: ", "" + sections.charAt(i));
		}

		return sectionsArr;
	}

	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final List<String> list = originalData;

			int count = list.size();
			final ArrayList<String> nlist = new ArrayList<String>(count);

			String filterableString;

			for (int i = 0; i < count; i++) {
				filterableString = list.get(i);
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(filterableString);
				}
			}

			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredData = (ArrayList<String>) results.values;

			notifyDataSetChanged();
		}

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mPhoneModels.clear();
		if (charText.length() == 0) {
			mPhoneModels.addAll(arraylist);
		} else {
			for (SponsoredPersonalModel wp : arraylist) {
				if (wp.getpromoname().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mPhoneModels.add(wp);
				}
			}
		}
		char firstChar = charText.toUpperCase().charAt(0);

		setSection(header, "" + firstChar);
		notifyDataSetChanged();
	}

}
