package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.model.IMGR_Model;
import com.imgr.chat.model.Product;

public class InviteAdapter extends BaseAdapter implements SectionIndexer {

	private ArrayList<String> contactnumber1;
	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;
	private static String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private ItemFilter mFilter = new ItemFilter();
	ArrayList<Product> objects;
	private List<Product> mPhoneModels = null;

	public InviteAdapter(Activity fragmentActivity,
			ArrayList<String> contactname, ArrayList<String> contactnumber,
			List<Product> products) {
		// TODO Auto-generated constructor stub
		this.mPhoneModels = products;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		// objects = products;
		context = fragmentActivity;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.objects = new ArrayList<Product>();
		this.objects.addAll(mPhoneModels);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	public Product getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	private void setSection(LinearLayout header, String label) {
		select = label.substring(0, 1).toUpperCase();
		TextView text = new TextView(context);
		// header.setBackgroundColor();
		text.setTextColor(Color.parseColor("#007AFF"));
		text.setText(select);
		text.setTextSize(20);
		text.setPadding(20, 0, 0, 0);
		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.checkbox_listview, null);

		LinearLayout header = (LinearLayout) rowView.findViewById(R.id.section);
		String name = mPhoneModels.get(position).getName();
		// String number = contactnumber1.get(position);
		char firstChar = name.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, name);
		} else {
			String preLabel = filteredData.get(position - 1);
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {
				setSection(header, name);
			} else {
				header.setVisibility(View.GONE);
			}
		}
		Product p = getProduct(position);
		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);

		CheckBox cbBuy = (CheckBox) rowView.findViewById(R.id.checkbox);
		cbBuy.setOnCheckedChangeListener(myCheckChangList);
		cbBuy.setTag(position);
		cbBuy.setChecked(mPhoneModels.get(position).box);
		textView.setText(mPhoneModels.get(position).getName() + " "
				+ mPhoneModels.get(position).getlast());

		return rowView;
	}

	private Product getProduct(int position) {
		// TODO Auto-generated method stub
		return ((Product) getItem(position));
	}

	public ArrayList<Product> getBox() {
		ArrayList<Product> box = new ArrayList<Product>();
		for (Product p : objects) {
			if (p.box)
				box.add(p);
		}
		return box;
	}

	OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			getProduct((Integer) buttonView.getTag()).box = isChecked;
		}
	};

	@Override
	public int getPositionForSection(int section) {

		for (int i = 0; i < this.getCount(); i++) {
			String item = getItem(i).getName();
			if (item.charAt(0) == sections.charAt(section))
				return i;
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int arg0) {

		return 0;
	}

	@Override
	public Object[] getSections() {

		String[] sectionsArr = new String[sections.length()];
		for (int i = 0; i < sections.length(); i++)
			sectionsArr[i] = "" + sections.charAt(i);
		return sectionsArr;
	}

	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final List<Product> list = mPhoneModels;

			int count = list.size();
			final ArrayList<String> nlist = new ArrayList<String>(count);

			String filterableString;

			for (int i = 0; i < count; i++) {
				filterableString = mPhoneModels.get(i).getName();
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(filterableString);
				}
			}

			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredData = (ArrayList<String>) results.values;
			Log.e("", "" + filteredData);
			notifyDataSetChanged();
		}

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mPhoneModels.clear();
		if (charText.length() == 0) {
			mPhoneModels.addAll(objects);
		} else {
			for (Product wp : objects) {
				if (wp.getFullname().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mPhoneModels.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}

}
