package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.imgr.chat.R;
import com.imgr.chat.model.CustomGalleryModel;
import com.imgr.chat.setting.CustomGridViewGalleryActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class GridviewAdapter extends BaseAdapter {

	private ArrayList<String> listFlag;
	private Activity activity;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	private List<CustomGalleryModel> mPhoneModels = null;
	private ArrayList<CustomGalleryModel> arraylist;

	public GridviewAdapter(Activity activity, ArrayList<String> listFlag,
			List<CustomGalleryModel> mPhoneModels) {
		super();
		this.mPhoneModels = mPhoneModels;
		this.listFlag = listFlag;
		this.activity = activity;
		this.arraylist = new ArrayList<CustomGalleryModel>();
		this.arraylist.addAll(mPhoneModels);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mPhoneModels.size();
	}

	@Override
	public CustomGalleryModel getItem(int position) {
		// TODO Auto-generated method stub
		return mPhoneModels.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder {
		public ImageView imgViewFlag;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder view;
		LayoutInflater inflator = activity.getLayoutInflater();

		if (convertView == null) {
			view = new ViewHolder();
			convertView = inflator.inflate(R.layout.gridview_row, null);

			view.imgViewFlag = (ImageView) convertView
					.findViewById(R.id.image_View);

			convertView.setTag(view);
		} else {
			view = (ViewHolder) convertView.getTag();
		}
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).cacheInMemory()
				.cacheOnDisc().build();

		// imageLoader.displayImage(listFlag.get(position), view.imgViewFlag);

		imageLoader.displayImage(mPhoneModels.get(position).geturl(),
				view.imgViewFlag, options, new SimpleImageLoadingListener() {
					@Override
					public void onLoadingComplete(Bitmap loadedImage) {
						Animation anim = AnimationUtils.loadAnimation(activity,
								R.anim.fade_in);
						// imgViewFlag.setAnimation(anim);
						// anim.start();
					}
				});
		view.imgViewFlag.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CustomGridViewGalleryActivity.path = mPhoneModels.get(position)
						.geturl();

				activity.finish();
				((Activity) activity).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);
			}
		});

		// view.imgViewFlag.setImageResource(listFlag.get(position));

		return convertView;
	}

}
