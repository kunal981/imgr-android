package com.imgr.chat.adapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ChatAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, SectionIndexer {
	LayoutInflater inflator;
	ArrayList<String[]> chat_List;
	Context activity;
	TextView tvHome;
	SharedPreferences sharedPreferences;
	Editor editor;
	String mStringfont, mStringBubble, mStringBubble_receive, mString_override;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	String mStringHeader, mStringLink, mStringImageUrl, MemberId, OwnId;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	// ArrayList<String>mArrayListDATE;
	private int[] mSectionIndices;
	private String[] mSectionLetters;
	String[] test;

	public ChatAdapter(Context context, ArrayList<String[]> messages) {

		this.chat_List = messages;

		this.activity = context;
		inflator = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		sharedPreferences = context.getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mDatabaseHelper = new DatabaseHelper(context);
		mDatasourceHandler = new DatasourceHandler(context);
		// FONT SECTION WORKING ON THIS
		mStringfont = sharedPreferences.getString(Constant.FONT_SET, "");

		// BUBBLE SECTION WORKING ON THIS
		mString_override = sharedPreferences.getString(
				Constant.BUBBLE_SET_OVERRIDE_FIXED, "");

		mStringBubble_receive = sharedPreferences.getString(
				Constant.BUBBLE_SET_FREIND, "");
		MemberId = sharedPreferences.getString(Constant.FRIEND_ID_PROMO, "");
		OwnId = sharedPreferences.getString(Constant.YOUR_ID_PROMO, "");
		// Log.e("mStringBubble_receive: ", "" + mStringBubble_receive);

		if (mStringBubble_receive != null) {
			if (mStringBubble_receive.equals("blue")) {
				mStringBubble = "blue";
			} else if (mStringBubble_receive.equals("green")) {
				mStringBubble = "green";
			} else if (mStringBubble_receive.equals("purple")) {
				mStringBubble = "purple";

			}

		} else {
			mStringBubble = "blue";
		}
		// Log.e("mStringBubble: ", "" + mStringBubble);

	}

	private int[] getSectionIndices() {
		int[] sections = null;
		ArrayList<Integer> sectionIndices = new ArrayList<Integer>();

		// char lastFirstChar = mArrayListDATE[0].charAt(0);
		// Log.e("lastFirstChar: ", "" + lastFirstChar);
		sectionIndices.add(0);

		final String temp[] = test;

		if (temp[1].equals("my")) {

			String mString = temp[8];
			// Log.e("mArrayListDATE: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[8].equals(mString)) {
					mString = temp[i];
					// Log.e("My SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}

		}
		if (temp[1].equals("pic")) {
			String mString = temp[6];
			// Log.e("mArrayListDATE: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[6].equals(mString)) {
					mString = temp[i];
					// Log.e("PIC SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}
		}

		if (temp[1].equals("friend")) {
			String mString = temp[8];
			// Log.e(" friend mArrayListDATE: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[8].equals(mString)) {
					mString = temp[i];
					// Log.e(" friend SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}

		}

		if (temp[1].equals("friend_pic")) {
			String mString = temp[5];
			// Log.e(" friend_pic SEE CALLING mString: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[5].equals(mString)) {
					mString = temp[i];
					// Log.e("friend_pic SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}
		}

		// Log.e("sections: ", "" + sections);
		return sections;
	}

	private String[] getSectionLetters() {
		String[] letters = new String[mSectionIndices.length];
		// Log.e("letters: ", "" + letters);
		for (int i = 0; i < mSectionIndices.length; i++) {
			letters[i] = test[mSectionIndices[i]];
		}
		// Log.e("last letters: ", "" + letters);
		return letters;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return chat_List.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View my_View = convertView;
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).cacheInMemory()
				.cacheOnDisc().build();
		if (position == 0) {
			// Log.e("", "NOT CALLING");
		} else {
			// Log.e("", "CALLING");
			// Arrays.fill(test, null);
			// test.
			test = chat_List.get(position);
			mSectionIndices = getSectionIndices();
			mSectionLetters = getSectionLetters();
		}

		if (mStringfont.equals("normal")) {
			final String temp[] = chat_List.get(position);
			if (temp[1].equals("my")) {
				if (temp[5].equals("0")) {
					// Log.e(" ", "0");
					my_View = (View) inflator.inflate(R.layout.simple_white,
							parent, false);
				} else {
					// Log.e(" ", "ELSE");
					String PROMO_ID = temp[7];
					// Log.e("PROMO_ID: ", "" + PROMO_ID);
					String header = MYpromo_data_header(PROMO_ID);
					String footer = MYpromo_data_footer_text(PROMO_ID);
					String footerlink = MYpromo_data_link(PROMO_ID);
					// Log.e("header: ", "" + header);
					// Log.e("footer: ", "" + footer);
					if (header != null) {
						if (footer != null) {

							String type = promo_type(PROMO_ID);
							if (type.matches("personal")) {
								// both here header and footer
								if (header.equals("")) {
									my_View = (View) inflator.inflate(
											R.layout.customchat_list_own,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);

									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
								} else {
									my_View = (View) inflator.inflate(
											R.layout.header_footer_white,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);
									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
									final TextView header_link = (TextView) my_View
											.findViewById(R.id.header);
									header_link.setText(header);
								}

							} else {
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

						}

						else {
							// no footer
							if (footer.equals("")) {
								// / Log.e(" ", "no footer");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							} else {
								// Log.e(" ", "no");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

							}

						}

					}

					else {
						if (footer != null) {
							if (footer.equals("")) {
								// Log.e(" ", "nothing simple image");
								// nothing simple image
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

							else {
								// both header and footer
								// /Log.e(" ", "both header and footer");
								my_View = (View) inflator.inflate(
										R.layout.header_footer_white, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

								final TextView header_link = (TextView) my_View
										.findViewById(R.id.header);
								header_link.setText(header);

							}
						}

						else {
							// Log.e(" ", "nothing happened");
							// nothing happened
							my_View = (View) inflator.inflate(
									R.layout.simple_white, parent, false);
						}

					}

				}
				// Log.e("temp[0]: ", "" + temp[0]);
				// Log.e("temp[1]: ", "" + temp[1]);
				// Log.e("temp[2]: ", "" + temp[2]);
				// Log.e("temp[3]: ", "" + temp[3]);
				// Log.e("temp[4]: ", "" + temp[4]);
				// Log.e("temp[5]: ", "" + temp[5]);
				// Log.e("temp[6]: ", "" + temp[6]);
				// Log.e("temp[7]: ", "" + temp[7]);
				// Log.e("temp[8]: ", "" + temp[8]);
				TextView tvName = (TextView) my_View
						.findViewById(R.id.usermessage);
				tvName.setTextSize(15.0f);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				tvHome.setTextSize(9.0f);

				ImageView mImagetick = (ImageView) my_View
						.findViewById(R.id.sent_pending_tick);

				tvName.setText(temp[0]);

				tvHome.setText(temp[6]);
				if (temp[2].equals("1")) {
					mImagetick.setBackgroundResource(R.drawable.conform_sent);
				} else {
					mImagetick.setBackgroundResource(R.drawable.pending_sent);
				}

			}
			if (temp[1].equals("pic")) {

				my_View = (View) inflator.inflate(R.layout.forimage, parent,
						false);

				ImageView tvName = (ImageView) my_View.findViewById(R.id.pic);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				String image = temp[3];
				// Log.e("PIC image: ", "" + image);
				Bitmap myBitmap = ConvertToImage(image);

				tvName.setImageBitmap(myBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(9.0f);

			}

			// Log.v("temp[2]", "" + temp[2]);
			// Log.v("temp[1]", "" + temp[1]);
			if (temp[1].equals("friend")) {

				// Log.e("", "ENTER HERE");
				// Log.e("FREDS temp[7]", "" + temp[7]);
				// Log.e("FREDS temp[6]", "" + temp[6]);
				// Log.e("FREDS temp[5]", "" + temp[5]);
				// // Log.e("FREDS temp[4]", "" + temp[4]);
				// Log.e("FREDS temp[3]", "" + temp[3]);
				// Log.e("FREDS temp[2]", "" + temp[2]);
				// Log.e("FREDS temp[1]", "" + temp[1]);
				// Log.e("FREDS temp[0]", "" + temp[0]);
				String picPath = promo_image(temp[7]);
				// Log.e("FREDS picPath", "" + picPath);

				if (temp[7].equals("0")) {

					// Log.e("", "last try");
					// Log.e("", "last try: " + temp[7]);
					my_View = (View) inflator.inflate(R.layout.all_simple,
							parent, false);
					RelativeLayout mRelativeLayout = (RelativeLayout) my_View
							.findViewById(R.id.frame);
					if (mStringBubble.equals("blue")) {

						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_blue);
					} else if (mStringBubble.equals("green")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_green);
					} else if (mStringBubble.equals("purple")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_purple);

					}

				} else {
					// Log.e("test:   ", "HIIIIIIIIIIIIIIII");
					String mString_header = promo_data_header(temp[7]);
					String footerlink = MYpromo_data_link(temp[7]);
					// Log.e("????????mString_header:   ", "" + mString_header);

					if (mString_header.equals("")) {
						// NO Header SHOW in the screen
						// Log.e("", "SINGLE");
						my_View = (View) inflator.inflate(
								R.layout.customforfriend_noheader, parent,
								false);
						FrameLayout mRelativeLayout = (FrameLayout) my_View
								.findViewById(R.id.frame);

						if (mStringBubble.equals("blue")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterblue);
						} else if (mStringBubble.equals("green")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfootergreen);

						} else if (mStringBubble.equals("purple")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterpurple);
						}
					}

					else {
						// Log.e("", "both");
						// Both Header and Footer show in screen
						my_View = (View) inflator.inflate(
								R.layout.custom_forfriend, parent, false);
						FrameLayout mRelativeLayout = (FrameLayout) my_View
								.findViewById(R.id.frame);

						if (mStringBubble.equals("blue")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.header_footer_blue);
						} else if (mStringBubble.equals("green")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.header_footer_green);

						} else if (mStringBubble.equals("purple")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.header_footer_purple);
						}

						TextView mTextViewheader = (TextView) my_View
								.findViewById(R.id.header);
						mTextViewheader.setText(mString_header);
						mTextViewheader.setTextSize(14.0f);
					}

					ImageView mImageViewIcon = (ImageView) my_View
							.findViewById(R.id.icon);
					// Log.e("Inside picPath", "" + picPath);
					imageLoader.displayImage(picPath, mImageViewIcon, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
					String mString_footer = promo_data_footer_text(temp[7]);
					// Log.e("mString_footer: ", "" + mString_footer);

					final TextView mTextViewFooter_link = (TextView) my_View
							.findViewById(R.id.footer_link);
					TextView mTextViewFooter = (TextView) my_View
							.findViewById(R.id.footer);

					mTextViewFooter.setText(mString_footer);
					mTextViewFooter.setTextSize(15.0f);
					mTextViewFooter_link.setText(footerlink);
					mTextViewFooter.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String mString_footer_link = mTextViewFooter_link
									.getText().toString();
							// Log.e("mString_footer_link: ", ""
							// + mString_footer_link);
							String test = mString_footer_link
									.substring(mString_footer_link.length() - 1);
							// Log.e("test: ", "" + test);
							String main_link;
							if (test.equals("=")) {
								main_link = mString_footer_link + MemberId;
							} else {
								main_link = mString_footer_link;
								// + "/"+ MemberId;
							}

							// String link = mString_footer_link + MemberId;
							Log.e("MemberId: ", "" + MemberId);

							if (!main_link.startsWith("https://")
									&& !main_link.startsWith("http://")) {
								main_link = "http://" + main_link;
							}
							Log.e("FINAl link: ", "" + main_link);
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri.parse(main_link));
							activity.startActivity(i);

						}
					});

				}

				TextView rec = (TextView) my_View
						.findViewById(R.id.frommessage);

				tvHome = (TextView) my_View.findViewById(R.id.seen);
				rec.setText(temp[3]);
				rec.setTextSize(15.0f);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(9.0f);

			}

			if (temp[1].equals("friend_pic")) {
				my_View = (View) inflator.inflate(R.layout.freindimage, parent,
						false);

				ImageView tvName = (ImageView) my_View
						.findViewById(R.id.pic_freind);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				Bitmap mBitmap = ConvertToImage(temp[3]);
				tvName.setImageBitmap(mBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(9.0f);

			}
		}

		// LARGER FONT SIZE SET ALL TEXT SIZE
		else {
			final String temp[] = chat_List.get(position);
			if (temp[1].equals("my")) {
				if (temp[5].equals("0")) {

					my_View = (View) inflator.inflate(R.layout.simple_white,
							parent, false);
				} else {
					// Log.e(" ", "ELSE");
					String PROMO_ID = temp[7];
					// Log.e("PROMO_ID: ", "" + PROMO_ID);
					String header = MYpromo_data_header(PROMO_ID);
					String footer = MYpromo_data_footer_text(PROMO_ID);
					String footerlink = MYpromo_data_link(PROMO_ID);
					// Log.e("header: ", "" + header);
					// Log.e("footer: ", "" + footer);
					if (header != null) {
						if (footer != null) {

							String type = promo_type(PROMO_ID);
							if (type.matches("personal")) {
								// both here header and footer
								if (header.equals("")) {
									my_View = (View) inflator.inflate(
											R.layout.customchat_list_own,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);

									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
								} else {
									my_View = (View) inflator.inflate(
											R.layout.header_footer_white,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);
									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
									final TextView header_link = (TextView) my_View
											.findViewById(R.id.header);
									header_link.setText(header);
								}

							} else {
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

						}

						else {
							// no footer
							if (footer.equals("")) {
								// / Log.e(" ", "no footer");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							} else {
								// Log.e(" ", "no");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

							}

						}

					}

					else {
						if (footer != null) {
							if (footer.equals("")) {
								// Log.e(" ", "nothing simple image");
								// nothing simple image
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

							else {
								// both header and footer
								// /Log.e(" ", "both header and footer");
								my_View = (View) inflator.inflate(
										R.layout.header_footer_white, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

								final TextView header_link = (TextView) my_View
										.findViewById(R.id.header);
								header_link.setText(header);

							}
						}

						else {
							// Log.e(" ", "nothing happened");
							// nothing happened
							my_View = (View) inflator.inflate(
									R.layout.simple_white, parent, false);
						}

					}

				}

				TextView tvName = (TextView) my_View
						.findViewById(R.id.usermessage);
				tvName.setTextSize(17.0f);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				tvHome.setTextSize(11.0f);

				ImageView mImagetick = (ImageView) my_View
						.findViewById(R.id.sent_pending_tick);
				tvName.setText(temp[0]);

				tvHome.setText(temp[6]);
				if (temp[2].equals("1")) {
					mImagetick.setBackgroundResource(R.drawable.conform_sent);
				} else {
					mImagetick.setBackgroundResource(R.drawable.pending_sent);
				}

			}
			if (temp[1].equals("pic")) {

				my_View = (View) inflator.inflate(R.layout.forimage, parent,
						false);

				ImageView tvName = (ImageView) my_View.findViewById(R.id.pic);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				String image = temp[3];
				// Log.e("PIC image: ", "" + image);
				Bitmap myBitmap = ConvertToImage(image);

				tvName.setImageBitmap(myBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(11.0f);

			}

			// Log.v("temp[2]", "" + temp[2]);
			if (temp[1].equals("friend")) {
				String picPath = promo_image(temp[7]);
				// String promo_count=temp[7];
				// Log.e("temp[7]", "" + temp[7]);

				if (temp[7].equals("0")) {

					my_View = (View) inflator.inflate(R.layout.all_simple,
							parent, false);
					RelativeLayout mRelativeLayout = (RelativeLayout) my_View
							.findViewById(R.id.frame);
					if (mStringBubble.equals("blue")) {

						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_blue);
					} else if (mStringBubble.equals("green")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_green);
					} else if (mStringBubble.equals("purple")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_purple);

					}

				} else {

					String mString_header = promo_data_header(temp[7]);
					String footerlink = MYpromo_data_link(temp[7]);
					// Log.e("????????mString_header:   ", "" + mString_header);

					if (mString_header.equals("")) {
						// NO Header SHOW in the screen
						// Log.e("", "SINGLE");
						my_View = (View) inflator.inflate(
								R.layout.customforfriend_noheader, parent,
								false);
						FrameLayout mRelativeLayout = (FrameLayout) my_View
								.findViewById(R.id.frame);

						if (mStringBubble.equals("blue")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterblue);
						} else if (mStringBubble.equals("green")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfootergreen);

						} else if (mStringBubble.equals("purple")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterpurple);
						}
					}

					else {
						// Log.e("", "both");
						// Both Header and Footer show in screen
						my_View = (View) inflator.inflate(
								R.layout.custom_forfriend, parent, false);
						FrameLayout mRelativeLayout = (FrameLayout) my_View
								.findViewById(R.id.frame);

						if (mStringBubble.equals("blue")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.header_footer_blue);
						} else if (mStringBubble.equals("green")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.header_footer_green);

						} else if (mStringBubble.equals("purple")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.header_footer_purple);
						}

						TextView mTextViewheader = (TextView) my_View
								.findViewById(R.id.header);
						mTextViewheader.setText(mString_header);
						mTextViewheader.setTextSize(17.0f);
					}

					ImageView mImageViewIcon = (ImageView) my_View
							.findViewById(R.id.icon);
					// Log.e("temp[6]", "" + temp[6]);
					imageLoader.displayImage(picPath, mImageViewIcon, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
					String mString_footer = promo_data_footer_text(temp[7]);
					// Log.e("mString_footer: ", "" + mString_footer);

					final TextView mTextViewFooter_link = (TextView) my_View
							.findViewById(R.id.footer_link);
					TextView mTextViewFooter = (TextView) my_View
							.findViewById(R.id.footer);

					mTextViewFooter.setText(mString_footer);
					mTextViewFooter.setTextSize(16.0f);

					mTextViewFooter_link.setText(footerlink);
					mTextViewFooter.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String mString_footer_link = mTextViewFooter_link
									.getText().toString();

							String test = mString_footer_link
									.substring(mString_footer_link.length() - 1);
							String main_link;
							if (test.equals("=")) {
								main_link = mString_footer_link + MemberId;
							} else {
								main_link = mString_footer_link;
								// + "/"+ MemberId;
							}

							// String link = mString_footer_link + MemberId;
							Log.e("MemberId: ", "" + MemberId);

							if (!main_link.startsWith("https://")
									&& !main_link.startsWith("http://")) {
								main_link = "http://" + main_link;
							}
							Log.e("FINAl link: ", "" + main_link);
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri.parse(main_link));
							activity.startActivity(i);

						}
					});

				}

				TextView rec = (TextView) my_View
						.findViewById(R.id.frommessage);

				tvHome = (TextView) my_View.findViewById(R.id.seen);
				rec.setText(temp[3]);
				rec.setTextSize(17.0f);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(11.0f);

			}

			if (temp[1].equals("friend_pic")) {
				my_View = (View) inflator.inflate(R.layout.freindimage, parent,
						false);

				ImageView tvName = (ImageView) my_View
						.findViewById(R.id.pic_freind);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				Bitmap mBitmap = ConvertToImage(temp[3]);
				tvName.setImageBitmap(mBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(11.0f);

			}
		}

		return my_View;
	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	public String promo_data_link(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringLink = mCursor.getString(2).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + mStringLink);
			} else {
				mStringLink = "NO";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringLink;
	}

	public String promo_image(String id) {
		String imagepath = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				imagepath = mCursor.getString(4).trim();
				// Log.e(" SSSSSSSSSSSSSSS imagepath: ", "   " + imagepath);
			} else {
				imagepath = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return imagepath.toString().trim();
	}

	public String promo_data_header(String id) {
		String header = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				header = mCursor.getString(1).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + header);
			} else {
				header = "NO";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return header;
	}

	public String promo_data_footer_text(String id) {
		String fotter_link = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);
			// Log.e(" id id: ", "   " + id);

			if (mCursor.getCount() != 0) {

				fotter_link = mCursor.getString(3).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + fotter_link);
			} else {
				fotter_link = "NO";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return fotter_link;
	}

	// my
	public String MYpromo_data_link(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringLink = mCursor.getString(2).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + mStringLink);
			} else {
				mStringLink = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringLink;
	}

	public String MYpromo_data_header(String id) {
		String header = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				header = mCursor.getString(1).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + header);
			} else {
				header = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return header;
	}

	public String MYpromo_data_footer_text(String id) {
		String fotter_link = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);
			// Log.e(" id id: ", "   " + id);

			if (mCursor.getCount() != 0) {

				fotter_link = mCursor.getString(3).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + fotter_link);
			} else {
				fotter_link = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return fotter_link;
	}

	public String promo_type(String id) {
		String type_personal_or_not = null;
		try {

			mCursor = mDatasourceHandler.promo_type(id);

			if (mCursor.getCount() != 0) {

				type_personal_or_not = mCursor.getString(9).trim();
				if (type_personal_or_not.matches("NO_Define")) {
					type_personal_or_not = "personal";
				} else if (type_personal_or_not.matches("personal")) {
					type_personal_or_not = "personal";
				} else if (type_personal_or_not.matches("no_personal")) {
					type_personal_or_not = "no_personal";
				}
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + mStringLink);
			} else {
				type_personal_or_not = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return type_personal_or_not;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final String temp[] = chat_List.get(position);
		HeaderViewHolder holder;
		// Log.e("", "HOW many Time calling");
		if (convertView == null) {
			holder = new HeaderViewHolder();
			convertView = inflator.inflate(R.layout.chat_header_view, parent,
					false);
			holder.text = (TextView) convertView.findViewById(R.id.text1);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}

		String matched_date = date();
		String headerChar = null;
		if (temp[1].equals("my")) {

			if (temp[8].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[8].split("/")[0];
			}
		}
		if (temp[1].equals("pic")) {
			if (temp[6].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[6].split("/")[0];
			}
		}

		if (temp[1].equals("friend")) {
			if (temp[8].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[8].split("/")[0];
			}
		}

		if (temp[1].equals("friend_pic")) {
			if (temp[5].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[5].split("/")[0];
			}
		}
		// Log.e("", "LASt HOW many Time calling");
		// set header text as first char in name

		String headerText = null;
		// if (headerChar % 2 == 0) {
		// headerText = headerChar + "\n" + headerChar + "\n" + headerChar;
		// } else {
		// headerText = headerChar + "\n" + headerChar;
		// }
		headerText = "" + headerChar;
		holder.text.setText(headerText);
		// Log.e("", "Final HOW many Time calling");
		return convertView;
	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public String date_new_style() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	/**
	 * Remember that these have to be static, postion=1 should always return the
	 * same Id that is.
	 */
	@Override
	public long getHeaderId(int position) {
		final String temp[] = chat_List.get(position);
		String outputDateStr = null;

		if (temp[1].equals("my")) {

			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[8]);
				outputDateStr = outputFormat.format(date);
				Log.e("my enhfbb", "" + outputDateStr);
				Log.e("my temp[8]", "" + temp[8]);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (temp[1].equals("pic")) {
			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[6]);
				outputDateStr = outputFormat.format(date);
				// Log.e("enhfbb", "" + outputDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (temp[1].equals("friend")) {
			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[8]);
				outputDateStr = outputFormat.format(date);
				// Log.e("enhfbb", "" + outputDateStr);
				Log.e("frds enhfbb", "" + outputDateStr);
				Log.e("frds temp[8]", "" + temp[8]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		if (temp[1].equals("friend_pic")) {
			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[5]);
				outputDateStr = outputFormat.format(date);
				// Log.e("enhfbb", "" + outputDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// return the first character of the country as ID because this is what
		// headers are based upon

		return Long.parseLong(outputDateStr);
	}

	@Override
	public int getPositionForSection(int section) {
		if (section >= mSectionIndices.length) {
			section = mSectionIndices.length - 1;
		} else if (section < 0) {
			section = 0;
		}
		return mSectionIndices[section];
	}

	@Override
	public int getSectionForPosition(int position) {
		for (int i = 0; i < mSectionIndices.length; i++) {
			if (position < mSectionIndices[i]) {
				return i - 1;
			}
		}
		return mSectionIndices.length - 1;
	}

	@Override
	public Object[] getSections() {
		return mSectionLetters;
	}

	public void clear() {

		// mArrayListDATE.get(0);
		// mArrayListDATE = new String[0];
		mSectionIndices = new int[0];
		mSectionLetters = new String[0];
		notifyDataSetChanged();
	}

	// public void restore() {
	// mCountries = mContext.getResources().getStringArray(R.array.countries);
	// mSectionIndices = getSectionIndices();
	// mSectionLetters = getSectionLetters();
	// notifyDataSetChanged();
	// }
	class HeaderViewHolder {
		TextView text;
	}
}
