package com.imgr.chat.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.imgr.chat.R;
import com.imgr.chat.home.Select_Delete_Java;
import com.imgr.chat.model.Delete_Model;

public class Delete extends BaseAdapter {
	ArrayList<Delete_Model> box_new = new ArrayList<Delete_Model>();
	private ArrayList<String> contactnumber1;
	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;

	// private ItemFilter mFilter = new ItemFilter();
	ArrayList<Delete_Model> objects;
	private List<Delete_Model> mPhoneModels = null;

	public Delete(Activity fragmentActivity, ArrayList<String> contactname,
			ArrayList<String> contactnumber, List<Delete_Model> products) {
		// TODO Auto-generated constructor stub
		this.mPhoneModels = products;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		// objects = products;
		context = fragmentActivity;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.objects = new ArrayList<Delete_Model>();
		this.objects.addAll(mPhoneModels);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	public Delete_Model getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.checkbox_listview_delete, null);

		Delete_Model p = getProduct(position);
		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);
		final TextView time = (TextView) rowView.findViewById(R.id.time);
		final TextView desc = (TextView) rowView.findViewById(R.id.desc);

		CheckBox cbBuy = (CheckBox) rowView.findViewById(R.id.checkbox);
		cbBuy.setOnCheckedChangeListener(myCheckChangList);
		cbBuy.setTag(position);
		cbBuy.setChecked(mPhoneModels.get(position).box);
		if (mPhoneModels.get(position).getFullname().equals("No")) {
			textView.setText(mPhoneModels.get(position).getNumber());
		} else if (mPhoneModels.get(position).getFullname().equals("No Name")) {
			textView.setText(mPhoneModels.get(position).getNumber());
		} else {
			textView.setText(mPhoneModels.get(position).getFullname());
		}
		String date_matched = date();
		if (date_matched.matches(mPhoneModels.get(position).getdate())) {
			time.setText(mPhoneModels.get(position).getTIME());
		} else {
			time.setText(mPhoneModels.get(position).getdate().split("/")[0]);
		}

		desc.setText(mPhoneModels.get(position).getMessage());
		return rowView;
	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("dd-MMM/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	private Delete_Model getProduct(int position) {
		// TODO Auto-generated method stub
		return ((Delete_Model) getItem(position));
	}

	public ArrayList<Delete_Model> getBox() {
		ArrayList<Delete_Model> box = new ArrayList<Delete_Model>();
		for (Delete_Model p : objects) {
			if (p.box)
				box.add(p);
		}
		return box;
	}

	OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			getProduct((Integer) buttonView.getTag()).box = isChecked;
			Log.e("SEE ", "" + getProduct((Integer) buttonView.getTag()).box);

			box_new.clear();
			for (Delete_Model p : objects) {
				if (p.box)
					box_new.add(p);
			}
			if (box_new.size() == 0) {
				Select_Delete_Java.changetextview(box_new.size());
			} else {
				Select_Delete_Java.changetextview(box_new.size());
			}

		}
	};

}
