package com.imgr.chat.xmpp;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;

public class ReadReceiptManager implements PacketListener {

	private static Map<Connection, ReadReceiptManager> instances = Collections
			.synchronizedMap(new WeakHashMap<Connection, ReadReceiptManager>());
	static {
		Connection
				.addConnectionCreationListener(new ConnectionCreationListener() {
					public void connectionCreated(Connection connection) {
						getInstanceFor(connection);
					}
				});
	}

	private Set<ReceiptReceivedListener> receiptReceivedListeners = Collections
			.synchronizedSet(new HashSet<ReceiptReceivedListener>());

	private ReadReceiptManager(Connection connection) {
		ServiceDiscoveryManager sdm = ServiceDiscoveryManager
				.getInstanceFor(connection);
		sdm.addFeature(ReadReceipt.NAMESPACE);
		instances.put(connection, this);

		connection.addPacketListener(this, new PacketExtensionFilter(
				ReadReceipt.NAMESPACE));
	}

	public static synchronized ReadReceiptManager getInstanceFor(
			Connection connection) {
		ReadReceiptManager receiptManager = instances.get(connection);

		if (receiptManager == null) {
			receiptManager = new ReadReceiptManager(connection);
		}

		return receiptManager;
	}

	public void processPacket(Packet packet) {
		ReadReceipt dr = (ReadReceipt) packet.getExtension(ReadReceipt.ELEMENT,
				ReadReceipt.NAMESPACE);

		if (dr != null) {
			for (ReceiptReceivedListener l : receiptReceivedListeners) {
				l.onReceiptReceived(packet.getFrom(), packet.getTo(),
						dr.getId());
			}
		}
	}

	public void addReadReceivedListener(ReceiptReceivedListener listener) {
		receiptReceivedListeners.add(listener);
	}

	public void removeRemoveReceivedListener(ReceiptReceivedListener listener) {
		receiptReceivedListeners.remove(listener);
	}
}
