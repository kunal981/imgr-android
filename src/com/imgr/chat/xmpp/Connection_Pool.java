package com.imgr.chat.xmpp;

import org.jivesoftware.smack.XMPPConnection;

import android.util.Log;

public class Connection_Pool {
	private XMPPConnection connection = null;

	private static Connection_Pool instance = null;

	public synchronized static Connection_Pool getInstance() {
		if (instance == null) {
			instance = new Connection_Pool();
			Log.i("CONNECTION INSTANCE:", "CREATED HERE");
		}
		return instance;
	}

	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}

	public XMPPConnection getConnection() {
		return this.connection;
	}

	public void diconnect_Connection() {

		if (connection.isConnected()) {
			connection.disconnect();
		}
	}

}
