package com.imgr.chat;

import java.util.Collection;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.search.UserSearch;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.imgr.chat.SplashActivity.login_Existing_User;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.home.MenuActivity;
import com.imgr.chat.register.CompleteProfileActivity;
import com.imgr.chat.register.VerificationActivity;
import com.imgr.chat.ui.TermsConditionActivity;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;

public class NewSplash extends Activity {

	private int SPLASH_TIME_OUT = 1000;
	SharedPreferences sharedPreferences, mSharedPreferences;
	Editor editor;
	String mString_class, mStringPhone, mStringusername, mStringPassword;
	ConnectionDetector mConnectionDetector;
	String Notification_receive;

	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;

	ConnectionConfiguration config;
	XMPPConnection connection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		mConnectionDetector = new ConnectionDetector(this);

		connectivity_Manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		activity = this;
		config = new ConnectionConfiguration("64.235.48.26", 5222);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setSecurityMode(SecurityMode.enabled);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		connection.DEBUG_ENABLED = true;
		config.setSASLAuthenticationEnabled(true);
		connection = new XMPPConnection(config);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mSharedPreferences = getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		mString_class = sharedPreferences.getString(Constant.classdefine, "");
		mStringPhone = sharedPreferences.getString(Constant.PHONENO, "");
		mStringusername = sharedPreferences.getString(Constant.USERNAME_XMPP,
				"");
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		Notification_receive = mSharedPreferences.getString(
				"Notification_Click_Message", "");

		Log.e("Notification_receive: ", "" + Notification_receive);
		Log.e("mStringusername: ", "" + mStringusername);
		Log.e("mStringPassword: ", "" + mStringPassword);
		NotificationManager notificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity

				if (mConnectionDetector.isConnectingToInternet()) {
					new login_Existing_User().execute();
				} else {
					LogMessage.showDialog(NewSplash.this, null,
							"No Internet Connection", null, "Ok");
					finish();
				}

			}
		}, SPLASH_TIME_OUT);

	}

	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {
			UI.showProgressDialog(NewSplash.this);
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("doInBackground", "doInBackground");
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			connection = new XMPPConnection(config);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			configure(ProviderManager.getInstance());
			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);
			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				Log.e("connected", "server connect");

				// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
				Log.e("" + mStringusername, "" + mStringPassword);
				connection.login(mStringusername, mStringPassword);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {
					Log.e("XMPPChatDemoActivity",
							"--------------------------------------");
					Log.e("XMPPChatDemoActivity", "RosterEntry " + entry);
					Log.e("XMPPChatDemoActivity", "User: " + entry.getUser());
					Log.e("XMPPChatDemoActivity", "Name: " + entry.getName());
					Log.e("XMPPChatDemoActivity",
							"Status: " + entry.getStatus());
					Log.e("XMPPChatDemoActivity", "Type: " + entry.getType());
					Presence entryPresence = roster
							.getPresence(entry.getUser());

					Log.e("XMPPChatDemoActivity", "Presence Status: "
							+ entryPresence.getStatus());
					Log.e("XMPPChatDemoActivity", "Presence Type: "
							+ entryPresence.getMode());

					Presence.Type type = entryPresence.getType();
					if (type == Presence.Type.available)
						Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
					Log.e("XMPPChatDemoActivity", "Presence : " + entryPresence);

				}
			} catch (XMPPException e) {
				Log.e("Cannot connect to XMPP server with default admin username and password.",
						"0");
				Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			Log.e("result", "" + result);
			if (result != null) {
				Log.e("ConnectionId", "" + connection.getConnectionID());
				Log.e("Servicename", "" + connection.getServiceName());
				Log.e("User", "" + connection.getUser());
				Intent intent_home;

				intent_home = new Intent(NewSplash.this, MenuActivity.class);
				intent_home.putExtra("fragment", "Message");
				startActivity(intent_home);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getApplicationContext(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

		/*** end of onPostExecute ***/

		public void configure(ProviderManager pm) {
			// Private Data Storage
			pm.addIQProvider("query", "jabber:iq:private",
					new PrivateDataManager.PrivateDataIQProvider());
			// Time
			try {
				pm.addIQProvider("query", "jabber:iq:time",
						Class.forName("org.jivesoftware.smackx.packet.Time"));
			} catch (ClassNotFoundException e) {
				Log.w("TestClient",
						"Can't load class for org.jivesoftware.smackx.packet.Time");
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Roster Exchange
			pm.addExtensionProvider("x", "jabber:x:roster",
					new RosterExchangeProvider());

			// Message Events
			pm.addExtensionProvider("x", "jabber:x:event",
					new MessageEventProvider());

			// Chat State
			pm.addExtensionProvider("active",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("composing",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("paused",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("inactive",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("gone",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			// XHTML
			pm.addExtensionProvider("html",
					"http://jabber.org/protocol/xhtml-im",
					new XHTMLExtensionProvider());

			// Group Chat Invitations
			pm.addExtensionProvider("x", "jabber:x:conference",
					new GroupChatInvitation.Provider());

			// Service Discovery # Items
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());

			// Service Discovery # Info
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Data Forms
			pm.addExtensionProvider("x", "jabber:x:data",
					new DataFormProvider());

			// MUC User
			pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
					new MUCUserProvider());

			// MUC Admin
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
					new MUCAdminProvider());

			// MUC Owner
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
					new MUCOwnerProvider());

			// SharedGroupsInfo
			pm.addIQProvider("sharedgroup",
					"http://www.jivesoftware.org/protocol/sharedgroup",
					new SharedGroupsInfo.Provider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Delayed Delivery
			pm.addExtensionProvider("x", "jabber:x:delay",
					new DelayInformationProvider());

			// Version
			try {
				pm.addIQProvider("query", "jabber:iq:version",
						Class.forName("org.jivesoftware.smackx.packet.Version"));
			} catch (ClassNotFoundException e) {
				// Not sure what's happening here.
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Offline Message Requests
			pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
					new OfflineMessageRequest.Provider());

			// Offline Message Indicator
			pm.addExtensionProvider("offline",
					"http://jabber.org/protocol/offline",
					new OfflineMessageInfo.Provider());

			// Last Activity
			pm.addIQProvider("query", "jabber:iq:last",
					new LastActivity.Provider());

			// User Search
			pm.addIQProvider("query", "jabber:iq:search",
					new UserSearch.Provider());

			// JEP-33: Extended Stanza Addressing
			pm.addExtensionProvider("addresses",
					"http://jabber.org/protocol/address",
					new MultipleAddressesProvider());

			// FileTransfer
			pm.addIQProvider("si", "http://jabber.org/protocol/si",
					new StreamInitiationProvider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());

			// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
			// IBBProviders.Open());

			// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
			// IBBProviders.Close());

			// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
			// new
			// IBBProviders.Data());

			// Privacy
			pm.addIQProvider("query", "jabber:iq:privacy",
					new PrivacyProvider());

			pm.addIQProvider("command", "http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider());
			pm.addExtensionProvider("malformed-action",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.MalformedActionError());
			pm.addExtensionProvider("bad-locale",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadLocaleError());
			pm.addExtensionProvider("bad-payload",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadPayloadError());
			pm.addExtensionProvider("bad-sessionid",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadSessionIDError());
			pm.addExtensionProvider("session-expired",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.SessionExpiredError());

			pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
					DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
			pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
					new DeliveryReceiptRequest().getNamespace(),
					new DeliveryReceiptRequest.Provider());
			/*
			 * pm.addExtensionProvider(ReadReceipt.ELEMENT,
			 * ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
			 */
		}

	}

}
